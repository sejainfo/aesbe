-- MySQL dump 10.13  Distrib 5.6.31, for debian-linux-gnu (i686)
--
-- Host: localhost    Database: aesbe
-- ------------------------------------------------------
-- Server version	5.6.31-0ubuntu0.15.10.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `oauth_access_tokens`
--

DROP TABLE IF EXISTS `oauth_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_access_tokens` (
  `access_token` varchar(40) NOT NULL,
  `client_id` varchar(80) NOT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `expires` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `scope` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`access_token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_access_tokens`
--

LOCK TABLES `oauth_access_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_access_tokens` DISABLE KEYS */;
INSERT INTO `oauth_access_tokens` VALUES ('13edc09086096fc0d8c92c78fe1c5721c8a0efc1','app_aesbe','aesbe','2016-07-31 07:04:57',NULL),('1d058b5f1d2dbc98325204d7de9754d338cff20f','app_aesbe','aesbe','2016-07-30 04:05:56',NULL),('3ec91ef1bba4324581ae91fcb5d034810a38451c','app_aesbe','aesbe@aesbe.org.br','2016-08-05 21:42:19',NULL),('44d158770271d120791be2ebd289474476e86be3','app_aesbe','copasa','2016-08-05 05:48:54',NULL),('741929a42cda46f3f0dfc62a963ff687f7edaaee','app_aesbe','aesbe@aesbe.org.br','2016-08-05 20:05:17',NULL),('82a73cc1ba5834b1799f22e4140134c51cb1c037','app_aesbe','aesbe','2016-08-05 03:04:26',NULL),('8d5dbb71eaa46d749244329f59a314c9dd5252d1','app_aesbe','aesbe','2016-07-30 05:33:40',NULL),('ae49f9b3f3fbfdb4b660ed4cc2766d59181b811b','app_aesbe','aesbe','2016-08-05 03:03:55',NULL),('bc2a4bb47523f339c1e15e18f31d567816123fea','app_aesbe','aesbe@aesbe.org.br','2016-08-05 23:29:06',NULL);
/*!40000 ALTER TABLE `oauth_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_authorization_codes`
--

DROP TABLE IF EXISTS `oauth_authorization_codes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_authorization_codes` (
  `authorization_code` varchar(40) NOT NULL,
  `client_id` varchar(80) NOT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `redirect_uri` varchar(2000) DEFAULT NULL,
  `expires` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `scope` varchar(2000) DEFAULT NULL,
  `id_token` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`authorization_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_authorization_codes`
--

LOCK TABLES `oauth_authorization_codes` WRITE;
/*!40000 ALTER TABLE `oauth_authorization_codes` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_authorization_codes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_clients`
--

DROP TABLE IF EXISTS `oauth_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_clients` (
  `client_id` varchar(80) NOT NULL,
  `client_secret` varchar(80) NOT NULL,
  `redirect_uri` varchar(2000) NOT NULL,
  `grant_types` varchar(80) DEFAULT NULL,
  `scope` varchar(2000) DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_clients`
--

LOCK TABLES `oauth_clients` WRITE;
/*!40000 ALTER TABLE `oauth_clients` DISABLE KEYS */;
INSERT INTO `oauth_clients` VALUES ('app_aesbe','$2y$10$ye6hfSppFDZSBXSObVMWaO/.Y8reDdASkkZ3rwrgt4zsPuEOHpyTW','localhost',NULL,NULL,NULL);
/*!40000 ALTER TABLE `oauth_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_jwt`
--

DROP TABLE IF EXISTS `oauth_jwt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_jwt` (
  `client_id` varchar(80) NOT NULL,
  `subject` varchar(80) DEFAULT NULL,
  `public_key` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_jwt`
--

LOCK TABLES `oauth_jwt` WRITE;
/*!40000 ALTER TABLE `oauth_jwt` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_jwt` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_refresh_tokens`
--

DROP TABLE IF EXISTS `oauth_refresh_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_refresh_tokens` (
  `refresh_token` varchar(40) NOT NULL,
  `client_id` varchar(80) NOT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `expires` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `scope` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`refresh_token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_refresh_tokens`
--

LOCK TABLES `oauth_refresh_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_refresh_tokens` DISABLE KEYS */;
INSERT INTO `oauth_refresh_tokens` VALUES ('01d7c4f7f5ad7e551e706991b7ab4386f5524278','app_aesbe','aesbe@aesbe.org.br','2016-08-19 22:29:06',NULL),('20a9651b6b5a0b1ceb2be0c36421a52921a5ca2d','app_aesbe','aesbe','2016-08-13 04:33:41',NULL),('27d8871c89c77bab78469ceccf31f0dd841eaba8','app_aesbe','aesbe','2016-07-28 01:31:15',NULL),('2cf64798b050cc630ca4a1cad2e2ed06a018a679','app_aesbe','geccal','2016-07-22 03:29:03',NULL),('31f2f7febe52a01adc7843e946c719fedfb7a587','app_aesbe','geccal','2016-07-22 03:28:30',NULL),('3676550f02c3e393f4893cea65096e1553489d59','app_aesbe','aesbe','2016-08-19 02:04:27',NULL),('3abfd085ffc7645b60af6016eaa378ef9a0a7ae7','app_aesbe','aesbe','2016-07-28 03:10:52',NULL),('417df7dc4b120dd96de21aa75fc936f1148512a0','app_aesbe','geccal','2016-07-22 03:31:07',NULL),('4354845e3bc33821e0e86d29644af5c1d6c5eef3','app_aesbe','aesbe','2016-07-27 04:23:16',NULL),('4723be05a84a09193b5fc6627a3f8cf647e66dc0','app_aesbe','aesbe','2016-07-24 01:50:34',NULL),('4d5c06757b818ba14554b61e42ea1f8f428d245d','app_aesbe','aesbe@aesbe.org.br','2016-08-19 19:05:17',NULL),('6d8653b6af622b4a457b5ec20dfdb0b0925687bd','app_aesbe','aesbe','2016-08-14 06:04:58',NULL),('715bb263c936135d6bff91cc028cc5ea46a6a8ca','app_aesbe','aesbe','2016-07-23 18:07:09',NULL),('7fd1fada654091ae42016ef339d75f2770726ece','app_aesbe','aesbe','2016-07-28 00:24:10',NULL),('8086b60b2d7f0057b447a78cc3ad04f221642949','app_aesbe','aesbe','2016-07-28 00:25:42',NULL),('971d921a3256ddc37c97ae7e48a611254115481f','app_aesbe','aesbe','2016-08-19 02:03:55',NULL),('9db9e3ac21b2e737c2bce330859b1372c1a6b3a3','app_aesbe','aesbe','2016-07-28 02:32:09',NULL),('b14896c80abefbd7515c2269e8baac2c99e2de2c','app_aesbe','aesbe','2016-07-27 03:54:23',NULL),('b582a6d245ed7aeedc0742685cc7048c513aa896','app_aesbe','aesbe','2016-07-28 00:23:21',NULL),('bd56e71aa1ce50e55731a5e2bf0c9290818a3892','app_aesbe','aesbe@aesbe.org.br','2016-08-19 20:42:19',NULL),('c07ae5377ee2db482be9e2e27d207b24b1a113bc','app_aesbe','geccal','2016-07-22 03:50:46',NULL),('e72a99e147bffb7c307cdbb7017716fdf9ab1a42','app_aesbe','aesbe','2016-07-27 02:54:12',NULL),('eada0402306c5e9b0c65e01236114d1d785fb954','app_aesbe','copasa','2016-08-19 04:48:54',NULL),('eb169888e3ed7aa8a9646674d90f9cb6a2b5f8a4','app_aesbe','aesbe','2016-08-13 03:05:56',NULL);
/*!40000 ALTER TABLE `oauth_refresh_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_scopes`
--

DROP TABLE IF EXISTS `oauth_scopes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_scopes` (
  `type` varchar(255) NOT NULL DEFAULT 'supported',
  `scope` varchar(2000) DEFAULT NULL,
  `client_id` varchar(80) DEFAULT NULL,
  `is_default` smallint(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_scopes`
--

LOCK TABLES `oauth_scopes` WRITE;
/*!40000 ALTER TABLE `oauth_scopes` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_scopes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_users`
--

DROP TABLE IF EXISTS `oauth_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_users` (
  `co_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `dt_cadastro` date NOT NULL,
  `username` varchar(45) NOT NULL,
  `password` varchar(100) NOT NULL,
  `dt_ultima_visita` date DEFAULT NULL,
  `co_pessoa` int(11) DEFAULT NULL,
  PRIMARY KEY (`co_usuario`),
  KEY `fk_oauth_users_tb_aesbe_pessoa_fisica1_idx` (`co_pessoa`),
  CONSTRAINT `fk_oauth_users_tb_aesbe_pessoa_fisica1` FOREIGN KEY (`co_pessoa`) REFERENCES `tb_aesbe_pessoa_fisica` (`co_pessoa`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_users`
--

LOCK TABLES `oauth_users` WRITE;
/*!40000 ALTER TABLE `oauth_users` DISABLE KEYS */;
INSERT INTO `oauth_users` VALUES (1,'2016-07-08','aesbe@aesbe.org.br','$2y$10$3GTfbcoyuR78Rk0BdxYmke9Z3ljX5ETA6HtxU4dilmYHDckwuZm1G',NULL,NULL),(2,'2016-08-05','copasa','$2y$10$3GTfbcoyuR78Rk0BdxYmke9Z3ljX5ETA6HtxU4dilmYHDckwuZm1G',NULL,NULL);
/*!40000 ALTER TABLE `oauth_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_aesbe_arquivo`
--

DROP TABLE IF EXISTS `tb_aesbe_arquivo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_aesbe_arquivo` (
  `co_arquivo` int(11) NOT NULL AUTO_INCREMENT,
  `ds_arquivo` varchar(200) NOT NULL,
  `co_tipo_arquivo` int(11) NOT NULL,
  PRIMARY KEY (`co_arquivo`),
  KEY `fk_tb_aesbe_arquivo_tb_aesbe_tipo_arquivo1_idx` (`co_tipo_arquivo`),
  CONSTRAINT `fk_tb_aesbe_arquivo_tb_aesbe_tipo_arquivo1` FOREIGN KEY (`co_tipo_arquivo`) REFERENCES `tb_aesbe_tipo_arquivo` (`co_tipo_arquivo`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=158 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_aesbe_arquivo`
--

LOCK TABLES `tb_aesbe_arquivo` WRITE;
/*!40000 ALTER TABLE `tb_aesbe_arquivo` DISABLE KEYS */;
INSERT INTO `tb_aesbe_arquivo` VALUES (1,'Planilha Banco de Preço-importar-novo - 2.xls',1),(2,'Planilha Banco de Preço-importar-novo - 2.xls',1),(3,'Planilha Banco de Preço-importar-novo - 2.xls',1),(4,'Planilha Banco de Preço-importar-novo - 2.xls',1),(5,'Planilha Banco de Preço-importar-novo - 2.xls',1),(6,'Planilha Banco de Preço-importar-novo - 2.xls',1),(7,'Planilha Banco de Preço-importar-novo - 2.xls',1),(8,'Planilha Banco de Preço-importar-novo - 2.xls',1),(9,'Planilha Banco de Preço-importar-novo - 2.xls',1),(10,'Planilha Banco de Preço-importar-novo - 2.xls',1),(11,'Planilha Banco de Preço-importar-novo - 2.xls',1),(12,'Planilha Banco de Preço-importar-novo - 2.xls',1),(13,'Planilha Banco de Preço-importar-novo - 2.xls',1),(14,'Planilha Banco de Preço-importar-novo - 2.xls',1),(15,'Planilha Banco de Preço-importar-novo - 2.xls',1),(16,'Planilha Banco de Preço-importar-novo - 2.xls',1),(17,'Planilha Banco de Preço-importar-novo - 2.xls',1),(18,'Planilha Banco de Preço-importar-novo - 2.xls',1),(19,'Planilha Banco de Preço-importar-novo - 2.xls',1),(20,'Planilha Banco de Preço-importar-novo - 2.xls',1),(21,'Planilha Banco de Preço-importar-novo - 2.xls',1),(22,'Planilha Banco de Preço-importar-novo - 2.xls',1),(23,'Planilha Banco de Preço-importar-novo - 2.xls',1),(24,'Planilha Banco de Preço-importar-novo - 2.xls',1),(25,'Planilha Banco de Preço-importar-novo - 2.xls',1),(26,'Planilha Banco de Preço-importar-novo - 2.xls',1),(27,'Planilha Banco de Preço-importar-novo - 2.xls',1),(28,'Planilha Banco de Preço-importar-novo - 2.xls',1),(29,'Planilha Banco de Preço-importar-novo - 2.xls',1),(30,'Planilha Banco de Preço-importar-novo - 2.xls',1),(31,'Planilha Banco de Preço-importar-novo - 2.xls',1),(32,'Planilha Banco de Preço-importar-novo - 2.xls',1),(33,'Planilha Banco de Preço-importar-novo - 2.xls',1),(34,'Planilha Banco de Preço-importar-novo - 2.xls',1),(35,'Planilha Banco de Preço-importar-novo - 2.xls',1),(36,'Planilha Banco de Preço-importar-novo - 2.xls',1),(37,'Planilha Banco de Preço-importar-novo - 2.xls',1),(38,'Planilha Banco de Preço-importar-novo - 2.xls',1),(39,'Planilha Banco de Preço-importar-novo - 2.xls',1),(40,'Planilha Banco de Preço-importar-novo - 2.xls',1),(41,'Planilha Banco de Preço-importar-novo - 2.xls',1),(42,'Planilha Banco de Preço-importar-novo - 2.xls',1),(43,'Planilha Banco de Preço-importar-novo - 2.xls',1),(44,'Planilha Banco de Preço-importar-novo - 2.xls',1),(45,'Planilha Banco de Preço-importar-novo - 2.xls',1),(46,'Planilha Banco de Preço-importar-novo - 2.xls',1),(47,'Planilha Banco de Preço-importar-novo - 2.xls',1),(48,'Planilha Banco de Preço-importar-novo - 2.xls',1),(49,'Planilha Banco de Preço-importar-novo - 2.xls',1),(50,'Planilha Banco de Preço-importar-novo - 2.xls',1),(51,'Planilha Banco de Preço-importar-novo - 2.xls',1),(52,'Planilha Banco de Preço-importar-novo - 2.xls',1),(53,'Planilha Banco de Preço-importar-novo - 2.xls',1),(54,'Planilha Banco de Preço-importar-novo - 2.xls',1),(55,'Planilha Banco de Preço-importar-novo - 2.xls',1),(56,'Planilha Banco de Preço-importar-novo - 2.xls',1),(57,'Planilha Banco de Preço-importar-novo - 2.xls',1),(58,'Planilha Banco de Preço-importar-novo - 2.xls',1),(59,'Planilha Banco de Preço-importar-novo - 2.xls',1),(60,'Planilha Banco de Preço-importar-novo - 2.xls',1),(61,'Planilha Banco de Preço-importar-novo - 2.xls',1),(62,'Planilha Banco de Preço-importar-novo - 2.xls',1),(63,'Planilha Banco de Preço-importar-novo - 2.xls',1),(64,'Planilha Banco de Preço-importar-novo - 2.xls',1),(65,'Planilha Banco de Preço-importar-novo - 2.xls',1),(66,'Planilha Banco de Preço-importar-novo - 2.xls',1),(67,'Planilha Banco de Preço-importar-novo - 2.xls',1),(68,'Planilha Banco de Preço-importar-novo - 2.xls',1),(69,'Planilha Banco de Preço-importar-novo - 2.xls',1),(70,'Planilha Banco de Preço-importar-novo - 2.xls',1),(71,'Planilha Banco de Preço-importar-novo - 2.xls',1),(72,'Planilha Banco de Preço-importar-novo - 2.xls',1),(73,'Planilha Banco de Preço-importar-novo - 2.xls',1),(74,'Planilha Banco de Preço-importar-novo - 2.xls',1),(75,'Planilha Banco de Preço-importar-novo - 2.xls',1),(76,'Planilha Banco de Preço-importar-novo - 2.xls',1),(77,'Planilha Banco de Preço-importar-novo - 2.xls',1),(78,'Planilha Banco de Preço-importar-novo - 2.xls',1),(79,'Planilha Banco de Preço-importar-novo - 2.xls',1),(80,'Planilha Banco de Preço-importar-novo - 2.xls',1),(81,'Planilha Banco de Preço-importar-novo - 2.xls',1),(82,'Planilha Banco de Preço.xls',1),(83,'Planilha Banco de Preço.xls',1),(84,'Planilha Banco de Preço.xls',1),(85,'Planilha Banco de Preço.xls',1),(86,'Planilha Banco de Preço.xls',1),(87,'Planilha Banco de Preço.xls',1),(88,'Planilha Banco de Preço.xls',1),(89,'Planilha Banco de Preço.xls',1),(90,'Planilha Banco de Preço.xls',1),(91,'Planilha Banco de Preço.xls',1),(92,'Planilha Banco de Preço.xls',1),(93,'Planilha Banco de Preço.xls',1),(94,'Planilha Banco de Preço.xls',1),(95,'Planilha Banco de Preço.xls',1),(96,'Planilha Banco de Preço.xls',1),(97,'Planilha Banco de Preço.xls',1),(98,'Planilha Banco de Preço.xls',1),(99,'Planilha Banco de Preço.xls',1),(100,'Planilha Banco de Preço.xls',1),(101,'Planilha Banco de Preço.xls',1),(102,'Planilha Banco de Preço.xls',1),(103,'Planilha Banco de Preço.xls',1),(104,'Planilha Banco de Preço.xls',1),(105,'Planilha Banco de Preço.xls',1),(106,'Planilha Banco de Preço.xls',1),(107,'Planilha Banco de Preço.xls',1),(108,'Planilha Banco de Preço.xls',1),(109,'Planilha Banco de Preço.xls',1),(110,'Planilha Banco de Preço.xls',1),(111,'Planilha Banco de Preço.xls',1),(112,'Planilha Banco de Preço.xls',1),(113,'Planilha Banco de Preço.xls',1),(114,'Planilha Banco de Preço.xls',1),(115,'Planilha Banco de Preço.xls',1),(116,'Planilha Banco de Preço.xls',1),(117,'Planilha Banco de Preço.xls',1),(118,'Planilha Banco de Preço.xls',1),(119,'Planilha Banco de Preço.xls',1),(120,'Planilha Banco de Preço.xls',1),(121,'Planilha Banco de Preço.xls',1),(122,'Planilha Banco de Preço.xls',1),(123,'Planilha Banco de Preço.xls',1),(124,'Planilha Banco de Preço.xls',1),(125,'Planilha Banco de Preço.xls',1),(126,'Planilha Banco de Preço.xls',1),(127,'Planilha Banco de Preço.xls',1),(128,'Planilha Banco de Preço.xls',1),(129,'Planilha Banco de Preço-importar-novo.xls',1),(130,'Planilha Banco de Preço-importar-novo - 2.xls',1),(131,'Planilha Banco de Preço-importar-novo - 2.xls',1),(132,'Planilha Banco de Preço.xls',1),(133,'Planilha Banco de Preço-importar-novo - 2.xls',1),(134,'Planilha Banco de Preço-importar-novo - 2.xls',1),(135,'Planilha Banco de Preço-importar-novo - 2.xls',1),(136,'Planilha Banco de Preço-importar.xls',1),(137,'Planilha Banco de Preço-importar-novo.xls',1),(138,'Planilha Banco de Preço-importar-novo - 2.xls',1),(139,'Planilha Banco de Preço-importar-novo.xls',1),(140,'Planilha Banco de Preço-importar-novo - 3.xls',1),(141,'Planilha Banco de Preço-importar-novo - 3.xls',1),(142,'Planilha Banco de Preço-importar-novo - 3.xls',1),(143,'Planilha Banco de Preço-importar-novo - 3.xls',1),(144,'Planilha Banco de Preço-importar-novo - 3.xls',1),(145,'Planilha Banco de Preço-importar-novo - 3.xls',1),(146,'Planilha Banco de Preço-importar-novo - 3.xls',1),(147,'Planilha Banco de Preço-importar-novo - 3.xls',1),(148,'Planilha Banco de Preço-importar-novo - 3.xls',1),(149,'Planilha Banco de Preço-importar-novo - 3.xls',1),(150,'Planilha Banco de Preço-importar-novo - 3.xls',1),(151,'Planilha Banco de Preço-importar-novo - 3.xls',1),(152,'Planilha Banco de Preço-importar-novo - 3.xls',1),(153,'Planilha Banco de Preço-importar-novo - 3.xls',1),(154,'Planilha Banco de Preço-importar-novo - 3.xls',1),(155,'Planilha Banco de Preço-importar-novo - 2.xls',1),(156,'Planilha Banco de Preço-importar-novo - 2.xls',1),(157,'Planilha Banco de Preço-importar-novo - 3.xls',1);
/*!40000 ALTER TABLE `tb_aesbe_arquivo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_aesbe_arquivo_proposta`
--

DROP TABLE IF EXISTS `tb_aesbe_arquivo_proposta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_aesbe_arquivo_proposta` (
  `co_arquivo_proposta` int(11) NOT NULL AUTO_INCREMENT,
  `dt_processamento` datetime NOT NULL,
  `qt_erros` int(11) NOT NULL,
  `qt_registros_processados` int(11) NOT NULL,
  `st_processamento` tinyint(1) NOT NULL,
  `co_usuario` int(11) NOT NULL,
  `co_proposta` int(11) NOT NULL,
  `co_arquivo` int(11) NOT NULL,
  PRIMARY KEY (`co_arquivo_proposta`),
  KEY `fk_tb_aesbe_arquivo_proposta_oauth_users1_idx` (`co_usuario`),
  KEY `fk_tb_aesbe_arquivo_proposta_tb_aesbe_proposta1_idx` (`co_proposta`),
  KEY `fk_tb_aesbe_arquivo_proposta_tb_aesbe_arquivo1_idx` (`co_arquivo`),
  CONSTRAINT `fk_tb_aesbe_arquivo_proposta_oauth_users1` FOREIGN KEY (`co_usuario`) REFERENCES `oauth_users` (`co_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tb_aesbe_arquivo_proposta_tb_aesbe_arquivo1` FOREIGN KEY (`co_arquivo`) REFERENCES `tb_aesbe_arquivo` (`co_arquivo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tb_aesbe_arquivo_proposta_tb_aesbe_proposta1` FOREIGN KEY (`co_proposta`) REFERENCES `tb_aesbe_proposta` (`co_proposta`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=132 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_aesbe_arquivo_proposta`
--

LOCK TABLES `tb_aesbe_arquivo_proposta` WRITE;
/*!40000 ALTER TABLE `tb_aesbe_arquivo_proposta` DISABLE KEYS */;
INSERT INTO `tb_aesbe_arquivo_proposta` VALUES (6,'2016-07-29 21:36:05',1,1,0,1,6,20),(7,'2016-07-29 21:36:55',1,1,0,1,6,29),(8,'2016-07-29 21:37:44',1,1,0,1,6,30),(9,'2016-07-29 21:39:08',1,1,0,1,6,31),(10,'2016-07-29 21:41:01',1,1,0,1,6,32),(11,'2016-07-29 21:45:37',1,1,0,1,6,33),(12,'2016-07-29 21:47:32',1,1,0,1,6,20),(13,'2016-07-29 22:02:43',1,1,0,1,6,36),(14,'2016-07-29 22:03:27',1,1,0,1,6,37),(15,'2016-07-29 22:06:42',1,1,0,1,6,38),(16,'2016-07-29 22:07:33',1,1,0,1,6,39),(17,'2016-07-29 22:08:17',1,1,0,1,6,40),(18,'2016-07-29 23:17:05',1,1,0,1,6,44),(19,'2016-07-29 23:17:52',1,1,0,1,6,45),(20,'2016-07-29 23:18:53',0,1,1,1,6,46),(21,'2016-07-30 18:30:21',0,1,1,1,6,47),(22,'2016-07-30 18:34:26',0,1,1,1,6,48),(23,'2016-07-30 18:36:42',0,1,1,1,6,49),(24,'2016-07-30 18:37:24',0,1,1,1,6,50),(25,'2016-07-30 18:38:03',0,1,1,1,6,51),(26,'2016-07-30 21:46:22',0,1,1,1,6,52),(27,'2016-07-30 21:46:55',0,1,1,1,6,53),(28,'2016-07-30 21:47:46',0,1,1,1,6,54),(29,'2016-07-30 21:49:38',0,1,1,1,6,55),(30,'2016-07-30 21:50:03',0,1,1,1,6,56),(31,'2016-07-30 21:51:07',0,1,1,1,6,57),(32,'2016-07-30 21:51:50',0,1,1,1,6,58),(33,'2016-07-30 21:52:51',0,1,1,1,6,59),(34,'2016-07-30 21:53:27',0,1,1,1,6,60),(35,'2016-07-30 21:53:50',0,1,1,1,6,61),(36,'2016-07-30 21:54:58',0,1,1,1,6,62),(37,'2016-07-30 21:59:48',0,1,1,1,6,63),(38,'2016-07-30 22:01:09',4,1,0,1,6,64),(39,'2016-07-30 22:01:34',4,1,0,1,6,65),(40,'2016-07-30 22:04:45',4,1,0,1,6,66),(41,'2016-07-30 22:05:44',4,1,0,1,6,67),(42,'2016-07-30 22:14:30',0,1,1,1,6,68),(43,'2016-07-30 22:15:39',0,1,1,1,6,69),(44,'2016-07-31 03:24:02',0,1,1,1,6,70),(45,'2016-07-31 03:24:41',0,1,1,1,6,71),(46,'2016-07-31 14:27:35',1,1,0,1,6,72),(47,'2016-07-31 16:25:00',1,1,0,1,6,73),(48,'2016-07-31 16:26:19',1,1,0,1,6,74),(49,'2016-07-31 16:26:27',1,1,0,1,6,75),(50,'2016-07-31 16:27:40',1,1,0,1,6,76),(51,'2016-07-31 16:31:43',1,1,0,1,6,77),(52,'2016-07-31 16:34:07',1,1,0,1,6,78),(53,'2016-07-31 16:35:22',1,1,0,1,6,79),(54,'2016-07-31 16:35:40',1,1,0,1,6,80),(55,'2016-07-31 17:58:06',1,1,0,1,6,81),(56,'2016-07-31 17:58:58',2480,124,0,1,6,82),(57,'2016-07-31 19:26:59',2480,124,0,1,6,83),(58,'2016-07-31 19:36:38',2480,124,0,1,6,84),(59,'2016-07-31 19:37:24',2480,124,0,1,6,85),(60,'2016-07-31 19:38:26',2480,124,0,1,6,86),(61,'2016-07-31 19:39:01',2480,124,0,1,6,87),(62,'2016-07-31 20:02:33',2480,124,0,1,6,88),(63,'2016-07-31 20:04:40',2480,124,0,1,6,89),(64,'2016-07-31 20:14:47',2480,124,0,1,6,90),(65,'2016-07-31 20:16:20',2480,124,0,1,6,91),(66,'2016-07-31 20:17:38',2480,124,0,1,6,92),(67,'2016-07-31 20:20:35',2480,124,0,1,6,93),(68,'2016-07-31 20:27:50',2480,124,0,1,6,94),(69,'2016-07-31 20:28:10',2480,124,0,1,6,95),(70,'2016-07-31 20:56:34',2480,124,0,1,6,96),(71,'2016-07-31 20:58:02',2480,124,0,1,6,97),(72,'2016-07-31 20:58:53',2480,124,0,1,6,98),(73,'2016-07-31 20:59:17',2480,124,0,1,6,99),(74,'2016-07-31 20:59:45',2480,124,0,1,6,100),(75,'2016-07-31 21:08:47',2480,124,0,1,6,101),(76,'2016-07-31 21:10:12',2480,124,0,1,6,102),(77,'2016-07-31 21:12:13',2480,124,0,1,6,103),(78,'2016-07-31 21:13:32',2480,124,0,1,6,104),(79,'2016-07-31 21:14:48',2480,124,0,1,6,105),(80,'2016-07-31 21:15:32',2480,124,0,1,6,106),(81,'2016-07-31 21:16:49',2480,124,0,1,6,107),(82,'2016-07-31 21:18:54',2480,124,0,1,6,108),(83,'2016-07-31 21:20:30',2480,124,0,1,6,109),(84,'2016-07-31 21:25:44',2480,124,0,1,6,110),(85,'2016-07-31 21:26:41',2480,124,0,1,6,111),(86,'2016-07-31 21:29:23',2480,124,0,1,6,112),(87,'2016-07-31 21:32:00',2480,124,0,1,6,113),(88,'2016-07-31 21:34:01',2480,124,0,1,6,114),(89,'2016-07-31 21:38:15',2480,124,0,1,6,115),(90,'2016-07-31 21:42:11',2480,124,0,1,6,116),(91,'2016-07-31 21:43:43',2480,124,0,1,6,117),(92,'2016-07-31 21:45:03',2480,124,0,1,6,118),(93,'2016-07-31 23:43:04',2480,124,0,1,6,119),(94,'2016-07-31 23:45:50',2480,124,0,1,6,120),(95,'2016-07-31 23:52:05',2480,124,0,1,6,121),(96,'2016-07-31 23:53:08',2480,124,0,1,6,122),(97,'2016-07-31 23:56:12',2480,124,0,1,6,123),(98,'2016-08-01 00:03:08',2480,124,0,1,6,124),(99,'2016-08-01 00:10:11',2480,124,0,1,6,125),(100,'2016-08-01 00:12:23',2480,124,0,1,6,126),(101,'2016-08-01 00:13:38',2480,124,0,1,6,127),(102,'2016-08-01 00:14:57',2480,124,0,1,6,128),(103,'2016-08-01 00:15:52',2477,124,0,1,6,129),(104,'2016-08-01 00:18:26',1,1,0,1,6,130),(105,'2016-08-01 00:19:09',1,1,0,1,6,131),(106,'2016-08-01 00:19:58',2480,124,0,1,6,132),(107,'2016-08-01 00:20:42',1,1,0,1,6,133),(108,'2016-08-01 00:23:22',1,1,0,1,6,134),(109,'2016-08-01 00:27:39',1,1,0,1,6,135),(110,'2016-08-01 00:27:56',867,124,0,1,6,136),(111,'2016-08-01 00:28:21',2477,124,0,1,6,137),(112,'2016-08-01 00:29:16',0,1,1,1,6,138),(113,'2016-08-01 00:34:57',2477,124,0,1,6,139),(114,'2016-08-01 00:38:20',124,124,0,1,6,140),(115,'2016-08-01 00:38:52',27,124,0,1,6,141),(116,'2016-08-01 00:39:11',27,124,0,1,6,142),(117,'2016-08-01 00:39:29',27,124,0,1,6,143),(118,'2016-08-01 00:40:48',27,124,0,1,6,144),(119,'2016-08-01 00:42:01',27,124,0,1,6,145),(120,'2016-08-01 00:42:29',27,124,0,1,6,146),(121,'2016-08-01 00:44:58',27,124,0,1,6,147),(122,'2016-08-01 00:45:18',27,124,0,1,6,148),(123,'2016-08-01 00:45:34',27,124,0,1,6,149),(124,'2016-08-01 00:46:39',27,124,0,1,6,150),(125,'2016-08-01 00:47:47',27,124,0,1,6,151),(126,'2016-08-01 00:48:16',0,124,1,1,6,152),(127,'2016-08-01 00:48:41',27,124,0,1,6,153),(128,'2016-08-01 00:49:38',0,124,1,1,6,154),(129,'2016-08-01 00:57:31',1,1,0,1,6,155),(130,'2016-08-01 00:57:51',1,1,0,1,6,156),(131,'2016-08-03 01:42:35',0,124,1,1,6,157);
/*!40000 ALTER TABLE `tb_aesbe_arquivo_proposta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_aesbe_descricao`
--

DROP TABLE IF EXISTS `tb_aesbe_descricao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_aesbe_descricao` (
  `co_descricao` int(11) NOT NULL AUTO_INCREMENT,
  `no_descricao` text NOT NULL,
  PRIMARY KEY (`co_descricao`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_aesbe_descricao`
--

LOCK TABLES `tb_aesbe_descricao` WRITE;
/*!40000 ALTER TABLE `tb_aesbe_descricao` DISABLE KEYS */;
INSERT INTO `tb_aesbe_descricao` VALUES (1,'125 MM X 75 MM (DE MAX X DE MIN)'),(2,'A GRANEL'),(3,'A GRANEL CARRETA CAPAC 18000 KG - N. ONU 1017 CL 2'),(4,'ADAPTADOR MACHO 3/4 POL X PE 20 MM'),(5,'ADAPTAVEL PARA DN 150X100'),(6,'ADAPTAVEL PARA DN 200X100'),(7,'AQUOSA DE CLORATO DE SODIO PEROXIDO DE HIDROGENIO'),(8,'CILINDRO CAPAC 50 E 68 KG - N. ONU 1017 CL 2'),(9,'CILINDRO CAPAC 900 KG - N. ONU 1017 CL 2'),(10,'COMERCIAL 98% A GRANEL CAS 7664-93-9'),(11,'CONTAINER FLEXIVEL C/1000 KG E A GRANEL'),(12,'COTOVELO 90 GR FEMEA 3/4 POL. X ADAPTADOR 20 MM'),(13,'DE 1000 PN 10 SDR 17'),(14,'DE 110 X DE 20 (RAMAL PE)-ARTICULADO'),(15,'DE 110 X DE 32 (RAMAL PE)-ARTICULADO'),(16,'DE 20 MM'),(17,'DE 32 MM'),(18,'DE 450 PN 10 SDR 17'),(19,'DE 560 PN 10 SDR 17'),(20,'DE 60(PVC) X DE 20(RAMAL PE) -ARTICULADO'),(21,'DE 60(PVC) X DE 32(RAMAL PE) -ARTICULADO'),(22,'DE 85(PVC) X DE 20(RAMAL PE)-ARTICULADO'),(23,'DE 85(PVC) X DE 32(RAMAL PE)-ARTICULADO'),(24,'DE 900 PN 10 SDR 17'),(25,'DN 100'),(26,'DN 100 - PARA RAMAIS PREDIAIS PE DE 20 - C/ PARAF.'),(27,'DN 100 (DE 110 MM)'),(28,'DN 100 (DE 110MM)-0,75MPA-(CM=5,83M)'),(29,'DN 100 (DE 110MM)-1,00MPA-(CM=5,83M)'),(30,'DN 100 (DE 118 MM) - PN 1,0 MPA - (CM = 5,83 M)'),(31,'DN 100-FL PN 10/16-CORPO CURTO L=190MM-PT 1,6MPA'),(32,'DN 1000'),(33,'DN 140 (DE 160MM)-1 MPA-(CM=5,81M)'),(34,'DN 150'),(35,'DN 150 (DE 170 MM) - PN 1,0 MPA - (CM = 5,83M)'),(36,'DN 150-FL PN 10/16-CORPO CURTO L=210MM-PT 1,6MPA'),(37,'DN 20 - PE 80 - 1,0 MPA'),(38,'DN 20 - QN 1,5 M3/H -CL C - RELOJ. COBRE+VIDRO'),(39,'DN 20-QN 0,75M3/H-CL B-CUP.VIDRO-MONOJ-MAG-R.INCL'),(40,'DN 20-QN 0,75M3/H-CL B-MONOJ-MAG-R.INCL-ANTIFRAUDE'),(41,'DN 20-QN 1,5M3/H-CL B-MULTIJ-MAG-R.INCL-MED.INDIV.'),(42,'DN 20-QN0,75B-C.POLI UJ-MAG-R.INCL-ANTI SUPER IMA'),(43,'DN 200'),(44,'DN 200 (DE 222 MM) - PN 1,0 MPA - (CM = 5,75M)'),(45,'DN 200-FL PN 10-CORPO CURTO L=230MM-PT 1,6MPA'),(46,'DN 25-QN 5 M3/H-CL B-MULTIJATO-MAGNETICO'),(47,'DN 250'),(48,'DN 250 (DE 274 MM) - PN 1,0 MPA - (CM = 5,75M)'),(49,'DN 300'),(50,'DN 300 (DE 326 MM) - PN 1,0 MPA - (CM = 5,75M)'),(51,'DN 350'),(52,'DN 400'),(53,'DN 450'),(54,'DN 50'),(55,'DN 50 - PARA RAMAIS PREDIAIS PE DE 20 - C/ PARAF.'),(56,'DN 50 - PARA RAMAIS PREDIAIS PE DE 32 - C/ PARAF.'),(57,'DN 50 (DE 60 MM)'),(58,'DN 50 (DE 60 MM)-0,75MPA-(CM=5,88M)'),(59,'DN 50 (DE 60 MM)-1,00MPA-(CM=5,88M)'),(60,'DN 500'),(61,'DN 600'),(62,'DN 600 MM (RUA/ESGOTO)'),(63,'DN 700'),(64,'DN 75'),(65,'DN 75 - PARA RAMAIS PREDIAIS PE DE 20 - C/ PARAF.'),(66,'DN 75 - PARA RAMAIS PREDIAIS PE DE 32 - C/ PARAF.'),(67,'DN 75 (DE 85 MM)'),(68,'DN 75 (DE 85 MM)-0,75MPA-(CM=5,85M)'),(69,'DN 75 (DE 85 MM)-1,00MPA-(CM=5,85M)'),(70,'DN 80'),(71,'DN 800'),(72,'DNR 20'),(73,'GRANULADO - SACO VALVULADO CAPAC 40 KG'),(74,'GRANULADO SACO VALVULADO CAPAC 25KG'),(75,'HIDROMETRO ULTRASSONICO FLANGEADO 2 POLEGADAS'),(76,'HIDROXIDO DE CALCIO EM SUSPENSAO 20% M/M A GRANEL'),(77,'ISENTO DE FERRO-LIQUIDO A GRANEL'),(78,'LIQUIDA - A GRANEL - BASIC. 57 A 67%'),(79,'LIQUIDO - CONC. 50% - CONTAINERS 1000 L'),(80,'LIQUIDO - CONC. 50% EM PESO - A GRANEL'),(81,'LIQUIDO A GRANEL'),(82,'LIQUIDO A GRANEL - CONC >= 20%'),(83,'LIQUIDO A GRANEL - N. ONU 1791 CL 8'),(84,'LIQUIDO A GRANEL - N. ONU 2426 CL 5,1'),(85,'LIQUIDO A GRANEL(CONCENTRACAO 38%)-N.ONU 2582 CL8'),(86,'LIQUIDO A GRANEL(CONCENTRACAO 48%)-N. ONU 1824 CL8'),(87,'MICROGRANULAR A GRANEL - MENOR QUE 2 MM'),(88,'MULTIDIAMETRAL DN 50 A DN 150 X DNR 20 (3/4 POL)'),(89,'NAO EMULSIONADO-P/REPARO PAVIMENTO (RECIP C/40KG)'),(90,'OXIGENIO LIQUIDO(LIQ. CRIOGENIO)- N.ONU 1073'),(91,'PAREDE MACICA - DN 100 (CM 5,90 M)'),(92,'PAREDE MACICA - DN 150 (CM 5,88 M)'),(93,'PAREDE MACICA - DN 200 (CM=5,88 M)'),(94,'PAREDE MACICA - DN 250 (CM=5,84 M)'),(95,'PAREDE MACICA - DN 300 (CM=5,82 M)'),(96,'PAREDE MACICA - DN 350 (CM=5,78 M)'),(97,'PAREDE MACICA - DN 400 (CM=5,77 M)'),(98,'SACO VALVULADO CAPAC 20 KG'),(99,'SACO VALVULADO CAPACIDADE 25 KG');
/*!40000 ALTER TABLE `tb_aesbe_descricao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_aesbe_empresa`
--

DROP TABLE IF EXISTS `tb_aesbe_empresa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_aesbe_empresa` (
  `co_empresa` int(11) NOT NULL AUTO_INCREMENT,
  `no_empresa` varchar(100) NOT NULL,
  `sg_empresa` varchar(45) DEFAULT NULL,
  `ds_sitio` varchar(100) DEFAULT NULL,
  `nu_cep` varchar(10) DEFAULT NULL,
  `ds_logradouro` varchar(100) DEFAULT NULL,
  `no_bairro` varchar(100) DEFAULT NULL,
  `nu_logradouro` varchar(10) DEFAULT NULL,
  `ds_complemento` varchar(30) DEFAULT NULL,
  `co_uf` int(11) NOT NULL,
  `nu_telefone` varchar(10) DEFAULT NULL,
  `nu_telefone_secundario` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`co_empresa`),
  KEY `fk_tb_aesbe_empresa_tb_aesbe_uf1_idx` (`co_uf`),
  CONSTRAINT `fk_tb_aesbe_empresa_tb_aesbe_uf1` FOREIGN KEY (`co_uf`) REFERENCES `tb_aesbe_uf` (`co_uf`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_aesbe_empresa`
--

LOCK TABLES `tb_aesbe_empresa` WRITE;
/*!40000 ALTER TABLE `tb_aesbe_empresa` DISABLE KEYS */;
INSERT INTO `tb_aesbe_empresa` VALUES (2,'Companhia de Saneamento de Minas Gerais','COPASA','','','','','','',1,NULL,NULL),(3,'Companhia de Saneamento Básico do Estado de São Paulo','SAPESP','','','','','','',2,NULL,NULL),(5,'Teste','teste',NULL,NULL,NULL,NULL,NULL,NULL,2,NULL,NULL),(6,'Águas e Esgotos do Piauí S.A.','AGESPISA','www.agespisa.com.br',NULL,NULL,NULL,NULL,NULL,22,'8632166320',NULL),(7,'Companhia de Saneamento Ambiental do Maranhão','CAEMA','www.caema.ma.gov.br',NULL,NULL,NULL,NULL,NULL,21,'9832195002',NULL),(8,'Companhia de Águas e Esgotos de Roraima','CAER','www.caer.com.br',NULL,NULL,NULL,NULL,NULL,14,'9521212267',NULL),(9,'Companhia de Águas e Esgotos de Rondônia','CAERD','www.caerd-ro.com.br/',NULL,NULL,NULL,NULL,NULL,11,'6932161712',NULL),(10,'Companhia de Águas e Esgotos do Rio Grande do Norte','CAERN','www.caern.com.br',NULL,NULL,NULL,NULL,NULL,24,'8432324110',NULL),(11,'Companhia de Água e Esgotos do Amapá','CAESA','www.caesa.ap.gov.br',NULL,NULL,NULL,NULL,NULL,16,'9691533196',NULL),(12,'Companhia de Saneamento Ambiental do Distrito Federal','CAESB','www.caesb.df.gov.br',NULL,NULL,NULL,NULL,NULL,53,'6132137115',NULL),(13,'Companhia de Água e Esgoto do Ceará','CAGECE','www.cagece.com.br/',NULL,NULL,NULL,NULL,NULL,23,'8531011729',NULL),(14,'Companhia de Água e Esgoto da Paraíba','CAGEPA','www.cagepa.pb.gov.br',NULL,NULL,NULL,NULL,NULL,25,'8332181230',NULL),(15,'Companhia de Saneamento de Alagoas','CASAL','www.casal.al.gov.br',NULL,NULL,NULL,NULL,NULL,27,'8233153055',NULL),(16,'Companhia Catarinense de Águas e Saneamento','CASAN','www.casan.com.br',NULL,NULL,NULL,NULL,NULL,42,'4832215012',NULL),(17,'Companhia Estadual de Águas e Esgotos','CEDAE','www.cedae.com.br',NULL,NULL,NULL,NULL,NULL,33,'',NULL),(18,'Companhia Espírito Santense de Saneamento','CESAN','www.cesan.com.br',NULL,NULL,NULL,NULL,NULL,32,'2721275000',NULL),(19,'Companhia Pernambucana de Saneamento','COMPESA','www.compesa.com.br',NULL,NULL,NULL,NULL,NULL,26,'',NULL),(20,'Companhia de Saneamento de Minas Gerais','COPASA','www.copasa.com.br',NULL,NULL,NULL,NULL,NULL,31,'',NULL),(21,'Companhia Riograndense de Saneamento','CORSAN','www.corsan.com.br',NULL,NULL,NULL,NULL,NULL,43,'5132155677',NULL),(22,'Companhia de Saneamento do Pará','COSANPA','www.cosanpa.pa.gov.br',NULL,NULL,NULL,NULL,NULL,15,'9132028400',NULL),(23,'Companhia de Saneamento de Sergipe','DESO','www.deso-se.com.br',NULL,NULL,NULL,NULL,NULL,28,'7932261002',NULL),(24,'Empresa Baiana de Águas e Saneamento S.A.','EMBASA','www.embasa.ba.gov.br',NULL,NULL,NULL,NULL,NULL,29,'7133724842',NULL),(25,'Companhia de Saneamento Básico do Estado de São Paulo','SABESP','www.sabesp.com.br',NULL,NULL,NULL,NULL,NULL,35,'1133888200',NULL),(26,'Saneamento de Goiás S.A.','SANEAGO','www.saneago.com.br',NULL,NULL,NULL,NULL,NULL,52,'6232433101',NULL),(27,'Odebrecht Ambiental/Saneatins','SANEATINS','www.odebrechtambiental.com',NULL,NULL,NULL,NULL,NULL,17,'6332186616',NULL),(28,'Companhia de Saneamento do Paraná','SANEPAR','www.sanepar.com.br',NULL,NULL,NULL,NULL,NULL,41,'4133303045',NULL),(29,'Empresa de Saneamento do Mato Grosso do Sul S.A.','SANESUL','www.sanesul.ms.gov.br',NULL,NULL,NULL,NULL,NULL,50,'6733187707',NULL),(30,'Associação das Empresas de Saneamento Básico Estaduais','Aesbe','www.aesbe.org.br',NULL,NULL,NULL,NULL,NULL,53,'6133264888',NULL),(31,'Departamento Estadual de Pavimentação e Saneamento','DEPASA','www.ac.gov.br/wps/portal/depasa/depasa/principal',NULL,NULL,NULL,NULL,NULL,12,'6832441490',NULL);
/*!40000 ALTER TABLE `tb_aesbe_empresa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_aesbe_esp_classe`
--

DROP TABLE IF EXISTS `tb_aesbe_esp_classe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_aesbe_esp_classe` (
  `co_esp_classe` int(11) NOT NULL AUTO_INCREMENT,
  `ds_esp_classe` text NOT NULL,
  PRIMARY KEY (`co_esp_classe`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_aesbe_esp_classe`
--

LOCK TABLES `tb_aesbe_esp_classe` WRITE;
/*!40000 ALTER TABLE `tb_aesbe_esp_classe` DISABLE KEYS */;
INSERT INTO `tb_aesbe_esp_classe` VALUES (1,'APLICACAO: UNIAO DE TUBOS DE MESMO MATERIAL OU NA UNIAO DE TUBOS DE\nDIFERENTES MATERIAIS.\nMATERIAIS :\n- DO CORPO: FERRO FUNDIDO NODULAR DIN GGG 50.\n- DOS ELEMENTOS DE FIXACAO: ACO CARBONO COM REVESTIMENTO A BASE DE ZINCO\n(GALVANIZADO A FOGO).\n- DA VEDACAO: JUNTA DE ELASTOMERO VULCANIZADO.\n- REVESTIMENTO EXTERNO E INTERNO: RILSAN NYLON 11 T 26 OU EPOXI POLIAMIDA. \nPRESSAO MINIMA DE TRABALHO: PN 16. \nNOTAS:\nNA SUPERFICIE EXTERNA DEVEM ESTAR GRAVADAS, DE FORMA VISIVEL E INDELEVEL,\nAS SEGUINTES INFORMACOES:\n- NOME OU MARCA DO FABRICANTE;\n- DIAMETRO NOMINAL (DN);\n- PRESSAO NOMINAL.(PN)'),(2,'CONFORME NORMA SABESP NTS 175'),(3,'CONFORME NORMA TECNICA SABESP - NTS 179'),(4,'FUNCAO: TAMPONAR POCO DE VISITA E DE INSPECAO\n APLICACAO: SISTEMA COLETOR DE ESGOTO OU SISTEMA DE DISTRIBUICAO DE AGUA\n NORMA DE FABRICACAO: NBR 10160\n NORMA COMPLEMENTAR: NTS 033\n CLASSIFICACAO: CLASSE D 400, ABNT 10160, ITEM 4.2.4\n MATERIAL: FERRO FUNDIDO CLASSE FE 50007,ABNT 10160, ITEM 5.1.1'),(5,'HIDROMETRO TAQUIMETRICO COM EXTREMIDADES ROSCADAS\nESPECIFICACAO TECNICA SABESP\nUTILIZACAO: EM INSTALACOES PREDIAIS DE AGUA \nTEMPERATURA DE SERVICO: MAXIMA DE 40 GRAUS CENTIGRADOS\nPRESSAO DE SERVICO: ATE 1 MPA (100 MCA)\nNORMA DE RECEBIMENTO: SABESP HID-011 - ESPECIFICACAO TECNICA'),(6,'PROJETO E APLICACAO NTS 189\n FABRICACAO:\n NORMA TECNICA SABESP - NTS 194 PARA TUBOS COM DE 63 A DE 630.\n NORMA ABNT - NBR 15561 PARA TUBOS COM DE 710 A DE 1600.\n NORMA ISO 4427 PARA TUBOS DE 1200 A DE 2000 E CLASSES DE PRESSAO NAO\n CONTEMPLADAS PELA 15561.\n OBSERVACAO: O COMPRIMENTO DOS TUBOS DEVE SER ESPECIFICADO PELA UNIDADE\nREQUISITANTE.'),(7,'TUBO PVC COM PONTA E BOLSA JUNTA ELASTICA\nNORMA DE FABRICACAO: ABNT NBR 7665/07.\nMATERIAL: COMPOSTO DE POLI CLORETO DE VINILA - PVC 12\nEXTREMIDADES: PONTA E BOLSA PARA JUNTA ELASTICA\nANEL DE VEDACAO : INTEGRADO AO TUBO OU DO TIPO REMOVIVEL\nELASTOMERO DO ANEL DE VEDACAO : EPDM\nUTILIZACAO: ADUTORAS E REDES DE AGUA\nNOTAS:\n- DEVE SER FORNECIDO ANEL DE VEDACAO E PASTA LUBRIFICANTE\n- OS MATERIAIS DEVEM SER INSPECIONADOS CONFORME NORMA ABNT NBR 7665/07.\n-VETADO O TRANSPORTE E A ENTREGA DE TUBOS ENVELOPADOS, OU SEJA,\nTUBOS DE DIAMETROS MENORES INSERIDOS DENTRO DE TUBOS DE DIAMETROS MAIORES.\nOS TUBOS DEVEM SER ACONDICIONADOS, TRANSPORTADOS E ENTREGUES DE\nMANEIRA A EVITAR DANOS AOS TUBOS, NAS BOLSAS E NOS ANEIS DE VEDACAO.\nO FORNECEDOR E RESPONSAVEL PELA INTEGRIDADE FISICA E FUNCIONAL\nDO MATERIAL FORNECIDO.'),(8,'TUBO PVC RIGIDO COM PONTA E BOLSA JUNTA ELASTICA INTEGRADO\nMATERIAL: COMPOSTO DE POLI (CLORETO DE VINILA) PVC 6,3\nEXTREMIDADES: PONTA E BOLSA PARA JUNTA ELASTICA INTEGRADA\nVEDACAO: ANEL DE ELASTOMERO INTEGRADO A BOLSA\nUTILIZACAO: ADUTORAS E REDES DE AGUA.\nNOTAS:\n- DEVE SER FORNECIDO ANEL DE VEDACAO INTEGRADO A BOLSA E PASTA LUBRIFICANTE\n- OS MATERIAIS DEVEM SER INSPECIONADOS NOS EXAMES E ENSAIOS CONFORME NORMAS\n VIGENTES E EXIGENCIAS COMPLEMENTARES DA NORMA TECNICA SABESP NTS 187.\n- VETADO O TRANSPORTE E A ENTREGA DE TUBOS ENVELOPADOS, OU SEJA,\nTUBOS DE DIAMETROS MENORES INSERIDOS DENTRO DE TUBOS DE DIAMETROS MAIORES.\nOS TUBOS DEVEM SER ACONDICIONADOS, TRANSPORTADOS E ENTREGUES DE\nMANEIRA A EVITAR DANOS AOS TUBOS, NAS BOLSAS E NOS ANEIS DE VEDACAO.\nO FORNECEDOR E RESPONSAVEL PELA INTEGRIDADE FISICA E FUNCIONAL\nDO MATERIAL FORNECIDO.'),(9,'TUBO PVC RIGIDO COM PONTA E BOLSA JUNTA ELASTICA INTEGRADO PARA\nCOLETOR DE ESGOTO\nMATERIAL: COMPOSTO DE POLI (CLORETO DE VINILA) PVC RIGIDO\nNORMA DE FABRICACAO: NBR 7362-1\nEXTREMIDADES: PONTA E BOLSA JUNTA ELASTICA INTEGRADA\nVEDACAO: ANEL DE ELASTOMERO INTEGRADO A BOLSA\nNORMA: NBR 7362-1 - ANEXO A\nUTILIZACAO: REDE COLETORA DE ESGOTO \nNOTA (S):\n- DEVE SER FORNECIDO ANEL DE VEDACAO INTEGRADO A BOLSA E PASTA LUBRIFICANTE\n- OS MATERIAIS DEVEM SER INSPECIONADOS NOS EXAMES E ENSAIOS CONFORME NORMAS\n VIGENTES E EXIGENCIAS COMPLEMENTARES NORMA TECNICA SABESP NTS 187.\n- VETADO O TRANSPORTE E A ENTREGA DE TUBOS ENVELOPADOS, OU SEJA,\nTUBOS DE DIAMETROS MENORES INSERIDOS DENTRO DE TUBOS DE DIAMETROS MAIORES.\nOS TUBOS DEVEM SER ACONDICIONADOS, TRANSPORTADOS E ENTREGUES DE\nMANEIRA A EVITAR DANOS AOS TUBOS, NAS BOLSAS E NOS ANEIS DE VEDACAO.\nO FORNECEDOR E RESPONSAVEL PELA INTEGRIDADE FISICA E FUNCIONAL\nDO MATERIAL FORNECIDO.'),(10,'UTILIZACAO: ADUTORAS E REDE DE AGUA POTAVEL\n APLICACAO: TRANSPORTE E DISTRIBUICAO DE AGUA\n NORMAS DE FABRIC.CONF. ISO-2531 E EN-545\n REVESTIMENTO INTERNO: ARGAMASSA DE CIMENTO CONF.NORMAS ISO-4179\n E ISOI-16132\n REVESTIMENTO EXTERNO: ZINCOMETALICO COM ACABAMENTO EM \nREVESTIMENTO ANTICORROSIVO ISO-8179/2\n BOLSAS COM ANEIS PARA JUNTA ELASTICA: CONFORME NORMA EM 681-1\n RASTREABILIDADE: OS TUBOS DEVEM APRESENTAR MARCACOES E \n RASTREABILIDADE DE FORMA VISIVEL E INDELEVEL NAS BOLSAS; \n NOTAS(S):\n . O MATERIAL DEVE SER FORNECIDO C/ COMPRIMENTO UTIL MINIMO\n DE 5,5(CINCO E MEIO)METROS, CONFORME NORMAS VIGENTES.\n . OS MATERIAIS DEVEM ATENDER E SER INSPECIONADOS CONFORME\n NORMAS VIGENTES'),(11,'UTILIZACAO: ADUTORAS E REDES DE AGUA POTAVEL\nCOMPOSTO DE:\n-CORPO EM FERRO FUNDIDO DUCTIL \n-CINTA E PARAFUSOS EM ACO INOX \n-ELEMENTO DE VEDACAO EM BORRACHA EPDM\n-FABRICACAO E ENSAIOS CONFORME NTS 182\nNOTAS:\n- DEVERA SER GRAVADO EM ALTO RELEVO:\nA) NOME OU MARCA DO FABRICANTE\nB) DIAMETRO NOMINAL (DN) DA TUBULACAO QUANDO EXCLUSIVA \nC) DIAMETRO NOMINAL (DN) DA DERIVACAO (SAIDA ROSQUEADA)\nOBS: ESTE MATERIAL DEVERA SER INSPECIONADO NO FORNECEDOR \nCONFORME NORMAS ESPECIFICAS'),(12,'UTILIZACAO: ADUTORAS E REDES DE AGUA POTAVEL.\nNORMA: ABNT NBR 7675.\nREVESTIMENTO INTERNO: ARGAMASSA DE CIMENTO.\nREVESTIMENTO EXTERNO: ZINCO E ESMALTE BETUMINOSO ANTICORROSIVO\nOU LIGA ZN-AL COM EPOXI.\nNOTA(S):\n- O MATERIAL DEVE SER FORNECIDO COM COMPRIMENTO UTIL DE 6(SEIS)METROS,\n NO MINIMO, CONFORME NORMA VIGENTE.\n- DEVEM SER FORNECIDOS OS ACESSORIOS CONFORME NORMAS.\n- OS MATERIAIS DEVEM ATENDER E SER INSPECIONADOS CONFORME NORMAS VIGENTES.'),(13,'UTILIZACAO: ADUTORAS E REDES DE AGUA POTAVEL.\nNORMA: ABNT NBR 7675.\nREVESTIMENTO INTERNO: ARGAMASSA DE CIMENTO.\nREVESTIMENTO EXTERNO: ZINCO E ESMALTE BETUMINOSO ANTICORROSIVO OU\n LIGA ZN-AL COM EPOXI.\nNOTA(S):\n- O MATERIAL DEVE SER FORNECIDO COM COMPRIMENTO UTIL DE 6(SEIS)METROS,\n NO MINIMO, CONFORME NORMA VIGENTE.\n- DEVEM SER FORNECIDOS OS ACESSORIOS CONFORME NORMAS.\n- OS MATERIAIS DEVEM ATENDER E SER INSPECIONADOS CONFORME NORMAS VIGENTES.'),(14,'UTILIZACAO: COLETORES DE ESGOTO.\nNORMA: ABNT NBR 15420.\nREVESTIMENTO INTERNO: ARGAMASSA DE CIMENTO ALUMINOSO.\nREVESTIMENTO EXTERNO: ZINCO E PINTURA EPOXI VERMELHA.\nREVESTIMENTO INTERIOR DA BOLSA: PINTURA EPOXI.\nNOTA(S):\n- O MATERIAL DEVE SER FORNECIDO COM COMPRIMENTO UTIL DE 6(SEIS)METROS,\n NO MINIMO, CONFORME NORMA VIGENTE.\n- DEVEM SER FORNECIDOS OS ACESSORIOS CONFORME NORMAS.\n- OS MATERIAIS DEVEM ATENDER E SER INSPECIONADOS CONFORME NORMAS VIGENTES.'),(15,'UTILIZACAO: EM LIGACOES PREDIAIS DE ESGOTO\n-NORMA: ABNT NBR 10570\n-CONFIGURACAO BASICA DO SELIM: COMPOSTO POR CORPO E SISTEMA DE FIXACAO:\n .CORPO: CONSTITUIDO DE DERIVACAO TIPO PONTA OU BOLSA COM  JUNTA ELASTICA PARA ACOPLAMENTO AO RAMAL PREDIAL DN 100 E \nPARTES ARTICULADAS OU FLEXIVEIS QUE PERMITAM O SEU  ENVOLVIMENTO AO COLETOR DA REDE \n. SISTEMA DE FIXACAO: TRAVAS, PARAFUSOS, CINTAS OU ABRACADEIRAS.\n CASO EXISTA ELEMENTO DE ENCAIXE NO FURO DO COLETOR ESTE NAO DEVE ULTRAPASSAR A SUA PAREDE EM MAIS DE 10 MM.\n .CASO SEJAM UTILIZADOS MATERIAIS METALICOS TAIS COMO:\n ABRACADEIRA, PARAFUSOS, PORCAS E ARRUELAS, ESSES MATERIAIS DEVERAO  SER DE ACO INOXIDAVEL AISI 304 L OU SUPERIOR.\nENSAIOS DE INSPECAO:\n.VISUAL: CORHOMOGENEA E ACABAMENTO ISENTO DE BOLHAS, REBARBAS  OU FISSURAS\n .MARCACOES DE FORMA VISIVEL E RESISTENTE: ALTO OU BAIXO RELEVO\n- MARCA OU NOME DO FABRICANTE\n- DIAMETRO DO RAMAL E DO TUBO COLETOR ONDE SERA INSTALADO O SELIM\n- RASTREABILIDADE: MES/ANO\n- NUMERO DA NORMA: NBR 10570 \n. DIMENSIONAL: PROFUNDIDADE MINIMA DA BOLSA OU COMPRIMENTO MINIMO DA PONTA, DIAMETRO INTERNO E ESPESSURA MINIMA DA PAREDE DA BOLSA; CANALETA DA JUNTA ELASTICA PARA ANEIS REMOVIVEIS  COM DIMENSOES CONFORME TABELA 2 DA NBR 7362 (JERI) OU DIMENSOES COMPATIVEIS COM ANEIS TOROIDAIS\n.SIMULAR A INSTALACAO NOS TUBOS DE PVC MACICO E CERAMICO  INCLUINDO FURO\n.O ANEL DA JUNTA ELASTICA E O ELEMENTO DE VEDACAO ENTRE O SELIM  E TUBO COLETOR NAO DEVE SOLTAR-SE DA SUA SEDE DURANTE O PROCESSO DE INSTALACAO\n.VERIFICAR A FIXACAO DO SELIM AO COLETOR. ESTE NAO DEVE APRESENTAR MOBILIDADE RADIAL OU AXIAL\n.VERIFICAR A ESTANQUEIDADE DO SELIM INSTALADO NOS COLETORES PVC  E CERAMICO CONFORME ITEM 5.3.1 DA NBR 7362 COM DEFLEXAO ANGULAR  MAIOR OU IGUAL A 2 GRAUS;\n- MATERIAL DO CORPO DO SELIM: PVC, POLIPROPILENO OU OUTRO  COMPOSTO ADEQUADO A UTILIZACAO EM ESGOTO\n- MATERIAL DE VEDACAO UTILIZADO NO ANEL DA BOLSA E ENTRE O SELIM E TUBO COLETOR: BORRACHA NITRILICA CONFORME TABELA 3 DA  NTS 234 OU TABELA 2A NTS 198; POLIURETANO OU OUTRO ELASTOMERO  /POLIMERO RESISTENTE AO ESGOTO\n- O SELIM DEVE SER FORNECIDO EM EMBALAGEM PLASTICA, COM TODOS OS ANEIS DE VEDACAO NECESSARIOS A ESTANQUEIDADE REQUERIDA, MANUAL ONDE CONSTE INSTRUCOES DE MONTAGEM, COMPONENTES DO SELIM, EQUIPAMENTOS E MATERIAIS PARA INSTALACAO'),(16,'UTILIZACAO: RAMAIS PREDIAIS DE AGUA\nNORMA: NORMA TECNICA SABESP NTS 048 - ESPECIFICACAO\nCOR : AZUL');
/*!40000 ALTER TABLE `tb_aesbe_esp_classe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_aesbe_esp_serie`
--

DROP TABLE IF EXISTS `tb_aesbe_esp_serie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_aesbe_esp_serie` (
  `co_esp_serie` int(11) NOT NULL AUTO_INCREMENT,
  `ds_esp_serie` text NOT NULL,
  PRIMARY KEY (`co_esp_serie`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_aesbe_esp_serie`
--

LOCK TABLES `tb_aesbe_esp_serie` WRITE;
/*!40000 ALTER TABLE `tb_aesbe_esp_serie` DISABLE KEYS */;
INSERT INTO `tb_aesbe_esp_serie` VALUES (1,'- ABRANGE TUBULACOES COM DIAMETROS EXTERNOS ATE 176MM.\n - ADEQUACAO DO DIAMETRO A SER UTILIZADO FEITA ATRAVES \nDOS FUROS DISTRIBUIDOS NO PERIMETRO DA CINTA.\n - OS FUROS NA CINTA DEVERAO TER IDENTIFICACAO VISIVEL \nE INDELEVEL DO DIAMETRO APLICADO(DN)'),(2,'DIAMETRO NOMINAL: DN');
/*!40000 ALTER TABLE `tb_aesbe_esp_serie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_aesbe_fornecedor`
--

DROP TABLE IF EXISTS `tb_aesbe_fornecedor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_aesbe_fornecedor` (
  `co_fornecedor` int(11) NOT NULL AUTO_INCREMENT,
  `no_fornecedor` varchar(200) NOT NULL,
  `nu_cnpj` char(14) NOT NULL,
  `no_representante` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`co_fornecedor`),
  UNIQUE KEY `nu_cnpj_UNIQUE` (`nu_cnpj`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_aesbe_fornecedor`
--

LOCK TABLES `tb_aesbe_fornecedor` WRITE;
/*!40000 ALTER TABLE `tb_aesbe_fornecedor` DISABLE KEYS */;
INSERT INTO `tb_aesbe_fornecedor` VALUES (5,'SAINT GOBAIN CANALIZAÇÃO LTDA','62330665000110',NULL);
/*!40000 ALTER TABLE `tb_aesbe_fornecedor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_aesbe_funcionalidade`
--

DROP TABLE IF EXISTS `tb_aesbe_funcionalidade`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_aesbe_funcionalidade` (
  `co_funcionalidade` int(11) NOT NULL AUTO_INCREMENT,
  `no_funcionalidade` varchar(45) NOT NULL,
  `no_funcionalidade_visivel` varchar(45) NOT NULL,
  `st_funcionalidade_visivel` tinyint(1) NOT NULL,
  `st_ativo` tinyint(1) NOT NULL,
  `co_modulo` int(11) NOT NULL,
  PRIMARY KEY (`co_funcionalidade`),
  KEY `fk_tb_aesbe_funcionalidade_tb_aesbe_modulo1_idx` (`co_modulo`),
  CONSTRAINT `fk_tb_aesbe_funcionalidade_tb_aesbe_modulo1` FOREIGN KEY (`co_modulo`) REFERENCES `tb_aesbe_modulo` (`co_modulo`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_aesbe_funcionalidade`
--

LOCK TABLES `tb_aesbe_funcionalidade` WRITE;
/*!40000 ALTER TABLE `tb_aesbe_funcionalidade` DISABLE KEYS */;
INSERT INTO `tb_aesbe_funcionalidade` VALUES (1,'bancoPreco.propostaMaterialPrincipal','Pesquisar Preço',1,1,1),(2,'bancoPreco.propostaMaterialImportarDados','Importar Planilha de Materiais',1,1,1),(3,'bancoPreco.propostaMaterialListaImportados','Lista de Arquivos Importados',1,1,1),(4,'bancoPreco.propostaMaterialListar','Meus Preços',1,1,1);
/*!40000 ALTER TABLE `tb_aesbe_funcionalidade` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_aesbe_material`
--

DROP TABLE IF EXISTS `tb_aesbe_material`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_aesbe_material` (
  `co_material` int(11) NOT NULL AUTO_INCREMENT,
  `co_unidade_medida` int(11) NOT NULL,
  `no_basico` varchar(100) NOT NULL,
  `nu_sabesp` double DEFAULT NULL,
  `co_esp_serie` int(11) DEFAULT NULL,
  `co_esp_classe` int(11) DEFAULT NULL,
  `co_descricao` int(11) DEFAULT NULL,
  PRIMARY KEY (`co_material`),
  KEY `fk_tb_aesbe_produto_tb_aesbe_unidade_medida1_idx` (`co_unidade_medida`),
  KEY `fk_tb_aesbe_produto_tb_aesbe_esp_classe1_idx` (`co_esp_classe`),
  KEY `fk_tb_aesbe_produto_tb_aesbe_esp_serie1_idx` (`co_esp_serie`),
  KEY `fk_tb_aesbe_produto_tb_aesbe_descricao1_idx` (`co_descricao`),
  CONSTRAINT `fk_tb_aesbe_produto_tb_aesbe_descricao1` FOREIGN KEY (`co_descricao`) REFERENCES `tb_aesbe_descricao` (`co_descricao`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tb_aesbe_produto_tb_aesbe_esp_classe1` FOREIGN KEY (`co_esp_classe`) REFERENCES `tb_aesbe_esp_classe` (`co_esp_classe`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tb_aesbe_produto_tb_aesbe_esp_serie1` FOREIGN KEY (`co_esp_serie`) REFERENCES `tb_aesbe_esp_serie` (`co_esp_serie`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tb_aesbe_produto_tb_aesbe_unidade_medida1` FOREIGN KEY (`co_unidade_medida`) REFERENCES `tb_aesbe_unidade_medida` (`co_unidade_medida`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=126 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_aesbe_material`
--

LOCK TABLES `tb_aesbe_material` WRITE;
/*!40000 ALTER TABLE `tb_aesbe_material` DISABLE KEYS */;
INSERT INTO `tb_aesbe_material` VALUES (1,4,'JUNTA FF DUCTIL C/BOLSAS ADAPTAVEL P/VARIACAO DIAMETRAL',4036000128,NULL,1,1),(2,4,'TE DE SERVICO INTEGRADO - NTS 175',7047800323,NULL,2,56),(3,4,'TE DE SERVICO INTEGRADO - NTS 175',7047800773,NULL,2,66),(4,4,'TE DE SERVICO INTEGRADO - NTS 175',7047800797,NULL,2,23),(5,4,'TE DE SERVICO INTEGRADO - NTS 175',7047800347,NULL,2,21),(6,4,'TE DE SERVICO INTEGRADO - NTS 175',7047800335,NULL,2,20),(7,4,'TE DE SERVICO INTEGRADO - NTS 175',7047800785,NULL,2,22),(8,4,'TE DE SERVICO INTEGRADO - NTS 175',7047801030,NULL,2,14),(9,4,'TE DE SERVICO INTEGRADO - NTS 175',7047800207,NULL,2,55),(10,4,'TE DE SERVICO INTEGRADO - NTS 175',7047801005,NULL,2,26),(11,4,'TE DE SERVICO INTEGRADO - NTS 175',7047801042,NULL,2,15),(12,4,'TE DE SERVICO INTEGRADO - NTS 175',7047800750,NULL,2,65),(13,4,'ADAPTADOR PP P/TUBO PE - NTS 179',7005200203,NULL,3,16),(14,4,'ADAPTADOR PP P/TUBO PE - NTS 179',7005200320,NULL,3,17),(15,4,'TAMPAO ARTICULADO FF',4064106018,NULL,4,62),(16,4,'HIDROMETRO TAQUIMETRICO EXTREMIDADES ROSCADAS',12010003358,NULL,5,41),(17,4,'HIDROMETRO TAQUIMETRICO EXTREMIDADES ROSCADAS',12010001258,NULL,5,42),(18,4,'HIDROMETRO TAQUIMETRICO EXTREMIDADES ROSCADAS',12010001210,NULL,5,39),(19,4,'HIDROMETRO TAQUIMETRICO EXTREMIDADES ROSCADAS',12010001003,NULL,5,46),(20,4,'HIDROMETRO TAQUIMETRICO EXTREMIDADES ROSCADAS',12010001192,NULL,5,40),(21,1,'TUBO DE POLIETILENO - PE 100- AGUA',7029405916,NULL,6,13),(22,1,'TUBO DE POLIETILENO - PE 100- AGUA',7029405564,NULL,6,19),(23,1,'TUBO DE POLIETILENO - PE 100- AGUA',7029405904,NULL,6,24),(24,1,'TUBO DE POLIETILENO - PE 100- AGUA',7029405450,NULL,6,18),(25,1,'TUBO PVC 12 DEFOFO JEI/JERI - NBR 7665/07',7066500200,NULL,7,44),(26,1,'TUBO PVC 12 DEFOFO JEI/JERI - NBR 7665/07',7066500250,NULL,7,48),(27,1,'TUBO PVC 12 DEFOFO JEI/JERI - NBR 7665/07',7066500303,NULL,7,50),(28,1,'TUBO PVC 12 DEFOFO JEI/JERI - NBR 7665/07',7066500157,NULL,7,35),(29,1,'TUBO PVC 12 DEFOFO JEI/JERI - NBR 7665/07',7066500108,NULL,7,30),(30,1,'TUBO PVC RIGIDO PBJEI/JERI - NBR 5647-1',7002700650,NULL,8,58),(31,1,'TUBO PVC RIGIDO PBJEI/JERI - NBR 5647-1',7002700958,NULL,8,69),(32,1,'TUBO PVC RIGIDO PBJEI/JERI - NBR 5647-1',7002701203,NULL,8,29),(33,1,'TUBO PVC RIGIDO PBJEI/JERI - NBR 5647-1',7002700909,NULL,8,68),(34,1,'TUBO PVC RIGIDO PBJEI/JERI - NBR 5647-1',7002701653,NULL,8,33),(35,1,'TUBO PVC RIGIDO PBJEI/JERI - NBR 5647-1',7002701150,NULL,8,28),(36,1,'TUBO PVC RIGIDO PBJEI/JERI - NBR 5647-1',7002700703,NULL,8,59),(37,1,'TUBO PVC RIGIDO PBJEI/JERI P/COLETOR ESGOTO - NBR 7362-1',7061304350,NULL,9,96),(38,1,'TUBO PVC RIGIDO PBJEI/JERI P/COLETOR ESGOTO - NBR 7362-1',7061304258,NULL,9,94),(39,1,'TUBO PVC RIGIDO PBJEI/JERI P/COLETOR ESGOTO - NBR 7362-1',7061304209,NULL,9,93),(40,1,'TUBO PVC RIGIDO PBJEI/JERI P/COLETOR ESGOTO - NBR 7362-1',7061304404,NULL,9,97),(41,1,'TUBO PVC RIGIDO PBJEI/JERI P/COLETOR ESGOTO - NBR 7362-1',7061304155,NULL,9,92),(42,1,'TUBO PVC RIGIDO PBJEI/JERI P/COLETOR ESGOTO - NBR 7362-1',7061304301,NULL,9,95),(43,1,'TUBO PVC RIGIDO PBJEI/JERI P/COLETOR ESGOTO - NBR 7362-1',7061304106,NULL,9,91),(44,1,'TUBO FF DUCTIL K9 REV INT CIM C/PONTA E BOLSA JE EN 681-1 P/AGUA',4004000104,2,10,25),(45,1,'TUBO FF DUCTIL K7 REV.INT CIM C/PONTA E BOLSA JE EN 681-1 AGUA',4003900202,2,10,43),(46,1,'TUBO FF DUCTIL K7 REV.INT CIM C/PONTA E BOLSA JE EN 681-1 AGUA',4003900159,2,10,34),(47,4,'COLAR DE TOMADA FF C/ CINTA INOX',4010109015,1,11,88),(48,1,'TUBO FF DUCTIL K 7 REV INT CIM C/PONTA E BOLSA JE2GS',4003100255,2,13,47),(49,1,'TUBO FF DUCTIL K 7 REV INT CIM C/PONTA E BOLSA JE2GS',4003100152,2,13,34),(50,1,'TUBO FF DUCTIL K 7 REV INT CIM C/PONTA E BOLSA JE2GS',4003100450,2,13,53),(51,1,'TUBO FF DUCTIL K 7 REV INT CIM C/PONTA E BOLSA JE2GS',4003100358,2,13,51),(52,1,'TUBO FF DUCTIL K 7 REV INT CIM C/PONTA E BOLSA JE2GS',4003100607,2,13,61),(53,1,'TUBO FF DUCTIL K 7 REV INT CIM C/PONTA E BOLSA JE2GS',4003100504,2,13,60),(54,1,'TUBO FF DUCTIL K 7 REV INT CIM C/PONTA E BOLSA JE2GS',4003100309,2,13,49),(55,1,'TUBO FF DUCTIL K 7 REV INT CIM C/PONTA E BOLSA JE2GS',4003100206,2,13,43),(56,1,'TUBO FF DUCTIL K 7 REV INT CIM C/PONTA E BOLSA JE2GS',4003100401,2,13,52),(57,1,'TUBO FF DUCTIL K 9 REV INT CIM C/PONTA E BOLSA JE2GS',4003300254,NULL,12,47),(58,1,'TUBO FF DUCTIL K 9 REV INT CIM C/PONTA E BOLSA JE2GS',4003300084,2,12,70),(59,1,'TUBO FF DUCTIL K 9 REV INT CIM C/PONTA E BOLSA JE2GS',4003300400,2,12,52),(60,1,'TUBO FF DUCTIL K 9 REV INT CIM C/PONTA E BOLSA JE2GS',4003300606,2,12,61),(61,1,'TUBO FF DUCTIL K 9 REV INT CIM C/PONTA E BOLSA JE2GS',4003300102,2,12,25),(62,1,'TUBO FF DUCTIL K 9 REV INT CIM C/PONTA E BOLSA JE2GS',4003300151,2,12,34),(63,1,'TUBO FF DUCTIL K 7 REV INT CIM C/PONTA E BOLSA JE P/ESGOTO',4080500702,2,14,63),(64,1,'TUBO FF DUCTIL K 9 REV INT CIM C/PONTA E BOLSA JE2GS P/ESGOTO',4080600306,2,14,49),(65,1,'TUBO FF DUCTIL K 9 REV INT CIM C/PONTA E BOLSA JE2GS P/ESGOTO',4080600100,2,14,25),(66,1,'TUBO FF DUCTIL K 7 REV INT CIM C/PONTA E BOLSA JE P/ESGOTO',4080500258,2,14,47),(67,1,'TUBO FF DUCTIL K 7 REV INT CIM C/PONTA E BOLSA JE P/ESGOTO',4080500404,2,14,52),(68,1,'TUBO FF DUCTIL K 7 REV INT CIM C/PONTA E BOLSA JE P/ESGOTO',4080500805,2,14,71),(69,1,'TUBO FF DUCTIL K 7 REV INT CIM C/PONTA E BOLSA JE P/ESGOTO',4080500350,2,14,51),(70,1,'TUBO FF DUCTIL K 9 REV INT CIM C/PONTA E BOLSA JE2GS P/ESGOTO',4080600082,2,14,70),(71,1,'TUBO FF DUCTIL K 7 REV INT CIM C/PONTA E BOLSA JE P/ESGOTO',4080500301,2,14,49),(72,1,'TUBO FF DUCTIL K 7 REV INT CIM C/PONTA E BOLSA JE P/ESGOTO',4080500507,2,14,60),(73,1,'TUBO FF DUCTIL K 7 REV INT CIM C/PONTA E BOLSA JE P/ESGOTO',4080500155,2,14,34),(74,1,'TUBO FF DUCTIL K 7 REV INT CIM C/PONTA E BOLSA JE P/ESGOTO',4080501007,2,14,32),(75,1,'TUBO FF DUCTIL K 7 REV INT CIM C/PONTA E BOLSA JE P/ESGOTO',4080500209,2,14,43),(76,4,'SELIM AJUSTAVEL A TUBOS PVC MACICO E CERAMICO',7085902008,NULL,15,6),(77,4,'SELIM AJUSTAVEL A TUBOS PVC MACICO E CERAMICO',7085901508,NULL,15,5),(78,1,'TUBO POLIETILENO - NTS 048',7004800205,NULL,16,37),(79,3,'CAL HIDRATADA',36026800206,NULL,NULL,98),(80,4,'CONEXOES METALICAS P/ LIGACOES DOMICILIARES',29005500180,NULL,NULL,4),(81,3,'CAL HIDRATADA',36026800012,NULL,NULL,2),(82,3,'CLORO LIQUIDO',36031000180,NULL,NULL,3),(83,3,'SOLUCAO',36088208001,NULL,NULL,7),(84,3,'CLORO LIQUIDO',36031001007,NULL,NULL,8),(85,3,'CLORO LIQUIDO',36031000908,NULL,NULL,9),(86,3,'ACIDO SULFURICO',36006000049,NULL,NULL,10),(87,3,'CAL VIRGEM',36027001003,NULL,NULL,11),(88,4,'CONEXOES METALICAS P/ LIGACOES DOMICILIARES',29005500130,NULL,NULL,12),(89,3,'HIPOCLORITO DE SODIO',36057000150,NULL,NULL,83),(90,4,'HIDROMETRO VOLUMETRICO C/EXTREMIDADES ROSCADAS',12055000154,NULL,NULL,38),(91,3,'SULFATO DE COBRE PENTAHIDRATADO',36084000505,NULL,NULL,74),(92,4,'HIDROMETRO ELETRONICO',12019000010,NULL,NULL,75),(93,3,'CAL HIDRATADA',36026800309,NULL,NULL,76),(94,3,'POLICLORETO DE ALUMINIO',36078400108,NULL,NULL,78),(95,3,'ACIDO FLUOSSILICICO',36004000255,NULL,NULL,82),(96,3,'PEROXIDO DE HIDROGENIO ESTABILIZADO',36077107566,NULL,NULL,79),(97,3,'PEROXIDO DE HIDROGENIO ESTABILIZADO',36077100031,NULL,NULL,80),(98,3,'HIDROXIDO DE SODIO',36056500500,NULL,NULL,86),(99,3,'CONCRETO RESINOSO',17042600404,NULL,NULL,89),(100,3,'CAL VIRGEM',36027000217,NULL,NULL,87),(101,3,'CARBONATO DE SODIO',36028200259,NULL,NULL,99),(102,3,'CLORETO FERRICO',36030500048,NULL,NULL,85),(103,2,'OXIGENIO',36071000101,NULL,NULL,90),(104,3,'FLUOSSILICATO DE SÓDIO',NULL,NULL,NULL,NULL),(105,3,'SULFATO DE ALUMINIO',36083605001,NULL,NULL,77),(106,3,'SULFATO DE ALUMINIO',36083604800,NULL,NULL,81),(107,3,'SULFATO DE ALUMINIO',36083602802,NULL,NULL,73),(108,3,'HIDROXIDO SODIO ESCAMAS,SC 25KG.ONU:1823 ',NULL,NULL,NULL,NULL),(109,4,'REGISTRO ESFERA PVC RIGIDO C/BORBOLETA',45022700200,NULL,NULL,72),(110,1,'TUBO POLIETILENO PE-80 DE   20 MM  (NBR 8417)',NULL,NULL,NULL,NULL),(111,4,'VALVULA GAVETA FF CUNHA MET. REVEST.C/ELASTOMERO C/BOLSAS JE CAB P/PVC',45070000107,NULL,NULL,27),(112,4,'VALVULA GAVETA FF CUNHA MET. REVEST.C/ELASTOMERO C/BOLSAS JE CAB P/PVC',45070000053,NULL,NULL,57),(113,4,'VALVULA GAVETA FF CUNHA MET. REVEST.C/ELASTOMERO C/BOLSAS JE CAB P/PVC',45070000077,NULL,NULL,67),(114,4,'VALVULA GAVETA FF CUNHA MET. REVEST. C/ELASTOMERO C/FLANGES/CABECOTE',45071000151,NULL,NULL,36),(115,4,'VALVULA GAVETA FF CUNHA MET. REVEST. C/ELASTOMERO C/FLANGES/CABECOTE',45071000102,NULL,NULL,31),(116,4,'VALVULA GAVETA FF CUNHA MET. REVEST. C/ELASTOMERO C/FLANGES/CABECOTE',45071000205,NULL,NULL,45),(117,4,'VALVULA GAVETA FF CUNHA MET. REVEST. C/ELASTOMERO C/BOLSAS JE CAB P/FF',45069000251,NULL,NULL,47),(118,4,'VALVULA GAVETA FF CUNHA MET. REVEST. C/ELASTOMERO C/BOLSAS JE CAB P/FF',45069000305,NULL,NULL,49),(119,4,'VALVULA GAVETA FF CUNHA MET. REVEST. C/ELASTOMERO C/BOLSAS JE CAB P/FF',45069000100,NULL,NULL,25),(120,4,'VALVULA GAVETA FF CUNHA MET. REVEST. C/ELASTOMERO C/BOLSAS JE CAB P/FF',45069000081,NULL,NULL,70),(121,4,'VALVULA GAVETA FF CUNHA MET. REVEST. C/ELASTOMERO C/BOLSAS JE CAB P/FF',45069000056,NULL,NULL,54),(122,4,'VALVULA GAVETA FF CUNHA MET. REVEST. C/ELASTOMERO C/BOLSAS JE CAB P/FF',45069000070,NULL,NULL,64),(123,4,'VALVULA GAVETA FF CUNHA MET. REVEST. C/ELASTOMERO C/BOLSAS JE CAB P/FF',45069000159,NULL,NULL,34),(124,4,'VALVULA GAVETA FF CUNHA MET. REVEST. C/ELASTOMERO C/BOLSAS JE CAB P/FF',45069000202,NULL,NULL,43),(125,3,'NITRATO DE AMONIO',36065007527,NULL,NULL,84);
/*!40000 ALTER TABLE `tb_aesbe_material` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_aesbe_modalidade_licitacao`
--

DROP TABLE IF EXISTS `tb_aesbe_modalidade_licitacao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_aesbe_modalidade_licitacao` (
  `co_modalidade` int(11) NOT NULL AUTO_INCREMENT,
  `ds_modalidade` varchar(45) NOT NULL,
  PRIMARY KEY (`co_modalidade`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_aesbe_modalidade_licitacao`
--

LOCK TABLES `tb_aesbe_modalidade_licitacao` WRITE;
/*!40000 ALTER TABLE `tb_aesbe_modalidade_licitacao` DISABLE KEYS */;
INSERT INTO `tb_aesbe_modalidade_licitacao` VALUES (1,'Pregão'),(2,'Dispensa');
/*!40000 ALTER TABLE `tb_aesbe_modalidade_licitacao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_aesbe_modulo`
--

DROP TABLE IF EXISTS `tb_aesbe_modulo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_aesbe_modulo` (
  `co_modulo` int(11) NOT NULL AUTO_INCREMENT,
  `no_modulo` varchar(45) NOT NULL,
  `no_modulo_visivel` varchar(45) NOT NULL,
  `st_modulo_visivel` tinyint(1) NOT NULL,
  `st_ativo` tinyint(1) NOT NULL,
  `ds_icon` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`co_modulo`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_aesbe_modulo`
--

LOCK TABLES `tb_aesbe_modulo` WRITE;
/*!40000 ALTER TABLE `tb_aesbe_modulo` DISABLE KEYS */;
INSERT INTO `tb_aesbe_modulo` VALUES (1,'banco_precos','Banco de Preços',1,1,'fa-bank');
/*!40000 ALTER TABLE `tb_aesbe_modulo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_aesbe_perfil`
--

DROP TABLE IF EXISTS `tb_aesbe_perfil`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_aesbe_perfil` (
  `co_perfil` int(11) NOT NULL AUTO_INCREMENT,
  `no_perfil` varchar(45) NOT NULL,
  `sg_perfil` varchar(45) NOT NULL,
  PRIMARY KEY (`co_perfil`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_aesbe_perfil`
--

LOCK TABLES `tb_aesbe_perfil` WRITE;
/*!40000 ALTER TABLE `tb_aesbe_perfil` DISABLE KEYS */;
INSERT INTO `tb_aesbe_perfil` VALUES (1,'Associada','ASSO'),(2,'Administrador','Admin');
/*!40000 ALTER TABLE `tb_aesbe_perfil` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_aesbe_perfil_funcionalidade`
--

DROP TABLE IF EXISTS `tb_aesbe_perfil_funcionalidade`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_aesbe_perfil_funcionalidade` (
  `co_perfil` int(11) NOT NULL,
  `co_funcionalidade` int(11) NOT NULL,
  PRIMARY KEY (`co_perfil`,`co_funcionalidade`),
  KEY `fk_tb_aesbe_perfil_has_tb_aesbe_funcionalidade_tb_aesbe_fun_idx` (`co_funcionalidade`),
  KEY `fk_tb_aesbe_perfil_has_tb_aesbe_funcionalidade_tb_aesbe_per_idx` (`co_perfil`),
  CONSTRAINT `fk_tb_aesbe_perfil_has_tb_aesbe_funcionalidade_tb_aesbe_funci1` FOREIGN KEY (`co_funcionalidade`) REFERENCES `tb_aesbe_funcionalidade` (`co_funcionalidade`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tb_aesbe_perfil_has_tb_aesbe_funcionalidade_tb_aesbe_perfil1` FOREIGN KEY (`co_perfil`) REFERENCES `tb_aesbe_perfil` (`co_perfil`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_aesbe_perfil_funcionalidade`
--

LOCK TABLES `tb_aesbe_perfil_funcionalidade` WRITE;
/*!40000 ALTER TABLE `tb_aesbe_perfil_funcionalidade` DISABLE KEYS */;
INSERT INTO `tb_aesbe_perfil_funcionalidade` VALUES (1,1),(1,2),(1,3),(1,4);
/*!40000 ALTER TABLE `tb_aesbe_perfil_funcionalidade` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_aesbe_perfil_oauth_users`
--

DROP TABLE IF EXISTS `tb_aesbe_perfil_oauth_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_aesbe_perfil_oauth_users` (
  `co_perfil` int(11) NOT NULL,
  `co_usuario` int(11) NOT NULL,
  PRIMARY KEY (`co_perfil`,`co_usuario`),
  KEY `fk_tb_aesbe_perfil_has_oauth_users_oauth_users1_idx` (`co_usuario`),
  KEY `fk_tb_aesbe_perfil_has_oauth_users_tb_aesbe_perfil1_idx` (`co_perfil`),
  CONSTRAINT `fk_tb_aesbe_perfil_has_oauth_users_oauth_users1` FOREIGN KEY (`co_usuario`) REFERENCES `oauth_users` (`co_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tb_aesbe_perfil_has_oauth_users_tb_aesbe_perfil1` FOREIGN KEY (`co_perfil`) REFERENCES `tb_aesbe_perfil` (`co_perfil`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_aesbe_perfil_oauth_users`
--

LOCK TABLES `tb_aesbe_perfil_oauth_users` WRITE;
/*!40000 ALTER TABLE `tb_aesbe_perfil_oauth_users` DISABLE KEYS */;
INSERT INTO `tb_aesbe_perfil_oauth_users` VALUES (1,2);
/*!40000 ALTER TABLE `tb_aesbe_perfil_oauth_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_aesbe_pessoa_fisica`
--

DROP TABLE IF EXISTS `tb_aesbe_pessoa_fisica`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_aesbe_pessoa_fisica` (
  `co_pessoa` int(11) NOT NULL AUTO_INCREMENT,
  `no_pessoa` varchar(100) NOT NULL,
  `ds_email` varchar(100) NOT NULL,
  `ds_email_secundario` varchar(100) DEFAULT NULL,
  `nu_tel_residencial` varchar(10) DEFAULT NULL,
  `nu_tel_comercial` varchar(10) DEFAULT NULL,
  `nu_celular` varchar(10) DEFAULT NULL,
  `st_presidente` tinyint(1) NOT NULL DEFAULT '0',
  `nu_cep` varchar(10) DEFAULT NULL,
  `ds_logradouro` varchar(100) DEFAULT NULL,
  `no_bairro` varchar(100) DEFAULT NULL,
  `nu_logradouro` varchar(10) DEFAULT NULL,
  `ds_complemento` varchar(30) DEFAULT NULL,
  `st_ativo` tinyint(1) NOT NULL,
  `co_uf` int(11) NOT NULL,
  `co_empresa` int(11) NOT NULL,
  PRIMARY KEY (`co_pessoa`),
  KEY `fk_tb_pessoa_fisica_tb_aesbe_uf1_idx` (`co_uf`),
  KEY `fk_tb_aesbe_pessoa_fisica_tb_aesbe_empresa1_idx` (`co_empresa`),
  CONSTRAINT `fk_tb_aesbe_pessoa_fisica_tb_aesbe_empresa1` FOREIGN KEY (`co_empresa`) REFERENCES `tb_aesbe_empresa` (`co_empresa`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tb_pessoa_fisica_tb_aesbe_uf1` FOREIGN KEY (`co_uf`) REFERENCES `tb_aesbe_uf` (`co_uf`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_aesbe_pessoa_fisica`
--

LOCK TABLES `tb_aesbe_pessoa_fisica` WRITE;
/*!40000 ALTER TABLE `tb_aesbe_pessoa_fisica` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_aesbe_pessoa_fisica` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_aesbe_proposta`
--

DROP TABLE IF EXISTS `tb_aesbe_proposta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_aesbe_proposta` (
  `co_proposta` int(11) NOT NULL AUTO_INCREMENT,
  `dt_proposta` datetime NOT NULL,
  `co_empresa` int(11) NOT NULL,
  PRIMARY KEY (`co_proposta`),
  KEY `fk_tb_aesbe_proposta_tb_aesbe_empresa1_idx` (`co_empresa`),
  CONSTRAINT `fk_tb_aesbe_proposta_tb_aesbe_empresa1` FOREIGN KEY (`co_empresa`) REFERENCES `tb_aesbe_empresa` (`co_empresa`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_aesbe_proposta`
--

LOCK TABLES `tb_aesbe_proposta` WRITE;
/*!40000 ALTER TABLE `tb_aesbe_proposta` DISABLE KEYS */;
INSERT INTO `tb_aesbe_proposta` VALUES (6,'2016-07-16 23:04:34',2),(7,'2016-07-26 00:52:00',3),(8,'2016-07-29 01:11:49',5);
/*!40000 ALTER TABLE `tb_aesbe_proposta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_aesbe_proposta_dados`
--

DROP TABLE IF EXISTS `tb_aesbe_proposta_dados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_aesbe_proposta_dados` (
  `co_proposta_dados` int(11) NOT NULL AUTO_INCREMENT,
  `dt_informacao` date NOT NULL,
  `vl_icms` double NOT NULL,
  `vl_ipi` double NOT NULL,
  `st_fob_cif` varchar(10) NOT NULL,
  `st_condicao_fornecimento` varchar(45) NOT NULL,
  `dt_proposta` date NOT NULL,
  `dt_contratacao` date NOT NULL,
  `qt_produto` int(11) NOT NULL,
  `dt_vigencia_contrato` date NOT NULL,
  `st_proposta` varchar(45) NOT NULL COMMENT '[original;nova]\n',
  `vl_com_imposto` double NOT NULL,
  `nu_contrato` int(11) NOT NULL,
  `nu_ata` double NOT NULL,
  `qt_dias_pagamento` int(11) NOT NULL,
  `tp_inspecao` varchar(45) NOT NULL,
  `st_nivel_atendimento` varchar(45) NOT NULL,
  `co_proposta` int(11) NOT NULL,
  `co_modalidade` int(11) NOT NULL,
  `co_material` int(11) NOT NULL,
  `st_especificacao_padrao` char(1) NOT NULL,
  `co_fornecedor` int(11) NOT NULL,
  `ds_observacao` varchar(100) DEFAULT NULL,
  `vl_iss` double DEFAULT NULL,
  PRIMARY KEY (`co_proposta_dados`),
  KEY `fk_tb_aesbe_proposta_dados_tb_aesbe_proposta1_idx` (`co_proposta`),
  KEY `fk_tb_aesbe_proposta_dados_tb_aesbe_modalidade_licitacao1_idx` (`co_modalidade`),
  KEY `fk_tb_aesbe_proposta_dados_tb_aesbe_material1_idx` (`co_material`),
  KEY `fk_tb_aesbe_proposta_dados_tb_aesbe_fornecedor1_idx` (`co_fornecedor`),
  CONSTRAINT `fk_tb_aesbe_proposta_dados_tb_aesbe_fornecedor1` FOREIGN KEY (`co_fornecedor`) REFERENCES `tb_aesbe_fornecedor` (`co_fornecedor`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tb_aesbe_proposta_dados_tb_aesbe_material1` FOREIGN KEY (`co_material`) REFERENCES `tb_aesbe_material` (`co_material`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tb_aesbe_proposta_dados_tb_aesbe_modalidade_licitacao1` FOREIGN KEY (`co_modalidade`) REFERENCES `tb_aesbe_modalidade_licitacao` (`co_modalidade`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tb_aesbe_proposta_dados_tb_aesbe_proposta1` FOREIGN KEY (`co_proposta`) REFERENCES `tb_aesbe_proposta` (`co_proposta`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=128 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_aesbe_proposta_dados`
--

LOCK TABLES `tb_aesbe_proposta_dados` WRITE;
/*!40000 ALTER TABLE `tb_aesbe_proposta_dados` DISABLE KEYS */;
INSERT INTO `tb_aesbe_proposta_dados` VALUES (1,'2016-07-25',15,5,'FOB','PARCELADO','2015-05-26','2015-06-30',2356,'2019-09-01','NOVA',223.56,123,123,30,'RECEBIMENTO','VERDE',6,1,44,'S',5,'',10),(2,'2016-07-25',16,6,'FOB','PARCELADO','2015-05-27','2015-07-01',2357,'2019-09-02','NOVA',224.56,124,124,31,'RECEBIMENTO','VERDE',6,1,45,'S',5,'',11),(3,'2016-07-25',17,7,'FOB','PARCELADO','2015-05-28','2015-07-02',2358,'2019-09-03','NOVA',225.56,125,125,32,'RECEBIMENTO','VERDE',6,1,46,'S',5,'',12),(4,'2016-07-25',18,8,'FOB','PARCELADO','2015-05-29','2015-07-03',2359,'2019-09-04','NOVA',226.56,126,126,33,'RECEBIMENTO','VERDE',6,1,48,'S',5,'',13),(5,'2016-07-25',19,9,'FOB','PARCELADO','2015-05-30','2015-07-04',2360,'2019-09-05','NOVA',227.56,127,127,34,'RECEBIMENTO','VERDE',6,1,49,'S',5,'',14),(6,'2016-07-25',20,10,'FOB','PARCELADO','2015-05-31','2015-07-05',2361,'2019-09-06','NOVA',228.56,128,128,35,'RECEBIMENTO','VERDE',6,1,50,'S',5,'',15),(7,'2016-07-25',21,11,'FOB','PARCELADO','2015-06-01','2015-07-06',2362,'2019-09-07','NOVA',229.56,129,129,36,'RECEBIMENTO','VERDE',6,1,51,'S',5,'',16),(8,'2016-07-25',22,12,'FOB','PARCELADO','2015-06-02','2015-07-07',2363,'2019-09-08','NOVA',230.56,130,130,37,'RECEBIMENTO','VERDE',6,1,52,'S',5,'',17),(9,'2016-07-25',23,13,'FOB','PARCELADO','2015-06-03','2015-07-08',2364,'2019-09-09','NOVA',231.56,131,131,38,'RECEBIMENTO','VERDE',6,1,53,'S',5,'',18),(10,'2016-07-25',24,14,'FOB','PARCELADO','2015-06-04','2015-07-09',2365,'2019-09-10','NOVA',232.56,132,132,39,'RECEBIMENTO','VERDE',6,1,54,'S',5,'',19),(11,'2016-07-25',25,15,'FOB','PARCELADO','2015-06-05','2015-07-10',2366,'2019-09-11','NOVA',233.56,133,133,40,'RECEBIMENTO','VERDE',6,1,55,'S',5,'',20),(12,'2016-07-25',26,16,'FOB','PARCELADO','2015-06-06','2015-07-11',2367,'2019-09-12','NOVA',234.56,134,134,41,'RECEBIMENTO','VERDE',6,1,56,'S',5,'',21),(13,'2016-07-25',27,17,'FOB','PARCELADO','2015-06-07','2015-07-12',2368,'2019-09-13','NOVA',235.56,135,135,42,'RECEBIMENTO','VERDE',6,1,58,'S',5,'',22),(14,'2016-07-25',28,18,'FOB','PARCELADO','2015-06-08','2015-07-13',2369,'2019-09-14','NOVA',236.56,136,136,43,'RECEBIMENTO','VERDE',6,1,59,'S',5,'',23),(15,'2016-07-25',29,19,'FOB','PARCELADO','2015-06-09','2015-07-14',2370,'2019-09-15','NOVA',237.56,137,137,44,'RECEBIMENTO','VERDE',6,1,60,'S',5,'',24),(16,'2016-07-25',30,20,'FOB','PARCELADO','2015-06-10','2015-07-15',2371,'2019-09-16','NOVA',238.56,138,138,45,'RECEBIMENTO','VERDE',6,1,61,'S',5,'',25),(17,'2016-07-25',31,21,'FOB','PARCELADO','2015-06-11','2015-07-16',2372,'2019-09-17','NOVA',239.56,139,139,46,'RECEBIMENTO','VERDE',6,1,62,'S',5,'',26),(18,'2016-07-25',32,22,'FOB','PARCELADO','2015-06-12','2015-07-17',2373,'2019-09-18','NOVA',240.56,140,140,47,'RECEBIMENTO','VERDE',6,1,63,'S',5,'',27),(19,'2016-07-25',33,23,'FOB','PARCELADO','2015-06-13','2015-07-18',2374,'2019-09-19','NOVA',241.56,141,141,48,'RECEBIMENTO','VERDE',6,1,64,'S',5,'',28),(20,'2016-07-25',34,24,'FOB','PARCELADO','2015-06-14','2015-07-19',2375,'2019-09-20','NOVA',242.56,142,142,49,'RECEBIMENTO','VERDE',6,1,65,'S',5,'',29),(21,'2016-07-25',35,25,'FOB','PARCELADO','2015-06-15','2015-07-20',2376,'2019-09-21','NOVA',243.56,143,143,50,'RECEBIMENTO','VERDE',6,1,66,'S',5,'',30),(22,'2016-07-25',36,26,'FOB','PARCELADO','2015-06-16','2015-07-21',2377,'2019-09-22','NOVA',244.56,144,144,51,'RECEBIMENTO','VERDE',6,1,67,'S',5,'',31),(23,'2016-07-25',37,27,'FOB','PARCELADO','2015-06-17','2015-07-22',2378,'2019-09-23','NOVA',245.56,145,145,52,'RECEBIMENTO','VERDE',6,1,68,'S',5,'',32),(24,'2016-07-25',38,28,'FOB','PARCELADO','2015-06-18','2015-07-23',2379,'2019-09-24','NOVA',246.56,146,146,53,'RECEBIMENTO','VERDE',6,1,69,'S',5,'',33),(25,'2016-07-25',39,29,'FOB','PARCELADO','2015-06-19','2015-07-24',2380,'2019-09-25','NOVA',247.56,147,147,54,'RECEBIMENTO','VERDE',6,1,70,'S',5,'',34),(26,'2016-07-25',40,30,'FOB','PARCELADO','2015-06-20','2015-07-25',2381,'2019-09-26','NOVA',248.56,148,148,55,'RECEBIMENTO','VERDE',6,1,71,'S',5,'',35),(27,'2016-07-25',41,31,'FOB','PARCELADO','2015-06-21','2015-07-26',2382,'2019-09-27','NOVA',249.56,149,149,56,'RECEBIMENTO','VERDE',6,1,72,'S',5,'',36),(28,'2016-07-25',42,32,'FOB','PARCELADO','2015-06-22','2015-07-27',2383,'2019-09-28','NOVA',250.56,150,150,57,'RECEBIMENTO','VERDE',6,1,73,'S',5,'',37),(29,'2016-07-25',43,33,'FOB','PARCELADO','2015-06-23','2015-07-28',2384,'2019-09-29','NOVA',251.56,151,151,58,'RECEBIMENTO','VERDE',6,1,74,'S',5,'',38),(30,'2016-07-25',44,34,'FOB','PARCELADO','2015-06-24','2015-07-29',2385,'2019-09-30','NOVA',252.56,152,152,59,'RECEBIMENTO','VERDE',6,1,75,'S',5,'',39),(31,'2016-07-25',45,35,'FOB','PARCELADO','2015-06-25','2015-07-30',2386,'2019-10-01','NOVA',253.56,153,153,60,'RECEBIMENTO','VERDE',6,1,21,'S',5,'',40),(32,'2016-07-25',46,36,'FOB','PARCELADO','2015-06-26','2015-07-31',2387,'2019-10-02','NOVA',254.56,154,154,61,'RECEBIMENTO','VERDE',6,1,22,'S',5,'',41),(33,'2016-07-25',47,37,'FOB','PARCELADO','2015-06-27','2015-08-01',2388,'2019-10-03','NOVA',255.56,155,155,62,'RECEBIMENTO','VERDE',6,1,23,'S',5,'',42),(34,'2016-07-25',48,38,'FOB','PARCELADO','2015-06-28','2015-08-02',2389,'2019-10-04','NOVA',256.56,156,156,63,'RECEBIMENTO','VERDE',6,1,24,'S',5,'',43),(35,'2016-07-25',49,39,'FOB','PARCELADO','2015-06-29','2015-08-03',2390,'2019-10-05','NOVA',257.56,157,157,64,'RECEBIMENTO','VERDE',6,1,25,'S',5,'',44),(36,'2016-07-25',50,40,'FOB','PARCELADO','2015-06-30','2015-08-04',2391,'2019-10-06','NOVA',258.56,158,158,65,'RECEBIMENTO','VERDE',6,1,26,'S',5,'',45),(37,'2016-07-25',51,41,'FOB','PARCELADO','2015-07-01','2015-08-05',2392,'2019-10-07','NOVA',259.56,159,159,66,'RECEBIMENTO','VERDE',6,1,27,'S',5,'',46),(38,'2016-07-25',52,42,'FOB','PARCELADO','2015-07-02','2015-08-06',2393,'2019-10-08','NOVA',260.56,160,160,67,'RECEBIMENTO','VERDE',6,1,28,'S',5,'',47),(39,'2016-07-25',53,43,'FOB','PARCELADO','2015-07-03','2015-08-07',2394,'2019-10-09','NOVA',261.56,161,161,68,'RECEBIMENTO','VERDE',6,1,29,'S',5,'',48),(40,'2016-07-25',54,44,'FOB','PARCELADO','2015-07-04','2015-08-08',2395,'2019-10-10','NOVA',262.56,162,162,69,'RECEBIMENTO','VERDE',6,1,30,'S',5,'',49),(41,'2016-07-25',55,45,'FOB','PARCELADO','2015-07-05','2015-08-09',2396,'2019-10-11','NOVA',263.56,163,163,70,'RECEBIMENTO','VERDE',6,1,31,'S',5,'',50),(42,'2016-07-25',56,46,'FOB','PARCELADO','2015-07-06','2015-08-10',2397,'2019-10-12','NOVA',264.56,164,164,71,'RECEBIMENTO','VERDE',6,1,32,'S',5,'',51),(43,'2016-07-25',57,47,'FOB','PARCELADO','2015-07-07','2015-08-11',2398,'2019-10-13','NOVA',265.56,165,165,72,'RECEBIMENTO','VERDE',6,1,33,'S',5,'',52),(44,'2016-07-25',58,48,'FOB','PARCELADO','2015-07-08','2015-08-12',2399,'2019-10-14','NOVA',266.56,166,166,73,'RECEBIMENTO','VERDE',6,1,34,'S',5,'',53),(45,'2016-07-25',59,49,'FOB','PARCELADO','2015-07-09','2015-08-13',2400,'2019-10-15','NOVA',267.56,167,167,74,'RECEBIMENTO','VERDE',6,1,35,'S',5,'',54),(46,'2016-07-25',60,50,'FOB','PARCELADO','2015-07-10','2015-08-14',2401,'2019-10-16','NOVA',268.56,168,168,75,'RECEBIMENTO','VERDE',6,1,36,'S',5,'',55),(47,'2016-07-25',61,51,'FOB','PARCELADO','2015-07-11','2015-08-15',2402,'2019-10-17','NOVA',269.56,169,169,76,'RECEBIMENTO','VERDE',6,1,37,'S',5,'',56),(48,'2016-07-25',62,52,'FOB','PARCELADO','2015-07-12','2015-08-16',2403,'2019-10-18','NOVA',270.56,170,170,77,'RECEBIMENTO','VERDE',6,1,38,'S',5,'',57),(49,'2016-07-25',63,53,'FOB','PARCELADO','2015-07-13','2015-08-17',2404,'2019-10-19','NOVA',271.56,171,171,78,'RECEBIMENTO','VERDE',6,1,39,'S',5,'',58),(50,'2016-07-25',64,54,'FOB','PARCELADO','2015-07-14','2015-08-18',2405,'2019-10-20','NOVA',272.56,172,172,79,'RECEBIMENTO','VERDE',6,1,40,'S',5,'',59),(51,'2016-07-25',65,55,'FOB','PARCELADO','2015-07-15','2015-08-19',2406,'2019-10-21','NOVA',273.56,173,173,80,'RECEBIMENTO','VERDE',6,1,41,'S',5,'',60),(52,'2016-07-25',66,56,'FOB','PARCELADO','2015-07-16','2015-08-20',2407,'2019-10-22','NOVA',274.56,174,174,81,'RECEBIMENTO','VERDE',6,1,42,'S',5,'',61),(53,'2016-07-25',67,57,'FOB','PARCELADO','2015-07-17','2015-08-21',2408,'2019-10-23','NOVA',275.56,175,175,82,'RECEBIMENTO','VERDE',6,1,43,'S',5,'',62),(54,'2016-07-25',68,58,'FOB','PARCELADO','2015-07-18','2015-08-22',2409,'2019-10-24','NOVA',276.56,176,176,83,'RECEBIMENTO','VERDE',6,1,57,'S',5,'',63),(55,'2016-07-25',69,59,'FOB','PARCELADO','2015-07-19','2015-08-23',2410,'2019-10-25','NOVA',277.56,177,177,84,'RECEBIMENTO','VERDE',6,1,78,'S',5,'',64),(56,'2016-07-25',70,60,'FOB','PARCELADO','2015-07-20','2015-08-24',2411,'2019-10-26','NOVA',278.56,178,178,85,'RECEBIMENTO','VERDE',6,1,110,'S',5,'',65),(57,'2016-07-25',71,61,'FOB','PARCELADO','2015-07-21','2015-08-25',2412,'2019-10-27','NOVA',279.56,179,179,86,'RECEBIMENTO','VERDE',6,1,103,'S',5,'',66),(58,'2016-07-25',72,62,'FOB','PARCELADO','2015-07-22','2015-08-26',2413,'2019-10-28','NOVA',280.56,180,180,87,'RECEBIMENTO','VERDE',6,1,79,'S',5,'',67),(59,'2016-07-25',73,63,'FOB','PARCELADO','2015-07-23','2015-08-27',2414,'2019-10-29','NOVA',281.56,181,181,88,'RECEBIMENTO','VERDE',6,1,81,'S',5,'',68),(60,'2016-07-25',74,64,'FOB','PARCELADO','2015-07-24','2015-08-28',2415,'2019-10-30','NOVA',282.56,182,182,89,'RECEBIMENTO','VERDE',6,1,82,'S',5,'',69),(61,'2016-07-25',75,65,'FOB','PARCELADO','2015-07-25','2015-08-29',2416,'2019-10-31','NOVA',283.56,183,183,90,'RECEBIMENTO','VERDE',6,1,83,'S',5,'',70),(62,'2016-07-25',76,66,'FOB','PARCELADO','2015-07-26','2015-08-30',2417,'2019-11-01','NOVA',284.56,184,184,91,'RECEBIMENTO','VERDE',6,1,84,'S',5,'',71),(63,'2016-07-25',77,67,'FOB','PARCELADO','2015-07-27','2015-08-31',2418,'2019-11-02','NOVA',285.56,185,185,92,'RECEBIMENTO','VERDE',6,1,85,'S',5,'',72),(64,'2016-07-25',78,68,'FOB','PARCELADO','2015-07-28','2015-09-01',2419,'2019-11-03','NOVA',286.56,186,186,93,'RECEBIMENTO','VERDE',6,1,86,'S',5,'',73),(65,'2016-07-25',79,69,'FOB','PARCELADO','2015-07-29','2015-09-02',2420,'2019-11-04','NOVA',287.56,187,187,94,'RECEBIMENTO','VERDE',6,1,87,'S',5,'',74),(66,'2016-07-25',80,70,'FOB','PARCELADO','2015-07-30','2015-09-03',2421,'2019-11-05','NOVA',288.56,188,188,95,'RECEBIMENTO','VERDE',6,1,89,'S',5,'',75),(67,'2016-07-25',81,71,'FOB','PARCELADO','2015-07-31','2015-09-04',2422,'2019-11-06','NOVA',289.56,189,189,96,'RECEBIMENTO','VERDE',6,1,91,'S',5,'',76),(68,'2016-07-25',82,72,'FOB','PARCELADO','2015-08-01','2015-09-05',2423,'2019-11-07','NOVA',290.56,190,190,97,'RECEBIMENTO','VERDE',6,1,93,'S',5,'',77),(69,'2016-07-25',83,73,'FOB','PARCELADO','2015-08-02','2015-09-06',2424,'2019-11-08','NOVA',291.56,191,191,98,'RECEBIMENTO','VERDE',6,1,94,'S',5,'',78),(70,'2016-07-25',84,74,'FOB','PARCELADO','2015-08-03','2015-09-07',2425,'2019-11-09','NOVA',292.56,192,192,99,'RECEBIMENTO','VERDE',6,1,95,'S',5,'',79),(71,'2016-07-25',85,75,'FOB','PARCELADO','2015-08-04','2015-09-08',2426,'2019-11-10','NOVA',293.56,193,193,100,'RECEBIMENTO','VERDE',6,1,96,'S',5,'',80),(72,'2016-07-25',86,76,'FOB','PARCELADO','2015-08-05','2015-09-09',2427,'2019-11-11','NOVA',294.56,194,194,101,'RECEBIMENTO','VERDE',6,1,97,'S',5,'',81),(73,'2016-07-25',87,77,'FOB','PARCELADO','2015-08-06','2015-09-10',2428,'2019-11-12','NOVA',295.56,195,195,102,'RECEBIMENTO','VERDE',6,1,98,'S',5,'',82),(74,'2016-07-25',88,78,'FOB','PARCELADO','2015-08-07','2015-09-11',2429,'2019-11-13','NOVA',296.56,196,196,103,'RECEBIMENTO','VERDE',6,1,99,'S',5,'',83),(75,'2016-07-25',89,79,'FOB','PARCELADO','2015-08-08','2015-09-12',2430,'2019-11-14','NOVA',297.56,197,197,104,'RECEBIMENTO','VERDE',6,1,100,'S',5,'',84),(76,'2016-07-25',90,80,'FOB','PARCELADO','2015-08-09','2015-09-13',2431,'2019-11-15','NOVA',298.56,198,198,105,'RECEBIMENTO','VERDE',6,1,101,'S',5,'',85),(77,'2016-07-25',91,81,'FOB','PARCELADO','2015-08-10','2015-09-14',2432,'2019-11-16','NOVA',299.56,199,199,106,'RECEBIMENTO','VERDE',6,1,102,'S',5,'',86),(78,'2016-07-25',92,82,'FOB','PARCELADO','2015-08-11','2015-09-15',2433,'2019-11-17','NOVA',300.56,200,200,107,'RECEBIMENTO','VERDE',6,1,104,'S',5,'',87),(79,'2016-07-25',93,83,'FOB','PARCELADO','2015-08-12','2015-09-16',2434,'2019-11-18','NOVA',301.56,201,201,108,'RECEBIMENTO','VERDE',6,1,105,'S',5,'',88),(80,'2016-07-25',94,84,'FOB','PARCELADO','2015-08-13','2015-09-17',2435,'2019-11-19','NOVA',302.56,202,202,109,'RECEBIMENTO','VERDE',6,1,106,'S',5,'',89),(81,'2016-07-25',95,85,'FOB','PARCELADO','2015-08-14','2015-09-18',2436,'2019-11-20','NOVA',303.56,203,203,110,'RECEBIMENTO','VERDE',6,1,107,'S',5,'',90),(82,'2016-07-25',96,86,'FOB','PARCELADO','2015-08-15','2015-09-19',2437,'2019-11-21','NOVA',304.56,204,204,111,'RECEBIMENTO','VERDE',6,1,108,'S',5,'',91),(83,'2016-07-25',97,87,'FOB','PARCELADO','2015-08-16','2015-09-20',2438,'2019-11-22','NOVA',305.56,205,205,112,'RECEBIMENTO','VERDE',6,1,125,'S',5,'',92),(87,'2016-07-25',101,91,'FOB','PARCELADO','2015-08-20','2015-09-24',2442,'2019-11-26','NOVA',309.56,209,209,116,'RECEBIMENTO','VERDE',6,1,4,'S',5,'',96),(88,'2016-07-25',102,92,'FOB','PARCELADO','2015-08-21','2015-09-25',2443,'2019-11-27','NOVA',310.56,210,210,117,'RECEBIMENTO','VERDE',6,1,5,'S',5,'',97),(89,'2016-07-25',103,93,'FOB','PARCELADO','2015-08-22','2015-09-26',2444,'2019-11-28','NOVA',311.56,211,211,118,'RECEBIMENTO','VERDE',6,1,6,'S',5,'',98),(90,'2016-07-25',104,94,'FOB','PARCELADO','2015-08-23','2015-09-27',2445,'2019-11-29','NOVA',312.56,212,212,119,'RECEBIMENTO','VERDE',6,1,7,'S',5,'',99),(91,'2016-07-25',105,95,'FOB','PARCELADO','2015-08-24','2015-09-28',2446,'2019-11-30','NOVA',313.56,213,213,120,'RECEBIMENTO','VERDE',6,1,8,'S',5,'',100),(92,'2016-07-25',106,96,'FOB','PARCELADO','2015-08-25','2015-09-29',2447,'2019-12-01','NOVA',314.56,214,214,121,'RECEBIMENTO','VERDE',6,1,9,'S',5,'',101),(93,'2016-07-25',107,97,'FOB','PARCELADO','2015-08-26','2015-09-30',2448,'2019-12-02','NOVA',315.56,215,215,122,'RECEBIMENTO','VERDE',6,1,10,'S',5,'',102),(94,'2016-07-25',108,98,'FOB','PARCELADO','2015-08-27','2015-10-01',2449,'2019-12-03','NOVA',316.56,216,216,123,'RECEBIMENTO','VERDE',6,1,11,'S',5,'',103),(95,'2016-07-25',109,99,'FOB','PARCELADO','2015-08-28','2015-10-02',2450,'2019-12-04','NOVA',317.56,217,217,124,'RECEBIMENTO','VERDE',6,1,12,'S',5,'',104),(96,'2016-07-25',110,100,'FOB','PARCELADO','2015-08-29','2015-10-03',2451,'2019-12-05','NOVA',318.56,218,218,125,'RECEBIMENTO','VERDE',6,1,13,'S',5,'',105),(97,'2016-07-25',111,101,'FOB','PARCELADO','2015-08-30','2015-10-04',2452,'2019-12-06','NOVA',319.56,219,219,126,'RECEBIMENTO','VERDE',6,1,14,'S',5,'',106),(98,'2016-07-25',112,102,'FOB','PARCELADO','2015-08-31','2015-10-05',2453,'2019-12-07','NOVA',320.56,220,220,127,'RECEBIMENTO','VERDE',6,1,15,'S',5,'',107),(99,'2016-07-25',113,103,'FOB','PARCELADO','2015-09-01','2015-10-06',2454,'2019-12-08','NOVA',321.56,221,221,128,'RECEBIMENTO','VERDE',6,1,16,'S',5,'',108),(100,'2016-07-25',114,104,'FOB','PARCELADO','2015-09-02','2015-10-07',2455,'2019-12-09','NOVA',322.56,222,222,129,'RECEBIMENTO','VERDE',6,1,17,'S',5,'',109),(101,'2016-07-25',115,105,'FOB','PARCELADO','2015-09-03','2015-10-08',2456,'2019-12-10','NOVA',323.56,223,223,130,'RECEBIMENTO','VERDE',6,1,18,'S',5,'',110),(102,'2016-07-25',116,106,'FOB','PARCELADO','2015-09-04','2015-10-09',2457,'2019-12-11','NOVA',324.56,224,224,131,'RECEBIMENTO','VERDE',6,1,19,'S',5,'',111),(103,'2016-07-25',117,107,'FOB','PARCELADO','2015-09-05','2015-10-10',2458,'2019-12-12','NOVA',325.56,225,225,132,'RECEBIMENTO','VERDE',6,1,20,'S',5,'',112),(104,'2016-07-25',118,108,'FOB','PARCELADO','2015-09-06','2015-10-11',2459,'2019-12-13','NOVA',326.56,226,226,133,'RECEBIMENTO','VERDE',6,1,76,'S',5,'',113),(105,'2016-07-25',119,109,'FOB','PARCELADO','2015-09-07','2015-10-12',2460,'2019-12-14','NOVA',327.56,227,227,134,'RECEBIMENTO','VERDE',6,1,77,'S',5,'',114),(106,'2016-07-25',120,110,'FOB','PARCELADO','2015-09-08','2015-10-13',2461,'2019-12-15','NOVA',328.56,228,228,135,'RECEBIMENTO','VERDE',6,1,80,'S',5,'',115),(107,'2016-07-25',121,111,'FOB','PARCELADO','2015-09-09','2015-10-14',2462,'2019-12-16','NOVA',329.56,229,229,136,'RECEBIMENTO','VERDE',6,1,88,'S',5,'',116),(108,'2016-07-25',122,112,'FOB','PARCELADO','2015-09-10','2015-10-15',2463,'2019-12-17','NOVA',330.56,230,230,137,'RECEBIMENTO','VERDE',6,1,90,'S',5,'',117),(109,'2016-07-25',123,113,'FOB','PARCELADO','2015-09-11','2015-10-16',2464,'2019-12-18','NOVA',331.56,231,231,138,'RECEBIMENTO','VERDE',6,1,92,'S',5,'',118),(110,'2016-07-25',124,114,'FOB','PARCELADO','2015-09-12','2015-10-17',2465,'2019-12-19','NOVA',332.56,232,232,139,'RECEBIMENTO','VERDE',6,1,109,'S',5,'',119),(111,'2016-07-25',125,115,'FOB','PARCELADO','2015-09-13','2015-10-18',2466,'2019-12-20','NOVA',333.56,233,233,140,'RECEBIMENTO','VERDE',6,1,111,'S',5,'',120),(112,'2016-07-25',126,116,'FOB','PARCELADO','2015-09-14','2015-10-19',2467,'2019-12-21','NOVA',334.56,234,234,141,'RECEBIMENTO','VERDE',6,1,112,'S',5,'',121),(113,'2016-07-25',127,117,'FOB','PARCELADO','2015-09-15','2015-10-20',2468,'2019-12-22','NOVA',335.56,235,235,142,'RECEBIMENTO','VERDE',6,1,113,'S',5,'',122),(114,'2016-07-25',128,118,'FOB','PARCELADO','2015-09-16','2015-10-21',2469,'2019-12-23','NOVA',336.56,236,236,143,'RECEBIMENTO','VERDE',6,1,114,'S',5,'',123),(115,'2016-07-25',129,119,'FOB','PARCELADO','2015-09-17','2015-10-22',2470,'2019-12-24','NOVA',337.56,237,237,144,'RECEBIMENTO','VERDE',6,1,115,'S',5,'',124),(116,'2016-07-25',130,120,'FOB','PARCELADO','2015-09-18','2015-10-23',2471,'2019-12-25','NOVA',338.56,238,238,145,'RECEBIMENTO','VERDE',6,1,116,'S',5,'',125),(117,'2016-07-25',131,121,'FOB','PARCELADO','2015-09-19','2015-10-24',2472,'2019-12-26','NOVA',339.56,239,239,146,'RECEBIMENTO','VERDE',6,1,117,'S',5,'',126),(118,'2016-07-25',132,122,'FOB','PARCELADO','2015-09-20','2015-10-25',2473,'2019-12-27','NOVA',340.56,240,240,147,'RECEBIMENTO','VERDE',6,1,118,'S',5,'',127),(119,'2016-07-25',133,123,'FOB','PARCELADO','2015-09-21','2015-10-26',2474,'2019-12-28','NOVA',341.56,241,241,148,'RECEBIMENTO','VERDE',6,1,119,'S',5,'',128),(120,'2016-07-25',134,124,'FOB','PARCELADO','2015-09-22','2015-10-27',2475,'2019-12-29','NOVA',342.56,242,242,149,'RECEBIMENTO','VERDE',6,1,120,'S',5,'',129),(121,'2016-07-25',135,125,'FOB','PARCELADO','2015-09-23','2015-10-28',2476,'2019-12-30','NOVA',343.56,243,243,150,'RECEBIMENTO','VERDE',6,1,121,'S',5,'',130),(122,'2016-07-25',136,126,'FOB','PARCELADO','2015-09-24','2015-10-29',2477,'2019-12-31','NOVA',344.56,244,244,151,'RECEBIMENTO','VERDE',6,1,122,'S',5,'',131),(123,'2016-07-25',137,127,'FOB','PARCELADO','2015-09-25','2015-10-30',2478,'2020-01-01','NOVA',345.56,245,245,152,'RECEBIMENTO','VERDE',6,1,123,'S',5,'',132),(124,'2016-07-25',138,128,'FOB','PARCELADO','2015-09-26','2015-10-31',2479,'2020-01-02','NOVA',346.56,246,246,153,'RECEBIMENTO','VERDE',6,1,124,'S',5,'',133),(125,'2016-07-25',98,88,'FOB','PARCELADO','2015-08-17','2015-09-21',2439,'2019-11-23','NOVA',306.56,206,206,113,'RECEBIMENTO','VERDE',6,1,1,'S',5,'',93),(126,'2016-07-25',99,89,'FOB','PARCELADO','2015-08-18','2015-09-22',2440,'2019-11-24','NOVA',307.56,207,207,114,'RECEBIMENTO','VERDE',6,1,2,'S',5,'',94),(127,'2016-07-25',100,90,'FOB','PARCELADO','2015-08-19','2015-09-23',2441,'2019-11-25','NOVA',308.56,208,208,115,'RECEBIMENTO','VERDE',6,1,3,'S',5,'',95);
/*!40000 ALTER TABLE `tb_aesbe_proposta_dados` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_aesbe_regiao`
--

DROP TABLE IF EXISTS `tb_aesbe_regiao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_aesbe_regiao` (
  `co_regiao` int(11) NOT NULL AUTO_INCREMENT,
  `no_regiao` varchar(45) NOT NULL,
  `nu_ordem_apresentacao` int(11) NOT NULL,
  PRIMARY KEY (`co_regiao`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_aesbe_regiao`
--

LOCK TABLES `tb_aesbe_regiao` WRITE;
/*!40000 ALTER TABLE `tb_aesbe_regiao` DISABLE KEYS */;
INSERT INTO `tb_aesbe_regiao` VALUES (1,'Norte',1),(2,'Nordeste',2),(3,'Centro-Oeste',3),(4,'Sudeste',4),(5,'Sul',5);
/*!40000 ALTER TABLE `tb_aesbe_regiao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_aesbe_tipo_arquivo`
--

DROP TABLE IF EXISTS `tb_aesbe_tipo_arquivo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_aesbe_tipo_arquivo` (
  `co_tipo_arquivo` int(11) NOT NULL,
  `ds_tipo_arquivo` varchar(100) NOT NULL,
  PRIMARY KEY (`co_tipo_arquivo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_aesbe_tipo_arquivo`
--

LOCK TABLES `tb_aesbe_tipo_arquivo` WRITE;
/*!40000 ALTER TABLE `tb_aesbe_tipo_arquivo` DISABLE KEYS */;
INSERT INTO `tb_aesbe_tipo_arquivo` VALUES (1,'PROPOSTA'),(2,'ATA');
/*!40000 ALTER TABLE `tb_aesbe_tipo_arquivo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_aesbe_uf`
--

DROP TABLE IF EXISTS `tb_aesbe_uf`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_aesbe_uf` (
  `co_uf` int(11) NOT NULL,
  `no_uf` varchar(100) NOT NULL,
  `co_regiao` int(11) NOT NULL,
  `sg_uf` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`co_uf`),
  KEY `fk_tb_aesbe_uf_tb_aesbe_regiao_idx` (`co_regiao`),
  CONSTRAINT `fk_tb_aesbe_uf_tb_aesbe_regiao` FOREIGN KEY (`co_regiao`) REFERENCES `tb_aesbe_regiao` (`co_regiao`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_aesbe_uf`
--

LOCK TABLES `tb_aesbe_uf` WRITE;
/*!40000 ALTER TABLE `tb_aesbe_uf` DISABLE KEYS */;
INSERT INTO `tb_aesbe_uf` VALUES (1,'Minas Gerais',4,NULL),(2,'São Paulo',4,NULL),(11,'Rondônia',1,'RO'),(12,'Acre',1,'AC'),(13,'Amazonas',1,'AM'),(14,'Roraima',1,'RR'),(15,'Pará',1,'PA'),(16,'Amapá',1,'AP'),(17,'Tocantins',1,'TO'),(21,'Maranhão',2,'MA'),(22,'Piauí',2,'PI'),(23,'Ceará',2,'CE'),(24,'Rio Grande do Norte',2,'RN'),(25,'Paraíba',2,'PB'),(26,'Pernambuco',2,'PE'),(27,'Alagoas',2,'AL'),(28,'Sergipe',2,'SE'),(29,'Bahia',2,'BA'),(31,'Minas Gerais',4,'MG'),(32,'Espírito Santo',4,'ES'),(33,'Rio de Janeiro',4,'RJ'),(35,'São Paulo',4,'SP'),(41,'Paraná',5,'PR'),(42,'Santa Catarina',5,'SC'),(43,'Rio Grande do Sul',5,'RS'),(50,'Mato Grosso do Sul',3,'MS'),(51,'Mato Grosso',3,'MT'),(52,'Goiás',3,'GO'),(53,'Distrito Federal',3,'DF');
/*!40000 ALTER TABLE `tb_aesbe_uf` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_aesbe_unidade_medida`
--

DROP TABLE IF EXISTS `tb_aesbe_unidade_medida`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_aesbe_unidade_medida` (
  `co_unidade_medida` int(11) NOT NULL AUTO_INCREMENT,
  `no_unidade_medida` varchar(45) NOT NULL,
  PRIMARY KEY (`co_unidade_medida`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_aesbe_unidade_medida`
--

LOCK TABLES `tb_aesbe_unidade_medida` WRITE;
/*!40000 ALTER TABLE `tb_aesbe_unidade_medida` DISABLE KEYS */;
INSERT INTO `tb_aesbe_unidade_medida` VALUES (1,'M'),(2,'M3'),(3,'KG'),(4,'UN');
/*!40000 ALTER TABLE `tb_aesbe_unidade_medida` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-08-05 22:24:24

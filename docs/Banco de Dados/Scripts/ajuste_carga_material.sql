-- carga especificação de classe 
select * from  aesbe.tb_aesbe_esp_classe;

select count(distinct(ds_classe)) from  aesbe.tb_aesbe_carga_material;
insert into aesbe.tb_aesbe_esp_classe  (ds_esp_classe)
select distinct(cm.ds_classe) from  aesbe.tb_aesbe_carga_material cm
where not exists 
(select 1 from aesbe.tb_aesbe_esp_classe m where m.ds_esp_classe = cm.ds_classe);

-- carga especificação de série 
select * from  aesbe.tb_aesbe_esp_serie;

select count(distinct(ds_serie)) from  aesbe.tb_aesbe_carga_material;
insert into aesbe.tb_aesbe_esp_serie  (ds_esp_serie)
select distinct(cm.ds_serie) from  aesbe.tb_aesbe_carga_material cm
where not exists 
(select 1 from aesbe.tb_aesbe_esp_serie m where m.ds_esp_serie = cm.ds_serie);
-- atualização especificação de classe material
begin;
update aesbe.tb_aesbe_material ma
inner join 
(select m.co_material, c.co_esp_classe from  aesbe.tb_aesbe_carga_material cm
inner join aesbe.tb_aesbe_material m on m.nu_sabesp = cm.nu_sabesp
inner join aesbe.tb_aesbe_esp_classe c on c.ds_esp_classe = cm.ds_classe
where m.nu_sabesp is not null) tmp on tmp.co_material = ma.co_material
set ma.co_esp_classe = tmp.co_esp_classe;

commit;

select * from aesbe.tb_aesbe_material ma
inner join 
(select m.co_material, c.co_esp_classe from  aesbe.tb_aesbe_carga_material cm
inner join aesbe.tb_aesbe_material m on m.nu_sabesp = cm.nu_sabesp
inner join aesbe.tb_aesbe_esp_classe c on c.ds_esp_classe = cm.ds_classe
where m.nu_sabesp is not null) tmp on tmp.co_material = ma.co_material
where 1 = 1;





select count(1) from aesbe.tb_aesbe_material where co_esp_classe is null;

-- atualização especificação de serie material
begin;
update aesbe.tb_aesbe_material ma
inner join 
(select m.co_material, c.co_esp_serie from  aesbe.tb_aesbe_carga_material cm
inner join aesbe.tb_aesbe_material m on m.nu_sabesp = cm.nu_sabesp
inner join aesbe.tb_aesbe_esp_serie c on c.ds_esp_serie = cm.ds_serie
where m.nu_sabesp is not null) tmp on tmp.co_material = ma.co_material
set ma.co_esp_serie = tmp.co_esp_serie
where 1 = 1;
commit;
select count(1) from aesbe.tb_aesbe_material where co_esp_serie is not null;


select * from aesbe.tb_aesbe_material where nu_sabesp is null;

update aesbe.tb_aesbe_material
set co_esp_classe = 24
where co_material = 110;
update aesbe.tb_aesbe_material
set co_esp_classe = 25
where co_material = 104;
update aesbe.tb_aesbe_material
set co_esp_classe = 26
where co_material = 108;

select * from aesbe.tb_aesbe_material where co_material in (123,124,125);

update aesbe.tb_aesbe_material
set co_esp_classe = 21
where co_material = 123;
update aesbe.tb_aesbe_material
set co_esp_classe = 21
where co_material = 124;
update aesbe.tb_aesbe_material
set co_esp_classe = 18
where co_material = 125;

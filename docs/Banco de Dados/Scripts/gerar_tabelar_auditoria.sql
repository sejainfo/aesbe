select concat('DELIMITER $$ ', char(13),
'create trigger ', cols.table_schema,
'.tga_', cols.table_name , ' after insert ' , char(13),
       ' on ', cols.table_schema , '.' , cols.table_name , char(13),
       ' FOR EACH ROW ' , char(13),
       ' BEGIN ' , char(13),
       ' insert into aesbe01.' ,  replace(cols.table_name, 'tb_', 'lg_') ,
       ' (tp_operacao_log, dt_operacao_log, no_usuario_banco_log, ' , group_concat(column_name) , ') ', char(13),
       ' values (''I'', sysdate(), session_user(), NEW.' , replace(group_concat(column_name), ',', ',NEW.') , ');', char(13),
       ' END$$', char(13)) tx_sql
 from information_schema.COLUMNS cols
where cols.table_name = 'tb_aesbe_arquivo';

select concat('create table ', 'aesbe01.lg_', replace(cols.table_name, 'tb_', ''),
       ' (co_lg_', replace(cols.table_name, 'tb_', ''), ' bigint auto_increment primary key, ',
       '  tp_operacao_log char(1), ',
       '  dt_operacao_log datetime, ',
       '  no_usuario_banco_log varchar(100), ' , group_concat(coluna), ');') tx_sql
 from (select cols.table_schema, cols.table_name, cols.ordinal_position,
case when cols.data_type in ('varchar', 'char') then
concat(cols.column_name, ' ', cols.data_type, '(', cols.character_maximum_length, ')')
else
concat(cols.column_name, ' ', cols.data_type)
end as coluna
from information_schema.COLUMNS cols
where cols.table_name = 'tb_aesbe_arquivo'
order by ordinal_position) cols
 order by ordinal_position;
select * from aesbe.oauth_users ou
inner join aesbe.tb_aesbe_perfil_oauth_users pou on ou.co_usuario = pou.co_usuario
inner join aesbe.tb_aesbe_perfil_funcionalidade pf on pf.co_perfil = pou.co_perfil
inner join aesbe.tb_aesbe_funcionalidade f on pf.co_funcionalidade = f.co_funcionalidade
inner join aesbe.tb_aesbe_modulo m on m.co_modulo = f.co_modulo
where m.st_ativo = 1 and m.st_modulo_visivel = 1 and 
f.st_ativo = 1;
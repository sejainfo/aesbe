INSERT INTO aesbe.tb_aesbe_uf (CO_UF, SG_UF, NO_UF, CO_REGIAO) VALUES (12, 'AC', 'Acre', 1);
INSERT INTO aesbe.tb_aesbe_uf (CO_UF, SG_UF, NO_UF, CO_REGIAO) VALUES (27, 'AL', 'Alagoas', 2);
INSERT INTO aesbe.tb_aesbe_uf (CO_UF, SG_UF, NO_UF, CO_REGIAO) VALUES (13, 'AM', 'Amazonas', 1);
INSERT INTO aesbe.tb_aesbe_uf (CO_UF, SG_UF, NO_UF, CO_REGIAO) VALUES (16, 'AP', 'Amapá', 1);
INSERT INTO aesbe.tb_aesbe_uf (CO_UF, SG_UF, NO_UF, CO_REGIAO) VALUES (29, 'BA', 'Bahia', 2);
INSERT INTO aesbe.tb_aesbe_uf (CO_UF, SG_UF, NO_UF, CO_REGIAO) VALUES (23, 'CE', 'Ceará', 2);
INSERT INTO aesbe.tb_aesbe_uf (CO_UF, SG_UF, NO_UF, CO_REGIAO) VALUES (53, 'DF', 'Distrito Federal', 3);
INSERT INTO aesbe.tb_aesbe_uf (CO_UF, SG_UF, NO_UF, CO_REGIAO) VALUES (32, 'ES', 'Espírito Santo', 4);
INSERT INTO aesbe.tb_aesbe_uf (CO_UF, SG_UF, NO_UF, CO_REGIAO) VALUES (52, 'GO', 'Goiás', 3);
INSERT INTO aesbe.tb_aesbe_uf (CO_UF, SG_UF, NO_UF, CO_REGIAO) VALUES (21, 'MA', 'Maranhão', 2);
INSERT INTO aesbe.tb_aesbe_uf (CO_UF, SG_UF, NO_UF, CO_REGIAO) VALUES (31, 'MG', 'Minas Gerais', 4);
INSERT INTO aesbe.tb_aesbe_uf (CO_UF, SG_UF, NO_UF, CO_REGIAO) VALUES (50, 'MS', 'Mato Grosso do Sul', 3);
INSERT INTO aesbe.tb_aesbe_uf (CO_UF, SG_UF, NO_UF, CO_REGIAO) VALUES (51, 'MT', 'Mato Grosso', 3);
INSERT INTO aesbe.tb_aesbe_uf (CO_UF, SG_UF, NO_UF, CO_REGIAO) VALUES (15, 'PA', 'Pará', 1);
INSERT INTO aesbe.tb_aesbe_uf (CO_UF, SG_UF, NO_UF, CO_REGIAO) VALUES (25, 'PB', 'Paraíba', 2);
INSERT INTO aesbe.tb_aesbe_uf (CO_UF, SG_UF, NO_UF, CO_REGIAO) VALUES (26, 'PE', 'Pernambuco', 2);
INSERT INTO aesbe.tb_aesbe_uf (CO_UF, SG_UF, NO_UF, CO_REGIAO) VALUES (22, 'PI', 'Piauí', 2);
INSERT INTO aesbe.tb_aesbe_uf (CO_UF, SG_UF, NO_UF, CO_REGIAO) VALUES (41, 'PR', 'Paraná', 5);
INSERT INTO aesbe.tb_aesbe_uf (CO_UF, SG_UF, NO_UF, CO_REGIAO) VALUES (33, 'RJ', 'Rio de Janeiro', 4);
INSERT INTO aesbe.tb_aesbe_uf (CO_UF, SG_UF, NO_UF, CO_REGIAO) VALUES (24, 'RN', 'Rio Grande do Norte', 2);
INSERT INTO aesbe.tb_aesbe_uf (CO_UF, SG_UF, NO_UF, CO_REGIAO) VALUES (11, 'RO', 'Rondônia', 1);
INSERT INTO aesbe.tb_aesbe_uf (CO_UF, SG_UF, NO_UF, CO_REGIAO) VALUES (14, 'RR', 'Roraima', 1);
INSERT INTO aesbe.tb_aesbe_uf (CO_UF, SG_UF, NO_UF, CO_REGIAO) VALUES (43, 'RS', 'Rio Grande do Sul', 5);
INSERT INTO aesbe.tb_aesbe_uf (CO_UF, SG_UF, NO_UF, CO_REGIAO) VALUES (42, 'SC', 'Santa Catarina', 5);
INSERT INTO aesbe.tb_aesbe_uf (CO_UF, SG_UF, NO_UF, CO_REGIAO) VALUES (28, 'SE', 'Sergipe', 2);
INSERT INTO aesbe.tb_aesbe_uf (CO_UF, SG_UF, NO_UF, CO_REGIAO) VALUES (35, 'SP', 'São Paulo', 4);
INSERT INTO aesbe.tb_aesbe_uf (CO_UF, SG_UF, NO_UF, CO_REGIAO) VALUES (17, 'TO', 'Tocantins', 1);

CREATE DATABASE  IF NOT EXISTS `db_aesbe` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `db_aesbe`;
-- MySQL dump 10.13  Distrib 5.5.49, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: db_aesbe
-- ------------------------------------------------------
-- Server version	5.5.49-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `oauth_access_tokens`
--

DROP TABLE IF EXISTS `oauth_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_access_tokens` (
  `access_token` varchar(40) NOT NULL,
  `client_id` varchar(80) NOT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `expires` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `scope` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`access_token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_access_tokens`
--

LOCK TABLES `oauth_access_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_access_tokens` DISABLE KEYS */;
INSERT INTO `oauth_access_tokens` VALUES ('2f680516e6d539d3c392f0a7750b7ea7d9d52901','app_aesbe','geccal','2016-07-08 04:50:46',NULL),('6a0c0c4d4ecec3082a3c2da74b97f4cf5559d3fa','app_aesbe','geccal','2016-07-08 04:28:30',NULL),('6f434d4ce28622e1a6cc92a1844446544a1b0704','app_aesbe','geccal','2016-07-08 04:31:06',NULL),('866bf05009e9c117aa683f5262ab1688ece1f1b9','app_aesbe','geccal','2016-07-08 04:29:03',NULL);
/*!40000 ALTER TABLE `oauth_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_authorization_codes`
--

DROP TABLE IF EXISTS `oauth_authorization_codes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_authorization_codes` (
  `authorization_code` varchar(40) NOT NULL,
  `client_id` varchar(80) NOT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `redirect_uri` varchar(2000) DEFAULT NULL,
  `expires` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `scope` varchar(2000) DEFAULT NULL,
  `id_token` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`authorization_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_authorization_codes`
--

LOCK TABLES `oauth_authorization_codes` WRITE;
/*!40000 ALTER TABLE `oauth_authorization_codes` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_authorization_codes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_clients`
--

DROP TABLE IF EXISTS `oauth_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_clients` (
  `client_id` varchar(80) NOT NULL,
  `client_secret` varchar(80) NOT NULL,
  `redirect_uri` varchar(2000) NOT NULL,
  `grant_types` varchar(80) DEFAULT NULL,
  `scope` varchar(2000) DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_clients`
--

LOCK TABLES `oauth_clients` WRITE;
/*!40000 ALTER TABLE `oauth_clients` DISABLE KEYS */;
INSERT INTO `oauth_clients` VALUES ('app_aesbe','$2y$10$GHvujwPa2wdrRe.J970LBO7pyzuWUA6yOfR/iWUvY5Jxxoc1.J6PS','localhost',NULL,NULL,NULL);
/*!40000 ALTER TABLE `oauth_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_jwt`
--

DROP TABLE IF EXISTS `oauth_jwt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_jwt` (
  `client_id` varchar(80) NOT NULL,
  `subject` varchar(80) DEFAULT NULL,
  `public_key` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_jwt`
--

LOCK TABLES `oauth_jwt` WRITE;
/*!40000 ALTER TABLE `oauth_jwt` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_jwt` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_refresh_tokens`
--

DROP TABLE IF EXISTS `oauth_refresh_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_refresh_tokens` (
  `refresh_token` varchar(40) NOT NULL,
  `client_id` varchar(80) NOT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `expires` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `scope` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`refresh_token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_refresh_tokens`
--

LOCK TABLES `oauth_refresh_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_refresh_tokens` DISABLE KEYS */;
INSERT INTO `oauth_refresh_tokens` VALUES ('2cf64798b050cc630ca4a1cad2e2ed06a018a679','app_aesbe','geccal','2016-07-22 03:29:03',NULL),('31f2f7febe52a01adc7843e946c719fedfb7a587','app_aesbe','geccal','2016-07-22 03:28:30',NULL),('417df7dc4b120dd96de21aa75fc936f1148512a0','app_aesbe','geccal','2016-07-22 03:31:07',NULL),('c07ae5377ee2db482be9e2e27d207b24b1a113bc','app_aesbe','geccal','2016-07-22 03:50:46',NULL);
/*!40000 ALTER TABLE `oauth_refresh_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_scopes`
--

DROP TABLE IF EXISTS `oauth_scopes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_scopes` (
  `type` varchar(255) NOT NULL DEFAULT 'supported',
  `scope` varchar(2000) DEFAULT NULL,
  `client_id` varchar(80) DEFAULT NULL,
  `is_default` smallint(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_scopes`
--

LOCK TABLES `oauth_scopes` WRITE;
/*!40000 ALTER TABLE `oauth_scopes` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_scopes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_users`
--

DROP TABLE IF EXISTS `oauth_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_users` (
  `co_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `dt_cadastro` date NOT NULL,
  `username` varchar(45) NOT NULL,
  `password` varchar(100) NOT NULL,
  `dt_ultima_visita` date DEFAULT NULL,
  `co_pessoa` int(11) DEFAULT NULL,
  PRIMARY KEY (`co_usuario`),
  KEY `fk_oauth_users_tb_aesbe_pessoa_fisica1_idx` (`co_pessoa`),
  CONSTRAINT `fk_oauth_users_tb_aesbe_pessoa_fisica1` FOREIGN KEY (`co_pessoa`) REFERENCES `tb_aesbe_pessoa_fisica` (`co_pessoa`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_users`
--

LOCK TABLES `oauth_users` WRITE;
/*!40000 ALTER TABLE `oauth_users` DISABLE KEYS */;
INSERT INTO `oauth_users` VALUES (1,'2016-07-08','geccal','$2y$10$clVvZqxXRKT0Xjj4m70K9uvkNjJjkIvG9CEB.jJFZBCc1JrXIoIwG',NULL,NULL);
/*!40000 ALTER TABLE `oauth_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_aesbe_empresa`
--

DROP TABLE IF EXISTS `tb_aesbe_empresa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_aesbe_empresa` (
  `co_empresa` int(11) NOT NULL AUTO_INCREMENT,
  `no_empresa` varchar(100) NOT NULL,
  `sg_empresa` varchar(45) DEFAULT NULL,
  `ds_sitio` varchar(100) DEFAULT NULL,
  `nu_cep` varchar(10) DEFAULT NULL,
  `ds_logradouro` varchar(100) DEFAULT NULL,
  `no_bairro` varchar(100) DEFAULT NULL,
  `nu_logradouro` varchar(10) DEFAULT NULL,
  `ds_complemento` varchar(30) DEFAULT NULL,
  `co_uf` int(11) NOT NULL,
  PRIMARY KEY (`co_empresa`),
  KEY `fk_tb_aesbe_empresa_tb_aesbe_uf1_idx` (`co_uf`),
  CONSTRAINT `fk_tb_aesbe_empresa_tb_aesbe_uf1` FOREIGN KEY (`co_uf`) REFERENCES `tb_aesbe_uf` (`co_uf`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_aesbe_empresa`
--

LOCK TABLES `tb_aesbe_empresa` WRITE;
/*!40000 ALTER TABLE `tb_aesbe_empresa` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_aesbe_empresa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_aesbe_perfil`
--

DROP TABLE IF EXISTS `tb_aesbe_perfil`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_aesbe_perfil` (
  `co_perfil` int(11) NOT NULL AUTO_INCREMENT,
  `no_perfil` varchar(45) NOT NULL,
  `sg_perfil` varchar(45) NOT NULL,
  PRIMARY KEY (`co_perfil`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_aesbe_perfil`
--

LOCK TABLES `tb_aesbe_perfil` WRITE;
/*!40000 ALTER TABLE `tb_aesbe_perfil` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_aesbe_perfil` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_aesbe_perfil_oauth_users`
--

DROP TABLE IF EXISTS `tb_aesbe_perfil_oauth_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_aesbe_perfil_oauth_users` (
  `tb_aesbe_perfil_co_perfil` int(11) NOT NULL,
  `oauth_users_co_usuario` int(11) NOT NULL,
  PRIMARY KEY (`tb_aesbe_perfil_co_perfil`,`oauth_users_co_usuario`),
  KEY `fk_tb_aesbe_perfil_has_oauth_users_oauth_users1_idx` (`oauth_users_co_usuario`),
  KEY `fk_tb_aesbe_perfil_has_oauth_users_tb_aesbe_perfil1_idx` (`tb_aesbe_perfil_co_perfil`),
  CONSTRAINT `fk_tb_aesbe_perfil_has_oauth_users_tb_aesbe_perfil1` FOREIGN KEY (`tb_aesbe_perfil_co_perfil`) REFERENCES `tb_aesbe_perfil` (`co_perfil`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tb_aesbe_perfil_has_oauth_users_oauth_users1` FOREIGN KEY (`oauth_users_co_usuario`) REFERENCES `oauth_users` (`co_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_aesbe_perfil_oauth_users`
--

LOCK TABLES `tb_aesbe_perfil_oauth_users` WRITE;
/*!40000 ALTER TABLE `tb_aesbe_perfil_oauth_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_aesbe_perfil_oauth_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_aesbe_pessoa_fisica`
--

DROP TABLE IF EXISTS `tb_aesbe_pessoa_fisica`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_aesbe_pessoa_fisica` (
  `co_pessoa` int(11) NOT NULL AUTO_INCREMENT,
  `no_pessoa` varchar(100) NOT NULL,
  `ds_email` varchar(100) NOT NULL,
  `ds_email_secundario` varchar(100) DEFAULT NULL,
  `nu_tel_residencial` varchar(10) DEFAULT NULL,
  `nu_tel_comercial` varchar(10) DEFAULT NULL,
  `nu_celular` varchar(10) DEFAULT NULL,
  `st_presidente` tinyint(1) NOT NULL DEFAULT '0',
  `nu_cep` varchar(10) DEFAULT NULL,
  `ds_logradouro` varchar(100) DEFAULT NULL,
  `no_bairro` varchar(100) DEFAULT NULL,
  `nu_logradouro` varchar(10) DEFAULT NULL,
  `ds_complemento` varchar(30) DEFAULT NULL,
  `co_uf` int(11) NOT NULL,
  `st_ativo` tinyint(1) NOT NULL,
  PRIMARY KEY (`co_pessoa`),
  KEY `fk_tb_pessoa_fisica_tb_aesbe_uf1_idx` (`co_uf`),
  CONSTRAINT `fk_tb_pessoa_fisica_tb_aesbe_uf1` FOREIGN KEY (`co_uf`) REFERENCES `tb_aesbe_uf` (`co_uf`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_aesbe_pessoa_fisica`
--

LOCK TABLES `tb_aesbe_pessoa_fisica` WRITE;
/*!40000 ALTER TABLE `tb_aesbe_pessoa_fisica` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_aesbe_pessoa_fisica` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_aesbe_regiao`
--

DROP TABLE IF EXISTS `tb_aesbe_regiao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_aesbe_regiao` (
  `co_regiao` int(11) NOT NULL AUTO_INCREMENT,
  `no_regiao` varchar(45) NOT NULL,
  `nu_ordem_apresentacao` int(11) NOT NULL,
  PRIMARY KEY (`co_regiao`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_aesbe_regiao`
--

LOCK TABLES `tb_aesbe_regiao` WRITE;
/*!40000 ALTER TABLE `tb_aesbe_regiao` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_aesbe_regiao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_aesbe_uf`
--

DROP TABLE IF EXISTS `tb_aesbe_uf`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_aesbe_uf` (
  `co_uf` int(11) NOT NULL AUTO_INCREMENT,
  `no_uf` varchar(45) NOT NULL,
  `co_regiao` int(11) NOT NULL,
  PRIMARY KEY (`co_uf`),
  KEY `fk_tb_aesbe_uf_tb_aesbe_regiao_idx` (`co_regiao`),
  CONSTRAINT `fk_tb_aesbe_uf_tb_aesbe_regiao` FOREIGN KEY (`co_regiao`) REFERENCES `tb_aesbe_regiao` (`co_regiao`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_aesbe_uf`
--

LOCK TABLES `tb_aesbe_uf` WRITE;
/*!40000 ALTER TABLE `tb_aesbe_uf` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_aesbe_uf` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-07-08  1:20:04

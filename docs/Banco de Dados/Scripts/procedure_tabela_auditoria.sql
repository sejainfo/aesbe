-- SELECT  column_name, data_type, data_length, data_precision, data_scale, nullable 
DROP PROCEDURE `TUTORIAL_CURSOR`.`teste`;
DELIMITER $$
CREATE PROCEDURE TUTORIAL_CURSOR.teste ()
BEGIN
-- VARIÁVEIS NECESSÁRIAS 
DECLARE P_TABLE  VARCHAR(100) DEFAULT 'pagamento';
DECLARE cOwnerDestino  VARCHAR(100) DEFAULT 'db_aesbe_auditoria';
DECLARE vNomeTable VARCHAR(50) DEFAULT CONCAT('lg_', P_TABLE);
DECLARE vScriptTable VARCHAR(10000) DEFAULT '';
DECLARE C_COLUMNS_REC INT DEFAULT 0;
-- -- CRIANDO O CURSOR 
DECLARE C_COLUMNS CURSOR FOR

SELECT  column_name, data_type, CHARACTER_MAXIMUM_LENGTH, NUMERIC_PRECISION, NUMERIC_SCALE, IS_NULLABLE
-- SELECT  column_name
FROM    information_schema.columns
WHERE   1 = 1 
AND table_schema       = 'db_aesbe'
AND table_name  = 'tb_aesbe_descricao'
ORDER BY COLUMN_KEY; 
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;
SET vScriptTable = concat('CREATE TABLE ',cOwnerDestino,'.',vNomeTable,'(CHR(13)'); 
--   -- Abertura do cursor
OPEN C_COLUMNS;
-- -- loop
read_loop: LOOP
FETCH C_COLUMNS INTO C_COLUMNS_REC;
-- SET vScriptTable = vScriptTable || '    ' || C_COLUMNS_REC.column_name  || ' '; 
-- call log_msg(vScriptTable);
END LOOP;

  END $$
DELIMITER ;

-- call TUTORIAL_CURSOR.teste();
CALL `TUTORIAL_CURSOR`.`teste`();

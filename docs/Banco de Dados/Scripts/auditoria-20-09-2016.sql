-- auditoria oauth_users
create table aesbe.lg_oauth_users (co_lg_oauth_users bigint auto_increment primary key,   tp_operacao_log char(1),   dt_operacao_log datetime,   no_usuario_banco_log varchar(100), co_usuario int,dt_cadastro date,username varchar(45),password varchar(100),dt_ultima_visita datetime,co_pessoa int);

DELIMITER $$ 
create trigger aesbe.tga_oauth_users_ins after insert 
 on aesbe.oauth_users
 FOR EACH ROW 
 BEGIN 
 insert into aesbe.lg_oauth_users (tp_operacao_log, dt_operacao_log, no_usuario_banco_log, co_usuario,dt_cadastro,username,password,dt_ultima_visita,co_pessoa) 
 values ('I', sysdate(), session_user(), NEW.co_usuario,NEW.dt_cadastro,NEW.username,NEW.password,NEW.dt_ultima_visita,NEW.co_pessoa);
 END$$

 DELIMITER $$ 
create trigger aesbe.tga_oauth_users_upd after update 
 on aesbe.oauth_users
 FOR EACH ROW 
 BEGIN 
 insert into aesbe.lg_oauth_users (tp_operacao_log, dt_operacao_log, no_usuario_banco_log, co_usuario,dt_cadastro,username,password,dt_ultima_visita,co_pessoa) 
 values ('U', sysdate(), session_user(), NEW.co_usuario,NEW.dt_cadastro,NEW.username,NEW.password,NEW.dt_ultima_visita,NEW.co_pessoa);
 END$$

 DELIMITER $$ 
create trigger aesbe.tga_oauth_users_del after delete 
 on aesbe.oauth_users
 FOR EACH ROW 
 BEGIN 
 insert into aesbe.lg_oauth_users (tp_operacao_log, dt_operacao_log, no_usuario_banco_log, co_usuario,dt_cadastro,username,password,dt_ultima_visita,co_pessoa) 
 values ('D', sysdate(), session_user(), OLD.co_usuario,OLD.dt_cadastro,OLD.username,OLD.password,OLD.dt_ultima_visita,OLD.co_pessoa);
 END$$

-- auditoria tb_aesbe_empresa
 create table aesbe.lg_aesbe_empresa (co_lg_aesbe_empresa bigint auto_increment primary key,   tp_operacao_log char(1),   dt_operacao_log datetime,   no_usuario_banco_log varchar(100), co_empresa int,no_empresa varchar(100),sg_empresa varchar(45),ds_sitio varchar(100),nu_cep varchar(10),ds_logradouro varchar(100),no_bairro varchar(100),nu_logradouro varchar(10),ds_complemento varchar(30),co_uf int,nu_telefone varchar(10),nu_telefone_secundario varchar(10));

 	DELIMITER $$ 
create trigger aesbe.tga_tb_aesbe_empresa_ins after insert 
 on aesbe.tb_aesbe_empresa
 FOR EACH ROW 
 BEGIN 
 insert into aesbe.lg_aesbe_empresa (tp_operacao_log, dt_operacao_log, no_usuario_banco_log, co_empresa,no_empresa,sg_empresa,ds_sitio,nu_cep,ds_logradouro,no_bairro,nu_logradouro,ds_complemento,co_uf,nu_telefone,nu_telefone_secundario) 
 values ('I', sysdate(), session_user(), NEW.co_empresa,NEW.no_empresa,NEW.sg_empresa,NEW.ds_sitio,NEW.nu_cep,NEW.ds_logradouro,NEW.no_bairro,NEW.nu_logradouro,NEW.ds_complemento,NEW.co_uf,NEW.nu_telefone,NEW.nu_telefone_secundario);
 END$$

 DELIMITER $$ 
create trigger aesbe.tga_tb_aesbe_empresa_upd after update 
 on aesbe.tb_aesbe_empresa
 FOR EACH ROW 
 BEGIN 
 insert into aesbe.lg_aesbe_empresa (tp_operacao_log, dt_operacao_log, no_usuario_banco_log, co_empresa,no_empresa,sg_empresa,ds_sitio,nu_cep,ds_logradouro,no_bairro,nu_logradouro,ds_complemento,co_uf,nu_telefone,nu_telefone_secundario) 
 values ('U', sysdate(), session_user(), NEW.co_empresa,NEW.no_empresa,NEW.sg_empresa,NEW.ds_sitio,NEW.nu_cep,NEW.ds_logradouro,NEW.no_bairro,NEW.nu_logradouro,NEW.ds_complemento,NEW.co_uf,NEW.nu_telefone,NEW.nu_telefone_secundario);
 END$$

 DELIMITER $$ 
create trigger aesbe.tga_tb_aesbe_empresa_del after delete 
 on aesbe.tb_aesbe_empresa
 FOR EACH ROW 
 BEGIN 
 insert into aesbe.lg_aesbe_empresa (tp_operacao_log, dt_operacao_log, no_usuario_banco_log, co_empresa,no_empresa,sg_empresa,ds_sitio,nu_cep,ds_logradouro,no_bairro,nu_logradouro,ds_complemento,co_uf,nu_telefone,nu_telefone_secundario) 
 values ('D', sysdate(), session_user(), OLD.co_empresa,OLD.no_empresa,OLD.sg_empresa,OLD.ds_sitio,OLD.nu_cep,OLD.ds_logradouro,OLD.no_bairro,OLD.nu_logradouro,OLD.ds_complemento,OLD.co_uf,OLD.nu_telefone,OLD.nu_telefone_secundario);
 END$$

-- auditoria tb_aesbe_arquivo
create table aesbe.lg_aesbe_arquivo (co_lg_aesbe_arquivo bigint auto_increment primary key,   tp_operacao_log char(1),   dt_operacao_log datetime,   no_usuario_banco_log varchar(100), co_arquivo int,ds_arquivo varchar(200),co_tipo_arquivo int);

	DELIMITER $$ 
create trigger aesbe.tga_tb_aesbe_arquivo_ins after insert 
 on aesbe.tb_aesbe_arquivo
 FOR EACH ROW 
 BEGIN 
 insert into aesbe.lg_aesbe_arquivo (tp_operacao_log, dt_operacao_log, no_usuario_banco_log, co_arquivo,ds_arquivo,co_tipo_arquivo) 
 values ('I', sysdate(), session_user(), NEW.co_arquivo,NEW.ds_arquivo,NEW.co_tipo_arquivo);
 END$$

 DELIMITER $$ 
create trigger aesbe.tga_tb_aesbe_arquivo_upd after update 
 on aesbe.tb_aesbe_arquivo
 FOR EACH ROW 
 BEGIN 
 insert into aesbe.lg_aesbe_arquivo (tp_operacao_log, dt_operacao_log, no_usuario_banco_log, co_arquivo,ds_arquivo,co_tipo_arquivo) 
 values ('U', sysdate(), session_user(), NEW.co_arquivo,NEW.ds_arquivo,NEW.co_tipo_arquivo);
 END$$

DELIMITER $$ 
create trigger aesbe.tga_tb_aesbe_arquivo_del after delete 
 on aesbe.tb_aesbe_arquivo
 FOR EACH ROW 
 BEGIN 
 insert into aesbe.lg_aesbe_arquivo (tp_operacao_log, dt_operacao_log, no_usuario_banco_log, co_arquivo,ds_arquivo,co_tipo_arquivo) 
 values ('D', sysdate(), session_user(), OLD.co_arquivo,OLD.ds_arquivo,OLD.co_tipo_arquivo);
 END$$

 -- auditoria tb_aesbe_arquivo_proposta
 create table aesbe.lg_aesbe_arquivo_proposta (co_lg_aesbe_arquivo_proposta bigint auto_increment primary key,   tp_operacao_log char(1),   dt_operacao_log datetime,   no_usuario_banco_log varchar(100), co_arquivo_proposta int,dt_processamento datetime,qt_erros int,qt_registros_processados int,st_processamento tinyint,co_usuario int,co_proposta int,co_arquivo int);

 	DELIMITER $$ 
create trigger aesbe.tga_tb_aesbe_arquivo_proposta_ins after insert 
 on aesbe.tb_aesbe_arquivo_proposta
 FOR EACH ROW 
 BEGIN 
 insert into aesbe.lg_aesbe_arquivo_proposta (tp_operacao_log, dt_operacao_log, no_usuario_banco_log, co_arquivo_proposta,dt_processamento,qt_erros,qt_registros_processados,st_processamento,co_usuario,co_proposta,co_arquivo) 
 values ('I', sysdate(), session_user(), NEW.co_arquivo_proposta,NEW.dt_processamento,NEW.qt_erros,NEW.qt_registros_processados,NEW.st_processamento,NEW.co_usuario,NEW.co_proposta,NEW.co_arquivo);
 END$$

 DELIMITER $$ 
create trigger aesbe.tga_tb_aesbe_arquivo_proposta_upd after update 
 on aesbe.tb_aesbe_arquivo_proposta
 FOR EACH ROW 
 BEGIN 
 insert into aesbe.lg_aesbe_arquivo_proposta (tp_operacao_log, dt_operacao_log, no_usuario_banco_log, co_arquivo_proposta,dt_processamento,qt_erros,qt_registros_processados,st_processamento,co_usuario,co_proposta,co_arquivo) 
 values ('U', sysdate(), session_user(), NEW.co_arquivo_proposta,NEW.dt_processamento,NEW.qt_erros,NEW.qt_registros_processados,NEW.st_processamento,NEW.co_usuario,NEW.co_proposta,NEW.co_arquivo);
 END$$

DELIMITER $$ 
create trigger aesbe.tga_tb_aesbe_arquivo_proposta_del after delete 
 on aesbe.tb_aesbe_arquivo_proposta
 FOR EACH ROW 
 BEGIN 
 insert into aesbe.lg_aesbe_arquivo_proposta (tp_operacao_log, dt_operacao_log, no_usuario_banco_log, co_arquivo_proposta,dt_processamento,qt_erros,qt_registros_processados,st_processamento,co_usuario,co_proposta,co_arquivo) 
 values ('D', sysdate(), session_user(), OLD.co_arquivo_proposta,OLD.dt_processamento,OLD.qt_erros,OLD.qt_registros_processados,OLD.st_processamento,OLD.co_usuario,OLD.co_proposta,OLD.co_arquivo);
 END$$

 -- auditoria tb_aesbe_desricao
 create table aesbe.lg_aesbe_descricao (co_lg_aesbe_descricao bigint auto_increment primary key,   tp_operacao_log char(1),   dt_operacao_log datetime,   no_usuario_banco_log varchar(100), co_descricao int,no_descricao text);

DELIMITER $$ 
create trigger aesbe.tga_tb_aesbe_descricao_ins after insert 
 on aesbe.tb_aesbe_descricao
 FOR EACH ROW 
 BEGIN 
 insert into aesbe.lg_aesbe_descricao (tp_operacao_log, dt_operacao_log, no_usuario_banco_log, co_descricao,no_descricao) 
 values ('I', sysdate(), session_user(), NEW.co_descricao,NEW.no_descricao);
 END$$

DELIMITER $$ 
create trigger aesbe.tga_tb_aesbe_descricao_upd after update 
 on aesbe.tb_aesbe_descricao
 FOR EACH ROW 
 BEGIN 
 insert into aesbe.lg_aesbe_descricao (tp_operacao_log, dt_operacao_log, no_usuario_banco_log, co_descricao,no_descricao) 
 values ('U', sysdate(), session_user(), NEW.co_descricao,NEW.no_descricao);
 END$$

 DELIMITER $$ 
create trigger aesbe.tga_tb_aesbe_descricao_del after delete 
 on aesbe.tb_aesbe_descricao
 FOR EACH ROW 
 BEGIN 
 insert into aesbe.lg_aesbe_descricao (tp_operacao_log, dt_operacao_log, no_usuario_banco_log, co_descricao,no_descricao) 
 values ('D', sysdate(), session_user(), OLD.co_descricao,OLD.no_descricao);
 END$$

-- auditoria tb_aesbe_esp_classe
 create table aesbe.lg_aesbe_esp_classe (co_lg_aesbe_esp_classe bigint auto_increment primary key,   tp_operacao_log char(1),   dt_operacao_log datetime,   no_usuario_banco_log varchar(100), co_esp_classe int,ds_esp_classe text);


 	DELIMITER $$ 
create trigger aesbe.tga_tb_aesbe_esp_classe_ins after insert 
 on aesbe.tb_aesbe_esp_classe
 FOR EACH ROW 
 BEGIN 
 insert into aesbe.lg_aesbe_esp_classe (tp_operacao_log, dt_operacao_log, no_usuario_banco_log, co_esp_classe,ds_esp_classe) 
 values ('I', sysdate(), session_user(), NEW.co_esp_classe,NEW.ds_esp_classe);
 END$$	

 DELIMITER $$ 
create trigger aesbe.tga_tb_aesbe_esp_classe_upd after update 
 on aesbe.tb_aesbe_esp_classe
 FOR EACH ROW 
 BEGIN 
 insert into aesbe.lg_aesbe_esp_classe (tp_operacao_log, dt_operacao_log, no_usuario_banco_log, co_esp_classe,ds_esp_classe) 
 values ('U', sysdate(), session_user(), NEW.co_esp_classe,NEW.ds_esp_classe);
 END$$

 DELIMITER $$ 
create trigger aesbe.tga_tb_aesbe_esp_classe_del after delete 
 on aesbe.tb_aesbe_esp_classe
 FOR EACH ROW 
 BEGIN 
 insert into aesbe.lg_aesbe_esp_classe (tp_operacao_log, dt_operacao_log, no_usuario_banco_log, co_esp_classe,ds_esp_classe) 
 values ('D', sysdate(), session_user(), OLD.co_esp_classe,OLD.ds_esp_classe);
 END$$

 -- auditoria tb_aesbe_esp_serie
 create table aesbe.lg_aesbe_esp_serie (co_lg_aesbe_esp_serie bigint auto_increment primary key,   tp_operacao_log char(1),   dt_operacao_log datetime,   no_usuario_banco_log varchar(100), co_esp_serie int,ds_esp_serie text);

 	DELIMITER $$ 
create trigger aesbe.tga_tb_aesbe_esp_serie_ins after insert 
 on aesbe.tb_aesbe_esp_serie
 FOR EACH ROW 
 BEGIN 
 insert into aesbe.lg_aesbe_esp_serie (tp_operacao_log, dt_operacao_log, no_usuario_banco_log, co_esp_serie,ds_esp_serie) 
 values ('I', sysdate(), session_user(), NEW.co_esp_serie,NEW.ds_esp_serie);
 END$$

DELIMITER $$ 
create trigger aesbe.tga_tb_aesbe_esp_serie_upd after update 
 on aesbe.tb_aesbe_esp_serie
 FOR EACH ROW 
 BEGIN 
 insert into aesbe.lg_aesbe_esp_serie (tp_operacao_log, dt_operacao_log, no_usuario_banco_log, co_esp_serie,ds_esp_serie) 
 values ('U', sysdate(), session_user(), NEW.co_esp_serie,NEW.ds_esp_serie);
 END$$

 DELIMITER $$ 
create trigger aesbe.tga_tb_aesbe_esp_serie_del after delete 
 on aesbe.tb_aesbe_esp_serie
 FOR EACH ROW 
 BEGIN 
 insert into aesbe.lg_aesbe_esp_serie (tp_operacao_log, dt_operacao_log, no_usuario_banco_log, co_esp_serie,ds_esp_serie) 
 values ('D', sysdate(), session_user(), OLD.co_esp_serie,OLD.ds_esp_serie);
 END$$

 -- auditoria tb_aesbe_fornecedor
 create table aesbe.lg_aesbe_fornecedor (co_lg_aesbe_fornecedor bigint auto_increment primary key,   tp_operacao_log char(1),   dt_operacao_log datetime,   no_usuario_banco_log varchar(100), co_fornecedor int,no_fornecedor varchar(200),nu_cnpj char(14),no_representante varchar(100));

 	DELIMITER $$ 
create trigger aesbe.tga_tb_aesbe_fornecedor_ins after insert 
 on aesbe.tb_aesbe_fornecedor
 FOR EACH ROW 
 BEGIN 
 insert into aesbe.lg_aesbe_fornecedor (tp_operacao_log, dt_operacao_log, no_usuario_banco_log, co_fornecedor,no_fornecedor,nu_cnpj,no_representante) 
 values ('I', sysdate(), session_user(), NEW.co_fornecedor,NEW.no_fornecedor,NEW.nu_cnpj,NEW.no_representante);
 END$$

 DELIMITER $$ 
create trigger aesbe.tga_tb_aesbe_fornecedor_upd after update 
 on aesbe.tb_aesbe_fornecedor
 FOR EACH ROW 
 BEGIN 
 insert into aesbe.lg_aesbe_fornecedor (tp_operacao_log, dt_operacao_log, no_usuario_banco_log, co_fornecedor,no_fornecedor,nu_cnpj,no_representante) 
 values ('U', sysdate(), session_user(), NEW.co_fornecedor,NEW.no_fornecedor,NEW.nu_cnpj,NEW.no_representante);
 END$$

 DELIMITER $$ 
create trigger aesbe.tga_tb_aesbe_fornecedor_del after delete 
 on aesbe.tb_aesbe_fornecedor
 FOR EACH ROW 
 BEGIN 
 insert into aesbe.lg_aesbe_fornecedor (tp_operacao_log, dt_operacao_log, no_usuario_banco_log, co_fornecedor,no_fornecedor,nu_cnpj,no_representante) 
 values ('D', sysdate(), session_user(), OLD.co_fornecedor,OLD.no_fornecedor,OLD.nu_cnpj,OLD.no_representante);
 END$$


-- auditoria tb_aesbe_funcionalidade
create table aesbe.lg_aesbe_funcionalidade (co_lg_aesbe_funcionalidade bigint auto_increment primary key,   tp_operacao_log char(1),   dt_operacao_log datetime,   no_usuario_banco_log varchar(100), co_funcionalidade int,no_funcionalidade varchar(45),no_funcionalidade_visivel varchar(45),st_funcionalidade_visivel tinyint,st_ativo tinyint,ds_metodo_http varchar(45),co_modulo int,ds_controller varchar(45),no_agrupador varchar(45));

	DELIMITER $$ 
create trigger aesbe.tga_tb_aesbe_funcionalidade_ins after insert 
 on aesbe.tb_aesbe_funcionalidade
 FOR EACH ROW 
 BEGIN 
 insert into aesbe.lg_aesbe_funcionalidade (tp_operacao_log, dt_operacao_log, no_usuario_banco_log, co_funcionalidade,no_funcionalidade,no_funcionalidade_visivel,st_funcionalidade_visivel,st_ativo,ds_metodo_http,co_modulo,ds_controller,no_agrupador) 
 values ('I', sysdate(), session_user(), NEW.co_funcionalidade,NEW.no_funcionalidade,NEW.no_funcionalidade_visivel,NEW.st_funcionalidade_visivel,NEW.st_ativo,NEW.ds_metodo_http,NEW.co_modulo,NEW.ds_controller,NEW.no_agrupador);
 END$$

 DELIMITER $$ 
create trigger aesbe.tga_tb_aesbe_funcionalidade_upd after update 
 on aesbe.tb_aesbe_funcionalidade
 FOR EACH ROW 
 BEGIN 
 insert into aesbe.lg_aesbe_funcionalidade (tp_operacao_log, dt_operacao_log, no_usuario_banco_log, co_funcionalidade,no_funcionalidade,no_funcionalidade_visivel,st_funcionalidade_visivel,st_ativo,ds_metodo_http,co_modulo,ds_controller,no_agrupador) 
 values ('U', sysdate(), session_user(), NEW.co_funcionalidade,NEW.no_funcionalidade,NEW.no_funcionalidade_visivel,NEW.st_funcionalidade_visivel,NEW.st_ativo,NEW.ds_metodo_http,NEW.co_modulo,NEW.ds_controller,NEW.no_agrupador);
 END$$

 DELIMITER $$ 
create trigger aesbe.tga_tb_aesbe_funcionalidade_del after delete 
 on aesbe.tb_aesbe_funcionalidade
 FOR EACH ROW 
 BEGIN 
 insert into aesbe.lg_aesbe_funcionalidade (tp_operacao_log, dt_operacao_log, no_usuario_banco_log, co_funcionalidade,no_funcionalidade,no_funcionalidade_visivel,st_funcionalidade_visivel,st_ativo,ds_metodo_http,co_modulo,ds_controller,no_agrupador) 
 values ('D', sysdate(), session_user(), OLD.co_funcionalidade,OLD.no_funcionalidade,OLD.no_funcionalidade_visivel,OLD.st_funcionalidade_visivel,OLD.st_ativo,OLD.ds_metodo_http,OLD.co_modulo,OLD.ds_controller,OLD.no_agrupador);
 END$$

 -- auditoria tb_aesbe_material
 create table aesbe.lg_aesbe_material (co_lg_aesbe_material bigint auto_increment primary key,   tp_operacao_log char(1),   dt_operacao_log datetime,   no_usuario_banco_log varchar(100), co_material int,co_unidade_medida int,no_basico varchar(100),nu_sabesp double,co_esp_serie int,co_esp_classe int,co_descricao int);

 	DELIMITER $$ 
create trigger aesbe.tga_tb_aesbe_material_ins after insert 
 on aesbe.tb_aesbe_material
 FOR EACH ROW 
 BEGIN 
 insert into aesbe.lg_aesbe_material (tp_operacao_log, dt_operacao_log, no_usuario_banco_log, co_material,co_unidade_medida,no_basico,nu_sabesp,co_esp_serie,co_esp_classe,co_descricao) 
 values ('I', sysdate(), session_user(), NEW.co_material,NEW.co_unidade_medida,NEW.no_basico,NEW.nu_sabesp,NEW.co_esp_serie,NEW.co_esp_classe,NEW.co_descricao);
 END$$

 DELIMITER $$ 
create trigger aesbe.tga_tb_aesbe_material_upd after update 
 on aesbe.tb_aesbe_material
 FOR EACH ROW 
 BEGIN 
 insert into aesbe.lg_aesbe_material (tp_operacao_log, dt_operacao_log, no_usuario_banco_log, co_material,co_unidade_medida,no_basico,nu_sabesp,co_esp_serie,co_esp_classe,co_descricao) 
 values ('U', sysdate(), session_user(), NEW.co_material,NEW.co_unidade_medida,NEW.no_basico,NEW.nu_sabesp,NEW.co_esp_serie,NEW.co_esp_classe,NEW.co_descricao);
 END$$

 DELIMITER $$ 
create trigger aesbe.tga_tb_aesbe_material_del after delete 
 on aesbe.tb_aesbe_material
 FOR EACH ROW 
 BEGIN 
 insert into aesbe.lg_aesbe_material (tp_operacao_log, dt_operacao_log, no_usuario_banco_log, co_material,co_unidade_medida,no_basico,nu_sabesp,co_esp_serie,co_esp_classe,co_descricao) 
 values ('D', sysdate(), session_user(), OLD.co_material,OLD.co_unidade_medida,OLD.no_basico,OLD.nu_sabesp,OLD.co_esp_serie,OLD.co_esp_classe,OLD.co_descricao);
 END$$

 -- auditoria tb_aesbe_perfil
 create table aesbe.lg_aesbe_perfil (co_lg_aesbe_perfil bigint auto_increment primary key,   tp_operacao_log char(1),   dt_operacao_log datetime,   no_usuario_banco_log varchar(100), co_perfil int,no_perfil varchar(45),sg_perfil varchar(45));

 	DELIMITER $$ 
create trigger aesbe.tga_tb_aesbe_perfil_ins after insert 
 on aesbe.tb_aesbe_perfil
 FOR EACH ROW 
 BEGIN 
 insert into aesbe.lg_aesbe_perfil (tp_operacao_log, dt_operacao_log, no_usuario_banco_log, co_perfil,no_perfil,sg_perfil) 
 values ('I', sysdate(), session_user(), NEW.co_perfil,NEW.no_perfil,NEW.sg_perfil);
 END$$

 DELIMITER $$ 
create trigger aesbe.tga_tb_aesbe_perfil_upd after update 
 on aesbe.tb_aesbe_perfil
 FOR EACH ROW 
 BEGIN 
 insert into aesbe.lg_aesbe_perfil (tp_operacao_log, dt_operacao_log, no_usuario_banco_log, co_perfil,no_perfil,sg_perfil) 
 values ('U', sysdate(), session_user(), NEW.co_perfil,NEW.no_perfil,NEW.sg_perfil);
 END$$

 DELIMITER $$ 
create trigger aesbe.tga_tb_aesbe_perfil_del after delete 
 on aesbe.tb_aesbe_perfil
 FOR EACH ROW 
 BEGIN 
 insert into aesbe.lg_aesbe_perfil (tp_operacao_log, dt_operacao_log, no_usuario_banco_log, co_perfil,no_perfil,sg_perfil) 
 values ('D', sysdate(), session_user(), OLD.co_perfil,OLD.no_perfil,OLD.sg_perfil);
 END$$

 -- auditoria tb_aesbe_perfil_funcionalidade
 create table aesbe.lg_aesbe_perfil_funcionalidade (co_lg_aesbe_perfil_funcionalidade bigint auto_increment primary key,   tp_operacao_log char(1),   dt_operacao_log datetime,   no_usuario_banco_log varchar(100), co_perfil int,co_funcionalidade int);

 	DELIMITER $$ 
create trigger aesbe.tga_tb_aesbe_perfil_funcionalidade_ins after insert 
 on aesbe.tb_aesbe_perfil_funcionalidade
 FOR EACH ROW 
 BEGIN 
 insert into aesbe.lg_aesbe_perfil_funcionalidade (tp_operacao_log, dt_operacao_log, no_usuario_banco_log, co_perfil,co_funcionalidade) 
 values ('I', sysdate(), session_user(), NEW.co_perfil,NEW.co_funcionalidade);
 END$$

 DELIMITER $$ 
create trigger aesbe.tga_tb_aesbe_perfil_funcionalidade_upd after update 
 on aesbe.tb_aesbe_perfil_funcionalidade
 FOR EACH ROW 
 BEGIN 
 insert into aesbe.lg_aesbe_perfil_funcionalidade (tp_operacao_log, dt_operacao_log, no_usuario_banco_log, co_perfil,co_funcionalidade) 
 values ('U', sysdate(), session_user(), NEW.co_perfil,NEW.co_funcionalidade);
 END$$

 DELIMITER $$ 
create trigger aesbe.tga_tb_aesbe_perfil_funcionalidade_del after delete 
 on aesbe.tb_aesbe_perfil_funcionalidade
 FOR EACH ROW 
 BEGIN 
 insert into aesbe.lg_aesbe_perfil_funcionalidade (tp_operacao_log, dt_operacao_log, no_usuario_banco_log, co_perfil,co_funcionalidade) 
 values ('D', sysdate(), session_user(), OLD.co_perfil,OLD.co_funcionalidade);
 END$$

 -- auditoria tb_aesbe_perfil_oauth_users
 create table aesbe.lg_aesbe_perfil_oauth_users (co_lg_aesbe_perfil_oauth_users bigint auto_increment primary key,   tp_operacao_log char(1),   dt_operacao_log datetime,   no_usuario_banco_log varchar(100), co_perfil int,co_usuario int);

 	DELIMITER $$ 
create trigger aesbe.tga_tb_aesbe_perfil_oauth_users_ins after insert 
 on aesbe.tb_aesbe_perfil_oauth_users
 FOR EACH ROW 
 BEGIN 
 insert into aesbe.lg_aesbe_perfil_oauth_users (tp_operacao_log, dt_operacao_log, no_usuario_banco_log, co_perfil,co_usuario) 
 values ('I', sysdate(), session_user(), NEW.co_perfil,NEW.co_usuario);
 END$$

 DELIMITER $$ 
create trigger aesbe.tga_tb_aesbe_perfil_oauth_users_upd after update 
 on aesbe.tb_aesbe_perfil_oauth_users
 FOR EACH ROW 
 BEGIN 
 insert into aesbe.lg_aesbe_perfil_oauth_users (tp_operacao_log, dt_operacao_log, no_usuario_banco_log, co_perfil,co_usuario) 
 values ('U', sysdate(), session_user(), NEW.co_perfil,NEW.co_usuario);
 END$$

 DELIMITER $$ 
create trigger aesbe.tga_tb_aesbe_perfil_oauth_users_del after delete 
 on aesbe.tb_aesbe_perfil_oauth_users
 FOR EACH ROW 
 BEGIN 
 insert into aesbe.lg_aesbe_perfil_oauth_users (tp_operacao_log, dt_operacao_log, no_usuario_banco_log, co_perfil,co_usuario) 
 values ('D', sysdate(), session_user(), OLD.co_perfil,OLD.co_usuario);
 END$$


-- adutoria tb_aesbe_pessoa_fisica
create table aesbe.lg_aesbe_pessoa_fisica (co_lg_aesbe_pessoa_fisica bigint auto_increment primary key,   tp_operacao_log char(1),   dt_operacao_log datetime,   no_usuario_banco_log varchar(100), co_pessoa int,no_pessoa varchar(100),ds_email varchar(100),ds_email_secundario varchar(100),nu_tel_residencial varchar(10),nu_tel_comercial varchar(10),nu_celular varchar(10),st_presidente tinyint,nu_cep varchar(10),ds_logradouro varchar(100),no_bairro varchar(100),nu_logradouro varchar(10),ds_complemento varchar(30),st_ativo tinyint,co_uf int,co_empresa int,no_cidade varchar(45),co_municipio int);

	DELIMITER $$ 
create trigger aesbe.tga_tb_aesbe_pessoa_fisica_ins after insert 
 on aesbe.tb_aesbe_pessoa_fisica
 FOR EACH ROW 
 BEGIN 
 insert into aesbe.lg_aesbe_pessoa_fisica (tp_operacao_log, dt_operacao_log, no_usuario_banco_log, co_pessoa,no_pessoa,ds_email,ds_email_secundario,nu_tel_residencial,nu_tel_comercial,nu_celular,st_presidente,nu_cep,ds_logradouro,no_bairro,nu_logradouro,ds_complemento,st_ativo,co_uf,co_empresa,no_cidade,co_municipio) 
 values ('I', sysdate(), session_user(), NEW.co_pessoa,NEW.no_pessoa,NEW.ds_email,NEW.ds_email_secundario,NEW.nu_tel_residencial,NEW.nu_tel_comercial,NEW.nu_celular,NEW.st_presidente,NEW.nu_cep,NEW.ds_logradouro,NEW.no_bairro,NEW.nu_logradouro,NEW.ds_complemento,NEW.st_ativo,NEW.co_uf,NEW.co_empresa,NEW.no_cidade,NEW.co_municipio);
 END$$

 DELIMITER $$ 
create trigger aesbe.tga_tb_aesbe_pessoa_fisica_upd after update 
 on aesbe.tb_aesbe_pessoa_fisica
 FOR EACH ROW 
 BEGIN 
 insert into aesbe.lg_aesbe_pessoa_fisica (tp_operacao_log, dt_operacao_log, no_usuario_banco_log, co_pessoa,no_pessoa,ds_email,ds_email_secundario,nu_tel_residencial,nu_tel_comercial,nu_celular,st_presidente,nu_cep,ds_logradouro,no_bairro,nu_logradouro,ds_complemento,st_ativo,co_uf,co_empresa,no_cidade,co_municipio) 
 values ('U', sysdate(), session_user(), NEW.co_pessoa,NEW.no_pessoa,NEW.ds_email,NEW.ds_email_secundario,NEW.nu_tel_residencial,NEW.nu_tel_comercial,NEW.nu_celular,NEW.st_presidente,NEW.nu_cep,NEW.ds_logradouro,NEW.no_bairro,NEW.nu_logradouro,NEW.ds_complemento,NEW.st_ativo,NEW.co_uf,NEW.co_empresa,NEW.no_cidade,NEW.co_municipio);
 END$$

 DELIMITER $$ 
create trigger aesbe.tga_tb_aesbe_pessoa_fisica_del after delete 
 on aesbe.tb_aesbe_pessoa_fisica
 FOR EACH ROW 
 BEGIN 
 insert into aesbe.lg_aesbe_pessoa_fisica (tp_operacao_log, dt_operacao_log, no_usuario_banco_log, co_pessoa,no_pessoa,ds_email,ds_email_secundario,nu_tel_residencial,nu_tel_comercial,nu_celular,st_presidente,nu_cep,ds_logradouro,no_bairro,nu_logradouro,ds_complemento,st_ativo,co_uf,co_empresa,no_cidade,co_municipio) 
 values ('D', sysdate(), session_user(), OLD.co_pessoa,OLD.no_pessoa,OLD.ds_email,OLD.ds_email_secundario,OLD.nu_tel_residencial,OLD.nu_tel_comercial,OLD.nu_celular,OLD.st_presidente,OLD.nu_cep,OLD.ds_logradouro,OLD.no_bairro,OLD.nu_logradouro,OLD.ds_complemento,OLD.st_ativo,OLD.co_uf,OLD.co_empresa,OLD.no_cidade,OLD.co_municipio);
 END$$


-- auditoria tb_aesbe_proposta
create table aesbe.lg_aesbe_proposta (co_lg_aesbe_proposta bigint auto_increment primary key,   tp_operacao_log char(1),   dt_operacao_log datetime,   no_usuario_banco_log varchar(100), co_proposta int,dt_proposta datetime,co_empresa int);

	DELIMITER $$ 
create trigger aesbe.tga_tb_aesbe_proposta_ins after insert 
 on aesbe.tb_aesbe_proposta
 FOR EACH ROW 
 BEGIN 
 insert into aesbe.lg_aesbe_proposta (tp_operacao_log, dt_operacao_log, no_usuario_banco_log, co_proposta,dt_proposta,co_empresa) 
 values ('I', sysdate(), session_user(), NEW.co_proposta,NEW.dt_proposta,NEW.co_empresa);
 END$$

 DELIMITER $$ 
create trigger aesbe.tga_tb_aesbe_proposta_upd after update 
 on aesbe.tb_aesbe_proposta
 FOR EACH ROW 
 BEGIN 
 insert into aesbe.lg_aesbe_proposta (tp_operacao_log, dt_operacao_log, no_usuario_banco_log, co_proposta,dt_proposta,co_empresa) 
 values ('U', sysdate(), session_user(), NEW.co_proposta,NEW.dt_proposta,NEW.co_empresa);
 END$$

 DELIMITER $$ 
create trigger aesbe.tga_tb_aesbe_proposta_del after delete 
 on aesbe.tb_aesbe_proposta
 FOR EACH ROW 
 BEGIN 
 insert into aesbe.lg_aesbe_proposta (tp_operacao_log, dt_operacao_log, no_usuario_banco_log, co_proposta,dt_proposta,co_empresa) 
 values ('D', sysdate(), session_user(), OLD.co_proposta,OLD.dt_proposta,OLD.co_empresa);
 END$$

 -- auditoria tb_aesbe_proposta_dados
 create table aesbe.lg_aesbe_proposta_dados (co_lg_aesbe_proposta_dados bigint auto_increment primary key,   tp_operacao_log char(1),   dt_operacao_log datetime,   no_usuario_banco_log varchar(100), co_proposta_dados int,dt_informacao date,vl_icms double,vl_ipi double,st_fob_cif varchar(10),st_condicao_fornecimento varchar(45),dt_proposta date,dt_contratacao date,qt_produto double,dt_vigencia_contrato date,st_proposta varchar(45),vl_com_imposto double,nu_contrato varchar(45),nu_ata varchar(45),qt_dias_pagamento int,tp_inspecao varchar(45),st_nivel_atendimento varchar(45),co_proposta int,co_modalidade int,co_material int,st_especificacao_padrao char(1),co_fornecedor int,ds_observacao varchar(100),vl_iss double);

 	DELIMITER $$ 
create trigger aesbe.tga_tb_aesbe_proposta_dados_ins after insert 
 on aesbe.tb_aesbe_proposta_dados
 FOR EACH ROW 
 BEGIN 
 insert into aesbe.lg_aesbe_proposta_dados (tp_operacao_log, dt_operacao_log, no_usuario_banco_log, co_proposta_dados,dt_informacao,vl_icms,vl_ipi,st_fob_cif,st_condicao_fornecimento,dt_proposta,dt_contratacao,qt_produto,dt_vigencia_contrato,st_proposta,vl_com_imposto,nu_contrato,nu_ata,qt_dias_pagamento,tp_inspecao,st_nivel_atendimento,co_proposta,co_modalidade,co_material,st_especificacao_padrao,co_fornecedor,ds_observacao,vl_iss) 
 values ('I', sysdate(), session_user(), NEW.co_proposta_dados,NEW.dt_informacao,NEW.vl_icms,NEW.vl_ipi,NEW.st_fob_cif,NEW.st_condicao_fornecimento,NEW.dt_proposta,NEW.dt_contratacao,NEW.qt_produto,NEW.dt_vigencia_contrato,NEW.st_proposta,NEW.vl_com_imposto,NEW.nu_contrato,NEW.nu_ata,NEW.qt_dias_pagamento,NEW.tp_inspecao,NEW.st_nivel_atendimento,NEW.co_proposta,NEW.co_modalidade,NEW.co_material,NEW.st_especificacao_padrao,NEW.co_fornecedor,NEW.ds_observacao,NEW.vl_iss);
 END$$


DELIMITER $$ 
create trigger aesbe.tga_tb_aesbe_proposta_dados_upd after update 
 on aesbe.tb_aesbe_proposta_dados
 FOR EACH ROW 
 BEGIN 
 insert into aesbe.lg_aesbe_proposta_dados (tp_operacao_log, dt_operacao_log, no_usuario_banco_log, co_proposta_dados,dt_informacao,vl_icms,vl_ipi,st_fob_cif,st_condicao_fornecimento,dt_proposta,dt_contratacao,qt_produto,dt_vigencia_contrato,st_proposta,vl_com_imposto,nu_contrato,nu_ata,qt_dias_pagamento,tp_inspecao,st_nivel_atendimento,co_proposta,co_modalidade,co_material,st_especificacao_padrao,co_fornecedor,ds_observacao,vl_iss) 
 values ('U', sysdate(), session_user(), NEW.co_proposta_dados,NEW.dt_informacao,NEW.vl_icms,NEW.vl_ipi,NEW.st_fob_cif,NEW.st_condicao_fornecimento,NEW.dt_proposta,NEW.dt_contratacao,NEW.qt_produto,NEW.dt_vigencia_contrato,NEW.st_proposta,NEW.vl_com_imposto,NEW.nu_contrato,NEW.nu_ata,NEW.qt_dias_pagamento,NEW.tp_inspecao,NEW.st_nivel_atendimento,NEW.co_proposta,NEW.co_modalidade,NEW.co_material,NEW.st_especificacao_padrao,NEW.co_fornecedor,NEW.ds_observacao,NEW.vl_iss);
 END$$


DELIMITER $$ 
create trigger aesbe.tga_tb_aesbe_proposta_dados_del after delete 
 on aesbe.tb_aesbe_proposta_dados
 FOR EACH ROW 
 BEGIN 
 insert into aesbe.lg_aesbe_proposta_dados (tp_operacao_log, dt_operacao_log, no_usuario_banco_log, co_proposta_dados,dt_informacao,vl_icms,vl_ipi,st_fob_cif,st_condicao_fornecimento,dt_proposta,dt_contratacao,qt_produto,dt_vigencia_contrato,st_proposta,vl_com_imposto,nu_contrato,nu_ata,qt_dias_pagamento,tp_inspecao,st_nivel_atendimento,co_proposta,co_modalidade,co_material,st_especificacao_padrao,co_fornecedor,ds_observacao,vl_iss) 
 values ('D', sysdate(), session_user(), OLD.co_proposta_dados,OLD.dt_informacao,OLD.vl_icms,OLD.vl_ipi,OLD.st_fob_cif,OLD.st_condicao_fornecimento,OLD.dt_proposta,OLD.dt_contratacao,OLD.qt_produto,OLD.dt_vigencia_contrato,OLD.st_proposta,OLD.vl_com_imposto,OLD.nu_contrato,OLD.nu_ata,OLD.qt_dias_pagamento,OLD.tp_inspecao,OLD.st_nivel_atendimento,OLD.co_proposta,OLD.co_modalidade,OLD.co_material,OLD.st_especificacao_padrao,OLD.co_fornecedor,OLD.ds_observacao,OLD.vl_iss);
 END$$



##check st_proposta
ALTER TABLE aesbe.tb_aesbe_proposta_dados
ADD CONSTRAINT ck_st_proposta CHECK (st_proposta in ('ORIGINAL', 'NOVA'));
##check st_fob_cif
ALTER TABLE aesbe.tb_aesbe_proposta_dados
ADD CONSTRAINT ck_st_fob_cif CHECK (st_fob_cif in ('FOB', 'CIF'));
##check st_nivel_atendimento
ALTER TABLE aesbe.tb_aesbe_proposta_dados
ADD CONSTRAINT ck_st_nivel_atendimento CHECK (st_nivel_atendimento in ('VERDE', 'AMARELO', 'VERMELHO'));
##check st_especificacao_padrao
ALTER TABLE aesbe.tb_aesbe_proposta_dados
ADD CONSTRAINT ck_st_especificacao_padrao CHECK (st_especificacao_padrao in ('S', 'N'));

select concat('DELIMITER $$ ', char(13),
'create trigger ', cols.table_schema,
'.tga_', cols.table_name , ' after insert ' , char(13),
       ' on ', cols.table_schema , '.' , cols.table_name , char(13),
       ' FOR EACH ROW ' , char(13),
       ' BEGIN ' , char(13),
       ' insert into aesbe01.' , replace(cols.table_name, 'tb_', 'lg_') ,
       ' (tp_operacao_log, dt_operacao_log, no_usuario_banco_log, ' , group_concat(column_name) , ') ', char(13),
       ' values (''I'', sysdate(), session_user(), NEW.' , replace(group_concat(column_name), ',', ',NEW.') , ');', char(13),
       ' END$$', char(13)) tx_sql
 from information_schema.COLUMNS cols
where cols.table_name = 'tb_veiculo';
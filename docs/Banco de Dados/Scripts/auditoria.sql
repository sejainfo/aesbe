/**
CRIAÇÃO BANCO PARA AUDITORIA
**/
DROP IF EXISTS aesbe01;
CREATE DATABASE aesbe01;


CREATE TABLE aesbe01.lg_oauth_users (
  co_usuario int(11) NOT NULL,
  dt_cadastro date NOT NULL,
  username varchar(45) NOT NULL,
  password varchar(100) NOT NULL,
  dt_ultima_visita date DEFAULT NULL,
  co_pessoa int(11) DEFAULT NULL,
  tp_operacao_log CHAR(1) NOT NULL,
  dt_operacao_log TIMESTAMP NOT NULL,
  no_usuario_banco_log VARCHAR(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/**
TRIGUER INSERT
**/
DROP TRIGGER aesbe.tg_ins_oauth_users;
DELIMITER $$
CREATE TRIGGER aesbe.tg_ins_oauth_users AFTER INSERT
ON aesbe.oauth_users
FOR EACH ROW
  BEGIN
    INSERT INTO aesbe01.lg_oauth_users (co_usuario, dt_cadastro, username, password,  dt_ultima_visita, co_pessoa, tp_operacao_log, dt_operacao_log, no_usuario_banco_log)
    VALUES (NEW.co_usuario, NEW.dt_cadastro, NEW.username, NEW.password, NEW.dt_ultima_visita, NEW.co_pessoa,
            'I', sysdate(), session_user());
  END$$

DELIMITER ;

/**
TRIGUER UPDATE
**/
DROP TRIGGER aesbe.tg_upd_oauth_users;
DELIMITER $$
CREATE TRIGGER aesbe.tg_upd_oauth_users AFTER UPDATE
ON aesbe.oauth_users
FOR EACH ROW
  BEGIN
    INSERT INTO aesbe01.lg_oauth_users (co_usuario, dt_cadastro, username, password,  dt_ultima_visita, co_pessoa, tp_operacao_log, dt_operacao_log, no_usuario_banco_log)
    VALUES (NEW.co_usuario, NEW.dt_cadastro, NEW.username, NEW.password, NEW.dt_ultima_visita, NEW.co_pessoa,
            'U', sysdate(), session_user());
  END$$

DELIMITER ;

/**
TRIGUER DELETE
**/
DROP TRIGGER aesbe.tg_del_oauth_users;
DELIMITER $$
CREATE TRIGGER aesbe.tg_del_oauth_users AFTER DELETE
ON aesbe.oauth_users
FOR EACH ROW
  BEGIN
    INSERT INTO aesbe01.lg_oauth_users (co_usuario, dt_cadastro, username, password,  dt_ultima_visita, co_pessoa, tp_operacao_log, dt_operacao_log, no_usuario_banco_log)
    VALUES (OLD.co_usuario, OLD.dt_cadastro, OLD.username, OLD.password, OLD.dt_ultima_visita, OLD.co_pessoa,
            'D', sysdate(), session_user());
  END$$

DELIMITER ;


-------------------------------------------------------------
/**
CRIAÇÃO TABELA AUDITORIA TB_REGISTRO_CAIXA
*/
DROP TABLE IF EXISTS aesbe01.lg_empresa;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;

CREATE TABLE aesbe01.lg_empresa (
  co_empresa int(11) NOT NULL,
  no_empresa varchar(100) NOT NULL,
  sg_empresa varchar(45) DEFAULT NULL,
  ds_sitio varchar(100) DEFAULT NULL,
  nu_cep varchar(10) DEFAULT NULL,
  ds_logradouro varchar(100) DEFAULT NULL,
  no_bairro varchar(100) DEFAULT NULL,
  nu_logradouro varchar(10) DEFAULT NULL,
  ds_complemento varchar(30) DEFAULT NULL,
  co_uf int(11) NOT NULL,
  tp_operacao_log CHAR(1) NOT NULL,
  dt_operacao_log TIMESTAMP NOT NULL,
  no_usuario_banco_log VARCHAR(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;


/**
TRIGUER INSERT
**/
DROP TRIGGER aesbe.tg_ins_empresa;
DELIMITER $$
CREATE TRIGGER aesbe.tg_ins_empresa AFTER INSERT
ON aesbe.tb_aesbe_empresa
FOR EACH ROW
  BEGIN
    INSERT INTO aesbe01.lg_empresa (co_empresa, no_empresa, sg_empresa, ds_sitio, nu_cep, ds_logradouro,
                                    no_bairro, nu_logradouro, ds_complemento, co_uf,
                                    tp_operacao_log, dt_operacao_log, no_usuario_banco_log)
    VALUES (NEW.co_empresa, NEW.no_empresa, NEW.sg_empresa, NEW.ds_sitio, NEW.nu_cep, NEW.ds_logradouro,
                                    NEW.no_bairro, NEW.nu_logradouro, NEW.ds_complemento, NEW.co_uf,
            'I', sysdate(), session_user());
  END$$

DELIMITER ;

/**
TRIGUER UPDATE
**/
DROP TRIGGER aesbe.tg_upd_empresa;
DELIMITER $$
CREATE TRIGGER aesbe.tg_upd_empresa AFTER UPDATE
ON aesbe.tb_aesbe_empresa
FOR EACH ROW
  BEGIN
    INSERT INTO aesbe01.lg_empresa (co_empresa, no_empresa, sg_empresa, ds_sitio, nu_cep, ds_logradouro,
                                    no_bairro, nu_logradouro, ds_complemento, co_uf,
                                    tp_operacao_log, dt_operacao_log, no_usuario_banco_log)
    VALUES (NEW.co_empresa, NEW.no_empresa, NEW.sg_empresa, NEW.ds_sitio, NEW.nu_cep, NEW.ds_logradouro,
                                    NEW.no_bairro, NEW.nu_logradouro, NEW.ds_complemento, NEW.co_uf,
            'U', sysdate(), session_user());
  END$$

DELIMITER ;


/**
TRIGUER DELETE
**/
DROP TRIGGER aesbe.tg_del_empresa;
DELIMITER $$
CREATE TRIGGER aesbe.tg_del_empresa AFTER DELETE
ON aesbe.tb_aesbe_empresa
FOR EACH ROW
  BEGIN
    INSERT INTO aesbe01.lg_empresa (co_empresa, no_empresa, sg_empresa, ds_sitio, nu_cep, ds_logradouro,
                                    no_bairro, nu_logradouro, ds_complemento, co_uf,
                                    tp_operacao_log, dt_operacao_log, no_usuario_banco_log)
    VALUES (OLD.co_empresa, OLD.no_empresa, OLD.sg_empresa, OLD.ds_sitio, OLD.nu_cep, OLD.ds_logradouro,
                                    OLD.no_bairro, OLD.nu_logradouro, OLD.ds_complemento, OLD.co_uf,
            'D', sysdate(), session_user());
  END$$

DELIMITER ;

CREATE TABLE aesbe01.lg_aesbe_pessoa_fisica (
  co_pessoa int(11) NOT NULL,
  no_pessoa varchar(100) NOT NULL,
  ds_email varchar(100) NOT NULL,
  ds_email_secundario varchar(100) DEFAULT NULL,
  nu_tel_residencial varchar(10) DEFAULT NULL,
  nu_tel_comercial varchar(10) DEFAULT NULL,
  nu_celular varchar(10) DEFAULT NULL,
  st_presidente tinyint(1) NOT NULL DEFAULT '0',
  nu_cep varchar(10) DEFAULT NULL,
  ds_logradouro varchar(100) DEFAULT NULL,
  no_bairro varchar(100) DEFAULT NULL,
  nu_logradouro varchar(10) DEFAULT NULL,
  ds_complemento varchar(30) DEFAULT NULL,
  co_uf int(11) NOT NULL,
  st_ativo tinyint(1) NOT NULL,
  tp_operacao_log CHAR(1) NOT NULL,
  dt_operacao_log TIMESTAMP NOT NULL,
  no_usuario_banco_log VARCHAR(100) NOT NULL

) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/**
TRIGUER INSERT
**/
DROP TRIGGER aesbe.tg_ins_pessoa_fisica;
DELIMITER $$
CREATE TRIGGER aesbe.tg_ins_pessoa_fisica AFTER INSERT
ON aesbe.tb_aesbe_pessoa_fisica
FOR EACH ROW
  BEGIN
    INSERT INTO aesbe01.lg_aesbe_pessoa_fisica
    (
      co_pessoa, no_pessoa, ds_email, ds_email_secundario, nu_tel_residencial, nu_tel_comercial,
      nu_celular, st_presidente, nu_cep, ds_logradouro, no_bairro, nu_logradouro,  ds_complemento,
      co_uf, st_ativo, tp_operacao_log, dt_operacao_log, no_usuario_banco_log)
    VALUES (NEW.co_pessoa, NEW.no_pessoa, NEW.ds_email, NEW.ds_email_secundario, NEW.nu_tel_residencial, NEW.nu_tel_comercial,
            NEW.nu_celular, NEW.st_presidente, NEW.nu_cep, NEW.ds_logradouro, NEW.no_bairro, NEW.nu_logradouro,  NEW.ds_complemento,
            NEW.co_uf, NEW.st_ativo,
            'I', sysdate(), session_user());
  END$$

DELIMITER ;


/**
TRIGUER UPDATE
**/
DROP TRIGGER aesbe.tg_upd_pessoa_fisisca;
DELIMITER $$
CREATE TRIGGER aesbe.tg_upd_pessoa_fisisca AFTER UPDATE
ON aesbe.tb_aesbe_pessoa_fisica
FOR EACH ROW
  BEGIN
    INSERT INTO aesbe01.lg_aesbe_pessoa_fisica
    (
      co_pessoa, no_pessoa, ds_email, ds_email_secundario, nu_tel_residencial, nu_tel_comercial,
      nu_celular, st_presidente, nu_cep, ds_logradouro, no_bairro, nu_logradouro,  ds_complemento,
      co_uf, st_ativo, tp_operacao_log, dt_operacao_log, no_usuario_banco_log)
    VALUES (NEW.co_pessoa, NEW.no_pessoa, NEW.ds_email, NEW.ds_email_secundario, NEW.nu_tel_residencial, NEW.nu_tel_comercial,
            NEW.nu_celular, NEW.st_presidente, NEW.nu_cep, NEW.ds_logradouro, NEW.no_bairro, NEW.nu_logradouro,  NEW.ds_complemento,
            NEW.co_uf, NEW.st_ativo,
            'U', sysdate(), session_user());
  END$$

DELIMITER ;

/**
TRIGUER DELETE
**/
DROP TRIGGER aesbe.tg_del_pessoa_fisica;
DELIMITER $$
CREATE TRIGGER aesbe.tg_del_pessoa_fisica AFTER DELETE
ON aesbe.tb_aesbe_pessoa_fisica
FOR EACH ROW
  BEGIN
    INSERT INTO aesbe01.lg_aesbe_pessoa_fisica
    (
      co_pessoa, no_pessoa, ds_email, ds_email_secundario, nu_tel_residencial, nu_tel_comercial,
      nu_celular, st_presidente, nu_cep, ds_logradouro, no_bairro, nu_logradouro,  ds_complemento,
      co_uf, st_ativo, tp_operacao_log, dt_operacao_log, no_usuario_banco_log)
    VALUES (OLD.co_pessoa, OLD.no_pessoa, OLD.ds_email, OLD.ds_email_secundario, OLD.nu_tel_residencial, OLD.nu_tel_comercial,
            OLD.nu_celular, OLD.st_presidente, OLD.nu_cep, OLD.ds_logradouro, OLD.no_bairro, OLD.nu_logradouro,  OLD.ds_complemento,
            OLD.co_uf, OLD.st_ativo,
            'U', sysdate(), session_user());
  END$$

DELIMITER ;

# auditoria movimentacao_conta_descricao
DROP TABLE IF EXISTS aesbe01.lg_aesbe_proposta;
CREATE TABLE aesbe01.lg_aesbe_proposta (
  co_proposta int(11) NOT NULL,
  dt_proposta datetime NOT NULL,
  co_empresa int(11) NOT NULL,
  tp_operacao_log CHAR(1) NOT NULL,
  dt_operacao_log TIMESTAMP NOT NULL,
  no_usuario_banco_log VARCHAR(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


# triguer insert
DROP TRIGGER aesbe.tg_ins_proposta;
DELIMITER $$
CREATE TRIGGER aesbe.tg_ins_proposta AFTER INSERT
ON aesbe.tb_aesbe_proposta
FOR EACH ROW
  BEGIN
    INSERT INTO aesbe01.lg_aesbe_proposta
    (
      co_proposta, dt_proposta, co_empresa, tp_operacao_log,
      dt_operacao_log, no_usuario_banco_log
    )
    VALUES (NEW.co_proposta, NEW.dt_proposta, NEW.co_empresa,
            'I', sysdate(), session_user());
  END$$

DELIMITER ;

# triguer update
DROP TRIGGER aesbe.tg_upd_proposta;
DELIMITER $$
CREATE TRIGGER aesbe.tg_upd_proposta AFTER UPDATE
ON aesbe.tb_aesbe_proposta
FOR EACH ROW
  BEGIN
    INSERT INTO aesbe01.lg_aesbe_proposta
    (
      co_proposta, dt_proposta, co_empresa, tp_operacao_log,
      dt_operacao_log, no_usuario_banco_log
    )
    VALUES (NEW.co_proposta, NEW.dt_proposta, NEW.co_empresa,
            'U', sysdate(), session_user());
  END$$

DELIMITER ;

# triguer delete
DROP TRIGGER aesbe.tg_del_proposta;
DELIMITER $$
CREATE TRIGGER aesbe.tg_del_proposta AFTER DELETE
ON aesbe.tb_aesbe_proposta
FOR EACH ROW
  BEGIN
    INSERT INTO aesbe01.lg_aesbe_proposta
    (
      co_proposta, dt_proposta, co_empresa, tp_operacao_log,
      dt_operacao_log, no_usuario_banco_log
    )
    VALUES (OLD.co_proposta, OLD.dt_proposta, OLD.co_empresa,
            'D', sysdate(), session_user());
  END$$

DELIMITER ;

CREATE TABLE aesbe01.lg_aesbe_proposta_dados (
  co_proposta_dados int(11) NOT NULL,
  dt_informacao date NOT NULL,
  vl_icms double NOT NULL,
  vl_ipi double NOT NULL,
  st_fob_cif varchar(10) NOT NULL,
  st_condicao_fornecimento varchar(45) NOT NULL,
  dt_proposta date NOT NULL,
  dt_contratacao date NOT NULL,
  qt_produto int(11) NOT NULL,
  dt_vigencia_contrato date NOT NULL,
  st_proposta varchar(45) NOT NULL COMMENT '[original;nova]\n',
  vl_com_imposto double NOT NULL,
  nu_contrato int(11) NOT NULL,
  nu_ata double NOT NULL,
  qt_dias_pagamento int(11) NOT NULL,
  tp_inspecao varchar(45) NOT NULL,
  st_nivel_atendimento varchar(45) NOT NULL,
  co_proposta int(11) NOT NULL,
  co_modalidade int(11) NOT NULL,
  co_material int(11) NOT NULL,
  st_especificacao_padrao char(1) NOT NULL,
  co_fornecedor int(11) NOT NULL,
  ds_observacao varchar(100) DEFAULT NULL,
  vl_iss double DEFAULT NULL,
  tp_operacao_log CHAR(1) NOT NULL,
  dt_operacao_log TIMESTAMP NOT NULL,
  no_usuario_banco_log VARCHAR(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

# triguer insert
DROP TRIGGER aesbe.tg_ins_proposta_dados;
DELIMITER $$
CREATE TRIGGER aesbe.tg_ins_proposta_dados AFTER INSERT
ON aesbe.tb_aesbe_proposta_dados
FOR EACH ROW
  BEGIN
    INSERT INTO aesbe01.lg_aesbe_proposta_dados
    (
      co_proposta_dados, dt_informacao, vl_icms, vl_ipi, st_fob_cif, st_condicao_fornecimento,
      dt_proposta, dt_contratacao, qt_produto, dt_vigencia_contrato, st_proposta, vl_com_imposto, nu_contrato,
      nu_ata, qt_dias_pagamento, tp_inspecao, st_nivel_atendimento, co_proposta, co_modalidade, co_material,
      st_especificacao_padrao, co_fornecedor, ds_observacao, vl_iss,
      tp_operacao_log, dt_operacao_log, no_usuario_banco_log
    )
    VALUES (NEW.co_proposta_dados, NEW.dt_informacao, NEW.vl_icms, NEW.vl_ipi, NEW.st_fob_cif, NEW.st_condicao_fornecimento,
            NEW.dt_proposta, NEW.dt_contratacao, NEW.qt_produto, NEW.dt_vigencia_contrato, NEW.st_proposta, NEW.vl_com_imposto, NEW.nu_contrato,
            NEW.nu_ata, NEW.qt_dias_pagamento, NEW.tp_inspecao, NEW.st_nivel_atendimento, NEW.co_proposta, NEW.co_modalidade, NEW.co_material,
            NEW.st_especificacao_padrao, NEW.co_fornecedor, NEW.ds_observacao, NEW.vl_iss,
            'I', sysdate(), session_user());
  END$$

DELIMITER ;

# triguer update
DROP TRIGGER aesbe.tg_upd_proposta_dados;
DELIMITER $$
CREATE TRIGGER aesbe.tg_upd_proposta_dados AFTER UPDATE
ON aesbe.tb_aesbe_proposta_dados
FOR EACH ROW
  BEGIN
    INSERT INTO aesbe01.lg_aesbe_proposta_dados
    (
      co_proposta_dados, dt_informacao, vl_icms, vl_ipi, st_fob_cif, st_condicao_fornecimento,
      dt_proposta, dt_contratacao, qt_produto, dt_vigencia_contrato, st_proposta, vl_com_imposto, nu_contrato,
      nu_ata, qt_dias_pagamento, tp_inspecao, st_nivel_atendimento, co_proposta, co_modalidade, co_material,
      st_especificacao_padrao, co_fornecedor, ds_observacao, vl_iss,
      tp_operacao_log, dt_operacao_log, no_usuario_banco_log
    )
    VALUES (NEW.co_proposta_dados, NEW.dt_informacao, NEW.vl_icms, NEW.vl_ipi, NEW.st_fob_cif, NEW.st_condicao_fornecimento,
            NEW.dt_proposta, NEW.dt_contratacao, NEW.qt_produto, NEW.dt_vigencia_contrato, NEW.st_proposta, NEW.vl_com_imposto, NEW.nu_contrato,
            NEW.nu_ata, NEW.qt_dias_pagamento, NEW.tp_inspecao, NEW.st_nivel_atendimento, NEW.co_proposta, NEW.co_modalidade, NEW.co_material,
            NEW.st_especificacao_padrao, NEW.co_fornecedor, NEW.ds_observacao, NEW.vl_iss,
            'u', sysdate(), session_user());
  END$$

DELIMITER ;

# triguer delete
DROP TRIGGER aesbe.tg_del_proposta_dados;
DELIMITER $$
CREATE TRIGGER aesbe.tg_del_proposta_dados AFTER DELETE
ON aesbe.tb_aesbe_proposta_dados
FOR EACH ROW
  BEGIN
    INSERT INTO aesbe01.lg_aesbe_proposta_dados
    (
      co_proposta_dados, dt_informacao, vl_icms, vl_ipi, st_fob_cif, st_condicao_fornecimento,
      dt_proposta, dt_contratacao, qt_produto, dt_vigencia_contrato, st_proposta, vl_com_imposto, nu_contrato,
      nu_ata, qt_dias_pagamento, tp_inspecao, st_nivel_atendimento, co_proposta, co_modalidade, co_material,
      st_especificacao_padrao, co_fornecedor, ds_observacao, vl_iss,
      tp_operacao_log, dt_operacao_log, no_usuario_banco_log
    )
    VALUES (old.co_proposta_dados, old.dt_informacao, old.vl_icms, old.vl_ipi, old.st_fob_cif, old.st_condicao_fornecimento,
            old.dt_proposta, old.dt_contratacao, old.qt_produto, old.dt_vigencia_contrato, old.st_proposta, old.vl_com_imposto, old.nu_contrato,
            old.nu_ata, old.qt_dias_pagamento, old.tp_inspecao, old.st_nivel_atendimento, old.co_proposta, old.co_modalidade, old.co_material,
            old.st_especificacao_padrao, old.co_fornecedor, old.ds_observacao, old.vl_iss,
            'd', sysdate(), session_user());
  END$$

DELIMITER ;
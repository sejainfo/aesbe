<?php
return array(
    'service_manager' => array(
        'factories' => array(
            'Core\\V1\\Rest\\Menu\\MenuResource' => 'Core\\V1\\Rest\\Menu\\MenuResourceFactory',
            'Core\\V1\\Rest\\Menu\\MenuService' => 'Core\\V1\\Rest\\Menu\\MenuServiceFactory',
            'Core\\V1\\Rest\\OauthUsers\\OauthUsersResource' => 'Core\\V1\\Rest\\OauthUsers\\OauthUsersResourceFactory',
            'Core\\V1\\Rest\\OauthUsers\\OauthUsersRepositoryService' => 'Core\\V1\\Rest\\OauthUsers\\OauthUsersRepositoryServiceFactory',
            'Core\\V1\\Rest\\OauthUsers\\OauthAccessTokensRepositoryService' => 'Core\\V1\\Rest\\OauthUsers\\OauthAccessTokensRepositoryServiceFactory',
            'Core\\V1\\Rest\\Perfil\\PerfilResource' => 'Core\\V1\\Rest\\Perfil\\PerfilResourceFactory',
            'Core\\V1\\Rest\\Modulo\\ModuloResource' => 'Core\\V1\\Rest\\Modulo\\ModuloResourceFactory',
            'Core\\V1\\Rest\\Funcionalidade\\FuncionalidadeResource' => 'Core\\V1\\Rest\\Funcionalidade\\FuncionalidadeResourceFactory',
            'Core\\V1\\Rest\\Perfil\\PerfilRepositoryService' => 'Core\\V1\\Rest\\Perfil\\PerfilRepositoryServiceFactory',
            'Core\\V1\\Rest\\Modulo\\ModuloRepositoryService' => 'Core\\V1\\Rest\\Modulo\\ModuloRepositoryServiceFactory',
            'Core\\V1\\Rest\\Funcionalidade\\FuncionalidadeRepositoryService' => 'Core\\V1\\Rest\\Funcionalidade\\FuncionalidadeRepositoryServiceFactory',
            'Core\\V1\\Rest\\PessoaFisica\\PessoaFisicaResource' => 'Core\\V1\\Rest\\PessoaFisica\\PessoaFisicaResourceFactory',
            'Core\\V1\\Rest\\PessoaFisica\\PessoaFisicaRepositoryService' => 'Core\\V1\\Rest\\PessoaFisica\\PessoaFisicaRepositoryServiceFactory',
            'Core\\V1\\Rest\\PerfilOauthUsers\\PerfilOauthUsersResource' => 'Core\\V1\\Rest\\PerfilOauthUsers\\PerfilOauthUsersResourceFactory',
            'Core\\V1\\Rest\\DadosSessao\\DadosSessaoResource' => 'Core\\V1\\Rest\\DadosSessao\\DadosSessaoResourceFactory',
        ),
    ),
    'router' => array(
        'routes' => array(
            'core.rest.menu' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/publico/menu[/:menu_id]',
                    'defaults' => array(
                        'controller' => 'Core\\V1\\Rest\\Menu\\Controller',
                    ),
                ),
            ),
            'core.rest.oauth-users' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/configuracoes/oauth-users[/:oauth_users_id]',
                    'defaults' => array(
                        'controller' => 'Core\\V1\\Rest\\OauthUsers\\Controller',
                    ),
                ),
            ),
            'core.rest.perfil' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/core/perfil[/:perfil_id]',
                    'defaults' => array(
                        'controller' => 'Core\\V1\\Rest\\Perfil\\Controller',
                    ),
                ),
            ),
            'core.rest.modulo' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/modulo[/:modulo_id]',
                    'defaults' => array(
                        'controller' => 'Core\\V1\\Rest\\Modulo\\Controller',
                    ),
                ),
            ),
            'core.rest.funcionalidade' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/funcionalidade[/:funcionalidade_id]',
                    'defaults' => array(
                        'controller' => 'Core\\V1\\Rest\\Funcionalidade\\Controller',
                    ),
                ),
            ),
            'core.rest.pessoa-fisica' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/core/pessoa-fisica[/:pessoa_fisica_id]',
                    'defaults' => array(
                        'controller' => 'Core\\V1\\Rest\\PessoaFisica\\Controller',
                    ),
                ),
            ),
            'core.rest.perfil-oauth-users' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/core/perfil-oauth-users[/:perfil_oauth_users_id]',
                    'defaults' => array(
                        'controller' => 'Core\\V1\\Rest\\PerfilOauthUsers\\Controller',
                    ),
                ),
            ),
            'core.rest.dados-sessao' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/publico/dados-sessao[/:dados_sessao_id]',
                    'defaults' => array(
                        'controller' => 'Core\\V1\\Rest\\DadosSessao\\Controller',
                    ),
                ),
            ),
        ),
    ),
    'zf-versioning' => array(
        'uri' => array(
            0 => 'core.rest.menu',
            1 => 'core.rest.oauth-users',
            2 => 'core.rest.perfil',
            3 => 'core.rest.modulo',
            4 => 'core.rest.funcionalidade',
            5 => 'core.rest.pessoa-fisica',
            6 => 'core.rest.perfil-oauth-users',
            7 => 'core.rest.dados-sessao',
        ),
    ),
    'zf-rest' => array(
        'Core\\V1\\Rest\\Menu\\Controller' => array(
            'listener' => 'Core\\V1\\Rest\\Menu\\MenuResource',
            'route_name' => 'core.rest.menu',
            'route_identifier_name' => 'menu_id',
            'collection_name' => 'menu',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Core\\V1\\Rest\\Menu\\MenuEntity',
            'collection_class' => 'Core\\V1\\Rest\\Menu\\MenuCollection',
            'service_name' => 'Menu',
        ),
        'Core\\V1\\Rest\\OauthUsers\\Controller' => array(
            'listener' => 'Core\\V1\\Rest\\OauthUsers\\OauthUsersResource',
            'route_name' => 'core.rest.oauth-users',
            'route_identifier_name' => 'oauth_users_id',
            'collection_name' => 'oauth_users',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Application\\Entity\\OauthUsersEntity',
            'collection_class' => 'Core\\V1\\Rest\\OauthUsers\\OauthUsersCollection',
            'service_name' => 'OauthUsers',
        ),
        'Core\\V1\\Rest\\Perfil\\Controller' => array(
            'listener' => 'Core\\V1\\Rest\\Perfil\\PerfilResource',
            'route_name' => 'core.rest.perfil',
            'route_identifier_name' => 'perfil_id',
            'collection_name' => 'perfil',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Application\\Entity\\AesbePerfilEntity',
            'collection_class' => 'Core\\V1\\Rest\\Perfil\\PerfilCollection',
            'service_name' => 'Perfil',
        ),
        'Core\\V1\\Rest\\Modulo\\Controller' => array(
            'listener' => 'Core\\V1\\Rest\\Modulo\\ModuloResource',
            'route_name' => 'core.rest.modulo',
            'route_identifier_name' => 'modulo_id',
            'collection_name' => 'modulo',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Core\\V1\\Rest\\Modulo\\ModuloEntity',
            'collection_class' => 'Core\\V1\\Rest\\Modulo\\ModuloCollection',
            'service_name' => 'Modulo',
        ),
        'Core\\V1\\Rest\\Funcionalidade\\Controller' => array(
            'listener' => 'Core\\V1\\Rest\\Funcionalidade\\FuncionalidadeResource',
            'route_name' => 'core.rest.funcionalidade',
            'route_identifier_name' => 'funcionalidade_id',
            'collection_name' => 'funcionalidade',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Core\\V1\\Rest\\Funcionalidade\\FuncionalidadeEntity',
            'collection_class' => 'Core\\V1\\Rest\\Funcionalidade\\FuncionalidadeCollection',
            'service_name' => 'Funcionalidade',
        ),
        'Core\\V1\\Rest\\PessoaFisica\\Controller' => array(
            'listener' => 'Core\\V1\\Rest\\PessoaFisica\\PessoaFisicaResource',
            'route_name' => 'core.rest.pessoa-fisica',
            'route_identifier_name' => 'pessoa_fisica_id',
            'collection_name' => 'pessoa_fisica',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Application\\Entity\\AesbePessoaFisicaEntity',
            'collection_class' => 'Core\\V1\\Rest\\PessoaFisica\\PessoaFisicaCollection',
            'service_name' => 'PessoaFisica',
        ),
        'Core\\V1\\Rest\\PerfilOauthUsers\\Controller' => array(
            'listener' => 'Core\\V1\\Rest\\PerfilOauthUsers\\PerfilOauthUsersResource',
            'route_name' => 'core.rest.perfil-oauth-users',
            'route_identifier_name' => 'perfil_oauth_users_id',
            'collection_name' => 'perfil_oauth_users',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Core\\V1\\Rest\\PerfilOauthUsers\\PerfilOauthUsersEntity',
            'collection_class' => 'Core\\V1\\Rest\\PerfilOauthUsers\\PerfilOauthUsersCollection',
            'service_name' => 'PerfilOauthUsers',
        ),
        'Core\\V1\\Rest\\DadosSessao\\Controller' => array(
            'listener' => 'Core\\V1\\Rest\\DadosSessao\\DadosSessaoResource',
            'route_name' => 'core.rest.dados-sessao',
            'route_identifier_name' => 'dados_sessao_id',
            'collection_name' => 'dados_sessao',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Core\\V1\\Rest\\DadosSessao\\DadosSessaoEntity',
            'collection_class' => 'Core\\V1\\Rest\\DadosSessao\\DadosSessaoCollection',
            'service_name' => 'DadosSessao',
        ),
    ),
    'zf-content-negotiation' => array(
        'controllers' => array(
            'Core\\V1\\Rest\\Menu\\Controller' => 'Json',
            'Core\\V1\\Rest\\OauthUsers\\Controller' => 'Json',
            'Core\\V1\\Rest\\Perfil\\Controller' => 'HalJson',
            'Core\\V1\\Rest\\Modulo\\Controller' => 'HalJson',
            'Core\\V1\\Rest\\Funcionalidade\\Controller' => 'HalJson',
            'Core\\V1\\Rest\\PessoaFisica\\Controller' => 'HalJson',
            'Core\\V1\\Rest\\PerfilOauthUsers\\Controller' => 'HalJson',
            'Core\\V1\\Rest\\DadosSessao\\Controller' => 'HalJson',
        ),
        'accept_whitelist' => array(
            'Core\\V1\\Rest\\Menu\\Controller' => array(
                0 => 'application/vnd.core.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Core\\V1\\Rest\\OauthUsers\\Controller' => array(
                0 => 'application/vnd.core.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Core\\V1\\Rest\\Perfil\\Controller' => array(
                0 => 'application/vnd.core.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Core\\V1\\Rest\\Modulo\\Controller' => array(
                0 => 'application/vnd.core.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Core\\V1\\Rest\\Funcionalidade\\Controller' => array(
                0 => 'application/vnd.core.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Core\\V1\\Rest\\PessoaFisica\\Controller' => array(
                0 => 'application/vnd.core.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Core\\V1\\Rest\\PerfilOauthUsers\\Controller' => array(
                0 => 'application/vnd.core.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Core\\V1\\Rest\\DadosSessao\\Controller' => array(
                0 => 'application/vnd.core.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
        ),
        'content_type_whitelist' => array(
            'Core\\V1\\Rest\\Menu\\Controller' => array(
                0 => 'application/vnd.core.v1+json',
                1 => 'application/json',
            ),
            'Core\\V1\\Rest\\OauthUsers\\Controller' => array(
                0 => 'application/vnd.core.v1+json',
                1 => 'application/json',
            ),
            'Core\\V1\\Rest\\Perfil\\Controller' => array(
                0 => 'application/vnd.core.v1+json',
                1 => 'application/json',
            ),
            'Core\\V1\\Rest\\Modulo\\Controller' => array(
                0 => 'application/vnd.core.v1+json',
                1 => 'application/json',
            ),
            'Core\\V1\\Rest\\Funcionalidade\\Controller' => array(
                0 => 'application/vnd.core.v1+json',
                1 => 'application/json',
            ),
            'Core\\V1\\Rest\\PessoaFisica\\Controller' => array(
                0 => 'application/vnd.core.v1+json',
                1 => 'application/json',
            ),
            'Core\\V1\\Rest\\PerfilOauthUsers\\Controller' => array(
                0 => 'application/vnd.core.v1+json',
                1 => 'application/json',
            ),
            'Core\\V1\\Rest\\DadosSessao\\Controller' => array(
                0 => 'application/vnd.core.v1+json',
                1 => 'application/json',
            ),
        ),
    ),
    'zf-hal' => array(
        'metadata_map' => array(
            'Core\\V1\\Rest\\Menu\\MenuEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'core.rest.menu',
                'route_identifier_name' => 'menu_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'Core\\V1\\Rest\\Menu\\MenuCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'core.rest.menu',
                'route_identifier_name' => 'menu_id',
                'is_collection' => true,
            ),
            'Core\\V1\\Rest\\OauthUsers\\OauthUsersEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'core.rest.oauth-users',
                'route_identifier_name' => 'oauth_users_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'Core\\V1\\Rest\\OauthUsers\\OauthUsersCollection' => array(
                'entity_identifier_name' => 'username',
                'route_name' => 'core.rest.oauth-users',
                'route_identifier_name' => 'oauth_users_id',
                'is_collection' => true,
            ),
            'BancoPreco\\V1\\Rest\\ArquivoProposta\\OauthUsersEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'core.rest.oauth-users',
                'route_identifier_name' => 'oauth_users_id',
                'hydrator' => 'DoctrineModule\\Stdlib\\Hydrator\\DoctrineObject',
            ),
            'Core\\V1\\Rest\\Perfil\\PerfilEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'core.rest.perfil',
                'route_identifier_name' => 'perfil_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'Core\\V1\\Rest\\Perfil\\PerfilCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'core.rest.perfil',
                'route_identifier_name' => 'perfil_id',
                'is_collection' => true,
            ),
            'Core\\V1\\Rest\\Modulo\\ModuloEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'core.rest.modulo',
                'route_identifier_name' => 'modulo_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'Core\\V1\\Rest\\Modulo\\ModuloCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'core.rest.modulo',
                'route_identifier_name' => 'modulo_id',
                'is_collection' => true,
            ),
            'Core\\V1\\Rest\\Funcionalidade\\FuncionalidadeEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'core.rest.funcionalidade',
                'route_identifier_name' => 'funcionalidade_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'Core\\V1\\Rest\\Funcionalidade\\FuncionalidadeCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'core.rest.funcionalidade',
                'route_identifier_name' => 'funcionalidade_id',
                'is_collection' => true,
            ),
            'Core\\V1\\Rest\\PessoaFisica\\PessoaFisicaEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'core.rest.pessoa-fisica',
                'route_identifier_name' => 'pessoa_fisica_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'Core\\V1\\Rest\\PessoaFisica\\PessoaFisicaCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'core.rest.pessoa-fisica',
                'route_identifier_name' => 'pessoa_fisica_id',
                'is_collection' => true,
            ),
            'Application\\Entity\\AesbePessoaFisicaEntity' => array(
                'entity_identifier_name' => 'coPessoa',
                'route_name' => 'core.rest.pessoa-fisica',
                'route_identifier_name' => 'pessoa_fisica_id',
                'hydrator' => 'DoctrineModule\\Stdlib\\Hydrator\\DoctrineObject',
            ),
            'Core\\V1\\Rest\\Teste\\TesteEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'core.rest.teste',
                'route_identifier_name' => 'teste_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'Application\\Entity\\AesbePerfilEntity' => array(
                'entity_identifier_name' => 'coPerfil',
                'route_name' => 'core.rest.perfil',
                'route_identifier_name' => 'perfil_id',
                'hydrator' => 'DoctrineModule\\Stdlib\\Hydrator\\DoctrineObject',
            ),
            'Core\\V1\\Rest\\PerfilOauthUsers\\PerfilOauthUsersEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'core.rest.perfil-oauth-users',
                'route_identifier_name' => 'perfil_oauth_users_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'Core\\V1\\Rest\\PerfilOauthUsers\\PerfilOauthUsersCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'core.rest.perfil-oauth-users',
                'route_identifier_name' => 'perfil_oauth_users_id',
                'is_collection' => true,
            ),
            'Application\\Entity\\OauthUsersEntity' => array(
                'entity_identifier_name' => 'username',
                'route_name' => 'core.rest.oauth-users',
                'route_identifier_name' => 'oauth_users_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'Core\\V1\\Rest\\DadosSessao\\DadosSessaoEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'core.rest.dados-sessao',
                'route_identifier_name' => 'dados_sessao_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'Core\\V1\\Rest\\DadosSessao\\DadosSessaoCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'core.rest.dados-sessao',
                'route_identifier_name' => 'dados_sessao_id',
                'is_collection' => true,
            ),
        ),
    ),
    'zf-mvc-auth' => array(
        'authorization' => array(
            'Core\\V1\\Rest\\Menu\\Controller' => array(
                'collection' => array(
                    'GET' => false,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
                'entity' => array(
                    'GET' => true,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
            ),
        ),
    ),
    'zf-content-validation' => array(
        'Core\\V1\\Rest\\PessoaFisica\\Controller' => array(
            'input_filter' => 'Core\\V1\\Rest\\PessoaFisica\\Validator',
        ),
        'Core\\V1\\Rest\\PerfilOauthUsers\\Controller' => array(
            'input_filter' => 'Core\\V1\\Rest\\PerfilOauthUsers\\Validator',
        ),
    ),
    'input_filter_specs' => array(
        'Core\\V1\\Rest\\PessoaFisica\\Validator' => array(
            0 => array(
                'required' => true,
                'validators' => array(),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                        'options' => array(),
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'stAtivo',
            ),
        ),
        'Core\\V1\\Rest\\PerfilOauthUsers\\Validator' => array(
            0 => array(
                'required' => true,
                'validators' => array(),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\Digits',
                        'options' => array(),
                    ),
                ),
                'name' => 'coUsuario',
            ),
            1 => array(
                'required' => true,
                'validators' => array(),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\Digits',
                        'options' => array(),
                    ),
                ),
                'name' => 'coPerfil',
            ),
        ),
    ),
);

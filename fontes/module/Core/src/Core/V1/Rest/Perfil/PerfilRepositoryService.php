<?php

namespace Core\V1\Rest\Perfil;

use Application\Entity\AesbePerfilEntity;
use Application\Entity\OauthUsersEntity;
use Application\Service\AbstractRepositoryService;

class PerfilRepositoryService extends AbstractRepositoryService
{

    /**
     * Vincular um perfil ao usuário
     * @param int $coUsuario
     * @param int $coPerfil
     */
    public function vincularPerfil($coUsuario, $coPerfil)
    {
        $this->removerPerfilUsuario($coUsuario);
        $this->adicionarPerfilUsuario($coUsuario, $coPerfil);
    }

    /**
     * Adicionar Perfil ao Usuário
     * @param int $coUsuario
     * @param int $coPerfil
     */
    public function adicionarPerfilUsuario($coUsuario, $coPerfil)
    {
        $perfilEntity = new AesbePerfilEntity();
        $perfilEntity->setCoPerfil($coPerfil);
        return $this->getPerfilRepository()->adicionarPerfil($coPerfil, $coUsuario);
    }

    /**
     * Remove todos os perfis de um determinado usuário
     * @param int $coUsuario
     * @return int
     */
    public function removerPerfilUsuario($coUsuario)
    {
        return $this->getPerfilRepository()->removerPerfil($coUsuario);
    }

    /**
     * @return \Application\Entity\AesbePerfilRepository
     */
    protected function getPerfilRepository()
    {
        return $this->getRepository();
    }


}
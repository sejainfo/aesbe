<?php
namespace Core\V1\Rest\Perfil;

class PerfilResourceFactory
{
    public function __invoke($services)
    {
        $srvPerfil = $services
            ->get('Core\\V1\\Rest\\Perfil\\PerfilRepositoryService');
        return new PerfilResource($srvPerfil);
    }
}

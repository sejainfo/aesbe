<?php

namespace Core\V1\Rest\Perfil;

use Commons\Pattern\Service\Impl\ZendServiceLookupManager;
use Zend\Log\Logger;
use Zend\Log\PsrLoggerAdapter;

class PerfilRepositoryServiceFactory
{
    public function __invoke($services)
    {
        /* @var $srvDoctrine \Doctrine\ORM\EntityManager */
        $srvDoctrine = $services->get('Doctrine\ORM\EntityManager');

        $repo = $srvDoctrine->getRepository('Application\Entity\AesbePerfilEntity');
        $lookupManager = new ZendServiceLookupManager($services);
        $logger = new PsrLoggerAdapter(new Logger());

        return new PerfilRepositoryService($repo, $lookupManager, $logger);
    }
}
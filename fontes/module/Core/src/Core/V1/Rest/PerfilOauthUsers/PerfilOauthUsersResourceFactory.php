<?php
namespace Core\V1\Rest\PerfilOauthUsers;

class PerfilOauthUsersResourceFactory
{
    public function __invoke($services)
    {
        $srvPerfil = $services
            ->get('Core\\V1\\Rest\\Perfil\\PerfilRepositoryService');
        return new PerfilOauthUsersResource($srvPerfil);
    }
}

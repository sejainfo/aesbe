<?php

namespace Core\V1\Rest\Modulo;

use Commons\Pattern\Service\Impl\ZendServiceLookupManager;
use Zend\Log\Logger;
use Zend\Log\PsrLoggerAdapter;

/**
 * Class ModuloRepositoryServiceFactory
 * @package Core\V1\Rest\Modulo
 */
class ModuloRepositoryServiceFactory
{
    public function __invoke($services)
    {
        /* @var $srvDoctrine \Doctrine\ORM\EntityManager */
        $srvDoctrine = $services->get('Doctrine\ORM\EntityManager');

        $repo = $srvDoctrine->getRepository('Application\Entity\AesbeModuloEntity');
        $lookupManager = new ZendServiceLookupManager($services);
        $logger = new PsrLoggerAdapter(new Logger());

        return new ModuloRepositoryService($repo, $lookupManager, $logger);

    }
}
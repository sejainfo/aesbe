<?php
namespace Core\V1\Rest\Modulo;

class ModuloResourceFactory
{
    public function __invoke($services)
    {
        return new ModuloResource();
    }
}

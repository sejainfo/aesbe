<?php
namespace Core\V1\Rest\Menu;

class MenuResourceFactory
{
    public function __invoke($services)
    {
        $srvOauthUsers = $services
            ->get('Core\\V1\\Rest\\OauthUsers\\OauthUsersRepositoryService');
//        $authentication = $services->get('authentication');
//        \Zend\Debug\Debug::dump($authentication);
        $srvMenu = $services
            ->get('Core\\V1\\Rest\\Menu\\MenuService');
        return new MenuResource($srvOauthUsers, $srvMenu);
    }
}

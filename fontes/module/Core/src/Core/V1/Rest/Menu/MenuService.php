<?php
namespace Core\V1\Rest\Menu;

use Application\Service\AbstractService;

class MenuService extends AbstractService
{

    public function montarMenuUsuario($username)
    {
        $arrayMenu = $this->getOauthUsersRepositoryService()->getMenu($username);
        $indice = 0;
        $count = count($arrayMenu);
        $menu = array();
        while ($indice < $count) {
            $coModulo = $arrayMenu[$indice]['coModulo'];
            $modulo = array(
                'idModulo' => $coModulo,
                'noModuloVisivel' => $arrayMenu[$indice]['noModuloVisivel'],
                'dsIcon' => $arrayMenu[$indice]['dsIcon'],
                'dsUri' => $arrayMenu[$indice]['dsUri']
            );

            while (array_key_exists($indice, $arrayMenu) &&
                $arrayMenu[$indice]['coModulo'] == $coModulo){
                $coModulo = $arrayMenu[$indice]['coModulo'];
                $agrupador = $arrayMenu[$indice]['noAgrupador'];
                $arrayAgrupador = array('noAgrupador' => $agrupador);
                while (array_key_exists($indice, $arrayMenu) &&
                    $arrayMenu[$indice]['noAgrupador'] == $agrupador){
                    $agrupador = $arrayMenu[$indice]['noAgrupador'];
                    $funcionalidade = array(
                        'noFuncionalidade' => $arrayMenu[$indice]['noFuncionalidade'],
                        'noFuncionalidadeVisivel' => $arrayMenu[$indice]['noFuncionalidadeVisivel']
                    );
                    $arrayAgrupador['menus'][] = $funcionalidade;
                    $indice++;
                }
                $modulo['funcionalidade'][] = $arrayAgrupador;
            }
            $menu[]=$modulo;
        }
        return $menu;
    }


    /**
     * @return \Core\V1\Rest\OauthUsers\OauthUsersRepositoryService
     */
    protected function getOauthUsersRepositoryService()
    {
        return $this->getLookupManager()->get('Core\\V1\\Rest\\OauthUsers\\OauthUsersRepositoryService');

    }
}

<?php
namespace Core\V1\Rest\Menu;

use Commons\Pattern\Service\Impl\ZendServiceLookupManager;
use Zend\Log\Logger;
use Zend\Log\PsrLoggerAdapter;

class MenuServiceFactory
{
    public function __invoke($services)
    {
        $lookupManager = new ZendServiceLookupManager($services);
        $logger = new PsrLoggerAdapter(new Logger());
        return new MenuService($lookupManager, $logger);
    }
}

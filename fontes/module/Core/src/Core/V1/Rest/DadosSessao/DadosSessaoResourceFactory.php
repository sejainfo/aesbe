<?php
namespace Core\V1\Rest\DadosSessao;

class DadosSessaoResourceFactory
{
    public function __invoke($services)
    {
        $srvUser = $services->get('Core\V1\Rest\OauthUsers\OauthUsersRepositoryService');
        return new DadosSessaoResource($srvUser);
    }
}

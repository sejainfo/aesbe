<?php

namespace Core\V1\Rest\Funcionalidade;

use Application\Service\AbstractRepositoryService;

class FuncionalidadeRepositoryService extends AbstractRepositoryService
{

    /**
     * @param $coPerfil
     * @return array
     */
    public function getFuncModByPerfil($coPerfil)
    {
        return $this->getAesbeFuncionalidadeRepository()->getFuncModByPerfil($coPerfil);
    }


    /**
     * @return \Application\Entity\AesbeFuncionalidadeEntityRepository
     */
    protected function getAesbeFuncionalidadeRepository()
    {
        return $this->getRepository();
    }

}
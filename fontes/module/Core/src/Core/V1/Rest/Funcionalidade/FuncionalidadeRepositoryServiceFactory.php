<?php

namespace Core\V1\Rest\Funcionalidade;

use Commons\Pattern\Service\Impl\ZendServiceLookupManager;
use Zend\Log\Logger;
use Zend\Log\PsrLoggerAdapter;

/**
 * Class FuncionalidadeServiceFactory
 * @package Core\V1\Rest\Funcionalidade
 */
class FuncionalidadeRepositoryServiceFactory
{
    /**
     * @param $services ServiceManager
     */
    public function __invoke($services)
    {
        /* @var $srvDoctrine \Doctrine\ORM\EntityManager */
        $srvDoctrine = $services->get('Doctrine\ORM\EntityManager');

        $repo = $srvDoctrine->getRepository('Application\Entity\AesbeFuncionalidadeEntity');
        $lookupManager = new ZendServiceLookupManager($services);
        $logger = new PsrLoggerAdapter(new Logger());

        return new FuncionalidadeRepositoryService($repo, $lookupManager, $logger);
    }
}
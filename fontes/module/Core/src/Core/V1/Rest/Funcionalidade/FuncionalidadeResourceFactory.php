<?php
namespace Core\V1\Rest\Funcionalidade;

class FuncionalidadeResourceFactory
{
    public function __invoke($services)
    {
        return new FuncionalidadeResource();
    }
}

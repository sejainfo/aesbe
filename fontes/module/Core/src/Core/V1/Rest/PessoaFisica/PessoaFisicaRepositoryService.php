<?php

namespace Core\V1\Rest\PessoaFisica;

use Application\Service\AbstractRepositoryService;
use Commons\Pattern\Repository\Impl\Reference;

class PessoaFisicaRepositoryService extends AbstractRepositoryService
{

    /**
     * Salva e retorna os dados da pessoa
     * @param $data
     * @return bool|\Doctrine\Common\Proxy\Proxy|null|object
     */
    public function salvar($data)
    {
        $data['empresaEntity'] = new Reference(
            'Application\Entity\AesbeEmpresaEntity',
            array(
                'coEmpresa' => $data['coEmpresa']
            )
        );
        $data['ufEntity'] = new Reference(
            'Application\Entity\AesbeUfEntity',
            array(
                'coUf' => $data['coUf']
            )
        );
        $data['municipioEntity'] = new Reference(
            'Application\Entity\AesbeMunicipioEntity',
            array(
                'coMunicipio' => $data['coMunicipio']
            )
        );
        $data['stAtivo'] = '0';
        $pessoa = $this->getPessoaFisicaRepository()->save($data);
        return $pessoa;

    }

    /**
     * @return \Commons\Pattern\Repository\Impl\SimpleEntityRepository
     */
    protected function getPessoaFisicaRepository()
    {
        return $this->getRepository();
    }


}
<?php
namespace Core\V1\Rest\PessoaFisica;

class PessoaFisicaResourceFactory
{
    public function __invoke($services)
    {
        $srvPessoaFisica = $services
            ->get('Core\\V1\\Rest\\PessoaFisica\\PessoaFisicaRepositoryService');
        return new PessoaFisicaResource($srvPessoaFisica);
    }
}

<?php

namespace Core\V1\Rest\PessoaFisica;

use Commons\Pattern\Service\Impl\ZendServiceLookupManager;
use Zend\Log\Logger;
use Zend\Log\PsrLoggerAdapter;

class PessoaFisicaRepositoryServiceFactory
{
    public function __invoke($services)
    {
        /* @var $srvDoctrine \Doctrine\ORM\EntityManager */
        $srvDoctrine = $services->get('Doctrine\ORM\EntityManager');

        $repo = $srvDoctrine->getRepository('Application\Entity\AesbePessoaFisicaEntity');
        $lookupManager = new ZendServiceLookupManager($services);
        $logger = new PsrLoggerAdapter(new Logger());

        return new PessoaFisicaRepositoryService($repo, $lookupManager, $logger);
    }
}
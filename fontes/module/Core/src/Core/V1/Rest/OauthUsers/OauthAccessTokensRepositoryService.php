<?php

namespace Core\V1\Rest\OauthUsers;

use Application\Service\AbstractRepositoryService;
use Application\Entity\OauthAccessTokens;

/**
 * Class OauthAccessTokensRepositoryService
 * @package Core\V1\Rest\OauthUsers
 */
class OauthAccessTokensRepositoryService extends AbstractRepositoryService
{

    /**
     * @param $token string
     * @return OauthAccessTokens
     */
    public function getUserByToken($token)
    {
        return $this->findOneBy(array('accessToken' => $token));
    }
}
<?php
namespace Core\V1\Rest\OauthUsers;

class OauthUsersResourceFactory
{
    public function __invoke($services)
    {
        $srvOauthUsers = $services
            ->get('Core\\V1\\Rest\\OauthUsers\\OauthUsersRepositoryService');
        return new OauthUsersResource($srvOauthUsers);
    }
}

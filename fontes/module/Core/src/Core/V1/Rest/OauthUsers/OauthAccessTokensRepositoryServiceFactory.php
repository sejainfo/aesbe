<?php

namespace Core\V1\Rest\OauthUsers;

use Commons\Pattern\Service\Impl\ZendServiceLookupManager;
use Zend\Log\Logger;
use Zend\Log\PsrLoggerAdapter;
use Zend\ServiceManager\ServiceManager;

/**
 * Class OauthAccessTokensRepositoryServiceFactory
 * @package Core\V1\Rest\OauthUsers
 */
class OauthAccessTokensRepositoryServiceFactory
{
    /**
     * @param $services ServiceManager
     * @return OauthAccessTokensRepositoryService
     */
    public function __invoke($services)
    {
        /* @var $srvDoctrine \Doctrine\ORM\EntityManager */
        $srvDoctrine = $services->get('Doctrine\ORM\EntityManager');
//        \Doctrine\Common\Util\Debug::dump($srvDoctrine);
        $repo = $srvDoctrine->getRepository('Application\Entity\OauthAccessTokens');
        $lookupManager = new ZendServiceLookupManager($services);
        $logger = new PsrLoggerAdapter(new Logger());
        return new OauthAccessTokensRepositoryService($repo, $lookupManager, $logger);
    }
}
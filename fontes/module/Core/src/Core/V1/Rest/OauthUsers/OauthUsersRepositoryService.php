<?php
namespace Core\V1\Rest\OauthUsers;

use Application\Entity\AesbePerfilEntity;
use Application\Entity\OauthUsersEntity;
use Application\Service\AbstractRepositoryService;

class OauthUsersRepositoryService extends AbstractRepositoryService
{
    use \ZF\OAuth2\Adapter\BcryptTrait;

    /**
     * Retorna Menu para listagem no client
     * @param string $username
     * @return array
     */
    public function getMenu($username)
    {
        return $this->getOauthUsersRepository()->listarMenus($username);
    }

    /**
     * Cria novo Usuário
     * @param $data
     * @return bool
     */
    public function criarUsuario($data)
    {
        $data['dtCadastro'] = new \DateTime();
        $data['username'] = $data['dsEmail'];
        $data['password'] = $this->getBcrypt()->create($data['password']);
        return $this->save($data);
    }

    /**
     * Retorna usuário pelo username
     * @param string $username
     * @return array
     */
    public function getUsuarioPorUsername($username)
    {
        return current($this->findBy(array('username' => $username)));
    }

    /**
     * Retorna empresa usuario
     * @param string $username
     * @return mixed
     */
    public function getCoEmpresaUsername($username)
    {
        $usuario = $this->getUsuarioPorUsername($username);
        return $usuario->getPessoaEntity()->getEmpresaEntity()->getCoEmpresa();
    }



    public function listarUsuariosPendentes($username)
    {
        /* @var $usuario \Application\Entity\OauthUsersEntity */
        $usuario = $this->getUsuarioPorUsername($username);
        /* @var $perfil \Application\Entity\AesbePerfilEntity */
        $perfil = $usuario->getPerfis();
        $perfil = $perfil[0];
        $coEmpresa = null;
        if ($perfil->getSgPerfil() != AesbePerfilEntity::SG_PERFIL_ADMIN){
            $coEmpresa = $usuario->getPessoaEntity()->getEmpresaEntity()->getCoEmpresa();
        }
        return $this->getOauthUsersRepository()->listarUsuariosPendentes($coEmpresa);
    }

    /**
     * @return \Application\Entity\OauthUsersRepository
     */
    protected function getOauthUsersRepository()
    {
        return $this->getRepository();
    }

    /**
     * @param $username
     * @return array
     */
    public function getDataUser($username)
    {
        $result = array();
        $resultPerfil = array();

        $user = $this->findOneBy(array('username' => $username));

        if(!$user){
            return $result;
        }

        $perfis = $user->getPerfis();

        foreach ($perfis as $perfil) {
            $resultPerfil[] = $perfil->getSgPerfil();
        }

        $strPerfil = implode(', ', $resultPerfil);

        $result[0]['username'] = $user->getUsername();
        $result[0]['nome'] = ($user->getPessoaEntity())? $user->getPessoaEntity()->getNoPessoa() : $user->getUsername();
        $result[0]['perfil'] = $strPerfil;

        return $result;
    }
}

<?php
return array(
    'service_manager' => array(
        'factories' => array(
            'Publico\\V1\\Rest\\Empresa\\EmpresaResource' => 'Publico\\V1\\Rest\\Empresa\\EmpresaResourceFactory',
            'Publico\\V1\\Rest\\SolicitarAcesso\\SolicitarAcessoResource' => 'Publico\\V1\\Rest\\SolicitarAcesso\\SolicitarAcessoResourceFactory',
            'Publico\\V1\\Rest\\SolicitarAcesso\\SolicitarAcessoService' => 'Publico\\V1\\Rest\\SolicitarAcesso\\SolicitarAcessoServiceFactory',
            'Publico\\V1\\Rest\\Uf\\UfResource' => 'Publico\\V1\\Rest\\Uf\\UfResourceFactory',
            'Publico\\V1\\Rest\\Uf\\UfRepositoryService' => 'Publico\\V1\\Rest\\Uf\\UfRepositoryServiceFactory',
            'Publico\\V1\\Rest\\Empresa\\EmpresaRepositoryService' => 'Publico\\V1\\Rest\\Empresa\\EmpresaRepositoryServiceFactory',
            'Publico\\V1\\Rest\\Municipio\\MunicipioResource' => 'Publico\\V1\\Rest\\Municipio\\MunicipioResourceFactory',
            'Publico\\V1\\Rest\\Municipio\\MunicipioRepositoryService' => 'Publico\\V1\\Rest\\Municipio\\MunicipioRepositoryServiceFactory',
        ),
    ),
    'router' => array(
        'routes' => array(
            'publico.rest.empresa' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/publico/empresa[/:empresa_id]',
                    'defaults' => array(
                        'controller' => 'Publico\\V1\\Rest\\Empresa\\Controller',
                    ),
                ),
            ),
            'publico.rest.solicitar-acesso' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/publico/solicitar-acesso[/:solicitar_acesso_id]',
                    'defaults' => array(
                        'controller' => 'Publico\\V1\\Rest\\SolicitarAcesso\\Controller',
                    ),
                ),
            ),
            'publico.rest.uf' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/publico/uf[/:uf_id]',
                    'defaults' => array(
                        'controller' => 'Publico\\V1\\Rest\\Uf\\Controller',
                    ),
                ),
            ),
            'publico.rest.municipio' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/publico/municipio[/:municipio_id]',
                    'defaults' => array(
                        'controller' => 'Publico\\V1\\Rest\\Municipio\\Controller',
                    ),
                ),
            ),
        ),
    ),
    'zf-versioning' => array(
        'uri' => array(
            0 => 'publico.rest.empresa',
            1 => 'publico.rest.solicitar-acesso',
            2 => 'publico.rest.uf',
            3 => 'publico.rest.municipio',
        ),
    ),
    'zf-rest' => array(
        'Publico\\V1\\Rest\\Empresa\\Controller' => array(
            'listener' => 'Publico\\V1\\Rest\\Empresa\\EmpresaResource',
            'route_name' => 'publico.rest.empresa',
            'route_identifier_name' => 'empresa_id',
            'collection_name' => 'empresa',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Application\\Entity\\AesbeEmpresaEntity',
            'collection_class' => 'Publico\\V1\\Rest\\Empresa\\EmpresaCollection',
            'service_name' => 'Empresa',
        ),
        'Publico\\V1\\Rest\\SolicitarAcesso\\Controller' => array(
            'listener' => 'Publico\\V1\\Rest\\SolicitarAcesso\\SolicitarAcessoResource',
            'route_name' => 'publico.rest.solicitar-acesso',
            'route_identifier_name' => 'solicitar_acesso_id',
            'collection_name' => 'solicitar_acesso',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Publico\\V1\\Rest\\SolicitarAcesso\\SolicitarAcessoEntity',
            'collection_class' => 'Publico\\V1\\Rest\\SolicitarAcesso\\SolicitarAcessoCollection',
            'service_name' => 'SolicitarAcesso',
        ),
        'Publico\\V1\\Rest\\Uf\\Controller' => array(
            'listener' => 'Publico\\V1\\Rest\\Uf\\UfResource',
            'route_name' => 'publico.rest.uf',
            'route_identifier_name' => 'uf_id',
            'collection_name' => 'uf',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Publico\\V1\\Rest\\Uf\\UfEntity',
            'collection_class' => 'Publico\\V1\\Rest\\Uf\\UfCollection',
            'service_name' => 'Uf',
        ),
        'Publico\\V1\\Rest\\Municipio\\Controller' => array(
            'listener' => 'Publico\\V1\\Rest\\Municipio\\MunicipioResource',
            'route_name' => 'publico.rest.municipio',
            'route_identifier_name' => 'municipio_id',
            'collection_name' => 'municipio',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(
                0 => 'noUf',
            ),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Publico\\V1\\Rest\\Municipio\\MunicipioEntity',
            'collection_class' => 'Publico\\V1\\Rest\\Municipio\\MunicipioCollection',
            'service_name' => 'Municipio',
        ),
    ),
    'zf-content-negotiation' => array(
        'controllers' => array(
            'Publico\\V1\\Rest\\Empresa\\Controller' => 'HalJson',
            'Publico\\V1\\Rest\\SolicitarAcesso\\Controller' => 'HalJson',
            'Publico\\V1\\Rest\\Uf\\Controller' => 'HalJson',
            'Publico\\V1\\Rest\\Municipio\\Controller' => 'HalJson',
        ),
        'accept_whitelist' => array(
            'Publico\\V1\\Rest\\Empresa\\Controller' => array(
                0 => 'application/vnd.publico.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Publico\\V1\\Rest\\SolicitarAcesso\\Controller' => array(
                0 => 'application/vnd.publico.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Publico\\V1\\Rest\\Uf\\Controller' => array(
                0 => 'application/vnd.publico.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Publico\\V1\\Rest\\Municipio\\Controller' => array(
                0 => 'application/vnd.publico.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
        ),
        'content_type_whitelist' => array(
            'Publico\\V1\\Rest\\Empresa\\Controller' => array(
                0 => 'application/vnd.publico.v1+json',
                1 => 'application/json',
            ),
            'Publico\\V1\\Rest\\SolicitarAcesso\\Controller' => array(
                0 => 'application/vnd.publico.v1+json',
                1 => 'application/json',
            ),
            'Publico\\V1\\Rest\\Uf\\Controller' => array(
                0 => 'application/vnd.publico.v1+json',
                1 => 'application/json',
            ),
            'Publico\\V1\\Rest\\Municipio\\Controller' => array(
                0 => 'application/vnd.publico.v1+json',
                1 => 'application/json',
            ),
        ),
    ),
    'zf-hal' => array(
        'metadata_map' => array(
            'Publico\\V1\\Rest\\Empresa\\EmpresaEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'publico.rest.empresa',
                'route_identifier_name' => 'empresa_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'Publico\\V1\\Rest\\Empresa\\EmpresaCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'publico.rest.empresa',
                'route_identifier_name' => 'empresa_id',
                'is_collection' => true,
            ),
            'Application\\Entity\\EmpresaEntity' => array(
                'entity_identifier_name' => 'coEmpresa',
                'route_name' => 'publico.rest.empresa',
                'route_identifier_name' => 'empresa_id',
                'hydrator' => 'DoctrineModule\\Stdlib\\Hydrator\\DoctrineObject',
            ),
            'Publico\\V1\\Rest\\SolicitarAcesso\\SolicitarAcessoEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'publico.rest.solicitar-acesso',
                'route_identifier_name' => 'solicitar_acesso_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'Publico\\V1\\Rest\\SolicitarAcesso\\SolicitarAcessoCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'publico.rest.solicitar-acesso',
                'route_identifier_name' => 'solicitar_acesso_id',
                'is_collection' => true,
            ),
            'Publico\\V1\\Rest\\Uf\\UfEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'publico.rest.uf',
                'route_identifier_name' => 'uf_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'Publico\\V1\\Rest\\Uf\\UfCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'publico.rest.uf',
                'route_identifier_name' => 'uf_id',
                'is_collection' => true,
            ),
            'Publico\\V1\\Rest\\Empresa2\\Empresa2Entity' => array(
                'entity_identifier_name' => 'coEmpresa',
                'route_name' => 'publico.rest.empresa2',
                'route_identifier_name' => 'empresa2_id',
                'hydrator' => 'DoctrineModule\\Stdlib\\Hydrator\\DoctrineObject',
            ),
            'Application\\Entity\\AesbeEmpresaEntity' => array(
                'entity_identifier_name' => 'coEmpresa',
                'route_name' => 'publico.rest.empresa',
                'route_identifier_name' => 'empresa_id',
                'hydrator' => 'DoctrineModule\\Stdlib\\Hydrator\\DoctrineObject',
            ),
            'Publico\\V1\\Rest\\Municipio\\MunicipioEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'publico.rest.municipio',
                'route_identifier_name' => 'municipio_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'Publico\\V1\\Rest\\Municipio\\MunicipioCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'publico.rest.municipio',
                'route_identifier_name' => 'municipio_id',
                'is_collection' => true,
            ),
        ),
    ),
    'zf-content-validation' => array(
        'Publico\\V1\\Rest\\SolicitarAcesso\\Controller' => array(
            'input_filter' => 'Publico\\V1\\Rest\\SolicitarAcesso\\Validator',
        ),
    ),
    'input_filter_specs' => array(
        'Publico\\V1\\Rest\\SolicitarAcesso\\Validator' => array(
            0 => array(
                'required' => true,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\NotEmpty',
                        'options' => array(
                            'message' => 'O campo Nome é obrigatório',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                        'options' => array(),
                    ),
                ),
                'name' => 'noPessoa',
                'error_message' => 'O campo Nome é obrigatório',
            ),
            1 => array(
                'required' => true,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\NotEmpty',
                        'options' => array(
                            'message' => 'O campo Email é obrigatório',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                        'options' => array(),
                    ),
                ),
                'name' => 'dsEmail',
                'error_message' => 'O campo Email é obrigatório',
            ),
            2 => array(
                'required' => false,
                'validators' => array(),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                        'options' => array(),
                    ),
                ),
                'name' => 'dsEmailSecundario',
            ),
            3 => array(
                'required' => true,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\NotEmpty',
                        'options' => array(
                            'message' => 'O campo senha é obrigatório',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                        'options' => array(),
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'password',
                'error_message' => 'O campo Senha é obrigatório',
            ),
            4 => array(
                'required' => true,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\NotEmpty',
                        'options' => array(
                            'message' => 'O campo Confirmar sua senha é obrigatório',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                        'options' => array(),
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'passwordConfirm',
                'error_message' => 'O campo Confirmar Senha é obrigatório',
            ),
            5 => array(
                'required' => false,
                'validators' => array(),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\Digits',
                        'options' => array(),
                    ),
                ),
                'name' => 'nuTelefoneComercial',
            ),
            6 => array(
                'required' => false,
                'validators' => array(),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\Digits',
                        'options' => array(),
                    ),
                ),
                'name' => 'nuCelular',
            ),
            7 => array(
                'required' => true,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\NotEmpty',
                        'options' => array(
                            'message' => 'O campo Presidente? é obrigatório',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                        'options' => array(),
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'stPresidente',
                'error_message' => 'O campo Presidente? é obrigatório',
            ),
            8 => array(
                'required' => false,
                'validators' => array(),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\Digits',
                        'options' => array(),
                    ),
                ),
                'name' => 'nuCep',
            ),
            9 => array(
                'required' => false,
                'validators' => array(),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\Digits',
                        'options' => array(),
                    ),
                ),
                'name' => 'coUf',
            ),
            10 => array(
                'required' => false,
                'validators' => array(),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\Digits',
                        'options' => array(),
                    ),
                ),
                'name' => 'coCidade',
            ),
            11 => array(
                'required' => false,
                'validators' => array(),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                        'options' => array(),
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'dsLogradouro',
            ),
            12 => array(
                'required' => false,
                'validators' => array(),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                        'options' => array(),
                    ),
                ),
                'name' => 'nuLogradouro',
            ),
            13 => array(
                'required' => false,
                'validators' => array(),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                        'options' => array(),
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'noBairro',
            ),
            14 => array(
                'required' => false,
                'validators' => array(),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                        'options' => array(),
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'dsComplemento',
            ),
            15 => array(
                'required' => true,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\NotEmpty',
                        'options' => array(
                            'message' => 'O campo Empresa é obrigatório',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\Digits',
                        'options' => array(),
                    ),
                ),
                'name' => 'coEmpresa',
                'error_message' => 'O campo Empresa é obrigatório',
            ),
        ),
    ),
);

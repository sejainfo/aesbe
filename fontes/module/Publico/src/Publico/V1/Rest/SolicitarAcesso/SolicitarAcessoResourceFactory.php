<?php
namespace Publico\V1\Rest\SolicitarAcesso;

class SolicitarAcessoResourceFactory
{
    public function __invoke($services)
    {
        $srvSolicitarAcesso = $services
            ->get('Publico\\V1\\Rest\\SolicitarAcesso\\SolicitarAcessoService');
        return new SolicitarAcessoResource($srvSolicitarAcesso);
    }
}

<?php
namespace Publico\V1\Rest\SolicitarAcesso;

use Commons\Pattern\Service\Impl\ZendServiceLookupManager;
use Zend\Log\Logger;
use Zend\Log\PsrLoggerAdapter;

class SolicitarAcessoServiceFactory
{
    public function __invoke($services)
    {
        $srvMail = $services->get('Application\Service\AesbeMail');
        $lookupManager = new ZendServiceLookupManager($services);
        $logger = new PsrLoggerAdapter(new Logger());
        return new SolicitarAcessoService($lookupManager, $logger, $srvMail);
    }
}

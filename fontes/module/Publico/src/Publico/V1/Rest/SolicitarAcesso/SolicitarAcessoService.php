<?php
namespace Publico\V1\Rest\SolicitarAcesso;

use Application\Service\AbstractService;
use Application\Service\AesbeMail;
use Commons\Pattern\Repository\Impl\Reference;
use Application\Service\ServiceException;

class SolicitarAcessoService extends AbstractService
{

    /**
     * @var AesbeMail
     */
    protected $srvMail;

    /**
     * SolicitarAcessoService constructor.
     */
    public function __construct($lookupManager, $logger, $srvMail)
    {
        parent::__construct($lookupManager, $logger);
        $this->srvMail = $srvMail;

    }

    public function solicitar($data)
    {
        $this->validarSolicitacaoAcesso($data);
        $dados = (array) $data;
        $pessoa = $this->getPessoaFisicaRepositoryService()->salvar($dados);
        $dados['pessoaEntity'] = new Reference(
            'Application\Entity\AesbePessoaFisicaEntity',
            array(
                'coPessoa' => $pessoa->getCoPessoa()
            )
        );
        $usuario = $this->getOauthUsersRepositoryService()->criarUsuario($dados);
//        $this->getPerfilRepositoryService()->adicionarPerfilUsuario($usuario->getCoUsuario(),1);
//        $this->srvMail->sendEmailText(
//            'Parabéns sua solicitação de Acesso foi realizado com sucesso!',
//            'AESBE - Solicitação de Acesso',
//            $data->dsEmail
//        );
    }

    /**
     * Valida regras negociais da solicitação de acesso
     * @param $data
     */
    public function validarSolicitacaoAcesso($data)
    {
        $this->validarEmail($data->dsEmail);
        $this->validarSenha($data);
    }

    /**
     * Valida se o email já esta sendo utilizado
     * @param string $dsEmail
     */
    public function validarEmail($dsEmail)
    {
        $emailCadastrado = $this->getPessoaFisicaRepositoryService()->findBy(array('dsEmail' => $dsEmail));
        if (count($emailCadastrado)) {
            throw new ServiceException($this->getTranslatorService()->translate('E003'));
        }
    }

    /**
     * Valida se a senha e a confirmação da senha são idênticas
     * @param $data
     */
    public function validarSenha($data)
    {
        if ($data->password != $data->passwordConfirm) {
            throw new ServiceException($this->getTranslatorService()->translate('E004'));
        }
    }

    /**
     * @return \Core\V1\Rest\OauthUsers\OauthUsersRepositoryService
     */
    protected function getOauthUsersRepositoryService()
    {
        return $this->getLookupManager()->get('Core\\V1\\Rest\\OauthUsers\\OauthUsersRepositoryService');

    }

    /**
     * @return \Core\V1\Rest\PessoaFisica\PessoaFisicaRepositoryService
     */
    protected function getPessoaFisicaRepositoryService()
    {
        return $this->getLookupManager()->get('Core\\V1\\Rest\\PessoaFisica\\PessoaFisicaRepositoryService');

    }

    /**
     * @return \Core\V1\Rest\Perfil\PerfilRepositoryService
     */
    protected function getPerfilRepositoryService()
    {
        return $this->getLookupManager()->get('Core\\V1\\Rest\\Perfil\\PerfilRepositoryService');

    }

}
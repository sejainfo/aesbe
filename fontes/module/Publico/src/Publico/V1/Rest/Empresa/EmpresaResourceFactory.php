<?php
namespace Publico\V1\Rest\Empresa;

class EmpresaResourceFactory
{
    public function __invoke($services)
    {
        $srvEmpresa = $services
            ->get('Publico\\V1\\Rest\\Empresa\\EmpresaRepositoryService');
        return new EmpresaResource($srvEmpresa);
    }
}

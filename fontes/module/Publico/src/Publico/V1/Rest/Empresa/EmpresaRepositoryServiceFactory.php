<?php
namespace Publico\V1\Rest\Empresa;

use Commons\Pattern\Service\Impl\ZendServiceLookupManager;
use Zend\Log\Logger;
use Zend\Log\PsrLoggerAdapter;

/**
 * Class EmpresaRepositoryServiceFactory
 * @package Core\V1\Rest\Empresa
 */
class EmpresaRepositoryServiceFactory
{
    /**
     * @param $services ServiceManager
     */
    public function __invoke($services)
    {
        /* @var $srvDoctrine \Doctrine\ORM\EntityManager */
        $srvDoctrine = $services->get('Doctrine\ORM\EntityManager');

        $repo = $srvDoctrine->getRepository('Application\Entity\AesbeEmpresaEntity');
        $lookupManager = new ZendServiceLookupManager($services);
        $logger = new PsrLoggerAdapter(new Logger());

        return new EmpresaRepositoryService($repo, $lookupManager, $logger);
    }
}
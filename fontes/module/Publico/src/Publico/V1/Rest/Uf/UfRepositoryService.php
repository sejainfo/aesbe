<?php

namespace Publico\V1\Rest\Uf;

use Application\Service\AbstractRepositoryService;

/**
 * Class UfRepositoryService
 * @package Publico\V1\Rest\Uf
 */
class UfRepositoryService extends AbstractRepositoryService
{

    public function listaUf()
    {
        $result = array();
        $i = 0;

        $ufs = $this->findAll();

        foreach ($ufs as $uf) {
            $result[$i]['coUf'] = $uf->getCoUf();
            $result[$i]['noUf'] = $uf->getNoUf();
            $result[$i]['sgUf'] = $uf->getSgUf();
            $i++;
        }

        return $result;
    }

}
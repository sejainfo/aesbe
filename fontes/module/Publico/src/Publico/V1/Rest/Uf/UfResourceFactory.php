<?php
namespace Publico\V1\Rest\Uf;

class UfResourceFactory
{
    public function __invoke($services)
    {
        $srvUf = $services->get('Publico\V1\Rest\Uf\UfRepositoryService');
        return new UfResource($srvUf);
    }
}

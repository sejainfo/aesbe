<?php

namespace Publico\V1\Rest\Uf;

use Commons\Pattern\Service\Impl\ZendServiceLookupManager;
use Zend\Log\Logger;
use Zend\Log\PsrLoggerAdapter;

/**
 * Class UfRepositoryServiceFactory
 * @package Publico\V1\Rest\Uf
 */
class UfRepositoryServiceFactory
{
    /**
     * @param $services
     * @return ModuloRepositoryService
     */
    public function __invoke($services)
    {
        /* @var $srvDoctrine \Doctrine\ORM\EntityManager */
        $srvDoctrine = $services->get('Doctrine\ORM\EntityManager');

        $repo = $srvDoctrine->getRepository('Application\Entity\AesbeUfEntity');
        $lookupManager = new ZendServiceLookupManager($services);
        $logger = new PsrLoggerAdapter(new Logger());

        return new UfRepositoryService($repo, $lookupManager, $logger);

    }
}
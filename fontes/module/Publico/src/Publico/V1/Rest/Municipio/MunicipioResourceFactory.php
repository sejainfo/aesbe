<?php
namespace Publico\V1\Rest\Municipio;

use Zend\ServiceManager\ServiceManager;

class MunicipioResourceFactory
{
    /**
     * @param $servicesv ServiceManager
     * @return MunicipioResource
     */
    public function __invoke($services)
    {
        $srvMunicipio = $services->get('Publico\V1\Rest\Municipio\MunicipioRepositoryService');
        return new MunicipioResource($srvMunicipio);
    }
}

<?php

namespace Publico\V1\Rest\Municipio;

use Commons\Pattern\Service\Impl\ZendServiceLookupManager;
use Zend\Log\Logger;
use Zend\Log\PsrLoggerAdapter;

/**
 * Class MunicipoRepositoryServiceFactory
 * @package Publico\V1\Rest\Municipio
 */
class MunicipioRepositoryServiceFactory
{
    /**
     * @param $services
     * @return ModuloRepositoryService
     */
    public function __invoke($services)
    {
        /* @var $srvDoctrine \Doctrine\ORM\EntityManager */
        $srvDoctrine = $services->get('Doctrine\ORM\EntityManager');

        $repo = $srvDoctrine->getRepository('Application\Entity\AesbeMunicipioEntity');
        $lookupManager = new ZendServiceLookupManager($services);
        $logger = new PsrLoggerAdapter(new Logger());

        return new MunicipioRepositoryService($repo, $lookupManager, $logger);

    }
}
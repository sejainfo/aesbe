<?php

namespace Publico\V1\Rest\Municipio;

use Application\Service\AbstractRepositoryService;

/**
 * Class MunicipoRepositoryService
 * @package Publico\V1\Rest\Municipio
 */
class MunicipioRepositoryService extends AbstractRepositoryService
{
    /**
     * @param $uf string
     */
    public function getListByUf($uf)
    {
        $result = array();
        $i = 0;
        $municipios = $this->findBy(array('sgUf' => $uf));

        foreach ($municipios as $municipio) {
            $result[$i]['coMunicipio'] = $municipio->getCoMunicipio();
            $result[$i]['noMunicipio'] = $municipio->getNoMunicipio();
            $result[$i]['sgUf'] = $municipio->getSgUf();
            $i++;
        }

        return $result;
    }

}
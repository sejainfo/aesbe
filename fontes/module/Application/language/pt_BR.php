<?php

return array(
    /* -------------------------------- */
    // MENSAGENS DO SISTEMA
    /* -------------------------------- */
    'MSG001' => 'O %s é inválido(a).',

    'MSG999' => 'Erro ao excecutar a operação.',
    /* -------------------------------- */
    // MENSAGENS DE SUCESSO
    /* -------------------------------- */
    'S001' => '',
    'MSG058' => 'As informações foram gravadas com sucesso. ',
    'MSG075' => 'A informação foi excluída com sucesso.',
    'MSG142' => 'Senha alterada com sucesso.',
    /* -------------------------------- */
    // MENSAGENS INFORMATIVAS
    /* -------------------------------- */
    'I001' => 'O campo %s é inválido.',
    /* -------------------------------- */
    // MENSAGENS DE ERRO
    /* -------------------------------- */
	'E001' => 'O campo <span class="campoErro">%s</span> é obrigatório.',
	'E002' => '%s é inválido.',
	'E003' => 'Email em uso.',
	'E004' => 'A senha e a confirmação não são idênticas.',
    /* -------------------------------- */
    // MENSAGENS gerais
    /* -------------------------------- */
    "Value is required and can't be empty" => 'O campo %s é de preenchimento obrigatório.',
    'The input does not appear to be a valid date' => 'A data informada é inválida.',
    'The input must contain only digits' => 'O campo %s pode ter somente números.',
    'Captcha value is wrong' => 'O Código de verificação é inválido(a).'
);

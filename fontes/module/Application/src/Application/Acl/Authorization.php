<?php

namespace Application\Acl;

use Core\V1\Rest\Perfil\PerfilRepositoryService;
use Zend\Permissions\Acl\Acl;
use Zend\Permissions\Acl\Role\GenericRole as Role;
use Zend\Permissions\Acl\Resource\GenericResource as Resource;
use Doctrine\ORM\EntityManager;
use Core\V1\Rest\Funcionalidade\FuncionalidadeRepositoryService;
use Core\V1\Rest\Modulo\ModuloRepositoryService;

/**
 * Description of Authorization
 *
 */
class Authorization extends Acl
{
    /**
     * @var PerfilRepositoryService
     */
    private $srvPerifl;
    /**
     * @var ModuloRepositoryService
     */
    private $srvModulo;

    /**
     * @var FuncionalidadeRepositoryService
     */
    private $srvFunc;

    /**
     *
     * @param EntityManager $entitym
     * @param string $coligada
     */
    public function __construct(
        PerfilRepositoryService $srvPerfil,
        ModuloRepositoryService $srvModulo,
        FuncionalidadeRepositoryService $srvFunc
    ) {
        $this->srvPerifl = $srvPerfil;
        $this->srvModulo = $srvModulo;
        $this->srvFunc = $srvFunc;
        $this->addResources();
        $this->addRoles();
        $this->setPrivileges();
    }

    /**
     *
     * @return
     */
    protected function addRoles()
    {
        $allPerfil = $this->srvPerifl->findAll();
        if ($allPerfil == null) {
            return null;
        }
        foreach ($allPerfil as $perfil) {
            $this->addRole(new Role($perfil->getCoPerfil()));
        }
    }

    protected function addResources()
    {
        $allModulos = $this->srvModulo->findAll();
        if ($allModulos == null) {
            return null;
        }
        foreach ($allModulos as $modulo) {
            $this->addResource(new Resource($modulo->getNoModulo()));
        }
    }

    public function setPrivileges()
    {
        $allFuncionalidades = $this->srvFunc->findAll();

        if (!$allFuncionalidades) {
            return null;
        }
        foreach ($allFuncionalidades as $funcionalidade) {
            foreach ($funcionalidade->getCoPerfil() as $perfil) {
                $this->allow(
                    $perfil->getCoPerfil(), $funcionalidade->getModuloEntity()->getNoModulo(),
                    $funcionalidade->getDsController().$funcionalidade->getDsMetodoHttp()
                );
            }
        }
    }
}


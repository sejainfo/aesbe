<?php

namespace Application\Acl;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Db\Adapter\Driver\Pdo\Pdo as PdoDriver;
use ZF\MvcAuth\Factory\OAuth2ServerFactory;

class OAuth2AdapterFactory implements FactoryInterface
{
    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return OAuth2Adapter
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $config = $serviceLocator->get('Config');
        $configOaut = $config['zf-mvc-auth']['authentication']['adapters']['oauthadapter']['storage'];
//
//        if (!isset($config['zf-oauth2']['db']) || empty($config['zf-oauth2']['db'])) {
////            throw new \Exception\RuntimeException(
//            throw new \Exception(
//                'The database configuration [\'zf-oauth2\'][\'db\'] for OAuth2 is missing'
//            );
//        }
//
//        $username = isset($config['zf-mvc-auth']['db']['username']) ? $config['zf-oauth2']['db']['username'] : null;
//        $password = isset($config['zf-oauth2']['db']['password']) ? $config['zf-oauth2']['db']['password'] : null;
//        $options  = isset($config['zf-oauth2']['db']['options']) ? $config['zf-oauth2']['db']['options'] : [];
//
//        $oauth2ServerConfig = [];
//        if (isset($config['zf-oauth2']['storage_settings']) && is_array($config['zf-oauth2']['storage_settings'])) {
//            $oauth2ServerConfig = $config['zf-oauth2']['storage_settings'];
//        }

        return new OAuth2Adapter([
            'dsn'      => $configOaut['dsn'],
            'username' => $configOaut['username'],
            'password' => $configOaut['password'],
            'options'  => array(),
        ], array());
    }
}
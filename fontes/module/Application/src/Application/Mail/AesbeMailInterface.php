<?php
namespace Application\Mail;

/**
 * 
 * @author Reginaldo Azevedo Junior
 *
 */
interface AesbeMailInterface
{
    /**
     * 
     * @param string $html
     * @param string $subject
     * @param string|array $address
     */
    public function sendEmail($html, $subject, $address);
    
}


<?php
namespace Application\Mail;

use \Zend\Mail\Message;
use \Zend\Mail\Transport\Sendmail;
use Zend\Mime\Part as MimePart;
use Zend\Mime\Message as MimeMessage;


/**
 * Class AbstractSendMail
 * @package Application\Mail
 */
abstract class AbstractSendMail implements AesbeMailInterface
{
    
    /**
     * 
     * @var string
     */
    const ENCONDING = "UTF-8";
    
    /**
     * 
     * @var string
     */
    const FROM = "aesbe@aesbe.org.br";
    
    /**
     * 
     * @var string
     */
    const FROM_ALIAS = "Associação das Empresas de Saneamento Básico Estaduais";
    
    /**
     * 
     * @var string
     */
    const TYPE_HTML = "text/html";
    
    /**
     * @var \Zend\Mail\Transport\Sendmail
     */
    protected $transport;
    
    
    public function __construct()
    {
        $this->transport = new Sendmail();
    }
    
    /**
     * @param string $html
     * @param string $subject
     * @param array $addresses
     * @param array|null $attachment
     * @return Message
     */
    protected function mountMail($html, $subject, array $addresses, array $attachment = null)
    {
        $mail = new Message();
        $html = new MimePart($html);
        $body = new MimeMessage();
        
        $html->type = "text/html";
        $arrayPart = array(
            $html
        );
        if (!empty($attachment)) {
            foreach ($attachment as $val) {
                $arrayPart[] = $val;
            }

        }
        $body->setParts($arrayPart);
        $mail->setBody($body);
        $mail->setEncoding(self::ENCONDING);
        $mail->setFrom(self::FROM, self::FROM_ALIAS);

        foreach ($addresses as $address) {
            $mail->addTo($address);
        }

        $mail->setSubject($subject);
        $html->type = self::TYPE_HTML;
        
        return $mail;
        
    }
    
        
}


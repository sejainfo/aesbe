<?php


namespace Application\Util;

class FormataNumero
{

    /**
     * Metodo responsavel em formatar numero acresentando 2 casas decimais para valores que nao tem casa
     * decimais ou retira o ponto para quem tem casa decimal
     *
     * @param float $valor
     * @return string null
     */
    public function formataNumeroParaAgenteFinanceiro($valor)
    {
        if (empty($valor)) {
            return null;
        }
        if (strpos($valor, '.') === false) {
            $valor = $valor . '00';
        } else {
            $valor = str_replace('.', '', $valor);
        }

        return $valor;
    }

    /**
     * Formatar valor numerico para NUMBER do Oracle
     *
     * @param float|int $valor
     * @return float int
     */
    public static function formatarFloatBanco($valor)
    {
        $valor = str_replace('.', '', $valor);
        $valor = str_replace(',', '.', $valor);
        return $valor;
    }

    /**
     * Formata um Número para Real
     *
     * @param float $valor
     * @param boolean $semCifrao
     * @param boolean $semDivisaoMilhares
     * @return float
     */
    public static function formatarParaReal($valor, $semCifrao = false, $semDivisaoMilhares = false)
    {
        if ($semDivisaoMilhares) {
            $valor = number_format($valor, 2, ",", "");
        } else {
            $valor = number_format($valor, 2, ",", ".");
        }

        if (!$semCifrao) {
            $valor = "R$" . $valor;
        }
        return $valor;
    }

    /**
     * Exibe a string de um texto por número por extenso
     *
     * @param number $numero
     * @return string
     */
    public static function valorPorExtenso($numero = 0)
    {

        $arrValores = self::retornaArrayValores();

        // Separação de reais e centavos
        $n = explode(".", $numero);
        // A construção é dividida por centenas, e a primeira centena sempre deve ter 3 dígitos.
        $qnt = ceil(strlen($n[0]) / 3);
        $qnt_digitos = ceil(strlen($n[0]) / 3) * 3;
        $u = sprintf("%0{$qnt_digitos}s", $n[0]);
        $inteiros = array();


        if ($u != '000') {
            // Constrói cada centena, da primeira à última.
            $inteiros = self::montaInteiros($u, $qnt, $arrValores);
        }
        $centavos = '';
        if (isset($n[1]) && $n[1] != '00') {
            // Caso os centavos venham escritos com apenas uma unidade (".1" invés de ".10")
            if (strlen($n[1]) == 1) {
                $n[1] .= '0';
            }
            $rest = $n[1];
            $dezena = substr($rest, - 2, 1);
            if ($arrValores['Dezenas'][$dezena]) {
                $centavos .= " E " . $arrValores['Dezenas'][$dezena];
            }
            $unid = substr($rest, - 2);
            if ($unid < 20) {
                $unid = substr($rest, - 2);
                if ($arrValores['unidades'][(int) $unid]) {
                    $centavos .= " E " . $arrValores['unidades'][(int) $unid];
                }
            } else {
                $unid = substr($rest, - 1);
                if ($arrValores['unidades'][$unid]) {
                    $centavos .= " E " . $arrValores['unidades'][$unid];
                }
            }
            if ($rest == "01") {
                $centavos .= " CENTAVO";
            } elseif ($centavos) {
                $centavos .= " CENTAVOS";
            }
        }
        $parteFinal = array_pop($inteiros);
        // Quando a última centena é 000, a moeda aparece sozinha e deve ser ligada à centena anterior.
        if ($parteFinal == " REAIS" || $parteFinal == " REAL") {
            // Ocorre apenas em números iguais a 10^(6+)
            if (substr($u, - 6) == '000000') {
                $parteFinal = " DE " . $parteFinal;
            }
            $parteFinal = array_pop($inteiros) . $parteFinal;
        }
        // Ocorre apenas em números maiores que 10^3 que não são iguais a 10^(6+)
        if ((bool) $inteiros) {
            $parteFinal = " E " . $parteFinal;
        }
        $parteInicial = implode(", ", $inteiros);
        $texto = $parteInicial . $parteFinal . $centavos;

        return $texto;
    }

    private static function montaInteiros($u, $qnt, $arrValores)
    {
        for ($i = $qnt; $i > 0; $i --) {

            $nfinal = '';
            $rest = substr($u, - 3 * $i, 3);
            if ($rest) {
                $centena = substr($rest, - 3, 1);
                if ($arrValores['Centenas'][$centena]) {
                    if ($nfinal) {
                        $nfinal .= " E ";
                    }
                    $nfinal = $arrValores['Centenas'][$centena];
                }
                $dezena = substr($rest, - 2, 1);
                if ($arrValores['Dezenas'][$dezena]) {
                    if ($nfinal) {
                        $nfinal .= " E ";
                    }
                    $nfinal .= $arrValores['Dezenas'][$dezena];
                }
                $unid = substr($rest, - 2);
                if ($unid < 20) {
                    $unid = substr($rest, - 2);
                    if ($arrValores['unidades'][(int) $unid]) {
                        if ($nfinal) {
                            $nfinal .= " E ";
                        }
                        $nfinal .= $arrValores['unidades'][(int) $unid];
                    }
                } else {
                    $unid = substr($rest, - 1);
                    if ($arrValores['unidades'][$unid]) {
                        if ($nfinal) {
                            $nfinal .= " E ";
                        }
                        $nfinal .= $arrValores['unidades'][$unid];
                    }
                }
                // Centenas particulares
                if ($rest == "100") {
                    $nfinal = "CEM";
                }

                if ($rest == "001") {
                    $nfinal .= " " . $arrValores['unidSing'][$i];
                } elseif (($rest != "000") || ($i == 1)) {
                    $nfinal .= " " . $arrValores['unidade'][$i];
                }
                // Apenas centenas que serão escritas vão no array $inteiros
                if ($nfinal != "") {
                    $inteiros[] = $nfinal;
                }
            }
        }
        return $inteiros;
    }

    public static function retornaArrayValores()
    {
        $unidades = array(
            "",
            "UM",
            "DOIS",
            "TRÊS",
            "QUATRO",
            "CINCO",
            "SEIS",
            "SETE",
            "OITO",
            "NOVE",
            "DEZ",
            "ONZE",
            "DOZE",
            "TREZE",
            "QUATORZE",
            "QUINZE",
            "DEZESSEIS",
            "DEZESSETE",
            "DEZOITO",
            "DEZENOVE"
        );
        $Dezenas = array(
            "",
            "",
            "VINTE",
            "TRINTA",
            "QUARENTA",
            "CINQUENTA",
            "SESSENTA",
            "SETENTA",
            "OITENTA",
            "NOVENTA"
        );
        $Centenas = array(
            "",
            "CENTO",
            "DUZENTOS",
            "TREZENTOS",
            "QUATROCENTOS",
            "QUINHENTOS",
            "SEISCENTOS",
            "SETECENTOS",
            "OITOCENTOS",
            "NOVECENTOS"
        );
        $unidade = array(
            "",
            "REAIS",
            "MIL",
            "MILHÕES",
            "BILHÕES",
            "TRILHÕES"
        );
        $unidSing = array(
            "",
            "REAL",
            "MIL",
            "MILHÃO",
            "BILHÃO",
            "TRILHÃO"
        );

        return array(
            'unidades' => $unidades,
            'Dezenas' => $Dezenas,
            'Centenas' => $Centenas,
            'unidade' => $unidade,
            'unidSing' => $unidSing,
        );
    }

    /**
     * Formata valor retirando ponto e adicionando virgula.
     * @param string $val
     */
    public function formataValoresPontoVirgula($val)
    {
        if (floatval($val) == '0') {
            return '0';
        }
        return number_format(floatval($val), 2, ',', '.');
    }

}

<?php

namespace Application\Entity;

use Commons\Pattern\Repository\Impl\SimpleEntityRepository;


class OauthUsersRepository extends SimpleEntityRepository
{
    /**
     * @param $username
     * @return array
     */
    public function listarMenus($username)
    {
        $qb = $this->createQueryBuilder('O');
        $query = $qb->select('M.coModulo', 'M.noModuloVisivel', 'M.dsIcon', 'M.dsUri', 'F.noAgrupador',
            'F.noFuncionalidade', 'F.noFuncionalidadeVisivel', 'F.coFuncionalidade')
            ->innerJoin('O.perfis', 'P')
            ->innerJoin('P.funcionalidades', 'F')
            ->innerJoin('F.moduloEntity', 'M')
            ->where($qb->expr()->eq('M.stAtivo', 1))
            ->andWhere($qb->expr()->eq('M.stModuloVisivel', 1))
            ->andWhere($qb->expr()->eq('F.stAtivo', 1))
            ->andWhere($qb->expr()->eq('O.username', ':username'))
            ->setParameter('username', $username)
            ->orderBy('M.noModuloVisivel', 'ASC')
            ->addOrderBy('F.noAgrupador', 'ASC');
        return $query->getQuery()->getResult();
    }


    public function listarUsuariosPendentes($coEmpresa = null)
    {
        $qb = $this->createQueryBuilder('O');
        $query = $qb->select('P.noPessoa', 'O.coUsuario', 'E.noEmpresa', 'P.dsEmail',
            'P.stAtivo', 'PE.noPerfil', 'P.coPessoa')
            ->innerJoin('O.pessoaEntity', 'P')
            ->leftJoin('O.perfis', 'PE')
            ->innerJoin('P.empresaEntity', 'E');
        if (!empty($coEmpresa)) {
            $query->where($qb->expr()->eq('P.empresaEntity', ':coEmpresa'))
                ->setParameter('coEmpresa', $coEmpresa);
        }
        return $query->getQuery()->getResult();
    }



}

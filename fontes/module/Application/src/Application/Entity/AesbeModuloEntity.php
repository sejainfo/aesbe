<?php

namespace Application\Entity;

use Commons\Pattern\Entity\Impl\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * AesbeModuloEntity
 *
 * @ORM\Table(name="tb_aesbe_modulo")
 * @ORM\Entity(repositoryClass="Commons\Pattern\Repository\Impl\SimpleEntityRepository")
 */
class AesbeModuloEntity extends AbstractEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="co_modulo", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $coModulo;

    /**
     * @var string
     *
     * @ORM\Column(name="no_modulo", type="string", length=45, nullable=false)
     */
    private $noModulo;

    /**
     * @var string
     *
     * @ORM\Column(name="no_modulo_visivel", type="string", length=45, nullable=false)
     */
    private $noModuloVisivel;

    /**
     * @var string
     *
     * @ORM\Column(name="st_modulo_visivel", type="boolean", nullable=false)
     */
    private $stModuloVisivel;

    /**
     * @var string
     *
     * @ORM\Column(name="st_ativo", type="boolean", nullable=false)
     */
    private $stAtivo;

    /**
     * @var string
     *
     * @ORM\Column(name="ds_icon", type="string", length=45, nullable=false)
     */
    private $dsIcon;

    /**
     * @var string
     *
     * @ORM\Column(name="ds_uri", type="string", length=45, nullable=false)
     */
    private $dsUri;

    /**
     * @return int
     */
    public function getCoModulo()
    {
        return $this->coModulo;
    }

    /**
     * @param int $coModulo
     * @return AesbeModuloEntity
     */
    public function setCoModulo($coModulo)
    {
        $this->coModulo = $coModulo;
        return $this;
    }

    /**
     * @return string
     */
    public function getNoModulo()
    {
        return $this->noModulo;
    }

    /**
     * @param string $noModulo
     * @return AesbeModuloEntity
     */
    public function setNoModulo($noModulo)
    {
        $this->noModulo = $noModulo;
        return $this;
    }

    /**
     * @return string
     */
    public function getNoModuloVisivel()
    {
        return $this->noModuloVisivel;
    }

    /**
     * @param string $noModuloVisivel
     * @return AesbeModuloEntity
     */
    public function setNoModuloVisivel($noModuloVisivel)
    {
        $this->noModuloVisivel = $noModuloVisivel;
        return $this;
    }

    /**
     * @return string
     */
    public function getStModuloVisivel()
    {
        return $this->stModuloVisivel;
    }

    /**
     * @param string $stModuloVisivel
     * @return AesbeModuloEntity
     */
    public function setStModuloVisivel($stModuloVisivel)
    {
        $this->stModuloVisivel = $stModuloVisivel;
        return $this;
    }

    /**
     * @return string
     */
    public function getStAtivo()
    {
        return $this->stAtivo;
    }

    /**
     * @param string $stAtivo
     * @return AesbeModuloEntity
     */
    public function setStAtivo($stAtivo)
    {
        $this->stAtivo = $stAtivo;
        return $this;
    }

    /**
     * @return string
     */
    public function getDsIcon()
    {
        return $this->dsIcon;
    }

    /**
     * @param string $dsIcon
     * @return AesbeModuloEntity
     */
    public function setDsIcon($dsIcon)
    {
        $this->dsIcon = $dsIcon;
        return $this;
    }

    /**
     * @return string
     */
    public function getDsUri()
    {
        return $this->dsUri;
    }

    /**
     * @param string $dsUri
     * @return AesbeModuloEntity
     */
    public function setDsUri($dsUri)
    {
        $this->dsUri = $dsUri;
        return $this;
    }





}
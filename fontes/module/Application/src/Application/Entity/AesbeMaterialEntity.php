<?php

namespace Application\Entity;

use Commons\Pattern\Entity\Impl\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;
use Zend\Stdlib\Hydrator;


/**
 * TbAesbeMaterial
 *
 * @ORM\Table(name="tb_aesbe_material", indexes={@ORM\Index(name="fk_tb_aesbe_produto_tb_aesbe_unidade_medida1_idx", columns={"co_unidade_medida"}), @ORM\Index(name="fk_tb_aesbe_produto_tb_aesbe_esp_classe1_idx", columns={"co_esp_classe"}), @ORM\Index(name="fk_tb_aesbe_produto_tb_aesbe_esp_serie1_idx", columns={"co_esp_serie"}), @ORM\Index(name="fk_tb_aesbe_produto_tb_aesbe_descricao1_idx", columns={"co_descricao"})})
 * @ORM\Entity(repositoryClass="Application\Entity\AesbeMaterialRepository")

 */
class AesbeMaterialEntity extends AbstractEntity

{

    /**
     * @var integer
     *
     * @ORM\Column(name="co_material", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $coMaterial;

    /**
     * @var string
     *
     * @ORM\Column(name="no_basico", type="string", length=100, nullable=false)
     */
    private $noBasico;

    /**
     * @var float
     *
     * @ORM\Column(name="nu_sabesp", type="float", precision=10, scale=0, nullable=true)
     */
    private $nuSabesp;

    /**
     * @var \Application\Entity\AesbeEspSerieEntity
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\AesbeEspSerieEntity")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="co_esp_serie", referencedColumnName="co_esp_serie", nullable=true)
     * })
     */
    private $espSerieEntity;

    /**
     * @var \Application\Entity\AesbeEspClasseEntity
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\AesbeEspClasseEntity")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="co_esp_classe", referencedColumnName="co_esp_classe", nullable=true)
     * })
     */
    private $espClasseEntity;

    /**
     * @var \Application\Entity\AesbeDescricaoEntity
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\AesbeDescricaoEntity")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="co_descricao", referencedColumnName="co_descricao", nullable=true)
     * })
     */
    private $descricaoEntity;

    /**
     * @var \Application\Entity\AesbeUnidadeMedidaEntity
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\AesbeUnidadeMedidaEntity")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="co_unidade_medida", referencedColumnName="co_unidade_medida", nullable=false)
     * })
     */
    private $unidadeMedidaEntity;

    /**
     * @return int
     */
    public function getCoMaterial()
    {
        return $this->coMaterial;
    }

    /**
     * @param int $coMaterial
     * @return AesbeMaterialEntity
     */
    public function setCoMaterial($coMaterial)
    {
        $this->coMaterial = $coMaterial;
        return $this;
    }

    /**
     * @return string
     */
    public function getNoBasico()
    {
        return $this->noBasico;
    }

    /**
     * @param string $noBasico
     * @return AesbeMaterialEntity
     */
    public function setNoBasico($noBasico)
    {
        $this->noBasico = $noBasico;
        return $this;
    }

    /**
     * @return float
     */
    public function getNuSabesp()
    {
        return $this->nuSabesp;
    }

    /**
     * @param float $nuSabesp
     * @return AesbeMaterialEntity
     */
    public function setNuSabesp($nuSabesp)
    {
        $this->nuSabesp = $nuSabesp;
        return $this;
    }

    /**
     * @return AesbeEspSerieEntity
     */
    public function getEspSerieEntity()
    {
        return $this->espSerieEntity;
    }

    /**
     * @param AesbeEspSerieEntity $espSerieEntity
     * @return AesbeMaterialEntity
     */
    public function setEspSerieEntity($espSerieEntity)
    {
        $this->espSerieEntity = $espSerieEntity;
        return $this;
    }

    /**
     * @return AesbeEspClasseEntity
     */
    public function getEspClasseEntity()
    {
        return $this->espClasseEntity;
    }

    /**
     * @param AesbeEspClasseEntity $espClasseEntity
     * @return AesbeMaterialEntity
     */
    public function setEspClasseEntity($espClasseEntity)
    {
        $this->espClasseEntity = $espClasseEntity;
        return $this;
    }

    /**
     * @return AesbeDescricaoEntity
     */
    public function getDescricaoEntity()
    {
        return $this->descricaoEntity;
    }

    /**
     * @param AesbeDescricaoEntity $descricaoEntity
     * @return AesbeMaterialEntity
     */
    public function setDescricaoEntity($descricaoEntity)
    {
        $this->descricaoEntity = $descricaoEntity;
        return $this;
    }

    /**
     * @return AesbeUnidadeMedidaEntity
     */
    public function getUnidadeMedidaEntity()
    {
        return $this->unidadeMedidaEntity;
    }

    /**
     * @param AesbeUnidadeMedidaEntity $unidadeMedidaEntity
     * @return AesbeMaterialEntity
     */
    public function setUnidadeMedidaEntity($unidadeMedidaEntity)
    {
        $this->unidadeMedidaEntity = $unidadeMedidaEntity;
        return $this;
    }
}

<?php

namespace Application\Entity;

use Commons\Pattern\Entity\Impl\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * AesbeEmpresaEntity
 *
 * @ORM\Table(name="tb_aesbe_empresa", indexes={@ORM\Index(name="fk_tb_aesbe_empresa_tb_aesbe_uf1_idx", columns={"co_uf"})})
 * @ORM\Entity(repositoryClass="Commons\Pattern\Repository\Impl\SimpleEntityRepository")
 */
class AesbeEmpresaEntity extends AbstractEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="co_empresa", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $coEmpresa;

    /**
     * @var string
     *
     * @ORM\Column(name="no_empresa", type="string", length=100, nullable=false)
     */
    private $noEmpresa;

    /**
     * @var string
     *
     * @ORM\Column(name="sg_empresa", type="string", length=45, nullable=true)
     */
    private $sgEmpresa;

    /**
     * @var string
     *
     * @ORM\Column(name="ds_sitio", type="string", length=100, nullable=true)
     */
    private $dsSitio;

    /**
     * @var string
     *
     * @ORM\Column(name="nu_cep", type="string", length=10, nullable=true)
     */
    private $nuCep;

    /**
     * @var string
     *
     * @ORM\Column(name="ds_logradouro", type="string", length=100, nullable=true)
     */
    private $dsLogradouro;

    /**
     * @var string
     *
     * @ORM\Column(name="no_bairro", type="string", length=100, nullable=true)
     */
    private $noBairro;

    /**
     * @var string
     *
     * @ORM\Column(name="nu_logradouro", type="string", length=10, nullable=true)
     */
    private $nuLogradouro;

    /**
     * @var string
     *
     * @ORM\Column(name="ds_complemento", type="string", length=30, nullable=true)
     */
    private $dsComplemento;

    /**
     * @var \Application\Entity\AesbeUfEntity
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\AesbeUfEntity")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="co_uf", referencedColumnName="co_uf")
     * })
     */
    private $coUf;

    /**
     * @return int
     */
    public function getCoEmpresa()
    {
        return $this->coEmpresa;
    }

    /**
     * @param int $coEmpresa
     * @return AesbeEmpresaEntity
     */
    public function setCoEmpresa($coEmpresa)
    {
        $this->coEmpresa = $coEmpresa;
        return $this;
    }

    /**
     * @return string
     */
    public function getNoEmpresa()
    {
        return $this->noEmpresa;
    }

    /**
     * @param string $noEmpresa
     * @return AesbeEmpresaEntity
     */
    public function setNoEmpresa($noEmpresa)
    {
        $this->noEmpresa = $noEmpresa;
        return $this;
    }

    /**
     * @return string
     */
    public function getSgEmpresa()
    {
        return $this->sgEmpresa;
    }

    /**
     * @param string $sgEmpresa
     * @return AesbeEmpresaEntity
     */
    public function setSgEmpresa($sgEmpresa)
    {
        $this->sgEmpresa = $sgEmpresa;
        return $this;
    }

    /**
     * @return string
     */
    public function getDsSitio()
    {
        return $this->dsSitio;
    }

    /**
     * @param string $dsSitio
     * @return AesbeEmpresaEntity
     */
    public function setDsSitio($dsSitio)
    {
        $this->dsSitio = $dsSitio;
        return $this;
    }

    /**
     * @return string
     */
    public function getNuCep()
    {
        return $this->nuCep;
    }

    /**
     * @param string $nuCep
     * @return AesbeEmpresaEntity
     */
    public function setNuCep($nuCep)
    {
        $this->nuCep = $nuCep;
        return $this;
    }

    /**
     * @return string
     */
    public function getDsLogradouro()
    {
        return $this->dsLogradouro;
    }

    /**
     * @param string $dsLogradouro
     * @return AesbeEmpresaEntity
     */
    public function setDsLogradouro($dsLogradouro)
    {
        $this->dsLogradouro = $dsLogradouro;
        return $this;
    }

    /**
     * @return string
     */
    public function getNoBairro()
    {
        return $this->noBairro;
    }

    /**
     * @param string $noBairro
     * @return AesbeEmpresaEntity
     */
    public function setNoBairro($noBairro)
    {
        $this->noBairro = $noBairro;
        return $this;
    }

    /**
     * @return string
     */
    public function getNuLogradouro()
    {
        return $this->nuLogradouro;
    }

    /**
     * @param string $nuLogradouro
     * @return AesbeEmpresaEntity
     */
    public function setNuLogradouro($nuLogradouro)
    {
        $this->nuLogradouro = $nuLogradouro;
        return $this;
    }

    /**
     * @return string
     */
    public function getDsComplemento()
    {
        return $this->dsComplemento;
    }

    /**
     * @param string $dsComplemento
     * @return AesbeEmpresaEntity
     */
    public function setDsComplemento($dsComplemento)
    {
        $this->dsComplemento = $dsComplemento;
        return $this;
    }

    /**
     * @return AesbeUfEntity
     */
    public function getCoUf()
    {
        return $this->coUf;
    }

    /**
     * @param AesbeUfEntity $coUf
     * @return AesbeEmpresaEntity
     */
    public function setCoUf($coUf)
    {
        $this->coUf = $coUf;
        return $this;
    }
}

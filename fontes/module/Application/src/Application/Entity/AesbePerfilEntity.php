<?php

namespace Application\Entity;

use Commons\Pattern\Entity\Impl\AbstractEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * AesbePerfilEntity
 *
 * @ORM\Table(name="tb_aesbe_perfil")
 * @ORM\Entity(repositoryClass="Application\Entity\AesbePerfilRepository")
 */
class AesbePerfilEntity extends AbstractEntity
{
    const SG_PERFIL_ADMIN = 'Admin';
    /**
     * @var integer
     *
     * @ORM\Column(name="co_perfil", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $coPerfil;

    /**
     * @var string
     *
     * @ORM\Column(name="no_perfil", type="string", length=45, nullable=false)
     */
    private $noPerfil;

    /**
     * @var string
     *
     * @ORM\Column(name="sg_perfil", type="string", length=45, nullable=false)
     */
    private $sgPerfil;



    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Application\Entity\AesbeFuncionalidadeEntity", inversedBy="coPerfil")
     * @ORM\JoinTable(name="tb_aesbe_perfil_funcionalidade",
     *   joinColumns={
     *     @ORM\JoinColumn(name="co_perfil", referencedColumnName="co_perfil")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="co_funcionalidade", referencedColumnName="co_funcionalidade")
     *   }
     * )
     */
    private $funcionalidades;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Application\Entity\OauthUsersEntity", inversedBy="coPerfil")
     * @ORM\JoinTable(name="tb_aesbe_perfil_oauth_users",
     *   joinColumns={
     *     @ORM\JoinColumn(name="co_perfil", referencedColumnName="co_perfil")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="co_usuario", referencedColumnName="co_usuario")
     *   }
     * )
     */
    private $oauthUsers;

    /**
     * Constructor
     */
    public function __construct(array $options = array())
    {
        $this->coUsuario = new ArrayCollection();
        $this->oauthUsers = new ArrayCollection();
        $this->funcionalidades = new ArrayCollection();
        parent::__construct($options);
    }

    /**
     * @return int
     */
    public function getCoPerfil()
    {
        return $this->coPerfil;
    }

    /**
     * @param int $coPerfil
     * @return AesbePerfilEntity
     */
    public function setCoPerfil($coPerfil)
    {
        $this->coPerfil = $coPerfil;
        return $this;
    }

    /**
     * @return string
     */
    public function getNoPerfil()
    {
        return $this->noPerfil;
    }

    /**
     * @param string $noPerfil
     * @return AesbePerfilEntity
     */
    public function setNoPerfil($noPerfil)
    {
        $this->noPerfil = $noPerfil;
        return $this;
    }

    /**
     * @return string
     */
    public function getSgPerfil()
    {
        return $this->sgPerfil;
    }

    /**
     * @param string $sgPerfil
     * @return AesbePerfilEntity
     */
    public function setSgPerfil($sgPerfil)
    {
        $this->sgPerfil = $sgPerfil;
        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCoUsuario()
    {
        return $this->coUsuario;
    }

    /**
     * Add Funcionalidade
     *
     * @param $funcionalidade
     * @return TbAcoes
     */
    public function addFuncionalidade($funcionalidade)
    {
        $this->funcionalidades[] = $funcionalidade;

        return $this;
    }

    /**
     * Remove Funcionalidade
     *
     * @param \Application\Entity\AesbeFuncionalidadeEntity
     */
    public function removeFuncionalidade($funcionalidade)
    {
        $this->funcionalidades->removeElement($funcionalidade);
    }

    /**
     * Get Funcionalidade
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFuncionalidades()
    {
        return $this->funcionalidades;
    }

    /**
     * Add coUsuario
     *
     * @param \Application\Entity\OauthUsers $oauthUser
     *
     * @return \Application\Entity\OauthUsers
     */
    public function addOauthUsers($oauthUser)
    {
        $this->oauthUsers[] = $oauthUser;
        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOauthUsers()
    {
        return $this->oauthUsers;
    }
}

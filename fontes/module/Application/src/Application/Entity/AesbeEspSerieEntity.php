<?php

namespace Application\Entity;

use Commons\Pattern\Entity\Impl\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * TbAesbeEspSerie
 *
 * @ORM\Table(name="tb_aesbe_esp_serie")
 * @ORM\Entity
 */
class AesbeEspSerieEntity extends AbstractEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="co_esp_serie", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $coEspSerie;

    /**
     * @var string
     *
     * @ORM\Column(name="ds_esp_serie", type="text", length=65535, nullable=false)
     */
    private $dsEspSerie;

    /**
     * @return int
     */
    public function getCoEspSerie()
    {
        return $this->coEspSerie;
    }

    /**
     * @param int $coEspSerie
     * @return AesbeEspSerieEntity
     */
    public function setCoEspSerie($coEspSerie)
    {
        $this->coEspSerie = $coEspSerie;
        return $this;
    }

    /**
     * @return string
     */
    public function getDsEspSerie()
    {
        return $this->dsEspSerie;
    }

    /**
     * @param string $dsEspSerie
     * @return AesbeEspSerieEntity
     */
    public function setDsEspSerie($dsEspSerie)
    {
        $this->dsEspSerie = $dsEspSerie;
        return $this;
    }
}

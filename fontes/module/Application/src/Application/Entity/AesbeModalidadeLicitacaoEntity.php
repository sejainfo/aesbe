<?php

namespace Application\Entity;

use Commons\Pattern\Entity\Impl\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * AesbeModalidadeLicitacaoEntity
 *
 * @ORM\Table(name="tb_aesbe_modalidade_licitacao")
 * @ORM\Entity(repositoryClass="Commons\Pattern\Repository\Impl\SimpleEntityRepository")
 */
class AesbeModalidadeLicitacaoEntity extends AbstractEntity
{
    const CO_MODALIDADE_PREGAO = 1;
    const CO_MODALIDADE_DISPENSA = 2;
    const CO_CONVITE = 3;
    /**
     * @var integer
     *
     * @ORM\Column(name="co_modalidade", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $coModalidade;

    /**
     * @var string
     *
     * @ORM\Column(name="ds_modalidade", type="string", length=45, nullable=false)
     */
    private $dsModalidade;

    /**
     * @return int
     */
    public function getCoModalidade()
    {
        return $this->coModalidade;
    }

    /**
     * @param int $coModalidade
     * @return AesbeModalidadeLicitacaoEntity
     */
    public function setCoModalidade($coModalidade)
    {
        $this->coModalidade = $coModalidade;
        return $this;
    }

    /**
     * @return string
     */
    public function getDsModalidade()
    {
        return $this->dsModalidade;
    }

    /**
     * @param string $dsModalidade
     * @return AesbeModalidadeLicitacaoEntity
     */
    public function setDsModalidade($dsModalidade)
    {
        $this->dsModalidade = $dsModalidade;
        return $this;
    }
}

<?php

namespace Application\Entity;

use Commons\Pattern\Entity\Impl\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * AesbeMunicipioEntity
 *
 * @ORM\Table(name="tb_aesbe_municipio", indexes={@ORM\Index(name="fk_tb_aesbe_municipio_tb_aesbe_uf1_idx", columns={"co_uf"})})
 * @ORM\Entity(repositoryClass="Commons\Pattern\Repository\Impl\SimpleEntityRepository")
 */
class AesbeMunicipioEntity extends AbstractEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="co_municipio", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $coMunicipio;

    /**
     * @var string
     *
     * @ORM\Column(name="no_municipio", type="string", length=50, nullable=false)
     */
    private $noMunicipio;

    /**
     * @var string
     *
     * @ORM\Column(name="sg_uf", type="string", length=50, nullable=false)
     */
    private $sgUf;

    /**
     * @return int
     */
    public function getCoMunicipio()
    {
        return $this->coMunicipio;
    }

    /**
     * @param int $coMunicipio
     */
    public function setCoMunicipio($coMunicipio)
    {
        $this->coMunicipio = $coMunicipio;
    }

    /**
     * @return string
     */
    public function getNoMunicipio()
    {
        return $this->noMunicipio;
    }

    /**
     * @param string $noMunicipio
     */
    public function setNoMunicipio($noMunicipio)
    {
        $this->noMunicipio = $noMunicipio;
    }

    /**
     * @return string
     */
    public function getSgUf()
    {
        return $this->sgUf;
    }

    /**
     * @param string $sgUf
     * @return AesbeMunicipioEntity
     */
    public function setSgUf($sgUf)
    {
        $this->sgUf = $sgUf;
        return $this;
    }

}

<?php

namespace Application\Entity;

use Commons\Pattern\Entity\Impl\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * AesbeModuloEntity
 *
 * @ORM\Table(name="tb_aesbe_funcionalidade")
 * @ORM\Entity(repositoryClass="Application\Entity\AesbeFuncionalidadeEntityRepository")
 */
class AesbeFuncionalidadeEntity extends AbstractEntity
{

    /**
     * @var integer
     *
     * @ORM\Column(name="co_funcionalidade", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $coFuncionalidade;

    /**
     * @var string
     *
     * @ORM\Column(name="no_funcionalidade", type="string", length=45, nullable=false)
     */
    private $noFuncionalidade;

    /**
     * @var string
     *
     * @ORM\Column(name="no_funcionalidade_visivel", type="string", length=45, nullable=false)
     */
    private $noFuncionalidadeVisivel;

    /**
     * @var string
     *
     * @ORM\Column(name="st_funcionalidade_visivel", type="boolean", nullable=false)
     */
    private $stFuncionalidadeVisivel;

    /**
     * @var string
     *
     * @ORM\Column(name="st_ativo", type="boolean", nullable=false)
     */
    private $stAtivo;

    /**
     * @var \Application\Entity\AesbeModuloEntity
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\AesbeModuloEntity")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="co_modulo", referencedColumnName="co_modulo")
     * })
     */
    private $moduloEntity;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Application\Entity\AesbePerfilEntity", mappedBy="funcionalidades")
     */
    private $coPerfil;

    /**
     * @var string
     *
     * @ORM\Column(name="ds_metodo_http", type="string", length=45, nullable=false)
     */
    private $dsMetodoHttp;

    /**
     * @var string
     *
     * @ORM\Column(name="ds_controller", type="string", length=45, nullable=false)
     */
    private $dsController;

    /**
     * @var string
     *
     * @ORM\Column(name="no_agrupador", type="string", length=45, nullable=false)
     */
    private $noAgrupador;

    /**
     * AesbeFuncionalidadeEntity constructor.
     */
    public function __construct(array $options = array())
    {
        $this->coPerfil = new \Doctrine\Common\Collections\ArrayCollection();
        parent::__construct($options);

    }


    /**
     * @return int
     */
    public function getCoFuncionalidade()
    {
        return $this->coFuncionalidade;
    }

    /**
     * @param int $coFuncionalidade
     * @return AesbeFuncionalidadeEntity
     */
    public function setCoFuncionalidade($coFuncionalidade)
    {
        $this->coFuncionalidade = $coFuncionalidade;
        return $this;
    }

    /**
     * @return string
     */
    public function getNoFuncionalidade()
    {
        return $this->noFuncionalidade;
    }

    /**
     * @param string $noFuncionalidade
     * @return AesbeFuncionalidadeEntity
     */
    public function setNoFuncionalidade($noFuncionalidade)
    {
        $this->noFuncionalidade = $noFuncionalidade;
        return $this;
    }

    /**
     * @return string
     */
    public function getNoFuncionalidadeVisivel()
    {
        return $this->noFuncionalidadeVisivel;
    }

    /**
     * @param string $noFuncionalidadeVisivel
     * @return AesbeFuncionalidadeEntity
     */
    public function setNoFuncionalidadeVisivel($noFuncionalidadeVisivel)
    {
        $this->noFuncionalidadeVisivel = $noFuncionalidadeVisivel;
        return $this;
    }

    /**
     * @return string
     */
    public function getStFuncionalidadeVisivel()
    {
        return $this->stFuncionalidadeVisivel;
    }

    /**
     * @param string $stFuncionalidadeVisivel
     * @return AesbeFuncionalidadeEntity
     */
    public function setStFuncionalidadeVisivel($stFuncionalidadeVisivel)
    {
        $this->stFuncionalidadeVisivel = $stFuncionalidadeVisivel;
        return $this;
    }

    /**
     * @return string
     */
    public function getStAtivo()
    {
        return $this->stAtivo;
    }

    /**
     * @param string $stAtivo
     * @return AesbeFuncionalidadeEntity
     */
    public function setStAtivo($stAtivo)
    {
        $this->stAtivo = $stAtivo;
        return $this;
    }

    /**
     * @return AesbeModuloEntity
     */
    public function getModuloEntity()
    {
        return $this->moduloEntity;
    }

    /**
     * @param AesbeModuloEntity $moduloEntity
     * @return AesbeFuncionalidadeEntity
     */
    public function setModuloEntity($moduloEntity)
    {
        $this->moduloEntity = $moduloEntity;
        return $this;
    }

    /**
     * Add Perfil
     *
     * @param \Application\Entity\AesbePerfilEntity
     *
     * @return TbPerfil
     */
    public function addIdAco(\Application\Entity\AesbePerfilEntity $coPerfil)
    {
        $this->coPerfil[] = $coPerfil;

        return $this;
    }

    /**
     * Remove Perfil
     *
     * @param \Application\Entity\AesbePerfilEntity
     */
    public function removeIdAco(\Application\Entity\AesbePerfilEntity $coPerfil)
    {
        $this->coPerfil->removeElement($coPerfil);
    }

    /**
     * Get Perfil
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCoPerfil()
    {
        return $this->coPerfil;
    }

    /**
     * @return string
     */
    public function getDsMetodoHttp()
    {
        return $this->dsMetodoHttp;
    }

    /**
     * @param string $dsMetodoHttp
     * @return AesbeFuncionalidadeEntity
     */
    public function setDsMetodoHttp($dsMetodoHttp)
    {
        $this->dsMetodoHttp = $dsMetodoHttp;
        return $this;
    }

    /**
     * @return string
     */
    public function getDsController()
    {
        return $this->dsController;
    }

    /**
     * @param string $dsController
     * @return AesbeFuncionalidadeEntity
     */
    public function setDsController($dsController)
    {
        $this->dsController = $dsController;
        return $this;
    }

    /**
     * @return string
     */
    public function getNoAgrupador()
    {
        return $this->noAgrupador;
    }

    /**
     * @param string $noAgrupador
     * @return AesbeFuncionalidadeEntity
     */
    public function setNoAgrupador($noAgrupador)
    {
        $this->noAgrupador = $noAgrupador;
        return $this;
    }
}
<?php

namespace Application\Entity;

use Commons\Pattern\Entity\Impl\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * AesbePropostaDadosEntity
 *
 * @ORM\Table(name="tb_aesbe_proposta_dados", indexes={@ORM\Index(name="fk_tb_aesbe_proposta_dados_tb_aesbe_proposta1_idx", columns={"co_proposta"}), @ORM\Index(name="fk_tb_aesbe_proposta_dados_tb_aesbe_modalidade_licitacao1_idx", columns={"co_modalidade"}), @ORM\Index(name="fk_tb_aesbe_proposta_dados_tb_aesbe_material1_idx", columns={"co_material"})})
 * @ORM\Entity(repositoryClass="Application\Entity\AesbePropostaDadosRepository")
 */
class AesbePropostaDadosEntity extends AbstractEntity
{

    /**
     * constante tipo de proposta
     */
    const ST_PROPOSTA_ORIGINAL = 'ORIGINAL';
    const ST_PROPOSTA_NOVA = 'NOVA';
    /**
     * constante FOB/CIF
     */
    const ST_FOB = 'FOB';
    const ST_CIF = 'CIF';
    /**
     * constante nível de atendimento
     */
    const ST_NVL_ATENDIMENTO_VERDE = 'VERDE';
    const ST_NVL_ATENDIMENTO_AMARELO = 'AMARELO';
    const ST_NVL_ATENDIMENTO_VERMELHO = 'VERMELHO';
    /**
     * constante especificação padrão
     */
    const ST_ESP_PADRAO_SIM = 'S';
    const ST_ESP_PADRAO_NAO = 'N';

    /**
     * constante tipo de inspeção
     */
    const TP_INSPECAO_FABRICA = 'FABRICA';
    const TP_INSPECAO_RECEBIMENTO = 'RECEBIMENTO';

    const ST_FORNECIMENTO_PARCELADO = 'PARCELADO';
    const ST_FORNECIMENTO_UNICA = 'UNICA';


    /**
     * @var integer
     *
     * @ORM\Column(name="co_proposta_dados", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $coPropostaDados;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_informacao", type="date", nullable=false)
     */
    private $dtInformacao;

    /**
     * @var float
     *
     * @ORM\Column(name="vl_icms", type="float", precision=10, scale=0, nullable=false)
     */
    private $vlIcms;

    /**
     * @var float
     *
     * @ORM\Column(name="vl_ipi", type="float", precision=10, scale=0, nullable=false)
     */
    private $vlIpi;

    /**
     * @var string
     *
     * @ORM\Column(name="st_fob_cif", type="string", length=10, nullable=false)
     */
    private $stFobCif;

    /**
     * @var string
     *
     * @ORM\Column(name="st_condicao_fornecimento", type="string", length=45, nullable=false)
     */
    private $stCondicaoFornecimento;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_proposta", type="date", nullable=false)
     */
    private $dtProposta;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_contratacao", type="date", nullable=false)
     */
    private $dtContratacao;

    /**
     * @var integer
     *
     * @ORM\Column(name="qt_produto", type="integer", nullable=false)
     */
    private $qtProduto;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_vigencia_contrato", type="date", nullable=false)
     */
    private $dtVigenciaContrato;

    /**
     * @var string
     *
     * @ORM\Column(name="st_proposta", type="string", length=45, nullable=false)
     */
    private $stProposta;

    /**
     * @var float
     *
     * @ORM\Column(name="vl_com_imposto", type="float", precision=10, scale=0, nullable=false)
     */
    private $vlComImposto;

    /**
     * @var integer
     *
     * @ORM\Column(name="nu_contrato", type="integer", nullable=true)
     */
    private $nuContrato;

    /**
     * @var float
     *
     * @ORM\Column(name="vl_iss", type="float", precision=10, scale=0, nullable=true)
     */
    private $vlIss;

    /**
     * @var string
     *
     * @ORM\Column(name="ds_observacao", type="string", length=100, nullable=true)
     */
    private $dsObservacao;

    /**
     * @var integer
     *
     * @ORM\Column(name="nu_ata", type="integer", nullable=true)
     */
    private $nuAta;

    /**
     * @var integer
     *
     * @ORM\Column(name="qt_dias_pagamento", type="integer", nullable=true)
     */
    private $qtDiasPagamento;

    /**
     * @var string
     *
     * @ORM\Column(name="tp_inspecao", type="string", length=45, nullable=true)
     */
    private $tpInspecao;

    /**
     * @var string
     *
     * @ORM\Column(name="st_nivel_atendimento", type="string", length=45, nullable=true)
     */
    private $stNivelAtendimento;

    /**
     * @var boolean
     *
     * @ORM\Column(name="st_especificacao_padrao", type="string", length=1, nullable=false)
     */
    private $stEspecificacaoPadrao;

    /**
     * @var \Application\Entity\AesbeMaterialEntity
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\AesbeMaterialEntity")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="co_material", referencedColumnName="co_material")
     * })
     */
    private $materialEntity;

    /**
     * @var \Application\Entity\AesbeModalidadeLicitacaoEntity
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\AesbeModalidadeLicitacaoEntity")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="co_modalidade", referencedColumnName="co_modalidade")
     * })
     */
    private $modalidadeLicitacaoEntity;

    /**
     * @var \Application\Entity\AesbePropostaEntity
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\AesbePropostaEntity")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="co_proposta", referencedColumnName="co_proposta")
     * })
     */
    private $propostaEntity;

    /**
     * @var \Application\Entity\AesbeFornecedorEntity
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\AesbeFornecedorEntity")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="co_fornecedor", referencedColumnName="co_fornecedor")
     * })
     */
    private $fornecedorEntity;

    /**
     * @return int
     */
    public function getCoPropostaDados()
    {
        return $this->coPropostaDados;
    }

    /**
     * @param int $coPropostaDados
     * @return AesbePropostaDadosEntity
     */
    public function setCoPropostaDados($coPropostaDados)
    {
        $this->coPropostaDados = $coPropostaDados;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDtInformacao()
    {
        return $this->dtInformacao;
    }

    /**
     * @param \DateTime $dtInformacao
     * @return AesbePropostaDadosEntity
     */
    public function setDtInformacao($dtInformacao)
    {
        $this->dtInformacao = $dtInformacao;
        return $this;
    }

    /**
     * @return float
     */
    public function getVlIcms()
    {
        return $this->vlIcms;
    }

    /**
     * @param float $vlIcms
     * @return AesbePropostaDadosEntity
     */
    public function setVlIcms($vlIcms)
    {
        $this->vlIcms = $vlIcms;
        return $this;
    }

    /**
     * @return string
     */
    public function getStFobCif()
    {
        return $this->stFobCif;
    }

    /**
     * @param string $stFobCif
     * @return AesbePropostaDadosEntity
     */
    public function setStFobCif($stFobCif)
    {
        $this->stFobCif = $stFobCif;
        return $this;
    }

    /**
     * @return string
     */
    public function getStCondicaoFornecimento()
    {
        return $this->stCondicaoFornecimento;
    }

    /**
     * @param string $stCondicaoFornecimento
     * @return AesbePropostaDadosEntity
     */
    public function setStCondicaoFornecimento($stCondicaoFornecimento)
    {
        $this->stCondicaoFornecimento = $stCondicaoFornecimento;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDtContratacao()
    {
        return $this->dtContratacao;
    }

    /**
     * @param \DateTime $dtContratacao
     * @return AesbePropostaDadosEntity
     */
    public function setDtContratacao($dtContratacao)
    {
        $this->dtContratacao = $dtContratacao;
        return $this;
    }

    /**
     * @return int
     */
    public function getQtProduto()
    {
        return $this->qtProduto;
    }

    /**
     * @param int $qtProduto
     * @return AesbePropostaDadosEntity
     */
    public function setQtProduto($qtProduto)
    {
        $this->qtProduto = $qtProduto;
        return $this;
    }

    /**
     * @return string
     */
    public function getStProposta()
    {
        return $this->stProposta;
    }

    /**
     * @param string $stProposta
     * @return AesbePropostaDadosEntity
     */
    public function setStProposta($stProposta)
    {
        $this->stProposta = $stProposta;
        return $this;
    }

    /**
     * @return float
     */
    public function getVlComImposto()
    {
        return $this->vlComImposto;
    }

    /**
     * @param float $vlComImposto
     * @return AesbePropostaDadosEntity
     */
    public function setVlComImposto($vlComImposto)
    {
        $this->vlComImposto = $vlComImposto;
        return $this;
    }

    /**
     * @return int
     */
    public function getNuContrato()
    {
        return $this->nuContrato;
    }

    /**
     * @param int $nuContrato
     * @return AesbePropostaDadosEntity
     */
    public function setNuContrato($nuContrato)
    {
        $this->nuContrato = $nuContrato;
        return $this;
    }

    /**
     * @return float
     */
    public function getVlIss()
    {
        return $this->vlIss;
    }

    /**
     * @param float $vlIss
     * @return AesbePropostaDadosEntity
     */
    public function setVlIss($vlIss)
    {
        $this->vlIss = $vlIss;
        return $this;
    }

    /**
     * @return string
     */
    public function getDsObservacao()
    {
        return $this->dsObservacao;
    }

    /**
     * @param string $dsObservacao
     * @return AesbePropostaDadosEntity
     */
    public function setDsObservacao($dsObservacao)
    {
        $this->dsObservacao = $dsObservacao;
        return $this;
    }

    /**
     * @return int
     */
    public function getNuAta()
    {
        return $this->nuAta;
    }

    /**
     * @param int $nuAta
     * @return AesbePropostaDadosEntity
     */
    public function setNuAta($nuAta)
    {
        $this->nuAta = $nuAta;
        return $this;
    }

    /**
     * @return int
     */
    public function getQtDiasPagamento()
    {
        return $this->qtDiasPagamento;
    }

    /**
     * @param int $qtDiasPagamento
     * @return AesbePropostaDadosEntity
     */
    public function setQtDiasPagamento($qtDiasPagamento)
    {
        $this->qtDiasPagamento = $qtDiasPagamento;
        return $this;
    }

    /**
     * @return string
     */
    public function getTpInspecao()
    {
        return $this->tpInspecao;
    }

    /**
     * @param string $tpInspecao
     * @return AesbePropostaDadosEntity
     */
    public function setTpInspecao($tpInspecao)
    {
        $this->tpInspecao = $tpInspecao;
        return $this;
    }

    /**
     * @return string
     */
    public function getStNivelAtendimento()
    {
        return $this->stNivelAtendimento;
    }

    /**
     * @param string $stNivelAtendimento
     * @return AesbePropostaDadosEntity
     */
    public function setStNivelAtendimento($stNivelAtendimento)
    {
        $this->stNivelAtendimento = $stNivelAtendimento;
        return $this;
    }

    /**
     * @return \Application\
     */
    public function getMaterialEntity()
    {
        return $this->materialEntity;
    }

    /**
     * @param \Application\ $materialEntity
     * @return AesbePropostaDadosEntity
     */
    public function setMaterialEntity($materialEntity)
    {
        $this->materialEntity = $materialEntity;
        return $this;
    }

    /**
     * @return \Application\
     */
    public function getModalidadeLicitacaoEntity()
    {
        return $this->modalidadeLicitacaoEntity;
    }

    /**
     * @param \Application\ $modalidadeLicitacaoEntity
     * @return AesbePropostaDadosEntity
     */
    public function setModalidadeLicitacaoEntity($modalidadeLicitacaoEntity)
    {
        $this->modalidadeLicitacaoEntity = $modalidadeLicitacaoEntity;
        return $this;
    }

    /**
     * @return \Application\
     */
    public function getPropostaEntity()
    {
        return $this->propostaEntity;
    }

    /**
     * @param \Application\ $propostaEntity
     * @return AesbePropostaDadosEntity
     */
    public function setPropostaEntity($propostaEntity)
    {
        $this->propostaEntity = $propostaEntity;
        return $this;
    }

    /**
     * @return float
     */
    public function getVlIpi()
    {
        return $this->vlIpi;
    }

    /**
     * @param float $vlIpi
     * @return AesbePropostaDadosEntity
     */
    public function setVlIpi($vlIpi)
    {
        $this->vlIpi = $vlIpi;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDtProposta()
    {
        return $this->dtProposta;
    }

    /**
     * @param \DateTime $dtProposta
     * @return AesbePropostaDadosEntity
     */
    public function setDtProposta($dtProposta)
    {
        $this->dtProposta = $dtProposta;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDtVigenciaContrato()
    {
        return $this->dtVigenciaContrato;
    }

    /**
     * @param \DateTime $dtVigenciaContrato
     * @return AesbePropostaDadosEntity
     */
    public function setDtVigenciaContrato($dtVigenciaContrato)
    {
        $this->dtVigenciaContrato = $dtVigenciaContrato;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getStEspecificacaoPadrao()
    {
        return $this->stEspecificacaoPadrao;
    }

    /**
     * @param boolean $stEspecificacaoPadrao
     * @return AesbePropostaDadosEntity
     */
    public function setStEspecificacaoPadrao($stEspecificacaoPadrao)
    {
        $this->stEspecificacaoPadrao = $stEspecificacaoPadrao;
        return $this;
    }

    /**
     * @return AesbeFornecedorEntity
     */
    public function getFornecedorEntity()
    {
        return $this->fornecedorEntity;
    }

    /**
     * @param AesbeFornecedorEntity $fornecedorEntity
     * @return AesbePropostaDadosEntity
     */
    public function setFornecedorEntity($fornecedorEntity)
    {
        $this->fornecedorEntity = $fornecedorEntity;
        return $this;
    }
}

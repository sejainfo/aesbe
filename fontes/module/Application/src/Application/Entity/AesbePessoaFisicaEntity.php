<?php

namespace Application\Entity;

use Commons\Pattern\Entity\Impl\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * AesbePessoaFisicaEntity
 *
 * @ORM\Table(name="tb_aesbe_pessoa_fisica", indexes={@ORM\Index(name="fk_tb_pessoa_fisica_tb_aesbe_uf1_idx", columns={"co_uf"})})
 * @ORM\Entity(repositoryClass="Commons\Pattern\Repository\Impl\SimpleEntityRepository")
 */
class AesbePessoaFisicaEntity extends AbstractEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="co_pessoa", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $coPessoa;

    /**
     * @var string
     *
     * @ORM\Column(name="no_pessoa", type="string", length=100, nullable=false)
     */
    private $noPessoa;

    /**
     * @var string
     *
     * @ORM\Column(name="ds_email", type="string", length=100, nullable=false)
     */
    private $dsEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="ds_email_secundario", type="string", length=100, nullable=true)
     */
    private $dsEmailSecundario;

    /**
     * @var string
     *
     * @ORM\Column(name="nu_tel_residencial", type="string", length=10, nullable=true)
     */
    private $nuTelResidencial;

    /**
     * @var string
     *
     * @ORM\Column(name="nu_tel_comercial", type="string", length=10, nullable=true)
     */
    private $nuTelComercial;

    /**
     * @var string
     *
     * @ORM\Column(name="nu_celular", type="string", length=10, nullable=true)
     */
    private $nuCelular;

    /**
     * @var boolean
     *
     * @ORM\Column(name="st_presidente", type="boolean", nullable=false)
     */
    private $stPresidente = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="nu_cep", type="string", length=10, nullable=true)
     */
    private $nuCep;

    /**
     * @var string
     *
     * @ORM\Column(name="ds_logradouro", type="string", length=100, nullable=true)
     */
    private $dsLogradouro;

    /**
     * @var string
     *
     * @ORM\Column(name="no_bairro", type="string", length=100, nullable=true)
     */
    private $noBairro;

    /**
     * @var string
     *
     * @ORM\Column(name="nu_logradouro", type="string", length=10, nullable=true)
     */
    private $nuLogradouro;

    /**
     * @var string
     *
     * @ORM\Column(name="ds_complemento", type="string", length=30, nullable=true)
     */
    private $dsComplemento;

    /**
     * @var boolean
     *
     * @ORM\Column(name="st_ativo", type="boolean", nullable=false)
     */
    private $stAtivo;

    /**
     * @var \Application\Entity\AesbeUfEntity
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\AesbeUfEntity")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="co_uf", referencedColumnName="co_uf")
     * })
     */
    private $ufEntity;

    /**
     * @var \Application\Entity\AesbeEmpresaEntity
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\AesbeEmpresaEntity")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="co_empresa", referencedColumnName="co_empresa")
     * })
     */
    private $empresaEntity;

    /**
     * @return int
     */
    public function getCoPessoa()
    {
        return $this->coPessoa;
    }

    /**
     * @param int $coPessoa
     * @return AesbePessoaFisicaEntity
     */
    public function setCoPessoa($coPessoa)
    {
        $this->coPessoa = $coPessoa;
        return $this;
    }

    /**
     * @return string
     */
    public function getNoPessoa()
    {
        return $this->noPessoa;
    }

    /**
     * @param string $noPessoa
     * @return AesbePessoaFisicaEntity
     */
    public function setNoPessoa($noPessoa)
    {
        $this->noPessoa = $noPessoa;
        return $this;
    }

    /**
     * @return string
     */
    public function getDsEmail()
    {
        return $this->dsEmail;
    }

    /**
     * @param string $dsEmail
     * @return AesbePessoaFisicaEntity
     */
    public function setDsEmail($dsEmail)
    {
        $this->dsEmail = $dsEmail;
        return $this;
    }

    /**
     * @return string
     */
    public function getDsEmailSecundario()
    {
        return $this->dsEmailSecundario;
    }

    /**
     * @param string $dsEmailSecundario
     * @return AesbePessoaFisicaEntity
     */
    public function setDsEmailSecundario($dsEmailSecundario)
    {
        $this->dsEmailSecundario = $dsEmailSecundario;
        return $this;
    }

    /**
     * @return string
     */
    public function getNuTelResidencial()
    {
        return $this->nuTelResidencial;
    }

    /**
     * @param string $nuTelResidencial
     * @return AesbePessoaFisicaEntity
     */
    public function setNuTelResidencial($nuTelResidencial)
    {
        $this->nuTelResidencial = $nuTelResidencial;
        return $this;
    }

    /**
     * @return string
     */
    public function getNuTelComercial()
    {
        return $this->nuTelComercial;
    }

    /**
     * @param string $nuTelComercial
     * @return AesbePessoaFisicaEntity
     */
    public function setNuTelComercial($nuTelComercial)
    {
        $this->nuTelComercial = $nuTelComercial;
        return $this;
    }

    /**
     * @return string
     */
    public function getNuCelular()
    {
        return $this->nuCelular;
    }

    /**
     * @param string $nuCelular
     * @return AesbePessoaFisicaEntity
     */
    public function setNuCelular($nuCelular)
    {
        $this->nuCelular = $nuCelular;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isStPresidente()
    {
        return $this->stPresidente;
    }

    /**
     * @param boolean $stPresidente
     * @return AesbePessoaFisicaEntity
     */
    public function setStPresidente($stPresidente)
    {
        $this->stPresidente = $stPresidente;
        return $this;
    }

    /**
     * @return string
     */
    public function getNuCep()
    {
        return $this->nuCep;
    }

    /**
     * @param string $nuCep
     * @return AesbePessoaFisicaEntity
     */
    public function setNuCep($nuCep)
    {
        $this->nuCep = $nuCep;
        return $this;
    }

    /**
     * @return string
     */
    public function getDsLogradouro()
    {
        return $this->dsLogradouro;
    }

    /**
     * @param string $dsLogradouro
     * @return AesbePessoaFisicaEntity
     */
    public function setDsLogradouro($dsLogradouro)
    {
        $this->dsLogradouro = $dsLogradouro;
        return $this;
    }

    /**
     * @return string
     */
    public function getNoBairro()
    {
        return $this->noBairro;
    }

    /**
     * @param string $noBairro
     * @return AesbePessoaFisicaEntity
     */
    public function setNoBairro($noBairro)
    {
        $this->noBairro = $noBairro;
        return $this;
    }

    /**
     * @return string
     */
    public function getNuLogradouro()
    {
        return $this->nuLogradouro;
    }

    /**
     * @param string $nuLogradouro
     * @return AesbePessoaFisicaEntity
     */
    public function setNuLogradouro($nuLogradouro)
    {
        $this->nuLogradouro = $nuLogradouro;
        return $this;
    }

    /**
     * @return string
     */
    public function getDsComplemento()
    {
        return $this->dsComplemento;
    }

    /**
     * @param string $dsComplemento
     * @return AesbePessoaFisicaEntity
     */
    public function setDsComplemento($dsComplemento)
    {
        $this->dsComplemento = $dsComplemento;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isStAtivo()
    {
        return $this->stAtivo;
    }

    /**
     * @param boolean $stAtivo
     * @return AesbePessoaFisicaEntity
     */
    public function setStAtivo($stAtivo)
    {
        $this->stAtivo = $stAtivo;
        return $this;
    }

    /**
     * @return AesbeUfEntity
     */
    public function getUfEntity()
    {
        return $this->ufEntity;
    }

    /**
     * @param AesbeUfEntity $ufEntity
     * @return AesbePessoaFisicaEntity
     */
    public function setUfEntity($ufEntity)
    {
        $this->ufEntity = $ufEntity;
        return $this;
    }

    /**
     * @return AesbeEmpresaEntity
     */
    public function getEmpresaEntity()
    {
        return $this->empresaEntity;
    }

    /**
     * @param AesbeEmpresaEntity $empresaEntity
     * @return AesbePessoaFisicaEntity
     */
    public function setEmpresaEntity($empresaEntity)
    {
        $this->empresaEntity = $empresaEntity;
        return $this;
    }
}

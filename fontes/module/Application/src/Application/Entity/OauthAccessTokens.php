<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OauthAccessTokens
 *
 * @ORM\Table(name="oauth_access_tokens")
 * @ORM\Entity(repositoryClass="Application\Entity\OauthAccessTokensRepository")
 */
class OauthAccessTokens
{
    /**
     * @var
     *
     * @ORM\Column(name="access_token", type="string", length=40, nullable=false)
     * @ORM\Id
     */
    private $accessToken;

    /**
     * @var \Application\Entity\OauthClients
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\OauthClients")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="client_id", referencedColumnName="client_id")
     * })
     */
    private $clientId;

    /**
     * @var \Application\Entity\OauthUsersEntity
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\OauthUsersEntity")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="username")
     * })
     */
    private $userId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="expires", type="datetime", nullable=false)
     */
    private $expires = 'CURRENT_TIMESTAMP';

    /**
     * @var string
     *
     * @ORM\Column(name="scope", type="string", length=2000, nullable=true)
     */
    private $scope;

    /**
     * @return string
     */
    public function getAccessToken()
    {
        return $this->accessToken;
    }

    /**
     * @param string $accessToken
     * @return OauthAccessTokens
     */
    public function setAccessToken($accessToken)
    {
        $this->accessToken = $accessToken;
        return $this;
    }

    /**
     * @return OauthClients
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * @param OauthClients $clientId
     * @return OauthAccessTokens
     */
    public function setClientId($clientId)
    {
        $this->clientId = $clientId;
        return $this;
    }

    /**
     * @return OauthUsersEntity
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param OauthUsersEntity $userId
     * @return OauthAccessTokens
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getExpires()
    {
        return $this->expires;
    }

    /**
     * @param \DateTime $expires
     * @return OauthAccessTokens
     */
    public function setExpires($expires)
    {
        $this->expires = $expires;
        return $this;
    }

    /**
     * @return string
     */
    public function getScope()
    {
        return $this->scope;
    }

    /**
     * @param string $scope
     * @return OauthAccessTokens
     */
    public function setScope($scope)
    {
        $this->scope = $scope;
        return $this;
    }

}


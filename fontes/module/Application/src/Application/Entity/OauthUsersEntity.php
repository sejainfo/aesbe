<?php

namespace Application\Entity;

use Commons\Pattern\Entity\Impl\AbstractEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * OauthUsers
 *
 * @ORM\Table(name="oauth_users", indexes={@ORM\Index(name="fk_oauth_users_tb_aesbe_pessoa_fisica1_idx", columns={"co_pessoa"})})
 * @ORM\Entity(repositoryClass="Application\Entity\OauthUsersRepository")
 */
class OauthUsersEntity extends AbstractEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="co_usuario", type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $coUsuario;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_cadastro", type="date", nullable=false)
     */
    private $dtCadastro;

    /**
     * @var string

     * @ORM\Column(name="username", type="string", length=45, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")

     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=100, nullable=false)
     */
    private $password;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_ultima_visita", type="datetime", nullable=true)
     */
    private $dtUltimaVisita;

    /**
     * @var \Application\Entity\AesbePessoaFisicaEntity
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\AesbePessoaFisicaEntity")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="co_pessoa", referencedColumnName="co_pessoa")
     * })
     */
    private $pessoaEntity;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Application\Entity\AesbePerfilEntity", mappedBy="oauthUsers")
     */
    private $perfis;

    /**
     * OauthUsersEntity constructor.
     * @param array $options
     */
    public function __construct(array $options = array())
    {
        $this->perfis = new ArrayCollection();
        parent::__construct($options);
    }


    /**
     * @return int
     */
    public function getCoUsuario()
    {
        return $this->coUsuario;
    }

    /**
     * @param int $coUsuario
     * @return OauthUsers
     */
    public function setCoUsuario($coUsuario)
    {
        $this->coUsuario = $coUsuario;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDtCadastro()
    {
        return $this->dtCadastro;
    }

    /**
     * @param \DateTime $dtCadastro
     * @return OauthUsers
     */
    public function setDtCadastro($dtCadastro)
    {
        $this->dtCadastro = $dtCadastro;
        return $this;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $username
     * @return OauthUsers
     */
    public function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return OauthUsers
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDtUltimaVisita()
    {
        return $this->dtUltimaVisita;
    }

    /**
     * @param \DateTime $dtUltimaVisita
     * @return OauthUsers
     */
    public function setDtUltimaVisita($dtUltimaVisita)
    {
        $this->dtUltimaVisita = $dtUltimaVisita;
        return $this;
    }

    /**
     * @return AesbePessoaFisicaEntity
     */
    public function getPessoaEntity()
    {
        return $this->pessoaEntity;
    }

    /**
     * @param AesbePessoaFisicaEntity $pessoaEntity
     * @return OauthUsers
     */
    public function setPessoaEntity($pessoaEntity)
    {
        $this->pessoaEntity = $pessoaEntity;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPerfis()
    {
        return $this->perfis;
    }

    /**
     * Add coUsuario
     *
     * @param \Application\Entity\AesbePerfilEntity $perfil
     *
     * @return TbAesbePerfil
     */
    public function addPerfil($perfil)
    {
        $this->perfils[] = $perfil;

        return $this;
    }

    /**
     * Remove perfil
     *
     * @param \Application\Entity\AesbePerfilEntity $perfil
     */
    public function removePerfil($perfil)
    {
        $this->perfis->removeElement($perfil);
    }

}
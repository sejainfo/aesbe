<?php

namespace Application\Entity;

use Commons\Pattern\Entity\Impl\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * AesbeArquivoPropostaEntity
 *
 * @ORM\Table(name="tb_aesbe_arquivo_proposta")
 * @ORM\Entity(repositoryClass="Application\Entity\AesbeArquivoPropostaRepository")
 */
class AesbeArquivoPropostaEntity extends AbstractEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="co_arquivo_proposta", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $coArquivoProposta;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_processamento", type="datetime", nullable=false)
     */
    private $dtProcessamento;

    /**
     * @var integer
     *
     * @ORM\Column(name="qt_erros", type="integer", nullable=false)
     */
    private $qtErros;

    /**
     * @var integer
     *
     * @ORM\Column(name="qt_registros_processados", type="integer", nullable=false)
     */
    private $qtRegistrosProcessados;

    /**
     * @var boolean
     *
     * @ORM\Column(name="st_processamento", type="boolean", nullable=false)
     */
    private $stProcessamento;

    /**
     * @var integer
     *
     * @ORM\Column(name="co_usuario", type="integer", nullable=false)
     */
    private $coUsuario;

    /**
     * @var \Application\Entity\AesbePropostaEntity
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\AesbePropostaEntity")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="co_proposta", referencedColumnName="co_proposta", nullable=false)
     * })
     */
    private $propostaEntity;

    /**
     * @var \Application\Entity\AesbeArquivoEntity
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\AesbeArquivoEntity")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="co_arquivo", referencedColumnName="co_arquivo", nullable=false)
     * })
     */
    private $arquivoEntity;

    /**
     * @return int
     */
    public function getCoArquivoProposta()
    {
        return $this->coArquivoProposta;
    }

    /**
     * @param int $coArquivoProposta
     * @return AesbeArquivoPropostaEntity
     */
    public function setCoArquivoProposta($coArquivoProposta)
    {
        $this->coArquivoProposta = $coArquivoProposta;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDtProcessamento()
    {
        return $this->dtProcessamento;
    }

    /**
     * @param \DateTime $dtProcessamento
     * @return AesbeArquivoPropostaEntity
     */
    public function setDtProcessamento($dtProcessamento)
    {
        $this->dtProcessamento = $dtProcessamento;
        return $this;
    }

    /**
     * @return int
     */
    public function getQtErros()
    {
        return $this->qtErros;
    }

    /**
     * @param int $qtErros
     * @return AesbeArquivoPropostaEntity
     */
    public function setQtErros($qtErros)
    {
        $this->qtErros = $qtErros;
        return $this;
    }

    /**
     * @return int
     */
    public function getQtRegistrosProcessados()
    {
        return $this->qtRegistrosProcessados;
    }

    /**
     * @param int $qtRegistrosProcessados
     * @return AesbeArquivoPropostaEntity
     */
    public function setQtRegistrosProcessados($qtRegistrosProcessados)
    {
        $this->qtRegistrosProcessados = $qtRegistrosProcessados;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isStProcessamento()
    {
        return $this->stProcessamento;
    }

    /**
     * @param boolean $stProcessamento
     * @return AesbeArquivoPropostaEntity
     */
    public function setStProcessamento($stProcessamento)
    {
        $this->stProcessamento = $stProcessamento;
        return $this;
    }

    /**
     * @return int
     */
    public function getCoUsuario()
    {
        return $this->coUsuario;
    }

    /**
     * @param int $coUsuario
     * @return AesbeArquivoPropostaEntity
     */
    public function setCoUsuario($coUsuario)
    {
        $this->coUsuario = $coUsuario;
        return $this;
    }

    /**
     * @return AesbePropostaEntity
     */
    public function getPropostaEntity()
    {
        return $this->propostaEntity;
    }

    /**
     * @param AesbePropostaEntity $propostaEntity
     * @return AesbeArquivoPropostaEntity
     */
    public function setPropostaEntity($propostaEntity)
    {
        $this->propostaEntity = $propostaEntity;
        return $this;
    }

    /**
     * @return AesbeArquivoEntity
     */
    public function getArquivoEntity()
    {
        return $this->arquivoEntity;
    }

    /**
     * @param AesbeArquivoEntity $arquivoEntity
     * @return AesbeArquivoPropostaEntity
     */
    public function setArquivoEntity($arquivoEntity)
    {
        $this->arquivoEntity = $arquivoEntity;
        return $this;
    }
}

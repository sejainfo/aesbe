<?php

namespace Application\Entity;

use Commons\Pattern\Entity\Impl\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * AesbeUfEntity
 *
 * @ORM\Table(name="tb_aesbe_uf", indexes={@ORM\Index(name="fk_tb_aesbe_uf_tb_aesbe_regiao_idx", columns={"co_regiao"})})
 * @ORM\Entity(repositoryClass="Application\Entity\AesbeUfEntityRepository")
 */
class AesbeUfEntity extends AbstractEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="co_uf", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $coUf;

    /**
     * @var string
     *
     * @ORM\Column(name="no_uf", type="string", length=45, nullable=false)
     */
    private $noUf;

    /**
     * @var string
     *
     * @ORM\Column(name="sg_uf", type="string", length=45, nullable=false)
     */
    private $sgUf;

    /**
     * @var \Application\Entity\AesbeRegiaoEntity
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\AesbeRegiaoEntity")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="co_regiao", referencedColumnName="co_regiao")
     * })
     */
    private $coRegiao;

    /**
     * @return int
     */
    public function getCoUf()
    {
        return $this->coUf;
    }

    /**
     * @param int $coUf
     * @return AesbeUfEntity
     */
    public function setCoUf($coUf)
    {
        $this->coUf = $coUf;
        return $this;
    }

    /**
     * @return string
     */
    public function getNoUf()
    {
        return $this->noUf;
    }

    /**
     * @param string $noUf
     * @return AesbeUfEntity
     */
    public function setNoUf($noUf)
    {
        $this->noUf = $noUf;
        return $this;
    }

    /**
     * @return string
     */
    public function getSgUf()
    {
        return $this->sgUf;
    }

    /**
     * @param string $sgUf
     * @return AesbeUfEntity
     */
    public function setSgUf($sgUf)
    {
        $this->sgUf = $sgUf;
        return $this;
    }

    /**
     * @return AesbeRegiaoEntity
     */
    public function getCoRegiao()
    {
        return $this->coRegiao;
    }

    /**
     * @param AesbeRegiaoEntity $coRegiao
     * @return AesbeUfEntity
     */
    public function setCoRegiao($coRegiao)
    {
        $this->coRegiao = $coRegiao;
        return $this;
    }
}

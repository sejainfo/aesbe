<?php

namespace Application\Entity;

use Commons\Pattern\Entity\Impl\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * TbAesbeDescricao
 *
 * @ORM\Table(name="tb_aesbe_descricao")
 * @ORM\Entity
 */
class AesbeDescricaoEntity extends AbstractEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="co_descricao", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $coDescricao;

    /**
     * @var string
     *
     * @ORM\Column(name="no_descricao", type="text", length=65535, nullable=false)
     */
    private $noDescricao;

    /**
     * @return int
     */
    public function getCoDescricao()
    {
        return $this->coDescricao;
    }

    /**
     * @param int $coDescricao
     * @return AesbeDescricaoEntity
     */
    public function setCoDescricao($coDescricao)
    {
        $this->coDescricao = $coDescricao;
        return $this;
    }

    /**
     * @return string
     */
    public function getNoDescricao()
    {
        return $this->noDescricao;
    }

    /**
     * @param string $noDescricao
     * @return AesbeDescricaoEntity
     */
    public function setNoDescricao($noDescricao)
    {
        $this->noDescricao = $noDescricao;
        return $this;
    }
}

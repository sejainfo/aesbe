<?php

namespace Application\Entity;

use Commons\Pattern\Entity\Impl\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * AesbeArquivoEntity
 *
 * @ORM\Table(name="tb_aesbe_arquivo")
 * @ORM\Entity(repositoryClass="Commons\Pattern\Repository\Impl\SimpleEntityRepository")
 */
class AesbeArquivoEntity extends AbstractEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="co_arquivo", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $coArquivo;

    /**
     * @var string
     *
     * @ORM\Column(name="ds_arquivo", type="string", length=200, nullable=false)
     */
    private $dsArquivo;

    /**
     * @var \Application\Entity\AesbeTipoArquivoEntity
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\AesbeTipoArquivoEntity")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="co_tipo_arquivo", referencedColumnName="co_tipo_arquivo", nullable=false)
     * })
     */
    private $tipoArquivoEntity;

    /**
     * @return int
     */
    public function getCoArquivo()
    {
        return $this->coArquivo;
    }

    /**
     * @param int $coArquivo
     * @return AesbeArquivoEntity
     */
    public function setCoArquivo($coArquivo)
    {
        $this->coArquivo = $coArquivo;
        return $this;
    }

    /**
     * @return string
     */
    public function getDsArquivo()
    {
        return $this->dsArquivo;
    }

    /**
     * @param string $dsArquivo
     * @return AesbeArquivoEntity
     */
    public function setDsArquivo($dsArquivo)
    {
        $this->dsArquivo = $dsArquivo;
        return $this;
    }

    /**
     * @return AesbeTipoArquivoEntity
     */
    public function getTipoArquivoEntity()
    {
        return $this->tipoArquivoEntity;
    }

    /**
     * @param AesbeTipoArquivoEntity $tipoArquivoEntity
     * @return AesbeArquivoEntity
     */
    public function setTipoArquivoEntity($tipoArquivoEntity)
    {
        $this->tipoArquivoEntity = $tipoArquivoEntity;
        return $this;
    }
}

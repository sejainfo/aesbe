<?php

namespace Application\Entity;

use Commons\Pattern\Repository\Impl\SimpleEntityRepository;

class AesbePerfilRepository extends SimpleEntityRepository
{
    public function adicionarPerfil($coPerfil, $coUsuario)
    {
        $entityManager = $this->getEntityManager();
        $entity = $this->find($coPerfil);
        $entity->addOauthUsers(
            $entityManager
                ->getReference('Application\Entity\OauthUsersEntity', $coUsuario));
        $entityManager->persist($entity);
        $entityManager->flush();
        $entityManager->clear();

    }

    public function removerPerfil($coUsuario)
    {
        $parametros = array(
            'co_usuario' => $coUsuario
        );
//        $sql = "DELETE
//              FROM
//                aesbe.tb_aesbe_perfil_oauth_users
//              WHERE co_usuario = :coUsuario";
//        $parametros[':coUsuario'] = $coUsuario;
        return $this->getEntityManager()->getConnection()
            ->delete('aesbe.tb_aesbe_perfil_oauth_users', $parametros);

    }
}
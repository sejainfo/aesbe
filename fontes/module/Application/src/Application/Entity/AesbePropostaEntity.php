<?php

namespace Application\Entity;

use Commons\Pattern\Entity\Impl\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * AesbePropostaEntity
 *
 * @ORM\Table(name="tb_aesbe_proposta", indexes={@ORM\Index(name="fk_tb_aesbe_proposta_tb_aesbe_empresa1_idx", columns={"co_empresa"})})
 * @ORM\Entity(repositoryClass="Commons\Pattern\Repository\Impl\SimpleEntityRepository")
 */
class AesbePropostaEntity extends AbstractEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="co_proposta", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $coProposta;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_proposta", type="datetime", nullable=false)
     */
    private $dtProposta;

    /**
     * @var \Application\Entity\AesbeEmpresaEntity
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\AesbeEmpresaEntity")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="co_empresa", referencedColumnName="co_empresa")
     * })
     */
    private $empresaEntity;

    /**
     * @return int
     */
    public function getCoProposta()
    {
        return $this->coProposta;
    }

    /**
     * @param int $coProposta
     * @return AesbeProposta
     */
    public function setCoProposta($coProposta)
    {
        $this->coProposta = $coProposta;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDtProposta()
    {
        return $this->dtProposta;
    }

    /**
     * @param \DateTime $dtProposta
     * @return AesbeProposta
     */
    public function setDtProposta($dtProposta)
    {
        $this->dtProposta = $dtProposta;
        return $this;
    }

    /**
     * @return \Application\
     */
    public function getEmpresaEntity()
    {
        return $this->empresaEntity;
    }

    /**
     * @param \Application\ $empresaEntity
     * @return AesbeProposta
     */
    public function setEmpresaEntity($empresaEntity)
    {
        $this->empresaEntity = $empresaEntity;
        return $this;
    }
}

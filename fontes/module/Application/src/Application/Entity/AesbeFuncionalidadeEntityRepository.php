<?php

namespace Application\Entity;

use Commons\Pattern\Repository\Impl\SimpleEntityRepository;

class AesbeFuncionalidadeEntityRepository extends SimpleEntityRepository
{
    public function getFuncModByPerfil($coPerfil)
    {
        $queryb = $this->createQueryBuilder('F');
        $queryb->select(
            "P.coPerfil, P.noPerfil, F.coFuncionalidade, F.noFuncionalidade, ".
            "F.noFuncionalidadeVisivel, M.coModulo, M.noModulo, M.noModuloVisivel"
        );
        $queryb->innerJoin(
            'Application\Entity\AesbePerfilEntity', 'P', 'WITH',
            'P MEMBER OF F.coPerfil'
        );

        $queryb->innerJoin(
            'F.moduloEntity', 'M'
        );
        $queryb->where("P.coPerfil = :PERFIL");

        $queryb->setParameter('PERFIL', $coPerfil);

        $result = $queryb->getQuery()->getResult();
        return $result;
    }
}
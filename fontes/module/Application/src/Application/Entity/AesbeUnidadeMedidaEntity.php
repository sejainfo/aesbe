<?php

namespace Application\Entity;

use Commons\Pattern\Entity\Impl\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * TbAesbeUnidadeMedida
 *
 * @ORM\Table(name="tb_aesbe_unidade_medida")
 * @ORM\Entity
 */
class AesbeUnidadeMedidaEntity extends AbstractEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="co_unidade_medida", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $coUnidadeMedida;

    /**
     * @var string
     *
     * @ORM\Column(name="no_unidade_medida", type="string", length=45, nullable=false)
     */
    private $noUnidadeMedida;

    /**
     * @return int
     */
    public function getCoUnidadeMedida()
    {
        return $this->coUnidadeMedida;
    }

    /**
     * @param int $coUnidadeMedida
     * @return AesbeUnidadeMedidaEntity
     */
    public function setCoUnidadeMedida($coUnidadeMedida)
    {
        $this->coUnidadeMedida = $coUnidadeMedida;
        return $this;
    }

    /**
     * @return string
     */
    public function getNoUnidadeMedida()
    {
        return $this->noUnidadeMedida;
    }

    /**
     * @param string $noUnidadeMedida
     * @return AesbeUnidadeMedidaEntity
     */
    public function setNoUnidadeMedida($noUnidadeMedida)
    {
        $this->noUnidadeMedida = $noUnidadeMedida;
        return $this;
    }
}

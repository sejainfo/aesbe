<?php

namespace Application\Entity;

use Commons\Pattern\Entity\Impl\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * AesbeUsuarioEntity
 *
 * @ORM\Table(name="tb_aesbe_usuario", indexes={@ORM\Index(name="fk_tb_aesbe_usuario_tb_aesbe_pessoa_fisica1_idx", columns={"co_pessoa"})})
 * @ORM\Entity
 */
class AesbeUsuarioEntity extends AbstractEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="co_usuario", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $coUsuario;

    /**
     * @var string
     *
     * @ORM\Column(name="ds_senha", type="text", length=65535, nullable=false)
     */
    private $dsSenha;

    /**
     * @var \Application\Entity\AesbePessoaFisicaEntity
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\AesbePessoaFisicaEntity")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="co_pessoa", referencedColumnName="co_pessoa")
     * })
     */
    private $pessoaEntity;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Application\Entity\AesbePerfilEntity", mappedBy="coUsuario")
     */
    private $perfilEntity;

    /**
     * Constructor
     */
    public function __construct(array $options = array())
    {
        $this->perfilEntity = new \Doctrine\Common\Collections\ArrayCollection();
        parent::__construct($options);
    }

    /**
     * @return int
     */
    public function getCoUsuario()
    {
        return $this->coUsuario;
    }

    /**
     * @param int $coUsuario
     * @return AesbeUsuarioEntity
     */
    public function setCoUsuario($coUsuario)
    {
        $this->coUsuario = $coUsuario;
        return $this;
    }

    /**
     * @return string
     */
    public function getDsSenha()
    {
        return $this->dsSenha;
    }

    /**
     * @param string $dsSenha
     * @return AesbeUsuarioEntity
     */
    public function setDsSenha($dsSenha)
    {
        $this->dsSenha = $dsSenha;
        return $this;
    }

    /**
     * @return AesbePessoaFisicaEntity
     */
    public function getPessoaEntity()
    {
        return $this->pessoaEntity;
    }

    /**
     * @param AesbePessoaFisicaEntity $pessoaEntity
     * @return AesbeUsuarioEntity
     */
    public function setPessoaEntity($pessoaEntity)
    {
        $this->pessoaEntity = $pessoaEntity;
        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPerfilEntity()
    {
        return $this->perfilEntity;
    }

    /**
     * @param \Doctrine\Common\Collections\Collection $perfilEntity
     * @return AesbeUsuarioEntity
     */
    public function setPerfilEntity($perfilEntity)
    {
        $this->perfilEntity = $perfilEntity;
        return $this;
    }
}


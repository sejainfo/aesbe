<?php

namespace Application\Entity;

use Commons\Pattern\Entity\Impl\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * TbAesbeEspClasse
 *
 * @ORM\Table(name="tb_aesbe_esp_classe")
 * @ORM\Entity
 */
class AesbeEspClasseEntity extends AbstractEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="co_esp_classe", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $coEspClasse;

    /**
     * @var string
     *
     * @ORM\Column(name="ds_esp_classe", type="text", length=65535, nullable=false)
     */
    private $dsEspClasse;

    /**
     * @return int
     */
    public function getCoEspClasse()
    {
        return $this->coEspClasse;
    }

    /**
     * @param int $coEspClasse
     * @return AesbeEspClasseEntity
     */
    public function setCoEspClasse($coEspClasse)
    {
        $this->coEspClasse = $coEspClasse;
        return $this;
    }

    /**
     * @return string
     */
    public function getDsEspClasse()
    {
        return $this->dsEspClasse;
    }

    /**
     * @param string $dsEspClasse
     * @return AesbeEspClasseEntity
     */
    public function setDsEspClasse($dsEspClasse)
    {
        $this->dsEspClasse = $dsEspClasse;
        return $this;
    }

}

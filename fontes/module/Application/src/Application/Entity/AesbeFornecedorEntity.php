<?php

namespace Application\Entity;

use Commons\Pattern\Entity\Impl\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * AesbeFornecedorEntity
 *
 * @ORM\Table(name="tb_aesbe_fornecedor")
 * @ORM\Entity(repositoryClass="Commons\Pattern\Repository\Impl\SimpleEntityRepository")
 */
class AesbeFornecedorEntity extends AbstractEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="co_fornecedor", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $coFornecedor;

    /**
     * @var string
     *
     * @ORM\Column(name="nu_cnpj", type="string", length=14, nullable=false)
     */
    private $nuCnpj;

    /**
     * @var string
     *
     * @ORM\Column(name="no_fornecedor", type="string", length=200, nullable=false)
     */
    private $noFornecedor;

    /**
     * @var string
     *
     * @ORM\Column(name="no_representante", type="string", length=200, nullable=false)
     */
    private $noRepresentante;

    /**
     * @return int
     */
    public function getCoFornecedor()
    {
        return $this->coFornecedor;
    }

    /**
     * @param int $coFornecedor
     * @return AesbeFornecedorEntity
     */
    public function setCoFornecedor($coFornecedor)
    {
        $this->coFornecedor = $coFornecedor;
        return $this;
    }

    /**
     * @return string
     */
    public function getNuCnpj()
    {
        return $this->nuCnpj;
    }

    /**
     * @param string $nuCnpj
     * @return AesbeFornecedorEntity
     */
    public function setNuCnpj($nuCnpj)
    {
        $this->nuCnpj = $nuCnpj;
        return $this;
    }

    /**
     * @return string
     */
    public function getNoFornecedor()
    {
        return $this->noFornecedor;
    }

    /**
     * @param string $noFornecedor
     * @return AesbeFornecedorEntity
     */
    public function setNoFornecedor($noFornecedor)
    {
        $this->noFornecedor = $noFornecedor;
        return $this;
    }

    /**
     * @return string
     */
    public function getNoRepresentante()
    {
        return $this->noRepresentante;
    }

    /**
     * @param string $noRepresentante
     * @return AesbeFornecedorEntity
     */
    public function setNoRepresentante($noRepresentante)
    {
        $this->noRepresentante = $noRepresentante;
        return $this;
    }

}

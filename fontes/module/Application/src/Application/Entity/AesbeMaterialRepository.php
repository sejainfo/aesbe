<?php

namespace Application\Entity;

use Commons\Pattern\Repository\Impl\SimpleEntityRepository;


class AesbeMaterialRepository extends SimpleEntityRepository
{
    const PERIODO_PERSONALIZADO = "P";


    public function listarMaterial()
    {
        $qb = $this->createQueryBuilder('M');
        $query = $qb->select(
            'M.nuSabesp, M.noBasico, EC.dsEspClasse, ES.dsEspSerie,
            D.noDescricao,U.noUnidadeMedida, M.coMaterial'
        )
            ->innerJoin('M.unidadeMedidaEntity', 'U')
            ->leftJoin('M.descricaoEntity', 'D')
            ->leftJoin('M.espClasseEntity', 'EC')
            ->leftJoin('M.espSerieEntity', 'ES');

        return $query->getQuery()->getResult();
    }

    public function buscarMaterialPesquisaPreco($params)
    {
        $parametros = array();
        $sql = "SELECT
                vl_com_imposto,
                pd.dt_informacao,
                pd.co_material,
                COALESCE(concat(m.no_basico,' - ',d.no_descricao), m.no_basico) as noBasico,
                ec.ds_esp_classe,
                es.ds_esp_serie,
                f.no_fornecedor,
                f.nu_cnpj,
                pd.qt_produto,
                pd.nu_contrato,
                pd.nu_ata,
                m.nu_sabesp,
                um.no_unidade_medida,
                e.no_empresa,
                pd.dt_proposta,
                pd.dt_contratacao,
                pd.dt_vigencia_contrato,
                pd.qt_dias_pagamento,
                pd.st_fob_cif,
                pd.co_proposta_dados,
                pd.vl_icms,
                pd.vl_ipi,
                pd.vl_iss,
                pd.st_condicao_fornecimento,
                pd.tp_inspecao,
                pd.st_nivel_atendimento,
                pd.st_especificacao_padrao,
                pd.ds_observacao,
                ml.ds_modalidade
              FROM
                aesbe.tb_aesbe_proposta_dados pd
                inner JOIN aesbe.tb_aesbe_fornecedor f ON f.co_fornecedor = pd.co_fornecedor
                INNER JOIN aesbe.tb_aesbe_proposta p ON p.co_proposta = pd.co_proposta
                INNER JOIN aesbe.tb_aesbe_material m ON m.co_material = pd.co_material
                LEFT JOIN aesbe.tb_aesbe_descricao d ON d.co_descricao = m.co_descricao
                INNER JOIN aesbe.tb_aesbe_unidade_medida um ON um.co_unidade_medida = m.co_unidade_medida
                INNER JOIN aesbe.tb_aesbe_empresa e ON e.co_empresa = p.co_empresa
                INNER JOIN aesbe.tb_aesbe_modalidade_licitacao ml ON ml.co_modalidade = pd.co_modalidade
                left JOIN aesbe.tb_aesbe_esp_classe ec ON ec.co_esp_classe = m.co_esp_classe
                left JOIN aesbe.tb_aesbe_esp_serie es ON es.co_esp_serie = m.co_esp_serie
              WHERE 1 = 1";
        if (!empty($params['search'])) {
            $sql .= " and m.no_basico like '%{$params['search']}%'";
        }
        if (!empty($params['coEmpresa'])) {
            $sql .= " and p.co_empresa = :coEmpresa";
            $parametros[':coEmpresa'] = $params['coEmpresa'];
        }
        if (!empty($params['coMaterial'])) {
            $sql .= " and pd.co_material = :coMaterial";
            $parametros[':coMaterial'] = $params['coMaterial'];
        }
        if (!empty($params['nuSabesp'])) {
            $sql .= " and m.nu_sabesp = :nuSabesp";
            $parametros[':nuSabesp'] = $params['nuSabesp'];
        }
        if (!empty($params['dtInicio']) && $params['periodo'] == $this::PERIODO_PERSONALIZADO) {
            $sql .= " and pd.dt_informacao >= :dtInicio";
            $parametros[':dtInicio'] = $params['dtInicio'];
        }
        if (!empty($params['dtFim']) && $params['periodo'] == $this::PERIODO_PERSONALIZADO) {
            $sql .= " and pd.dt_informacao <= :dtFim";
            $parametros[':dtFim'] = $params['dtFim'];
        }

        if (!empty($params['periodo']) && $params['periodo'] != $this::PERIODO_PERSONALIZADO) {
            $dtAtual = new \DateTime();
            $iterval = "P".$params['periodo']."D";
            $dtAtual->sub(new \DateInterval($iterval));
            $sql .= " and pd.dt_informacao <= :dtFim";
            $parametros[':dtFim'] = $dtAtual->format("Y-m-d");
        }
        $sql .= " order by pd.co_material asc, pd.vl_com_imposto asc, pd.qt_produto asc";
        return $this->getEntityManager()->getConnection()->fetchAll($sql, $parametros);

    }

}

<?php

namespace Application\Entity;

use Commons\Pattern\Repository\Impl\SimpleEntityRepository;


class AesbeArquivoPropostaRepository extends SimpleEntityRepository
{
    /**
     * Lista Arquivos da Empresa
     * @param int $coEmpresa
     * @return array
     */
    public function listarArquivos($coEmpresa)
    {
        $qb = $this->createQueryBuilder('AP');
        $query = $qb->select('AP')
            ->innerJoin('AP.propostaEntity', 'P')
            ->andWhere($qb->expr()->eq('P.empresaEntity', ':coEmpresa'))
            ->setParameter('coEmpresa', $coEmpresa);
        return $query->getQuery()->getResult();
    }
}

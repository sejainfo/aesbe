<?php

namespace Application\Entity;

use Commons\Pattern\Entity\Impl\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * AesbeRegiaoEntity
 *
 * @ORM\Table(name="tb_aesbe_regiao")
 * @ORM\Entity
 */
class AesbeRegiaoEntity extends AbstractEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="co_regiao", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $coRegiao;

    /**
     * @var string
     *
     * @ORM\Column(name="no_regiao", type="string", length=45, nullable=false)
     */
    private $noRegiao;

    /**
     * @var integer
     *
     * @ORM\Column(name="nu_ordem_apresentacao", type="integer", nullable=false)
     */
    private $nuOrdemApresentacao;

    /**
     * @return int
     */
    public function getCoRegiao()
    {
        return $this->coRegiao;
    }

    /**
     * @param int $coRegiao
     * @return AesbeRegiaoEntity
     */
    public function setCoRegiao($coRegiao)
    {
        $this->coRegiao = $coRegiao;
        return $this;
    }

    /**
     * @return string
     */
    public function getNoRegiao()
    {
        return $this->noRegiao;
    }

    /**
     * @param string $noRegiao
     * @return AesbeRegiaoEntity
     */
    public function setNoRegiao($noRegiao)
    {
        $this->noRegiao = $noRegiao;
        return $this;
    }

    /**
     * @return int
     */
    public function getNuOrdemApresentacao()
    {
        return $this->nuOrdemApresentacao;
    }

    /**
     * @param int $nuOrdemApresentacao
     * @return AesbeRegiaoEntity
     */
    public function setNuOrdemApresentacao($nuOrdemApresentacao)
    {
        $this->nuOrdemApresentacao = $nuOrdemApresentacao;
        return $this;
    }
}

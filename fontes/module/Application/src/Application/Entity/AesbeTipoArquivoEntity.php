<?php

namespace Application\Entity;

use Commons\Pattern\Entity\Impl\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * AesbeTipoArquivoEntity
 *
 * @ORM\Table(name="tb_aesbe_tipo_arquivo")
 * @ORM\Entity
 */
class AesbeTipoArquivoEntity extends AbstractEntity
{
    const CO_TIPO_PROPOSTA = 1;
    const CO_TIPO_ATA = 2;

    /**
     * @var integer
     *
     * @ORM\Column(name="co_tipo_arquivo", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $coTipoArquivo;

    /**
     * @var string
     *
     * @ORM\Column(name="ds_tipo_arquivo", type="string", length=100, nullable=false)
     */
    private $dsTipoArquivo;

    /**
     * @return int
     */
    public function getCoTipoArquivo()
    {
        return $this->coTipoArquivo;
    }

    /**
     * @param int $coTipoArquivo
     * @return AesbeTipoArquivoEntity
     */
    public function setCoTipoArquivo($coTipoArquivo)
    {
        $this->coTipoArquivo = $coTipoArquivo;
        return $this;
    }

    /**
     * @return string
     */
    public function getDsTipoArquivo()
    {
        return $this->dsTipoArquivo;
    }

    /**
     * @param string $dsTipoArquivo
     * @return AesbeTipoArquivoEntity
     */
    public function setDsTipoArquivo($dsTipoArquivo)
    {
        $this->dsTipoArquivo = $dsTipoArquivo;
        return $this;
    }
}

<?php
namespace Application\Service;

use Application\Mail\AbstractSendMail;
use Commons\Pattern\Service\Service;

/**
 * Class AesbeMail
 * @package Application\Service
 */
class AesbeMail extends AbstractSendMail implements Service
{
    
    /**
     * @param string $html
     * @param string $subject
     * @param array|string $addresses
     * @param array|null $attachment
     */
    public function sendEmail($html, $subject, $addresses, array $attachment = null)
    {

        if (!is_array($addresses)) {
            $address[] = $addresses;
        }else {
            $address = $addresses;
        }

        $messege = $this->mountMail(utf8_decode($html), $subject, $address, $attachment);
        $this->transport->send($messege);
    }

    /**
     * @param $text
     * @param $subject
     * @param $address
     * @param array|null $attachment
     */
    public function sendEmailText($text, $subject, $address, array $attachment = null)
    {
        $html = $this->getDefaultHtml($text);
        $this->sendEmail($html, $subject, $address, $attachment);
    }

    /**
     * @param $text
     * @return string
     */
    private function getDefaultHtml($text)
    {
        $html = '
            <table cellspacing="0" cellpadding="0" border="0" style="border-collapse:collapse;width:600px;background-color:#ffffff;border:1px solid #d9d9d9; margin: auto;">
                <tbody>
                    <tr>
                        <td valign="top" align="center" style="font-family:Helvetica,Arial,sans-serif;line-height:160%">
                            <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="border-collapse:collapse">
                                <tbody><tr>
                                        <td align="center" style="background-color:#ffffff;font-family:Helvetica,Arial,sans-serif;line-height:160%;padding-top:40px;padding-bottom:40px;background:#fff">
                                            <img width="188" style="border:0;min-height:auto;line-height:100%;outline:none;text-decoration:none;" alt="Aesbe" src="http://www.aesbe.org.br/images/logo.jpg">
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-family:Helvetica,Arial,sans-serif;line-height:160%;color:#404040;font-size:16px;padding-top:14px;padding-bottom:40px;padding-right:72px;padding-left:72px;background:#ffffff">
                            <table width="100%" cellspacing="0" cellpadding="0" border="0" style="border-collapse:collapse;background-color:#ffffff">
                                <tbody>
                                  <tr>
                                    <td style="font-family:Helvetica,Arial,sans-serif;line-height:160%;padding-bottom:32px;text-align:center">
                                      <p style="margin:0">'.$text.'</p>
                                    </td>
                                  </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-family:Helvetica,Arial,sans-serif;line-height:160%;background:#ffffff">
                            <table width="100%" cellspacing="0" cellpadding="0" border="0" style="border-collapse:collapse;background-color:#ffffff">
                                <tbody>
                                    <tr>
                                      <td style="font-family:Helvetica,Arial,sans-serif;line-height:160%;padding-bottom:40px;text-align:center">
                                            <p style="margin:0;color:#999;font-size:12px">Aesbe '.date('Y').'</p>
                                            <span style="color:#ffffff;font-size:0;min-height:0"></span>
                                      </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
            <br /><br /><br />
        ';

        return $html;
    }
}
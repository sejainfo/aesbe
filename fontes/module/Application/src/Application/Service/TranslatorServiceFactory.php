<?php

namespace Application\Service;


use Commons\Pattern\Service\Impl\ZendServiceLookupManager;
use Zend\Log\Logger;
use Zend\Log\PsrLoggerAdapter;

class TranslatorServiceFactory
{
    public function __invoke($services)
    {
        $lookupManager = new ZendServiceLookupManager($services);
        $logger = new PsrLoggerAdapter(new Logger());
        $translator = new TranslatorService($lookupManager, $logger);
        $srvTranslator = $services->get('Zend\I18n\Translator\TranslatorInterface');
        $translator->setTranslator($srvTranslator);
        return $translator;
    }
}
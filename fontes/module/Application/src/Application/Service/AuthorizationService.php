<?php

namespace Application\Service;

use Application\Acl\Authorization;
use Application\Entity\OauthAccessTokens;
use Core\V1\Rest\Funcionalidade\FuncionalidadeRepositoryService;
use Core\V1\Rest\Modulo\ModuloRepositoryService;
use Core\V1\Rest\OauthUsers\OauthAccessTokensRepositoryService;
use Core\V1\Rest\Perfil\PerfilRepositoryService;
use Herrera\Json\Exception\Exception;
use ZF\ApiProblem\ApiProblem;
use ZF\Rest\AbstractResourceListener;


/**
 * Class AuthorizationService
 * @package Application\Service
 */
class AuthorizationService extends AbstractResourceListener
{
    /**
     * @var Authorization
     */
    private $authorization;

    /**
     * @var FuncionalidadeRepositoryService
     */
    private $srvFunc;

    /**
     * @var OauthAccessTokensRepositoryService
     */
    private $srvOauthAcess;

    /**
     * AuthorizationService constructor.
     */
    public function __construct(
        PerfilRepositoryService $srvPerfil,
        ModuloRepositoryService $srvModulo,
        FuncionalidadeRepositoryService $srvFunc,
        OauthAccessTokensRepositoryService $srvOauth
    ) {
        $this->srvFunc = $srvFunc;
        $this->authorization = new Authorization($srvPerfil, $srvModulo, $srvFunc);
        $this->srvOauthAcess = $srvOauth;
    }

    /**
     * @param $perfil
     * @param $modulo
     * @param $funcionalidade
     * @return bool
     */
    public function hasPermission($perfil, $modulo, $funcionalidade)
    {
        return $this->authorization->isAllowed($perfil, $modulo, $funcionalidade);
    }

    /**
     * @param $coPerfil
     * @return mixed
     */
    public function getPermissions($coPerfil)
    {
        return $this->srvFunc->getFuncModByPerfil($coPerfil);
    }


    public function preDispatch($method, $path)
    {

        if (isset($_SERVER['SHELL'])) {
            return true;
        }
        if (
            !preg_match('/^.*apigility.*/', $path) && !preg_match('/^.*oauth$/', $path) &&
            !preg_match('/^.*publico.*$/', $path)
        ) {
            $typeMethod = $this->typeMethodRequest($method, $path);
            $token = str_replace('Bearer ', '',$_SERVER['HTTP_AUTHORIZATION']);
            $requestRoute = $this->getModuloFuncionalidade($path);

            if(!$token){
                return null;
            }
            $access = $this->srvOauthAcess->getUserByToken($token   );
            $perfis = $access->getUserId()->getPerfis();
            foreach ($perfis as $perfil) {
                $permission = $this->hasPermission(
                    $perfil->getCoPerfil(), $requestRoute['modulo'],
                    $requestRoute['funcionalidade'].$typeMethod
                );
                if ($permission) {
                    return true;
                }
            }
            return false;
        }
        return true;
    }

    /**
     * @param $method
     * @param $path
     * @return null|string
     */
    public function typeMethodRequest($method, $path)
    {

        if (preg_match('/^.*POST.*/',$method) && preg_match('/^.*\/[0-9]+\/?$/',$path)) {
            return 'PUT';
        }

        if (preg_match('/^.*POST.*/',$method)) {
            return 'POST';
        }

        if (preg_match('/^.*GET.*/',$method) && preg_match('/^.*\/[0-9]+\/?$/',$path)) {
            return 'FETCH';
        }

        if (preg_match('/^.*GET.*/',$method)) {
            return 'FETCHALL';
        }

        if (preg_match('/^.*PUT.*/',$method)) {
            return 'PUT';
        }

        return null;
    }

    public function getModuloFuncionalidade($path)
    {
        preg_match_all('/^\/([\w-]+)\/([\w-]+)?[\/\?]?.*/', $path, $result);

        if(!isset($result[1][0]) || !isset($result[2][0])) {
            throw new \Exception('Nao foi possivel detectar modulo/funcionalidade');
        }

        $data['modulo'] = $result[1][0];
        $data['funcionalidade'] = $result[2][0];

        return $data;
    }
}
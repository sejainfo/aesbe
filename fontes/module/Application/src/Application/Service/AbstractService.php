<?php
namespace Application\Service;

use Commons\Pattern\Service\Impl\AbstractCoreService;

/**
 * Created by IntelliJ IDEA.
 * User: familia
 * Date: 04/06/16
 * Time: 15:31
 */
class AbstractService extends AbstractCoreService
{
    /**
     * @return \Application\Service\TranslatorService
     */
    protected function getTranslatorService()
    {
        return $this->getLookupManager()->get('Application\\Service\\TranslatorService');

    }
}

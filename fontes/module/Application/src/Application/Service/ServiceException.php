<?php
namespace Application\Service;

class ServiceException extends \RuntimeException
{

    public function __construct($message, $code = 406, $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}

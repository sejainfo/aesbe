<?php
namespace Application\Service;

use Commons\Pattern\Service\Impl\RepositoryService;

/**
 * Created by IntelliJ IDEA.
 * User: familia
 * Date: 04/06/16
 * Time: 15:31
 */
class AbstractRepositoryService extends RepositoryService
{
    public function delete($id)
    {
        parent::delete($id);
        return true;
    }

    /**
     * @return \Application\Service\TranslatorService
     */
    protected function getTranslatorService()
    {
        return $this->getLookupManager()->get('Application\\Service\\TranslatorService');

    }

    /**
     * @return \Core\V1\Rest\OauthUsers\OauthUsersRepositoryService
     */
    protected function getOauthUsersRepositoryService()
    {
        return $this->getLookupManager()->get('Core\\V1\\Rest\\OauthUsers\\OauthUsersRepositoryService');

    }

}
<?php

namespace Application\Service;


use Commons\Pattern\Service\Impl\AbstractCoreService;

class TranslatorService extends AbstractCoreService
{
    /** @var  \Zend\Mvc\I18n\Translator */
    private $translator;

    public function translate($msg)
    {
        return $this->translator->translate($msg);
    }

    public function translateWithParam($msg, $param)
    {
        switch (true) {
            case is_array($param):
                $translateMsg = vsprintf($this->translate($msg), $param);
                break;
            default:
                $translateMsg = sprintf($this->translate($msg), $param);
                break;
        }
        return $translateMsg;
    }

    /**
     * @param \Zend\Mvc\I18n\Translator $translator
     */
    public function setTranslator($translator)
    {
        $this->translator = $translator;
    }

}
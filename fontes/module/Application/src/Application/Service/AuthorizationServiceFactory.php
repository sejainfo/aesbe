<?php

namespace Application\Service;

use Zend\ServiceManager\ServiceManager;


/**
 * Class AuthorizationFactory
 * @package Application\Service
 */
class AuthorizationServiceFactory
{

    /**
     * @param $services ServiceManager
     * @return AuthorizationService
     */
    public function __invoke($services)
    {
        $srvPerfil = $services->get('Core\V1\Rest\Perfil\PerfilRepositoryService');
        $srvModulo = $services->get('Core\V1\Rest\Modulo\ModuloRepositoryService');
        $srvFunc = $services->get('Core\V1\Rest\Funcionalidade\FuncionalidadeRepositoryService');
        $srvOauth = $services->get('Core\V1\Rest\OauthUsers\OauthAccessTokensRepositoryService');

        return new AuthorizationService($srvPerfil, $srvModulo, $srvFunc, $srvOauth);
    }
}
<?php
/**
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3-Clause
 * @copyright Copyright (c) 2014 Zend Technologies USA Inc. (http://www.zend.com)
 */

namespace Application;

use Application\Entity\OauthClients;
use Application\Entity\OauthUsersEntity;
use Application\Service\AuthorizationService;
use Herrera\Json\Exception\Exception;
use OAuth2\Server;
use Zend\Debug\Debug;
use Zend\EventManager\EventManager;
use Zend\Http\Response;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use ZF\ApiProblem\ApiProblem;
use ZF\ApiProblem\ApiProblemResponse;
use ZF\MvcAuth\Factory\NamedOAuth2ServerFactory;

class Module
{
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager = $e->getApplication()->getEventManager();

        $eventManager->attach('dispatch', function($e) {
            $services = $e->getApplication()->getServiceManager();

            /* @var $authorization \Application\Service\AuthorizationService */
            $authorization = $services->get('Application\Service\AuthorizationService');

            $method = $e->getRequest()->getMethod();
            $path = $e->getRequest()->getUri()->getPath();
            $path = str_replace('/public/', "/",$path);

            $allowed = $authorization->preDispatch($method, $path);

            if (!$allowed) {
                $response = $e->getResponse();
                $response->getHeaders()->addHeaderLine('Content-Type', 'application/json; charset=utf-8');
                $response->getHeaders()->addHeaderLine('Access-Control-Allow-Origin', '*');
                $response->setStatusCode(403);
                $response->setContent('{}');
                return $response;
            }
        });

    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

}

<?php
namespace BancoPreco\V1\Rest\ModeloArquivoProposta;

class ModeloArquivoPropostaResourceFactory
{
    public function __invoke($services)
    {
        $srvModeloArquivoProposta = $services->get('BancoPreco\\V1\\Rest\\ModeloArquivoProposta\\ModeloArquivoPropostaService');
        $srvArquivoExcelMaterial = $services->get('BancoPreco\\V1\\Rest\\ArquivoProposta\\ArquivoExcelMaterialService');

        return new ModeloArquivoPropostaResource($srvModeloArquivoProposta, $srvArquivoExcelMaterial);
    }
}

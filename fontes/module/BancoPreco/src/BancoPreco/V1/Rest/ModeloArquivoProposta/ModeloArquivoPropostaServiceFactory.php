<?php

namespace BancoPreco\V1\Rest\ModeloArquivoProposta;

use Commons\Pattern\Service\Impl\ZendServiceLookupManager;
use Zend\Log\Logger;
use Zend\Log\PsrLoggerAdapter;

/**
 * Class ModeloArquivoPropostaServiceFactory
 * @package BancoPreco\V1\Rest\ModeloArquivoProposta
 */
class ModeloArquivoPropostaServiceFactory
{
    public function __invoke($services)
    {
        $lookupManager = new ZendServiceLookupManager($services);
        $logger = new PsrLoggerAdapter(new Logger());
        return new ModeloArquivoPropostaService($lookupManager, $logger);
    }
}
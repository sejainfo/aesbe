<?php
namespace BancoPreco\V1\Rest\ModeloArquivoProposta;

use ZF\ApiProblem\ApiProblem;
use ZF\Rest\AbstractResourceListener;

class ModeloArquivoPropostaResource extends AbstractResourceListener
{

    /* @var $repo \BancoPreco\V1\Rest\ModeloArquivoProposta\ModeloArquivoPropostaService */
    protected $srvModeloArquivoProposta;

    /* @var $srvArquivoExcelMaterial \BancoPreco\V1\Rest\ArquivoProposta\ArquivoExcelMaterialService */
    protected $srvArquivoExcelMaterial;

    public function __construct($srvModeloArquivoProposta, $srvArquivoExcelMaterial)
    {
        $this->srvModeloArquivoProposta = $srvModeloArquivoProposta;
        $this->srvArquivoExcelMaterial = $srvArquivoExcelMaterial;
    }
    /**
     * Create a resource
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function create($data)
    {
        return new ApiProblem(405, 'The POST method has not been defined');
    }

    /**
     * Delete a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function delete($id)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for individual resources');
    }

    /**
     * Delete a collection, or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function deleteList($data)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for collections');
    }

    /**
     * Fetch a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function fetch($id)
    {
        return new ApiProblem(405, 'The GET method has not been defined for individual resources');
    }

    /**
     * Fetch all or a subset of resources
     *
     * @param  array $params
     * @return ApiProblem|mixed
     */
    public function fetchAll($params = array())
    {
        return $this->srvModeloArquivoProposta->montarArquivoProposta();
    }

    /**
     * Patch (partial in-place update) a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patch($id, $data)
    {
        return new ApiProblem(405, 'The PATCH method has not been defined for individual resources');
    }

    /**
     * Replace a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function replaceList($data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for collections');
    }

    /**
     * Update a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function update($id, $data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for individual resources');
    }
}

<?php

namespace BancoPreco\V1\Rest\ModeloArquivoProposta;

use BancoPreco\V1\Rest\ArquivoProposta\ExcelUtil;
use Application\Service\AbstractService;

/**
 * Class ModeloArquivoPropostaService
 * @package BancoPreco\V1\Rest\ModeloArquivoProposta
 */
class ModeloArquivoPropostaService extends AbstractService
{
    /**
     * Criação do arquivo modelo para importação
     */
    public function montarArquivoProposta()
    {
        return $this->getArquivoExcelMaterialService()->createExcelFromArray($this->montaArrayMaterial(), 'Planilha Banco de Preço');
    }

    /**
     * @return array
     */
    public function montaArrayMaterial()
    {
        $materiais = $this->getMaterialRepositoryService()->listarMaterial();
        $arrMateriais = array();
        $i = 1;
        foreach ($materiais as $material) {
            $arrMateriais['A'][$i] = $material['coMaterial'];
            $arrMateriais['B'][$i] = $material['nuSabesp'];
            $arrMateriais['C'][$i] = '';
            $arrMateriais['D'][$i] = $material['noUnidadeMedida'];
            $arrMateriais['E'][$i] = $material['noBasico'];
            $arrMateriais['F'][$i] = $material['noDescricao'];
            $arrMateriais['G'][$i] = $material['dsEspClasse'];
            $arrMateriais['H'][$i] = $material['dsEspSerie'];
            $i++;
        }

        return $arrMateriais;
    }

    /**
     * @return \BancoPreco\V1\Rest\Material\MaterialRepositoryService
     */
    protected function getMaterialRepositoryService()
    {
        return $this->getLookupManager()->get('BancoPreco\\V1\\Rest\\Material\\MaterialRepositoryService');

    }

    /**
     * @return \BancoPreco\V1\Rest\ArquivoProposta\ArquivoExcelMaterialService
     */
    protected function getArquivoExcelMaterialService()
    {
        return $this->getLookupManager()->get('BancoPreco\\V1\\Rest\\ArquivoProposta\\ArquivoExcelMaterialService');

    }
}
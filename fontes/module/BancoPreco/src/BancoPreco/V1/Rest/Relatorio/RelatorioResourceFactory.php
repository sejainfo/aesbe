<?php
namespace BancoPreco\V1\Rest\Relatorio;

class RelatorioResourceFactory
{
    public function __invoke($services)
    {
        $srvRelatorio = $services
            ->get('BancoPreco\\V1\\Rest\\Relatorio\\RelatorioService');
        return new RelatorioResource($srvRelatorio);
    }
}

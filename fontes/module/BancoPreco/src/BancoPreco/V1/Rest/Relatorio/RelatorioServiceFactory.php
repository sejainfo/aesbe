<?php
namespace BancoPreco\V1\Rest\Relatorio;

use Commons\Pattern\Service\Impl\ZendServiceLookupManager;
use Zend\Log\Logger;
use Zend\Log\PsrLoggerAdapter;

class RelatorioServiceFactory
{
    public function __invoke($services)
    {
        $lookupManager = new ZendServiceLookupManager($services);
        $logger = new PsrLoggerAdapter(new Logger());
        $srvViewRender = $services->get('ViewRenderer');
        $config = $services->get('config');
        $diretorio = $config['path']['upload'];
        return new RelatorioService($lookupManager, $logger, $srvViewRender, $diretorio);
    }
}

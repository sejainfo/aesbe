<?php
namespace BancoPreco\V1\Rest\Relatorio;

use Application\Service\AbstractService;
use Commons\Pattern\Di\LookupManager;
use Psr\Log\LoggerInterface;
use Zend\Mime\Mime;
use Zend\Validator\EmailAddress;
use Zend\View\Model\ViewModel;
use Zend\Mime\Part as MimePart;
use Zend\Http\Response;

class RelatorioService extends AbstractService
{
    protected $srvViewRender;

    protected $diretorioUpload;

    /**
     * RelatorioService constructor.
     */
    public function __construct(LookupManager $lookupManager, LoggerInterface $logger, $srvViewRender, $diretorioUpload)
    {
        $this->srvViewRender = $srvViewRender;
        $this->diretorioUpload = $diretorioUpload;
        parent::__construct($lookupManager, $logger);
    }

    /**
     * Geração do relatório do Banco de Preços
     * @param array $params
     * @param null|string $noArquivo
     * @return int|Response
     */
    public function gerarRelatorio($params, $noArquivo = null)
    {
        ini_set('memory_limit', '-1');
        set_time_limit(0);
        $material = $this->getMaterialRepositoryService()->buscarMaterialPesquisaPreco($params);
        $viewModel = new ViewModel(
            array(
                'material' => $material
            )
        );
        $viewModel->setTemplate('banco-preco/relatorio-pesquisa-preco');
        $relatorio = $this->srvViewRender->render($viewModel);
        $nome = 'Relatório Pesquisa de Preços - AESBE';
        if ($noArquivo) {
            return file_put_contents($noArquivo, $relatorio);
        }
        $response = new Response();
        $headers = $response->getHeaders();
        $headers->addHeaderLine('Content-Type', 'application/vnd.ms-excel;charset=utf-8')
            ->addHeaderLine(
                'Content-Disposition',
                sprintf("attachment; filename=\"%s\"", $nome . '.xls')
            )
            ->addHeaderLine('Accept-Ranges', 'bytes')
            ->addHeaderLine('Content-Length', strlen($relatorio));

        $response->setContent($relatorio);
        return $response;
    }


    /**
     * Enviar relatório por email
     * @param $data
     * @throws \Exception
     */
    public function enviarRelatorioPorEmail($data)
    {
        $arrayEmail = $this->validarEmailRelatorio($data->listaEmail);
        if (!count($data) || !count($arrayEmail)) {
            throw new \Exception('Campo email obrigatório');
        }
        $dtAtual = new \DateTime();

        $nomeArquivo = "Relatorio_Banco_Preco-".$dtAtual->format('d-m-Y').'.xlsx';
        $path = $this->diretorioUpload."/".$nomeArquivo;



        $this->gerarRelatorio($data->itemPesquisa, $path);

        $attachment = new MimePart(fopen($path, 'r'));
        $attachment->type = 'application/vnd.ms-excel;charset=utf-8';
        $attachment->filename = $nomeArquivo;
        $attachment->encoding = Mime::ENCODING_BASE64;
        $attachment->disposition = Mime::DISPOSITION_ATTACHMENT;

        $this->getAesbeEmailService()->sendEmailText(
            'AESBE - Relatório Banco de Preço - Pesquisa realizada em '.$dtAtual->format('d/m/Y - H:i:s'),
            'AESBE - Relatório Banco de Preço - '.$dtAtual->format('d/m/Y - H:i:s'),
            $arrayEmail,
            array($attachment)
        );

    }

    /**
     * Valida email para envio do relatório
     * @param $data
     * @return array
     * @throws \Exception
     */
    private function validarEmailRelatorio($data)
    {
        $email = array();
        $validator = new EmailAddress();
        foreach ($data as $value) {
            if (!$validator->isValid($value['email'])) {
                throw new \Exception('Email inválido');
            }
            array_push($email, $value['email']);
        }
        return $email;

    }


    /**
     * @return \BancoPreco\V1\Rest\Material\MaterialRepositoryService
     */
    protected function getMaterialRepositoryService()
    {
        return $this->getLookupManager()->get('BancoPreco\\V1\\Rest\\Material\\MaterialRepositoryService');
    }

    /**
     * @return \Application\Service\AesbeMail
     */
    protected function getAesbeEmailService()
    {
        return $this->getLookupManager()->get('Application\Service\AesbeMail');

    }


}

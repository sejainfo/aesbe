<?php
namespace BancoPreco\V1\Rest\ArquivoProposta;

use ZF\ApiProblem\ApiProblem;
use ZF\Rest\AbstractResourceListener;

class ArquivoPropostaResource extends AbstractResourceListener
{

    /* @var $srvArquivoProposta \BancoPreco\V1\Rest\ArquivoProposta\ArquivoPropostaService */
    protected $srvArquivoProposta;
    /* @var $srvArquivoExcelMaterial \BancoPreco\V1\Rest\ArquivoProposta\ArquivoExcelMaterialService */
    protected $srvArquivoExcelMaterial;

    public function __construct($srvArquivoProposta, $srvArquivoExcelMaterial)
    {
        $this->srvArquivoProposta = $srvArquivoProposta;
        $this->srvArquivoExcelMaterial = $srvArquivoExcelMaterial;
    }
    /**
     * Create a resource
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function create($data)
    {
        try{
            ini_set('memory_limit', '-1');
            $file = $data->file;
            $this->srvArquivoExcelMaterial->validaTipoExtensaoArquivo($file);
            $this->srvArquivoExcelMaterial->validaTamanhoArquivo($file);
            $arrExcel = $this->srvArquivoExcelMaterial->parserExcelToArray($file['tmp_name']);
            $result = $this->srvArquivoProposta->importarArquivo(
                $arrExcel, $file, $file['name'], $this->getIdentity()->getRoleId()
            );
            if (isset($result['erros'])) {
                return new ApiProblem(402, 'Erro ao importar arquivo.', null, null, $result);
            }
            return $result;
        }catch(\Exception $e) {
            return new ApiProblem(405, $e->getMessage());
        }
    }

    /**
     * Delete a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function delete($id)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for individual resources');
    }

    /**
     * Delete a collection, or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function deleteList($data)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for collections');
    }

    /**
     * Fetch a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function fetch($id)
    {
        return $this->srvArquivoProposta->montarArquivoProposta();

    }

    /**
     * Fetch all or a subset of resources
     *
     * @param  array $params
     * @return ApiProblem|mixed
     */
    public function fetchAll($params = array())
    {
        return $this->srvArquivoProposta->getArquivoImportados($this->getIdentity()->getRoleId());
    }

    /**
     * Patch (partial in-place update) a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patch($id, $data)
    {
        return new ApiProblem(405, 'The PATCH method has not been defined for individual resources');
    }

    /**
     * Replace a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function replaceList($data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for collections');
    }

    /**
     * Update a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function update($id, $data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for individual resources');
    }
}

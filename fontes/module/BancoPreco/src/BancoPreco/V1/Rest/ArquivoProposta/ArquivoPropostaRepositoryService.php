<?php
namespace BancoPreco\V1\Rest\ArquivoProposta;

use Application\Service\AbstractRepositoryService;
use Commons\Pattern\Repository\Impl\Reference;

class ArquivoPropostaRepositoryService extends AbstractRepositoryService
{

    /**
     * Salva a proposta em disco e na no histórico de arquivos processados
     * @param file $file
     * @param array $result
     * @param string $TypeFile
     * @param string $nomeCampo
     * @return bool
     */
    public function salvaArquivoMaterialEmDisco($file, $result, $TypeFile, $nomeCampo)
    {
        $stProcessamento = ($result['totalErros'] == 0) ? '1' : '0';

        //Salva arquivo
        $coArquivo = $this->getArquivoRepositoryService()->gravarArquivo($file['name'],
            $TypeFile,
            $nomeCampo);
        $dadosArquivoProposta = array();
        $dadosArquivoProposta['propostaEntity'] = new Reference(
            'Application\Entity\AesbePropostaEntity',
            array(
                'coProposta' => $result['coProposta']
            )
        );
        $dadosArquivoProposta['arquivoEntity'] = new Reference(
            'Application\Entity\AesbeArquivoEntity',
            array(
                'coArquivo' => $coArquivo
            )
        );
        $dadosArquivoProposta['coUsuario'] = 1;
        $dadosArquivoProposta['qtErros'] = $result['totalErros'];
        $dadosArquivoProposta['stProcessamento'] = $stProcessamento;
        $dadosArquivoProposta['qtRegistrosProcessados'] = $result['registrosProcessados'];
        $dadosArquivoProposta['dtProcessamento'] = new \DateTime();
        return $this->save($dadosArquivoProposta);
    }

    public function listarArquivosImportados($username)
    {
        return $this->getArquivoPropostaRepository()->listarArquivos($this->getOauthUsersRepositoryService()->getCoEmpresaUsername($username));
    }

    /**
     * @return \BancoPreco\V1\Rest\Arquivo\ArquivoRepositoryService
     */
    protected function getArquivoRepositoryService()
    {
        return $this->getLookupManager()->get('BancoPreco\\V1\\Rest\\Arquivo\\ArquivoRepositoryService');
    }

    /**
     * @return \Application\Entity\AesbeArquivoPropostaRepository
     */
    protected function getArquivoPropostaRepository()
    {
        return $this->getRepository();
    }
}

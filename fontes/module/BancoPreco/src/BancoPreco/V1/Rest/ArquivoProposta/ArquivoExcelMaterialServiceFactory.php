<?php

namespace BancoPreco\V1\Rest\ArquivoProposta;

use Commons\Pattern\Service\Impl\ZendServiceLookupManager;
use Zend\Log\Logger;
use Zend\Log\PsrLoggerAdapter;

class ArquivoExcelMaterialServiceFactory
{
    public function __invoke($services)
    {
        $lookupManager = new ZendServiceLookupManager($services);
        $logger = new PsrLoggerAdapter(new Logger());
        return new ArquivoExcelMaterialService($lookupManager, $logger);
    }
}

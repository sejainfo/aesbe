<?php

namespace BancoPreco\V1\Rest\ArquivoProposta;

use Application\Service\AbstractService;

class ArquivoExcelMaterialService extends AbstractService
{
    /**
     * @param string $file
     * @return array
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Reader_Exception
     */
    public function parserExcelToArray($file)
    {
        try {
            $Reader = \PHPExcel_IOFactory::createReaderForFile($file);
            /** @var \PHPExcel $objXLS */
            $objXLS = $Reader->load($file);
            $lastColumn = $objXLS->getSheet(0)->getHighestColumn();
            $countRows = $objXLS->getSheet(0)->getHighestRow();
            for ($i = 'A'; ; $i++) {
                for ($j = 1; $j <= $countRows; $j++) {
                    $cell = $objXLS->getSheet(0)->getCell($i . $j);
                    $cellValue = $cell->getValue();
                    if (\PHPExcel_Shared_Date::isDateTime($cell)) {
                        $arrXls[$i][$j] = date('d/m/Y', \PHPExcel_Shared_Date::ExcelToPHP($cellValue));
                    } else {
                        $arrXls[$i][$j] = !is_null($cellValue) ? $cellValue : '';
                    }
                }
                if ($i == $lastColumn) {
                    break;
                }
            }
            return $arrXls;
        } catch (\Exception $e) {
            throw new ServiceException('Ocorreu um erro ao tentar importar o arquivo');
        }
    }

    /**
     * @param $arrDados
     * @param string $titulo
     * @param string $type
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Reader_Exception
     */
    public function createExcelFromArray($arrDados, $titulo = 'Planilha Excel', $type = "Excel5")
    {
        $objPHPExcel = \PHPExcel_IOFactory::load(__DIR__ . '/Layout.xls');
        $objPHPExcel->getProperties()->setCreator("AESBE")
            ->setLastModifiedBy("AESBE")
            ->setTitle($titulo);
        foreach ($arrDados as $key => $value) {
            foreach ($value as $k => $v) {
                if (!$k) {
                    $k = 1;
                }
                $linha = ++$k;
                $indice = $key . $linha;
                $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValueExplicit($indice, $v, \PHPExcel_Cell_DataType::TYPE_STRING);
            }
        }
        //bloqueia planilha
//        $objPHPExcel->getActiveSheet()
//            ->getProtection()->setSheet(true);
//        //libera campos que poderão ser preenchidos
//        $objPHPExcel->getActiveSheet()
//            ->getStyle('C2:C500')
//            ->getProtection()
//            ->setLocked(\PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
//        $objPHPExcel->getActiveSheet()
//            ->getStyle('J2:AD500')
//            ->getProtection()
//            ->setLocked(\PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
//
//        $objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(false);
//        // this did not work
//         $objPHPExcel->getActiveSheet()->getProtection()->setFormatColumns(false);
//        $objPHPExcel->getActiveSheet()->getProtection()->setFormatRows(false);
//        $objPHPExcel->getActiveSheet()->getProtection()->setDeleteRows(true);

        // Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $titulo . '.xls"');
        header('Cache-Control: max-age=0');

        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, $type);
        $objWriter->save('php://output');
        // Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $titulo . '.xls"');
        header('Cache-Control: max-age=0');

        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, $type);
        $objWriter->save('php://output');
        exit;
    }

    /**
     * @param $file
     * @throws \Exception
     */
    public function validaTipoExtensaoArquivo($file)
    {
        $strExtensao = array('xls', 'xlsx');

        if ($file['error'] == 1) {
            throw new \Exception('Ocorreu um erro ao tentar importar o arquivo');
        }
        $explode = explode('.', $file['name']);
        $extensao = $explode[1];
        if (!isset($extensao) || !in_array($extensao, $strExtensao)) {
            throw new \Exception('Extensão de arquivo inválida. Somente permitido arquivos com extensões: .xls ou .xlsx');
        }
    }

    /**
     * @param $file
     * @param int $size
     *  -- 3145728kb equivale a 3MB
     */
    public function validaTamanhoArquivo($file, $size = 3145728)
    {
        if ($file['size'] > $size) {
            throw new \Exception('Tamanho do arquivo excede 3MB (megabytes). Operação não permitida.');
        }
    }
}
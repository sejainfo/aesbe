<?php

namespace BancoPreco\V1\Rest\ArquivoProposta;

use Commons\Pattern\Service\Impl\ZendServiceLookupManager;
use Zend\Log\Logger;
use Zend\Log\PsrLoggerAdapter;

class ArquivoPropostaRepositoryServiceFactory
{
    public function __invoke($services)
    {
        /* @var $srvDoctrine \Doctrine\ORM\EntityManager */
        $srvDoctrine = $services->get('Doctrine\ORM\EntityManager');
        $repo = $srvDoctrine->getRepository('Application\Entity\AesbeArquivoPropostaEntity');
        $lookupManager = new ZendServiceLookupManager($services);
        $logger = new PsrLoggerAdapter(new Logger());
        return new ArquivoPropostaRepositoryService($repo, $lookupManager, $logger);
    }
}

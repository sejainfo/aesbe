<?php
namespace BancoPreco\V1\Rest\ArquivoProposta;

use Application\Entity\AesbeModalidadeLicitacaoEntity;
use Application\Entity\AesbePropostaDadosEntity;
use Application\Entity\AesbeTipoArquivoEntity;
use Application\Service\AbstractService;
use Application\Util\FormataNumero;
use Commons\Country\Brazil\Validator\CnpjValidator;
use Commons\Pattern\Repository\Impl\Reference;
use Zend\Filter\Digits;

class ArquivoPropostaService extends AbstractService
{

    /**
     * @param $arrExcel
     * @param $file
     * @return array
     */
    public function importarArquivo($arrExcel, $file, $nomeCampo, $username)
    {
        $formattedArrExecel = $this->formatArrayMaterialExcel($arrExcel);
        $materialValido = $this->getMaterialRepositoryService()->listarMaterial();
        $result = array();
        $result['registrosProcessados'] = (max(array_keys($formattedArrExecel)) -1);
        $proposta = $this->getPropostaRepositoryService()->getProposta(
            $this->getOauthUsersRepositoryService()->getCoEmpresaUsername($username)
        );
        foreach ($formattedArrExecel as $key => $data) {
            $erros = $this->validaCamposEmBrancoArquivoMaterial($data);
            $erros = $this->validaCamposInvalidosMaterial($data, $erros);
            $erros = $this->validaRegrasArquivoMaterial($data, $erros, $materialValido);
            if (count($erros)) {
                foreach ($erros as $erro)
                    $result['erros'][] = array(
                        'linha' => $key,
                        'conteudo' => $erro['noBasico'],
                        'observacao' => $erro['dsErroXls'],
                    );
            }

            if (!isset($result['erros'])) {
                $fornecedor = $this->getFornecedorRepositoryService()
                    ->getFornecedor($data['nuCnpj'], $data['noFornecedor']);


                $data = $this->getPropostaDadosRepositoryService()->formatarDataArquivo($data);
                $this->getPropostaDadosRepositoryService()
                    ->atualizarDadosProposta($data, $proposta, current($fornecedor));
            }
        }
        $result['totalErros'] = !isset($result['erros']) ? 0 : count($result['erros']);
        $result['processadoEm'] = date('d/m/Y H:i:s');
        $result['coProposta'] = $proposta->getCoProposta();
        $this->getArquivoPropostaRepositoryService()
            ->salvaArquivoMaterialEmDisco($file, $result, AesbeTipoArquivoEntity::CO_TIPO_PROPOSTA, $nomeCampo);
        return $result;
    }



    /**
     * Formata em arrray as informações de material da planilha importada
     * @param array $arrExcel
     * @return array
     */
    private function formatArrayMaterialExcel($arrExcel)
    {
        $arrResult = array();
        $digits  = new Digits();
        foreach ($arrExcel as $key=>$arrColumn) {
            foreach($arrColumn as $k=>$v) {
                switch($key) {
                    case 'A':
                        $arrResult[$k]['coMaterial']= $digits->filter($v);
                        break;
                    case 'C':
                        $stProposta = '';
                        switch (strtoupper($v)) {
                            case 'ORIGINAL' :
                                $stProposta = AesbePropostaDadosEntity::ST_PROPOSTA_ORIGINAL;
                                break;
                            case 'NOVA' :
                                $stProposta = AesbePropostaDadosEntity::ST_PROPOSTA_NOVA;
                                break;
                        }
                        $arrResult[$k]['stProposta'] = $stProposta;
                        break;
                    case 'E':
                        $arrResult[$k]['noBasico']= $v;
                        break;
                    case 'J':
                        $arrResult[$k]['noFornecedor'] = strtoupper($v);
                        break;
                    case 'K':
                        $arrResult[$k]['nuCnpj'] = $digits->filter($v);
                        break;
                    case 'L':
                        $arrResult[$k]['vlComImposto'] = $v;
                        break;
                    case 'M':
                        $arrResult[$k]['dtProposta'] = $v;
                        break;
                    case 'N':
                        $arrResult[$k]['dtContratacao'] = $v;
                        break;
                    case 'O':
                        $arrResult[$k]['dtVigenciaContrato'] = $v;
                        break;
                    case 'P':
                        $arrResult[$k]['nuContrato'] = $v;
                        break;
                    case 'Q':
                        $arrResult[$k]['nuAta'] = $v;
                        break;
                    case 'R':
                        $coModalidade = '';
                        switch (strtoupper($v)) {
                            case 'PREGãO' :
                                $coModalidade = AesbeModalidadeLicitacaoEntity::CO_MODALIDADE_PREGAO;
                                break;
                            case 'DISPENSA' :
                                $coModalidade = AesbeModalidadeLicitacaoEntity::CO_MODALIDADE_DISPENSA;
                                break;
                            case 'CONVITE' :
                                $coModalidade = AesbeModalidadeLicitacaoEntity::CO_CONVITE;
                                break;
                        }
                        $arrResult[$k]['coModalidade'] = $coModalidade;
                        break;
                    case 'S':
                        $arrResult[$k]['qtProduto'] = $v;
                        break;
                    case 'T':
                        $arrResult[$k]['qtDiasPagamento'] = $digits->filter($v);;
                        break;
                    case 'U':
                        $stFobCif = '';
                        switch (strtoupper($v)) {
                            case 'FOB' :
                                $stFobCif = AesbePropostaDadosEntity::ST_FOB;
                                break;
                            case 'CIF' :
                                $stFobCif = AesbePropostaDadosEntity::ST_CIF;
                                break;
                        }
                        $arrResult[$k]['stFobCif'] = $stFobCif;
                        break;
                    case 'V':
                        $arrResult[$k]['vlIpi'] = $v;
                        break;
                    case 'W':
                        $arrResult[$k]['vlIcms'] = $v;
                        break;
                    case 'X':
                        $arrResult[$k]['vlIss'] = $v;
                        break;
                    case 'Y':
                        $stCondicaoFornecimento = '';
                        switch (strtoupper($v)) {
                            case 'PARCELADO' :
                                $stCondicaoFornecimento = AesbePropostaDadosEntity::ST_FORNECIMENTO_PARCELADO;
                                break;
                            case 'UNICA' :
                                $stCondicaoFornecimento = AesbePropostaDadosEntity::ST_FORNECIMENTO_UNICA;
                                break;
                        }
                        $arrResult[$k]['stCondicaoFornecimento'] = $stCondicaoFornecimento;
                        break;
                    case 'Z':
                        $tpInspecao = '';
                        switch (strtoupper($v)) {
                            case 'FáBRICA' :
                                $tpInspecao = AesbePropostaDadosEntity::TP_INSPECAO_FABRICA;
                                break;
                            case 'RECEBIMENTO' :
                                $tpInspecao = AesbePropostaDadosEntity::TP_INSPECAO_RECEBIMENTO;
                                break;
                        }
                        $arrResult[$k]['tpInspecao'] = $tpInspecao;
                        break;
                    case 'AA':
                        $stNivelAtendimento = '';
                        switch (strtoupper($v)) {
                            case 'VERDE' :
                                $stNivelAtendimento = AesbePropostaDadosEntity::ST_NVL_ATENDIMENTO_VERDE;
                                break;
                            case 'AMARELO' :
                                $stNivelAtendimento = AesbePropostaDadosEntity::ST_NVL_ATENDIMENTO_AMARELO;
                                break;
                            case 'VERMELHO' :
                                $stNivelAtendimento = AesbePropostaDadosEntity::ST_NVL_ATENDIMENTO_VERMELHO;
                                break;
                        }
                        $arrResult[$k]['stNivelAtendimento'] = $stNivelAtendimento;
                        break;
                    case 'AB':
                        $stEspecificacaoPadrao = '';
                        switch (strtoupper($v)) {
                            case 'S' :
                                $stEspecificacaoPadrao = AesbePropostaDadosEntity::ST_ESP_PADRAO_SIM;
                                break;
                            case 'N' :
                                $stEspecificacaoPadrao = AesbePropostaDadosEntity::ST_ESP_PADRAO_NAO;
                                break;
                        }
                        $arrResult[$k]['stEspecificacaoPadrao'] = $stEspecificacaoPadrao;
                        break;

                    case 'AC':
                        $arrResult[$k]['dsObservacao'] = $v;
                        break;
                    case 'AD':
                        $arrResult[$k]['dtInformacao'] = $v;
                        break;
                }
            }
        }
        unset($arrResult[1]);
        $arrNotEmptyLines = array();
        foreach ($arrResult as $key => $value) {
            $arrFilter = array_filter($value);
            if (!count($arrFilter)) {
                break;
            }
            $arrNotEmptyLines[$key] = $value;
        }
        return $arrNotEmptyLines;
    }

    /**
     * Valida prenchimento dos campos obrigatórios
     * @param $value
     * @return array
     */
    private function validaCamposEmBrancoArquivoMaterial($value)
    {
        $erros = array();
        if (empty($value['coMaterial'])) {
            $erro['dsErroXls'] = $this->getTranslatorService()->translateWithParam('E001', 'CÓDIGO AESBE');
            $erro['noBasico'] = $value['noBasico'];
            array_push($erros, $erro);
        }

        if (empty($value['noFornecedor'])) {
            $erro['noBasico'] = $value['noBasico'];
            $erro['dsErroXls'] = $this->getTranslatorService()->translateWithParam('E001', 'FORNECEDOR');
            array_push($erros, $erro);
        }
        if (empty($value['nuCnpj'])) {
            $erro['noBasico'] = $value['noBasico'];
            $erro['dsErroXls'] = $this->getTranslatorService()->translateWithParam('E001', 'CNPJ FORNECEDOR');
            array_push($erros, $erro);
        }
        if (empty($value['vlComImposto'])) {
            $erro['noBasico'] = $value['noBasico'];
            $erro['dsErroXls'] = $this->getTranslatorService()->translateWithParam('E001', 'PREÇO MATERIAL C/IMPOSTOS');
            array_push($erros, $erro);
        }
        if (empty($value['dtProposta'])) {
            $erro['noBasico'] = $value['noBasico'];
            $erro['dsErroXls'] = $this->getTranslatorService()->translateWithParam('E001', 'DATA DA PROPOSTA');
            array_push($erros, $erro);
        }
        if (empty($value['dtContratacao'])) {
            $erro['noBasico'] = $value['noBasico'];
            $erro['dsErroXls'] = $this->getTranslatorService()->translateWithParam('E001', 'DATA CONTRATAÇÃO');
            array_push($erros, $erro);
        }
        if (empty($value['dtVigenciaContrato'])) {
            $erro['noBasico'] = $value['noBasico'];
            $erro['dsErroXls'] = $this->getTranslatorService()->translateWithParam('E001', 'DATA VIGÊNCIA CONTRATO');
            array_push($erros, $erro);
        }
        if (empty($value['nuContrato'])) {
            $erro['noBasico'] = $value['noBasico'];
            $erro['dsErroXls'] = $this->getTranslatorService()->translateWithParam('E001', 'NÚMERO CONTRATO');
            array_push($erros, $erro);
        }
        if (empty($value['nuAta'])) {
            $erro['noBasico'] = $value['noBasico'];
            $erro['dsErroXls'] = $this->getTranslatorService()->translateWithParam('E001', 'NÚMERO ATA REGISTRO DE PREÇOS');
            array_push($erros, $erro);
        }
        if (empty($value['coModalidade'])) {
            $erro['noBasico'] = $value['noBasico'];
            $erro['dsErroXls'] = $this->getTranslatorService()->translateWithParam('E001', 'MODALIDADE');
            array_push($erros, $erro);
        }
        if (empty($value['qtProduto'])) {
            $erro['noBasico'] = $value['noBasico'];
            $erro['dsErroXls'] = $this->getTranslatorService()->translateWithParam('E001', 'QUANTIDADE');
            array_push($erros, $erro);
        }
        if (empty($value['qtDiasPagamento'])) {
            $erro['noBasico'] = $value['noBasico'];
            $erro['dsErroXls'] = $this->getTranslatorService()->translateWithParam('E001', 'CONDIÇÕES DE PAGAMENTO');
            array_push($erros, $erro);
        }
        if (empty($value['stFobCif'])) {
            $erro['noBasico'] = $value['noBasico'];
            $erro['dsErroXls'] = $this->getTranslatorService()->translateWithParam('E001', 'FOB/CIF');
            array_push($erros, $erro);
        }
        if (empty($value['vlIpi']) && $value['vlIpi'] != 0) {
            $erro['noBasico'] = $value['noBasico'];
            $erro['dsErroXls'] = $this->getTranslatorService()->translateWithParam('E001', 'IPI');
            array_push($erros, $erro);
        }
        if (empty($value['stCondicaoFornecimento'])) {
            $erro['noBasico'] = $value['noBasico'];
            $erro['dsErroXls'] = $this->getTranslatorService()->translateWithParam('E001', 'CONDIÇÕES DE FORNECIMENTO');
            array_push($erros, $erro);
        }
        if (empty($value['tpInspecao'])) {
            $erro['noBasico'] = $value['noBasico'];
            $erro['dsErroXls'] = $this->getTranslatorService()->translateWithParam('E001', 'TIPO DE INSPEÇÃO');
            array_push($erros, $erro);
        }
        if (empty($value['stNivelAtendimento'])) {
            $erro['noBasico'] = $value['noBasico'];
            $erro['dsErroXls'] = $this->getTranslatorService()->translateWithParam('E001', 'NIVEL DE ATENDIMENTO');
            array_push($erros, $erro);
        }
        if (empty($value['stEspecificacaoPadrao'])) {
            $erro['noBasico'] = $value['noBasico'];
            $erro['dsErroXls'] = $this->getTranslatorService()->translateWithParam('E001', 'ESPECIFICAÇÃO PADRÃO');
            array_push($erros, $erro);
        }
        if ($value['stEspecificacaoPadrao'] == 'N' && empty($value['dsObservacao'])) {
            $erro['noBasico'] = $value['noBasico'];
            $erro['dsErroXls'] = $this->getTranslatorService()->translateWithParam('E001', 'OBSERVAÇÕES');
            array_push($erros, $erro);
        }
        if (empty($value['dtInformacao'])) {
            $erro['noBasico'] = $value['noBasico'];
            $erro['dsErroXls'] = $this->getTranslatorService()->translateWithParam('E001', 'DATA DA INFORMAÇÃO');
            array_push($erros, $erro);
        }
        return $erros;
    }


    /**
     * Verifica campos inválidos
     * @param array $arrDados
     * @param array $erros
     * @return mixed
     */
    public function validaCamposInvalidosMaterial($arrDados, $erros)
    {
        if(count($erros)) {
            return $erros;
        }
        $cnpjValidator = new CnpjValidator();
        if (!is_numeric($arrDados['coMaterial'])) {
            $erro['noBasico'] = $arrDados['noBasico'];
            $erro['dsErroXls'] = $this->getTranslatorService()->translateWithParam('E002', 'CÓDIGO AESBE');
            $erro['noCampo'] = 'coMaterial';
            $erro['stSalvar'] = false;
            array_push($erros, $erro);
        }

        if (!is_numeric($arrDados['nuCnpj']) || !$cnpjValidator->isValid($arrDados['nuCnpj'])) {
            $erro['noBasico'] = $arrDados['noBasico'];
            $erro['dsErroXls'] = $this->getTranslatorService()->translateWithParam('E002', 'CNPJ');
            $erro['noCampo'] = 'nuCnpj';
            $erro['stSalvar'] = false;
            array_push($erros, $erro);
        }
        if (!is_numeric($arrDados['vlComImposto'])) {
            $erro['noBasico'] = $arrDados['noBasico'];
            $erro['dsErroXls'] = $this->getTranslatorService()->translateWithParam('E002', 'PREÇO MATERIAL C/IMPOSTOS');
            $erro['noCampo'] = 'vlComImposto';
            $erro['stSalvar'] = false;
            array_push($erros, $erro);
        }
        if (!is_numeric($arrDados['qtProduto'])) {
            $erro['noBasico'] = $arrDados['noBasico'];
            $erro['dsErroXls'] = $this->getTranslatorService()->translateWithParam('E002', 'QUANTIDADE');
            $erro['noCampo'] = 'Quantidade';
            $erro['stSalvar'] = false;
            array_push($erros, $erro);
        }
        if (!is_numeric($arrDados['qtDiasPagamento'])) {
            $erro['noBasico'] = $arrDados['noBasico'];
            $erro['dsErroXls'] = $this->getTranslatorService()->translateWithParam('E002', 'CONDIÇÕES DE PAGAMENTO');
            $erro['noCampo'] = 'qtDiasPagamento';
            $erro['stSalvar'] = false;
            array_push($erros, $erro);
        }
        if (!is_numeric($arrDados['vlIpi'])) {
            $erro['noBasico'] = $arrDados['noBasico'];
            $erro['dsErroXls'] = $this->getTranslatorService()->translateWithParam('E002', 'IPI');
            $erro['noCampo'] = 'vlIpi';
            $erro['stSalvar'] = false;
            array_push($erros, $erro);
        }
        if (!is_numeric($arrDados['vlIss'])) {
            $erro['noBasico'] = $arrDados['noBasico'];
            $erro['dsErroXls'] = $this->getTranslatorService()->translateWithParam('E002', 'ISS');
            $erro['noCampo'] = 'vlIss';
            $erro['stSalvar'] = false;
            array_push($erros, $erro);
        }
        return $erros;
    }

    /**
     * Validação de regras negocias do arquivo de material
     * @param array $arrXls
     * @param array $erros
     * @param $materialValido
     * @return mixed
     */
    private function validaRegrasArquivoMaterial($arrXls, $erros, $materialValido)
    {
        if (count($erros)) {
            return $erros;
        }
        //valida se o material existe
        if (!in_array($arrXls['coMaterial'], array_column($materialValido, 'coMaterial'))) {
            $erro['dsErroXls'] = sprintf($this->translate('E049'));
            $erro['noCampo'] = 'nuCnpj';
            array_push($erros, $erro);
        }
        return $erros;
    }

    /**
     * Lista arquivos importados
     * @return array
     */
    public function getArquivoImportados($username)
    {
        $arquivos = $this->getArquivoPropostaRepositoryService()->listarArquivosImportados($username);
        $result = array();
        foreach ($arquivos as $key=>$arquivo) {
            $result[$key]['arquivo'] = $arquivo->getArquivoEntity()->getDsArquivo();
            $result[$key]['dtProcessamento'] = $arquivo->getDtProcessamento()->format('d/m/Y H:i:s');
            $result[$key]['dtProposta'] = $arquivo->getPropostaEntity()->getDtProposta()->format('d/m/Y');
            $result[$key]['qtRegistros'] = $arquivo->getQtRegistrosProcessados();
            $result[$key]['qtErros'] = $arquivo->getQtErros();
            $result[$key]['stProcessamento'] = $arquivo->isStProcessamento() ? 'Sim' : 'Não';
        }

        return $result;
    }

    /**
     * @return \BancoPreco\V1\Rest\Proposta\PropostaRepositoryService
     */
    protected function getPropostaRepositoryService()
    {
        return $this->getLookupManager()->get('BancoPreco\\V1\\Rest\\Proposta\\PropostaRepositoryService');

    }

    /**
     * @return \BancoPreco\V1\Rest\PropostaDados\PropostaDadosRepositoryService
     */
    protected function getPropostaDadosRepositoryService()
    {
        return $this->getLookupManager()->get('BancoPreco\\V1\\Rest\\PropostaDados\\PropostaDadosRepositoryService');

    }

    /**
     * @return \BancoPreco\V1\Rest\ArquivoProposta\ArquivoExcelMaterialService
     */
    protected function getArquivoExcelMaterialService()
    {
        return $this->getLookupManager()->get('BancoPreco\\V1\\Rest\\ArquivoProposta\\ArquivoExcelMaterialService');

    }

    /**
     * @return \BancoPreco\V1\Rest\Fornecedor\FornecedorRepositoryService
     */
    protected function getFornecedorRepositoryService()
    {
        return $this->getLookupManager()->get('BancoPreco\\V1\\Rest\\Fornecedor\\FornecedorRepositoryService');

    }

    /**
     * @return \BancoPreco\V1\Rest\Material\MaterialRepositoryService
     */
    protected function getMaterialRepositoryService()
    {
        return $this->getLookupManager()->get('BancoPreco\\V1\\Rest\\Material\\MaterialRepositoryService');

    }

    /**
     * @return \BancoPreco\V1\Rest\ArquivoProposta\ArquivoPropostaRepositoryService
     */
    protected function getArquivoPropostaRepositoryService()
    {
        return $this->getLookupManager()->get('BancoPreco\\V1\\Rest\\ArquivoProposta\\ArquivoPropostaRepositoryService');

    }
    /**
     * @return \Core\V1\Rest\OauthUsers\OauthUsersRepositoryService
     */
    protected function getOauthUsersRepositoryService()
    {
        return $this->getLookupManager()->get('Core\\V1\\Rest\\OauthUsers\\OauthUsersRepositoryService');

    }
}

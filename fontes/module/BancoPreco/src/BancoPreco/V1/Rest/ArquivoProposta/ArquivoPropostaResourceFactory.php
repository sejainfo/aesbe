<?php
namespace BancoPreco\V1\Rest\ArquivoProposta;

class ArquivoPropostaResourceFactory
{
    public function __invoke($services)
    {
        $srvArquivoProposta = $services->get('BancoPreco\\V1\\Rest\\ArquivoProposta\\ArquivoPropostaService');
        $srvArquivoExcelMaterial = $services->get('BancoPreco\\V1\\Rest\\ArquivoProposta\\ArquivoExcelMaterialService');
        return new ArquivoPropostaResource($srvArquivoProposta, $srvArquivoExcelMaterial);
    }
}

<?php
namespace BancoPreco\V1\Rest\EspecificacaoSerie;

class EspecificacaoSerieResourceFactory
{
    public function __invoke($services)
    {
        return new EspecificacaoSerieResource();
    }
}

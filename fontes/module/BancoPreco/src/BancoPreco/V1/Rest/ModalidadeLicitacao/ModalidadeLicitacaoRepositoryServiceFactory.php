<?php

namespace BancoPreco\V1\Rest\ModalidadeLicitacao;

use Commons\Pattern\Service\Impl\ZendServiceLookupManager;
use Zend\Log\Logger;
use Zend\Log\PsrLoggerAdapter;

class ModalidadeLicitacaoRepositoryServiceFactory
{
    public function __invoke($services)
    {
        /* @var $srvDoctrine \Doctrine\ORM\EntityManager */
        $srvDoctrine = $services->get('Doctrine\ORM\EntityManager');
        $repo = $srvDoctrine->getRepository('Application\Entity\AesbeModalidadeLicitacaoEntity');
        $lookupManager = new ZendServiceLookupManager($services);
        $logger = new PsrLoggerAdapter(new Logger());
        return new ModalidadeLicitacaoRepositoryService($repo, $lookupManager, $logger);
    }
}

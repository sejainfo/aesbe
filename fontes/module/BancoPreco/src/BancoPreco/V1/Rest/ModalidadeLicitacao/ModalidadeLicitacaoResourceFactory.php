<?php
namespace BancoPreco\V1\Rest\ModalidadeLicitacao;

class ModalidadeLicitacaoResourceFactory
{
    public function __invoke($services)
    {
        $srvModalidade = $services
            ->get('BancoPreco\\V1\\Rest\\ModalidadeLicitacao\\ModalidadeLicitacaoRepositoryService');
        return new ModalidadeLicitacaoResource($srvModalidade);
    }
}

<?php

namespace BancoPreco\V1\Rest\Fornecedor;

use Commons\Pattern\Service\Impl\ZendServiceLookupManager;
use Zend\Log\Logger;
use Zend\Log\PsrLoggerAdapter;

class FornecedorRepositoryServiceFactory
{
    public function __invoke($services)
    {
        /* @var $srvDoctrine \Doctrine\ORM\EntityManager */
        $srvDoctrine = $services->get('Doctrine\ORM\EntityManager');
        $repo = $srvDoctrine->getRepository('Application\Entity\AesbeFornecedorEntity');
        $lookupManager = new ZendServiceLookupManager($services);
        $logger = new PsrLoggerAdapter(new Logger());
        return new FornecedorRepositoryService($repo, $lookupManager, $logger);
    }
}

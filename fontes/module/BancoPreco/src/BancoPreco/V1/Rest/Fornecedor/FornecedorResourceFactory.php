<?php
namespace BancoPreco\V1\Rest\Fornecedor;

class FornecedorResourceFactory
{
    public function __invoke($services)
    {
        $srvFornecedor = $services->get('BancoPreco\V1\Rest\Fornecedor\FornecedorRepositoryService');
        return new FornecedorResource($srvFornecedor);
    }
}

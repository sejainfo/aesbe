<?php
namespace BancoPreco\V1\Rest\Fornecedor;

use Application\Service\AbstractRepositoryService;

class FornecedorRepositoryService extends AbstractRepositoryService
{

    /**
     * Insere e retorna o Fornecedor
     * @param int $nuCnpj
     * @param string $noFornecedor
     * @return array|bool|\Doctrine\Common\Proxy\Proxy|null|object
     */
    public function getFornecedor($nuCnpj, $noFornecedor)
    {
        $fornecedor = $this->getFornecedorRepository()->findBy(array('nuCnpj' => $nuCnpj));
        if (count($fornecedor)) {
            return $fornecedor;
        }
        $fornecedor = $this->getFornecedorRepository()->save(
            array('nuCnpj' => $nuCnpj, 'noFornecedor' => $noFornecedor)
        );
        return $fornecedor;
    }

    /**
     * @return \Commons\Pattern\Repository\Impl\SimpleEntityRepository
     */
    protected function getFornecedorRepository()
    {
        return $this->getRepository();
    }

    public function getListaFornecedor()
    {
        $result = array();
        $i = 0;
        $fornecedores = $this->findAll();

        foreach ($fornecedores as $fornecedor) {
            $result[$i]['coFornecedor'] = $fornecedor->getCoFornecedor();
            $result[$i]['noFornecedor'] = $fornecedor->getNoFornecedor();
            $result[$i]['nuCnpj'] = $fornecedor->getNuCnpj();
            $result[$i]['noRepresentante'] = $fornecedor->getNoRepresentante();
            $i++;
        }

        return $result;
    }


}

<?php
namespace BancoPreco\V1\Rest\PropostaDados;

class PropostaDadosResourceFactory
{
    public function __invoke($services)
    {
        $srvPropostaDadosRepo = $services
            ->get('BancoPreco\\V1\\Rest\\PropostaDados\\PropostaDadosRepositoryService');
        $srvMaterialRepo = $services
            ->get('BancoPreco\\V1\\Rest\\Material\\MaterialRepositoryService');
        return new PropostaDadosResource($srvPropostaDadosRepo, $srvMaterialRepo);
    }
}

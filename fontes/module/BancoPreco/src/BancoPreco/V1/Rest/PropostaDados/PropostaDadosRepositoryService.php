<?php
namespace BancoPreco\V1\Rest\PropostaDados;

use Application\Entity\AesbePropostaDadosEntity;
use Application\Service\AbstractRepositoryService;
use Application\Service\ServiceException;
use Commons\Country\Brazil\Validator\CnpjValidator;
use Commons\Pattern\Repository\Impl\Reference;

class PropostaDadosRepositoryService extends AbstractRepositoryService
{

    /**
     * Recupera os dados de uma proposta
     * @param array $data
     * @return array
     */
    public function buscarDadosProposta($data)
    {
        return $this->getPropostaDadosRepository()->buscarDadosProposta($data);
    }

    /**
     * Atualiza os dados de um material para uma determinada empresa
     * @param $data
     * @param $proposta
     * @param $fornecedor
     * @return bool
     */
    public function atualizarDadosProposta($data, $proposta, $fornecedor)
    {
        $propostaDados = $this->getPropostaDadosRepository()->findBy(
            array(
                'materialEntity' => $data['coMaterial'],
                'propostaEntity' => $proposta->getCoProposta()
            )
        );
        $coPropostaDados = null;
        if (count($propostaDados)) {
            $propostaDados = current($propostaDados);
            $coPropostaDados = $propostaDados->getCoPropostaDados();
        }
        $arrPropostaDados = $data;
        $arrPropostaDados['materialEntity'] = new Reference(
            'Application\Entity\AesbeMaterialEntity',
            array(
                'coMaterial' => $data['coMaterial']
            )
        );
        $arrPropostaDados['modalidadeLicitacaoEntity'] = new Reference(
            'Application\Entity\AesbeModalidadeLicitacaoEntity',
            array(
                'coModalidade' => $data['coModalidade']
            )
        );
        $arrPropostaDados['propostaEntity'] = new Reference(
            'Application\Entity\AesbePropostaEntity',
            array(
                'coProposta' => $proposta->getCoProposta()
            )
        );
        $arrPropostaDados['fornecedorEntity'] = $fornecedor;
        return $this->save($arrPropostaDados, $coPropostaDados);
    }

    /**
     * Retorna dados de uma determinada proposta
     * @param int $coProposta
     * @return mixed
     */
    public function getPropostaDados($coProposta)
    {
        $propostaDados = $this->find($coProposta);

        $return = $propostaDados->toArray();
        $return['materialEntity'] = $propostaDados->getMaterialEntity()->toArray();
        $return['materialEntity']['descricaoEntity'] = $propostaDados->getMaterialEntity()->getDescricaoEntity()->toArray();
        $return['fornecedorEntity'] = $propostaDados->getFornecedorEntity()->toArray();
        $return['modalidadeLicitacaoEntity'] = $propostaDados->getModalidadeLicitacaoEntity()->toArray();
        return $return;
    }

    /**
     * Atualiza dados da proposta
     * @param array $data
     * @return bool
     */
    public function atualizarPropostaDados($data)
    {
        $data = (array)$data;
        $this->validaRegraNegocio($data);
        $fornecedor = $this->getFornecedorRepositoryService()
            ->getFornecedor($data['nuCnpj'], $data['noFornecedor']);
        //@todo ajustar para pegar do usuário logado
        $coEmpresa = $this->getOauthUsersRepositoryService()->getCoEmpresaUsername($data['username']);
        $proposta = $this->getPropostaRepositoryService()->getProposta($coEmpresa);
        $data['dtProposta'] = $this->formatarDataPost($data['dtProposta']);
        $data['dtContratacao'] = $this->formatarDataPost($data['dtContratacao']);
        $data['dtInformacao'] = $this->formatarDataPost($data['dtInformacao']);
        $data['dtVigenciaContrato'] = $this->formatarDataPost($data['dtVigenciaContrato']);
        $data['coMaterial'] = $data['coMaterial']['id'];
        return $this->atualizarDadosProposta($data, $proposta, current($fornecedor));

    }

    /**
     * Forma data
     * @param string $dtParamentro
     * @return \DateTime
     */
    public function formatarDataPost($dtParamentro)
    {
        $date = explode('T', $dtParamentro);
        return \DateTime::createFromFormat('Y-m-d', $date[0]);
    }

    /**
     * Formata datas do arquivo de importação para salvar os dados
     * @param array $data
     * @return mixed
     */
    public function formatarDataArquivo($data)
    {
        $dtIterval = new \DateInterval('P1D');
        $data['dtProposta'] = \DateTime::createFromFormat('d/m/Y', $data['dtProposta']);
        $data['dtProposta']->add($dtIterval);
        $data['dtContratacao'] = \DateTime::createFromFormat('d/m/Y', $data['dtContratacao']);
        $data['dtContratacao']->add($dtIterval);
        $data['dtInformacao'] = \DateTime::createFromFormat('d/m/Y', $data['dtInformacao']);
        $data['dtInformacao']->add($dtIterval);
        $data['dtVigenciaContrato'] = \DateTime::createFromFormat('d/m/Y', $data['dtVigenciaContrato']);
        $data['dtVigenciaContrato']->add($dtIterval);
        return $data;

    }


    public function validaRegraNegocio($data)
    {
        $cnpjValidator = new CnpjValidator();
        if (!$cnpjValidator->isValid($data['nuCnpj'])) {
            throw new ServiceException($this->getTranslatorService()->translateWithParam('E002', 'CNPJ Fornecedor'));
        }
        if (!in_array($data['stFobCif'],
            array(AesbePropostaDadosEntity::ST_FOB, AesbePropostaDadosEntity::ST_CIF))
        ) {
            throw new ServiceException($this->getTranslatorService()->translateWithParam('E002', 'FOB/CIF'));
        }
        if (!in_array($data['stCondicaoFornecimento'],
            array(AesbePropostaDadosEntity::ST_FORNECIMENTO_PARCELADO, AesbePropostaDadosEntity::ST_FORNECIMENTO_UNICA))
        ) {
            throw new ServiceException($this->getTranslatorService()->translateWithParam('E002',
                'Condições de Fornecimento'));
        }
        if (!in_array($data['tpInspecao'],
            array(AesbePropostaDadosEntity::TP_INSPECAO_FABRICA, AesbePropostaDadosEntity::TP_INSPECAO_RECEBIMENTO))
        ) {
            throw new ServiceException($this->getTranslatorService()->translateWithParam('E002', 'Tipo de Inspeção'));
        }
        if (!in_array($data['stEspecificacaoPadrao'],
            array(AesbePropostaDadosEntity::ST_ESP_PADRAO_NAO, AesbePropostaDadosEntity::ST_ESP_PADRAO_SIM))
        ) {
            throw new ServiceException($this->getTranslatorService()->translateWithParam('E002',
                'Especificação Padrão'));
        }
        if (!in_array($data['stNivelAtendimento'],
            array(
                AesbePropostaDadosEntity::ST_NVL_ATENDIMENTO_VERDE,
                AesbePropostaDadosEntity::ST_NVL_ATENDIMENTO_VERMELHO,
                AesbePropostaDadosEntity::ST_NVL_ATENDIMENTO_AMARELO
            ))
        ) {
            throw new ServiceException($this->getTranslatorService()->translateWithParam('E002',
                'Nível de Atendimento'));
        }

        if ($data['stEspecificacaoPadrao'] == AesbePropostaDadosEntity::ST_ESP_PADRAO_NAO &&
            empty($data['dsObservacao'])
        ) {
            throw new ServiceException($this->getTranslatorService()->translateWithParam('E001',
                'Observação'));
        }

    }

    /**
     * @return \Application\Entity\AesbePropostaDadosRepository
     */
    protected function getPropostaDadosRepository()
    {
        return $this->getRepository();
    }

    /**
     * @return \BancoPreco\V1\Rest\Fornecedor\FornecedorRepositoryService
     */
    protected function getFornecedorRepositoryService()
    {
        return $this->getLookupManager()->get('BancoPreco\\V1\\Rest\\Fornecedor\\FornecedorRepositoryService');

    }

    /**
     * @return \BancoPreco\V1\Rest\Proposta\PropostaRepositoryService
     */
    protected function getPropostaRepositoryService()
    {
        return $this->getLookupManager()->get('BancoPreco\\V1\\Rest\\Proposta\\PropostaRepositoryService');

    }

    /**
     * @return \Core\V1\Rest\OauthUsers\OauthUsersRepositoryService
     */
    protected function getOauthUsersRepositoryService()
    {
        return $this->getLookupManager()->get('Core\\V1\\Rest\\OauthUsers\\OauthUsersRepositoryService');

    }
}

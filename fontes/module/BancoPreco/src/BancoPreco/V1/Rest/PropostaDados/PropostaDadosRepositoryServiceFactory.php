<?php

namespace BancoPreco\V1\Rest\PropostaDados;

use Commons\Pattern\Service\Impl\ZendServiceLookupManager;
use Zend\Log\Logger;
use Zend\Log\PsrLoggerAdapter;

class PropostaDadosRepositoryServiceFactory
{
    public function __invoke($services)
    {
        /* @var $srvDoctrine \Doctrine\ORM\EntityManager */
        $srvDoctrine = $services->get('Doctrine\ORM\EntityManager');
        $repo = $srvDoctrine->getRepository('Application\Entity\AesbePropostaDadosEntity');
        $lookupManager = new ZendServiceLookupManager($services);
        $logger = new PsrLoggerAdapter(new Logger());
        return new PropostaDadosRepositoryService($repo, $lookupManager, $logger);
    }
}

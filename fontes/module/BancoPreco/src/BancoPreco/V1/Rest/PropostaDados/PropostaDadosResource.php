<?php
namespace BancoPreco\V1\Rest\PropostaDados;

use ZF\ApiProblem\ApiProblem;
use ZF\Rest\AbstractResourceListener;

class PropostaDadosResource extends AbstractResourceListener
{

    /* @var $srvPropostaDadosRepo \BancoPreco\V1\Rest\PropostaDados\PropostaDadosRepositoryService */
    protected $srvPropostaDadosRepo;
    /* @var $srvMaterialRepo \BancoPreco\V1\Rest\Material\MaterialRepositoryService */
    protected $srvMaterialRepo;

    /**
     * PropostaDadosResource constructor.
     */
    public function __construct($srvPropostaDadosRepo, $srvMaterialRepo)
    {
        $this->srvPropostaDadosRepo = $srvPropostaDadosRepo;
        $this->srvMaterialRepo = $srvMaterialRepo;
    }


    /**
     * Create a resource
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function create($data)
    {
        try{
            $data['username'] = $this->getIdentity()->getRoleId();
            return $this->srvPropostaDadosRepo->atualizarPropostaDados($data);
        }catch(\Exception $e){
            return new ApiProblem(405, $e->getMessage());
        }
    }

    /**
     * Delete a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function delete($id)
    {
        try{
            return $this->srvPropostaDadosRepo->delete($id);
        }catch(\Exception $e){
            return new ApiProblem(405, $e->getMessage());
        }
    }

    /**
     * Delete a collection, or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function deleteList($data)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for collections');
    }

    /**
     * Fetch a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function fetch($id)
    {
        try{
            return $this->srvPropostaDadosRepo->getPropostaDados($id);
        }catch(\Exception $e){
            return new ApiProblem(405, $e->getMessage());
        }
    }

    /**
     * Fetch all or a subset of resources
     *
     * @param  array $params
     * @return ApiProblem|mixed
     */
    public function fetchAll($params = array())
    {
        try{
            $params['username'] = $this->getIdentity()->getRoleId();
            return $this->srvMaterialRepo->buscarMaterialPesquisaPreco($params);
        }catch(\Exception $e){
            return new ApiProblem(405, $e->getMessage());
        }
    }

    /**
     * Patch (partial in-place update) a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patch($id, $data)
    {
        return new ApiProblem(405, 'The PATCH method has not been defined for individual resources');
    }

    /**
     * Replace a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function replaceList($data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for collections');
    }

    /**
     * Update a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function update($id, $data)
    {
        try{
            $data->username = $this->getIdentity()->getRoleId();
            return $this->srvPropostaDadosRepo->atualizarPropostaDados($data);
        }catch(\Exception $e){
            return new ApiProblem(405, $e->getMessage());
        }
    }
}

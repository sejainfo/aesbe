<?php
namespace BancoPreco\V1\Rest\Proposta;

use Application\Service\AbstractRepositoryService;
use Commons\Pattern\Repository\Impl\Reference;

class PropostaRepositoryService extends AbstractRepositoryService
{

    /**
     * Cria e/ou retorta uma proposta para uma determinada empresa
     * @param int $coEmpresa
     * @return array|bool|\Doctrine\Common\Proxy\Proxy|null|object
     */
    public function getProposta($coEmpresa)
    {
        $proposta = $this->getPropostaRepository()->findBy(array('empresaEntity' => $coEmpresa));
        if (count($proposta)) {
            return current($proposta);
        }
        $proposta = $this->getPropostaRepository()->save(
            array(
                'dtProposta' => new \DateTime(),
                'empresaEntity' => new Reference(
                    'Application\Entity\AesbeEmpresaEntity',
                    array(
                        'coEmpresa' => $coEmpresa
                    )
                )
            )
        );
        return $proposta;
    }

    /**
     * @return \Commons\Pattern\Repository\Impl\SimpleEntityRepository
     */
    protected function getPropostaRepository()
    {
        return $this->getRepository();
    }


}

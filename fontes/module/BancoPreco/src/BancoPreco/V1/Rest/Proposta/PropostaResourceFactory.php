<?php
namespace BancoPreco\V1\Rest\Proposta;

class PropostaResourceFactory
{
    public function __invoke($services)
    {
        return new PropostaResource();
    }
}

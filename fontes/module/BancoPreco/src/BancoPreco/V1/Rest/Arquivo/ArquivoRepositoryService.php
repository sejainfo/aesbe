<?php
namespace BancoPreco\V1\Rest\Arquivo;

use Application\Service\AbstractRepositoryService;
use Commons\Pattern\Di\LookupManager;
use Commons\Pattern\Repository\Impl\Reference;
use Commons\Pattern\Repository\Repository;
use Psr\Log\LoggerInterface;

class ArquivoRepositoryService extends AbstractRepositoryService
{

    protected $diretorioUpload;

    /**
     * ArquivoRepositoryService constructor.
     * @param $diretorioUpload
     */
    public function __construct(Repository $repo, LookupManager $lookupManager, LoggerInterface $logger,
                                $diretorioUpload)
    {
        parent::__construct($repo, $lookupManager, $logger);
        $this->diretorioUpload = $diretorioUpload;
    }


    /**
     * @return \Commons\Pattern\Repository\Impl\SimpleEntityRepository
     */
    protected function getArquivoRepository()
    {
        return $this->getRepository();
    }

    /**
     * Grava arquivo fisicamente e na tabela
     * @param strint $nomeArquivoOriginal
     * @param int $tipoArquivo
     * @param string $nomeCampo
     * @return mixed
     * @throws \Exception
     */
    public function gravarArquivo($nomeArquivoOriginal, $tipoArquivo, $nomeCampo)
    {
        $dadosArquivo = array();
        $dadosArquivo['tipoArquivoEntity'] = new Reference(
            'Application\Entity\AesbeTipoArquivoEntity',
            array(
                'coTipoArquivo' => $tipoArquivo
            )
        );
        $dadosArquivo['dsArquivo'] = $nomeArquivoOriginal;

        $arquivo = $this->save($dadosArquivo);

        //Grava o arquivo Fisicamente
        $this->gravarArquivoEmDisco($arquivo->getCoArquivo(), $nomeArquivoOriginal, $nomeCampo);

        return $arquivo->getCoArquivo();
    }

    /**
     *
     * @param integer $coArquivo
     * @param string $nomeArquivoOriginal
     * @param string $nomeCampo
     * @throws \Exception
     */
    public function gravarArquivoEmDisco($coArquivo, $nomeArquivoOriginal, $nomeCampo)
    {
        $adapter = new \Zend\File\Transfer\Adapter\Http();
        $adapter->setDestination($this->diretorioUpload);

        if (!$adapter->receive($nomeCampo)) {
            throw new \Exception('Erro ao realizar o upload do documento');
        }

        $caminho = $this->diretorioUpload . '/' . $nomeArquivoOriginal;

        $dsArquivo = file_get_contents($caminho);

        file_put_contents($this->diretorioUpload . "/$coArquivo", $dsArquivo);

        unlink($caminho);
    }
}

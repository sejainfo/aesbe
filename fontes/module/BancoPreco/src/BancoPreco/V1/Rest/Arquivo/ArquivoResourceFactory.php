<?php
namespace BancoPreco\V1\Rest\Arquivo;

class ArquivoResourceFactory
{
    public function __invoke($services)
    {
        return new ArquivoResource();
    }
}

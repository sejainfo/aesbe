<?php

namespace BancoPreco\V1\Rest\Arquivo;

use Commons\Pattern\Service\Impl\ZendServiceLookupManager;
use Zend\Log\Logger;
use Zend\Log\PsrLoggerAdapter;

class ArquivoRepositoryServiceFactory
{
    public function __invoke($services)
    {
        /* @var $srvDoctrine \Doctrine\ORM\EntityManager */
        $srvDoctrine = $services->get('Doctrine\ORM\EntityManager');
        $repo = $srvDoctrine->getRepository('Application\Entity\AesbeArquivoEntity');
        $lookupManager = new ZendServiceLookupManager($services);
        $logger = new PsrLoggerAdapter(new Logger());
        $config = $services->get('config');
        $diretorio = $config['path']['upload'];
        return new ArquivoRepositoryService($repo, $lookupManager, $logger, $diretorio);
    }
}

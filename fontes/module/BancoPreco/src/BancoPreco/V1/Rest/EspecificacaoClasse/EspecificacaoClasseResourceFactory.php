<?php
namespace BancoPreco\V1\Rest\EspecificacaoClasse;

class EspecificacaoClasseResourceFactory
{
    public function __invoke($services)
    {
        return new EspecificacaoClasseResource();
    }
}

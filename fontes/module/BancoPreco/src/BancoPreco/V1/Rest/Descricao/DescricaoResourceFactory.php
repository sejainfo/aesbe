<?php
namespace BancoPreco\V1\Rest\Descricao;

class DescricaoResourceFactory
{
    public function __invoke($services)
    {
        return new DescricaoResource();
    }
}

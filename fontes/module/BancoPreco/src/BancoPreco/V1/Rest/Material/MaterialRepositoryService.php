<?php
namespace BancoPreco\V1\Rest\Material;

use Application\Service\AbstractRepositoryService;

class MaterialRepositoryService extends AbstractRepositoryService
{

    public function listarMaterial()
    {
        return $this->getMaterialRepository()->listarMaterial();

    }

    public function buscarMaterialPesquisaPreco($params)
    {
        if ($params['stMeusPrecos']){
            $params['search'] = null;
            $params['coEmpresa'] = $this->getOauthUsersRepositoryService()->getCoEmpresaUsername($params['username']);
        }
        return $this->getMaterialRepository()->buscarMaterialPesquisaPreco($params);

    }

    /**
     * @return \Application\Entity\AesbeMaterialRepository
     */
    protected function getMaterialRepository()
    {
        return $this->getRepository();
    }


}

<?php
namespace BancoPreco\V1\Rest\Material;

class MaterialResourceFactory
{
    public function __invoke($services)
    {
        $srvMaterial = $services
            ->get('BancoPreco\\V1\\Rest\\Material\\MaterialRepositoryService');
        return new MaterialResource($srvMaterial);
    }
}

<?php

namespace BancoPreco\V1\Rest\Material;

use Commons\Pattern\Service\Impl\ZendServiceLookupManager;
use Zend\Log\Logger;
use Zend\Log\PsrLoggerAdapter;

class MaterialRepositoryServiceFactory
{
    public function __invoke($services)
    {
        /* @var $srvDoctrine \Doctrine\ORM\EntityManager */
        $srvDoctrine = $services->get('Doctrine\ORM\EntityManager');
//        $repo = $srvDoctrine->getRepository('Application\Entity\TipoPagamentoEntity');
        $repo = $srvDoctrine->getRepository('Application\Entity\AesbeMaterialEntity');
        $lookupManager = new ZendServiceLookupManager($services);
        $logger = new PsrLoggerAdapter(new Logger());
        return new MaterialRepositoryService($repo, $lookupManager, $logger);
    }
}

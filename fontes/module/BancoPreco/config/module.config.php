<?php
return array(
    'view_manager' => array(
        'template_path_stack' => array(
            'banco-preco' => __DIR__ . '/../view',
        ),
    ),
    'service_manager' => array(
        'factories' => array(
            'BancoPreco\\V1\\Rest\\Material\\MaterialResource' => 'BancoPreco\\V1\\Rest\\Material\\MaterialResourceFactory',
            'BancoPreco\\V1\\Rest\\Material\\MaterialRepositoryService' => 'BancoPreco\\V1\\Rest\\Material\\MaterialRepositoryServiceFactory',
            'BancoPreco\\V1\\Rest\\Descricao\\DescricaoResource' => 'BancoPreco\\V1\\Rest\\Descricao\\DescricaoResourceFactory',
            'BancoPreco\\V1\\Rest\\EspecificacaoClasse\\EspecificacaoClasseResource' => 'BancoPreco\\V1\\Rest\\EspecificacaoClasse\\EspecificacaoClasseResourceFactory',
            'BancoPreco\\V1\\Rest\\EspecificacaoSerie\\EspecificacaoSerieResource' => 'BancoPreco\\V1\\Rest\\EspecificacaoSerie\\EspecificacaoSerieResourceFactory',
            'BancoPreco\\V1\\Rest\\ArquivoProposta\\ArquivoPropostaResource' => 'BancoPreco\\V1\\Rest\\ArquivoProposta\\ArquivoPropostaResourceFactory',
            'BancoPreco\\V1\\Rest\\ArquivoProposta\\ArquivoPropostaService' => 'BancoPreco\\V1\\Rest\\ArquivoProposta\\ArquivoPropostaServiceFactory',
            'BancoPreco\\V1\\Rest\\ArquivoProposta\\ArquivoPropostaRepositoryService' => 'BancoPreco\\V1\\Rest\\ArquivoProposta\\ArquivoPropostaRepositoryServiceFactory',
            'BancoPreco\\V1\\Rest\\ArquivoProposta\\ArquivoExcelMaterialService' => 'BancoPreco\\V1\\Rest\\ArquivoProposta\\ArquivoExcelMaterialServiceFactory',
            'BancoPreco\\V1\\Rest\\PropostaDados\\PropostaDadosResource' => 'BancoPreco\\V1\\Rest\\PropostaDados\\PropostaDadosResourceFactory',
            'BancoPreco\\V1\\Rest\\PropostaDados\\PropostaDadosRepositoryService' => 'BancoPreco\\V1\\Rest\\PropostaDados\\PropostaDadosRepositoryServiceFactory',
            'BancoPreco\\V1\\Rest\\Proposta\\PropostaResource' => 'BancoPreco\\V1\\Rest\\Proposta\\PropostaResourceFactory',
            'BancoPreco\\V1\\Rest\\Proposta\\PropostaRepositoryService' => 'BancoPreco\\V1\\Rest\\Proposta\\PropostaRepositoryServiceFactory',
            'BancoPreco\\V1\\Rest\\Fornecedor\\FornecedorRepositoryService' => 'BancoPreco\\V1\\Rest\\Fornecedor\\FornecedorRepositoryServiceFactory',
            'BancoPreco\\V1\\Rest\\ModeloArquivoProposta\\ModeloArquivoPropostaService' => 'BancoPreco\\V1\\Rest\\ModeloArquivoProposta\\ModeloArquivoPropostaServiceFactory',
            'BancoPreco\\V1\\Rest\\ModeloArquivoProposta\\ModeloArquivoPropostaResource' => 'BancoPreco\\V1\\Rest\\ModeloArquivoProposta\\ModeloArquivoPropostaResourceFactory',
            'BancoPreco\\V1\\Rest\\Fornecedor\\FornecedorResource' => 'BancoPreco\\V1\\Rest\\Fornecedor\\FornecedorResourceFactory',
            'BancoPreco\\V1\\Rest\\Arquivo\\ArquivoResource' => 'BancoPreco\\V1\\Rest\\Arquivo\\ArquivoResourceFactory',
            'BancoPreco\\V1\\Rest\\Arquivo\\ArquivoRepositoryService' => 'BancoPreco\\V1\\Rest\\Arquivo\\ArquivoRepositoryServiceFactory',
            'BancoPreco\\V1\\Rest\\ModalidadeLicitacao\\ModalidadeLicitacaoResource' => 'BancoPreco\\V1\\Rest\\ModalidadeLicitacao\\ModalidadeLicitacaoResourceFactory',
            'BancoPreco\\V1\\Rest\\ModalidadeLicitacao\\ModalidadeLicitacaoRepositoryService' => 'BancoPreco\\V1\\Rest\\ModalidadeLicitacao\\ModalidadeLicitacaoRepositoryServiceFactory',
            'BancoPreco\\V1\\Rest\\Relatorio\\RelatorioResource' => 'BancoPreco\\V1\\Rest\\Relatorio\\RelatorioResourceFactory',
            'BancoPreco\\V1\\Rest\\Relatorio\\RelatorioService' => 'BancoPreco\\V1\\Rest\\Relatorio\\RelatorioServiceFactory',
        ),
    ),
    'router' => array(
        'routes' => array(
            'banco-preco.rest.material' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/banco-preco/material[/:material_id]',
                    'defaults' => array(
                        'controller' => 'BancoPreco\\V1\\Rest\\Material\\Controller',
                    ),
                ),
            ),
            'banco-preco.rest.descricao' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/banco-preco/descricao[/:descricao_id]',
                    'defaults' => array(
                        'controller' => 'BancoPreco\\V1\\Rest\\Descricao\\Controller',
                    ),
                ),
            ),
            'banco-preco.rest.especificacao-classe' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/banco-preco/especificacao-classe[/:especificacao_classe_id]',
                    'defaults' => array(
                        'controller' => 'BancoPreco\\V1\\Rest\\EspecificacaoClasse\\Controller',
                    ),
                ),
            ),
            'banco-preco.rest.especificacao-serie' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/banco-preco/especificacao-serie[/:especificacao_serie_id]',
                    'defaults' => array(
                        'controller' => 'BancoPreco\\V1\\Rest\\EspecificacaoSerie\\Controller',
                    ),
                ),
            ),
            'banco-preco.rest.arquivo-proposta' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/banco-preco/arquivo-proposta[/:arquivo_proposta_id]',
                    'defaults' => array(
                        'controller' => 'BancoPreco\\V1\\Rest\\ArquivoProposta\\Controller',
                    ),
                ),
            ),
            'banco-preco.rest.proposta-dados' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/banco-preco/proposta-dados[/:proposta_dados_id]',
                    'defaults' => array(
                        'controller' => 'BancoPreco\\V1\\Rest\\PropostaDados\\Controller',
                    ),
                ),
            ),
            'banco-preco.rest.proposta' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/banco-preco/proposta[/:proposta_id]',
                    'defaults' => array(
                        'controller' => 'BancoPreco\\V1\\Rest\\Proposta\\Controller',
                    ),
                ),
            ),
            'banco-preco.rest.modelo-arquivo-proposta' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/banco-preco/modelo-arquivo-proposta[/:modelo_arquivo_proposta_id]',
                    'defaults' => array(
                        'controller' => 'BancoPreco\\V1\\Rest\\ModeloArquivoProposta\\Controller',
                    ),
                ),
            ),
            'banco-preco.rest.fornecedor' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/banco-preco/fornecedor[/:fornecedor_id]',
                    'defaults' => array(
                        'controller' => 'BancoPreco\\V1\\Rest\\Fornecedor\\Controller',
                    ),
                ),
            ),
            'banco-preco.rest.arquivo' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/banco-preco/arquivo[/:arquivo_id]',
                    'defaults' => array(
                        'controller' => 'BancoPreco\\V1\\Rest\\Arquivo\\Controller',
                    ),
                ),
            ),
            'banco-preco.rest.modalidade-licitacao' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/banco-preco/modalidade-licitacao[/:modalidade_licitacao_id]',
                    'defaults' => array(
                        'controller' => 'BancoPreco\\V1\\Rest\\ModalidadeLicitacao\\Controller',
                    ),
                ),
            ),
            'banco-preco.rest.relatorio' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/banco-preco/relatorio[/:relatorio_id]',
                    'defaults' => array(
                        'controller' => 'BancoPreco\\V1\\Rest\\Relatorio\\Controller',
                    ),
                ),
            ),
        ),
    ),
    'zf-versioning' => array(
        'uri' => array(
            0 => 'banco-preco.rest.material',
            1 => 'banco-preco.rest.descricao',
            2 => 'banco-preco.rest.especificacao-classe',
            3 => 'banco-preco.rest.especificacao-serie',
            4 => 'banco-preco.rest.arquivo-proposta',
            5 => 'banco-preco.rest.proposta-dados',
            6 => 'banco-preco.rest.proposta',
            8 => 'banco-preco.rest.modelo-arquivo-proposta',
            9 => 'banco-preco.rest.fornecedor',
            10 => 'banco-preco.rest.arquivo',
            11 => 'banco-preco.rest.modalidade-licitacao',
            12 => 'banco-preco.rest.relatorio',
        ),
    ),
    'zf-rest' => array(
        'BancoPreco\\V1\\Rest\\Material\\Controller' => array(
            'listener' => 'BancoPreco\\V1\\Rest\\Material\\MaterialResource',
            'route_name' => 'banco-preco.rest.material',
            'route_identifier_name' => 'material_id',
            'collection_name' => 'material',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Application\\Entity\\AesbeMaterialEntity',
            'collection_class' => 'BancoPreco\\V1\\Rest\\Material\\MaterialCollection',
            'service_name' => 'Material',
        ),
        'BancoPreco\\V1\\Rest\\Descricao\\Controller' => array(
            'listener' => 'BancoPreco\\V1\\Rest\\Descricao\\DescricaoResource',
            'route_name' => 'banco-preco.rest.descricao',
            'route_identifier_name' => 'descricao_id',
            'collection_name' => 'descricao',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Application\\Entity\\AesbeDescricaoEntity',
            'collection_class' => 'BancoPreco\\V1\\Rest\\Descricao\\DescricaoCollection',
            'service_name' => 'Descricao',
        ),
        'BancoPreco\\V1\\Rest\\EspecificacaoClasse\\Controller' => array(
            'listener' => 'BancoPreco\\V1\\Rest\\EspecificacaoClasse\\EspecificacaoClasseResource',
            'route_name' => 'banco-preco.rest.especificacao-classe',
            'route_identifier_name' => 'especificacao_classe_id',
            'collection_name' => 'especificacao_classe',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Application\\Entity\\AesbeEspClasseEntity',
            'collection_class' => 'BancoPreco\\V1\\Rest\\EspecificacaoClasse\\EspecificacaoClasseCollection',
            'service_name' => 'EspecificacaoClasse',
        ),
        'BancoPreco\\V1\\Rest\\EspecificacaoSerie\\Controller' => array(
            'listener' => 'BancoPreco\\V1\\Rest\\EspecificacaoSerie\\EspecificacaoSerieResource',
            'route_name' => 'banco-preco.rest.especificacao-serie',
            'route_identifier_name' => 'especificacao_serie_id',
            'collection_name' => 'especificacao_serie',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Application\\Entity\\AesbeEspSerieEntity',
            'collection_class' => 'BancoPreco\\V1\\Rest\\EspecificacaoSerie\\EspecificacaoSerieCollection',
            'service_name' => 'EspecificacaoSerie',
        ),
        'BancoPreco\\V1\\Rest\\ArquivoProposta\\Controller' => array(
            'listener' => 'BancoPreco\\V1\\Rest\\ArquivoProposta\\ArquivoPropostaResource',
            'route_name' => 'banco-preco.rest.arquivo-proposta',
            'route_identifier_name' => 'arquivo_proposta_id',
            'collection_name' => 'arquivo_proposta',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'BancoPreco\\V1\\Rest\\ArquivoProposta\\ArquivoPropostaEntity',
            'collection_class' => 'BancoPreco\\V1\\Rest\\ArquivoProposta\\ArquivoPropostaCollection',
            'service_name' => 'ArquivoProposta',
        ),
        'BancoPreco\\V1\\Rest\\PropostaDados\\Controller' => array(
            'listener' => 'BancoPreco\\V1\\Rest\\PropostaDados\\PropostaDadosResource',
            'route_name' => 'banco-preco.rest.proposta-dados',
            'route_identifier_name' => 'proposta_dados_id',
            'collection_name' => 'proposta_dados',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(
                0 => 'search',
                1 => 'coMaterial',
                2 => 'nuSabesp',
                3 => 'coEmpresa',
                4 => 'stMeusPrecos',
                5 => 'periodo',
                6 => 'dtInicio',
                7 => 'dtFim',
            ),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Application\\Entity\\AesbePropostaDadosEntity',
            'collection_class' => 'BancoPreco\\V1\\Rest\\PropostaDados\\PropostaDadosCollection',
            'service_name' => 'PropostaDados',
        ),
        'BancoPreco\\V1\\Rest\\Proposta\\Controller' => array(
            'listener' => 'BancoPreco\\V1\\Rest\\Proposta\\PropostaResource',
            'route_name' => 'banco-preco.rest.proposta',
            'route_identifier_name' => 'proposta_id',
            'collection_name' => 'proposta',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'BancoPreco\\V1\\Rest\\Proposta\\PropostaEntity',
            'collection_class' => 'BancoPreco\\V1\\Rest\\Proposta\\PropostaCollection',
            'service_name' => 'Proposta',
        ),
        'BancoPreco\\V1\\Rest\\ModeloArquivoProposta\\Controller' => array(
            'listener' => 'BancoPreco\\V1\\Rest\\ModeloArquivoProposta\\ModeloArquivoPropostaResource',
            'route_name' => 'banco-preco.rest.modelo-arquivo-proposta',
            'route_identifier_name' => 'modelo_arquivo_proposta_id',
            'collection_name' => 'modelo_arquivo_proposta',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'BancoPreco\\V1\\Rest\\ModeloArquivoProposta\\ModeloArquivoPropostaEntity',
            'collection_class' => 'BancoPreco\\V1\\Rest\\ModeloArquivoProposta\\ModeloArquivoPropostaCollection',
            'service_name' => 'ModeloArquivoProposta',
        ),
        'BancoPreco\\V1\\Rest\\Fornecedor\\Controller' => array(
            'listener' => 'BancoPreco\\V1\\Rest\\Fornecedor\\FornecedorResource',
            'route_name' => 'banco-preco.rest.fornecedor',
            'route_identifier_name' => 'fornecedor_id',
            'collection_name' => 'fornecedor',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Application\\Entity\\AesbeFornecedor',
            'collection_class' => 'BancoPreco\\V1\\Rest\\Fornecedor\\FornecedorCollection',
            'service_name' => 'Fornecedor',
        ),
        'BancoPreco\\V1\\Rest\\Arquivo\\Controller' => array(
            'listener' => 'BancoPreco\\V1\\Rest\\Arquivo\\ArquivoResource',
            'route_name' => 'banco-preco.rest.arquivo',
            'route_identifier_name' => 'arquivo_id',
            'collection_name' => 'arquivo',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Application\\Entity\\AesbeArquivoEntity',
            'collection_class' => 'BancoPreco\\V1\\Rest\\Arquivo\\ArquivoCollection',
            'service_name' => 'Arquivo',
        ),
        'BancoPreco\\V1\\Rest\\ModalidadeLicitacao\\Controller' => array(
            'listener' => 'BancoPreco\\V1\\Rest\\ModalidadeLicitacao\\ModalidadeLicitacaoResource',
            'route_name' => 'banco-preco.rest.modalidade-licitacao',
            'route_identifier_name' => 'modalidade_licitacao_id',
            'collection_name' => 'modalidade_licitacao',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Application\\Entity\\AesbeModalidadeLicitacaoEntity',
            'collection_class' => 'BancoPreco\\V1\\Rest\\ModalidadeLicitacao\\ModalidadeLicitacaoCollection',
            'service_name' => 'ModalidadeLicitacao',
        ),
        'BancoPreco\\V1\\Rest\\Relatorio\\Controller' => array(
            'listener' => 'BancoPreco\\V1\\Rest\\Relatorio\\RelatorioResource',
            'route_name' => 'banco-preco.rest.relatorio',
            'route_identifier_name' => 'relatorio_id',
            'collection_name' => 'relatorio',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(
                0 => 'search',
                1 => 'coMaterial',
                2 => 'nuSabesp',
                3 => 'coEmpresa',
                4 => 'stMeusPrecos',
            ),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'BancoPreco\\V1\\Rest\\Relatorio\\RelatorioEntity',
            'collection_class' => 'BancoPreco\\V1\\Rest\\Relatorio\\RelatorioCollection',
            'service_name' => 'Relatorio',
        ),
    ),
    'zf-content-negotiation' => array(
        'controllers' => array(
            'BancoPreco\\V1\\Rest\\Material\\Controller' => 'HalJson',
            'BancoPreco\\V1\\Rest\\Descricao\\Controller' => 'HalJson',
            'BancoPreco\\V1\\Rest\\EspecificacaoClasse\\Controller' => 'HalJson',
            'BancoPreco\\V1\\Rest\\EspecificacaoSerie\\Controller' => 'HalJson',
            'BancoPreco\\V1\\Rest\\ArquivoProposta\\Controller' => 'HalJson',
            'BancoPreco\\V1\\Rest\\PropostaDados\\Controller' => 'Json',
            'BancoPreco\\V1\\Rest\\Proposta\\Controller' => 'HalJson',
            'BancoPreco\\V1\\Rest\\ModeloArquivoProposta\\Controller' => 'HalJson',
            'BancoPreco\\V1\\Rest\\Fornecedor\\Controller' => 'HalJson',
            'BancoPreco\\V1\\Rest\\Arquivo\\Controller' => 'HalJson',
            'BancoPreco\\V1\\Rest\\ModalidadeLicitacao\\Controller' => 'HalJson',
            'BancoPreco\\V1\\Rest\\Relatorio\\Controller' => 'HalJson',
        ),
        'accept_whitelist' => array(
            'BancoPreco\\V1\\Rest\\Material\\Controller' => array(
                0 => 'application/vnd.banco-preco.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'BancoPreco\\V1\\Rest\\Descricao\\Controller' => array(
                0 => 'application/vnd.banco-preco.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'BancoPreco\\V1\\Rest\\EspecificacaoClasse\\Controller' => array(
                0 => 'application/vnd.banco-preco.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'BancoPreco\\V1\\Rest\\EspecificacaoSerie\\Controller' => array(
                0 => 'application/vnd.banco-preco.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'BancoPreco\\V1\\Rest\\ArquivoProposta\\Controller' => array(
                0 => 'application/vnd.banco-preco.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'BancoPreco\\V1\\Rest\\PropostaDados\\Controller' => array(
                0 => 'application/vnd.banco-preco.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'BancoPreco\\V1\\Rest\\Proposta\\Controller' => array(
                0 => 'application/vnd.banco-preco.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'BancoPreco\\V1\\Rest\\ModeloArquivoProposta\\Controller' => array(
                0 => 'application/vnd.banco-preco.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'BancoPreco\\V1\\Rest\\Fornecedor\\Controller' => array(
                0 => 'application/vnd.banco-preco.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'BancoPreco\\V1\\Rest\\Arquivo\\Controller' => array(
                0 => 'application/vnd.banco-preco.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'BancoPreco\\V1\\Rest\\ModalidadeLicitacao\\Controller' => array(
                0 => 'application/vnd.banco-preco.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'BancoPreco\\V1\\Rest\\Relatorio\\Controller' => array(
                0 => 'application/vnd.banco-preco.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
        ),
        'content_type_whitelist' => array(
            'BancoPreco\\V1\\Rest\\Material\\Controller' => array(
                0 => 'application/vnd.banco-preco.v1+json',
                1 => 'application/json',
            ),
            'BancoPreco\\V1\\Rest\\Descricao\\Controller' => array(
                0 => 'application/vnd.banco-preco.v1+json',
                1 => 'application/json',
            ),
            'BancoPreco\\V1\\Rest\\EspecificacaoClasse\\Controller' => array(
                0 => 'application/vnd.banco-preco.v1+json',
                1 => 'application/json',
            ),
            'BancoPreco\\V1\\Rest\\EspecificacaoSerie\\Controller' => array(
                0 => 'application/vnd.banco-preco.v1+json',
                1 => 'application/json',
            ),
            'BancoPreco\\V1\\Rest\\ArquivoProposta\\Controller' => array(
                0 => 'application/vnd.banco-preco.v1+json',
                1 => 'application/json',
            ),
            'BancoPreco\\V1\\Rest\\PropostaDados\\Controller' => array(
                0 => 'application/vnd.banco-preco.v1+json',
                1 => 'application/json',
            ),
            'BancoPreco\\V1\\Rest\\Proposta\\Controller' => array(
                0 => 'application/vnd.banco-preco.v1+json',
                1 => 'application/json',
            ),
            'BancoPreco\\V1\\Rest\\ModeloArquivoProposta\\Controller' => array(
                0 => 'application/vnd.banco-preco.v1+json',
                1 => 'application/json',
            ),
            'BancoPreco\\V1\\Rest\\Fornecedor\\Controller' => array(
                0 => 'application/vnd.banco-preco.v1+json',
                1 => 'application/json',
            ),
            'BancoPreco\\V1\\Rest\\Arquivo\\Controller' => array(
                0 => 'application/vnd.banco-preco.v1+json',
                1 => 'application/json',
            ),
            'BancoPreco\\V1\\Rest\\ModalidadeLicitacao\\Controller' => array(
                0 => 'application/vnd.banco-preco.v1+json',
                1 => 'application/json',
            ),
            'BancoPreco\\V1\\Rest\\Relatorio\\Controller' => array(
                0 => 'application/vnd.banco-preco.v1+json',
                1 => 'application/json',
            ),
        ),
    ),
    'zf-hal' => array(
        'metadata_map' => array(
            'BancoPreco\\V1\\Rest\\Material\\MaterialEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'banco-preco.rest.material',
                'route_identifier_name' => 'material_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'BancoPreco\\V1\\Rest\\Material\\MaterialCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'banco-preco.rest.material',
                'route_identifier_name' => 'material_id',
                'is_collection' => true,
            ),
            'Application\\Entity\\AesbeMaterialEntity' => array(
                'entity_identifier_name' => 'coMaterial',
                'route_name' => 'banco-preco.rest.material',
                'route_identifier_name' => 'material_id',
                'hydrator' => 'DoctrineModule\\Stdlib\\Hydrator\\DoctrineObject',
            ),
            'BancoPreco\\V1\\Rest\\Descricao\\DescricaoEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'banco-preco.rest.descricao',
                'route_identifier_name' => 'descricao_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'BancoPreco\\V1\\Rest\\Descricao\\DescricaoCollection' => array(
                'entity_identifier_name' => 'coDescricao',
                'route_name' => 'banco-preco.rest.descricao',
                'route_identifier_name' => 'descricao_id',
                'is_collection' => true,
            ),
            'Application\\Entity\\AesbeDescricaoEntity' => array(
                'entity_identifier_name' => 'coDescricao',
                'route_name' => 'banco-preco.rest.descricao',
                'route_identifier_name' => 'descricao_id',
                'hydrator' => 'DoctrineModule\\Stdlib\\Hydrator\\DoctrineObject',
            ),
            'BancoPreco\\V1\\Rest\\EspecificacaoClasse\\EspecificacaoClasseEntity' => array(
                'entity_identifier_name' => 'coEspClasse',
                'route_name' => 'banco-preco.rest.especificacao-classe',
                'route_identifier_name' => 'especificacao_classe_id',
                'hydrator' => 'DoctrineModule\\Stdlib\\Hydrator\\DoctrineObject',
            ),
            'BancoPreco\\V1\\Rest\\EspecificacaoClasse\\EspecificacaoClasseCollection' => array(
                'entity_identifier_name' => 'coEspClasse',
                'route_name' => 'banco-preco.rest.especificacao-classe',
                'route_identifier_name' => 'especificacao_classe_id',
                'is_collection' => true,
            ),
            'Application\\Entity\\AesbeEspClasseEntity' => array(
                'entity_identifier_name' => 'coEspClasse',
                'route_name' => 'banco-preco.rest.especificacao-classe',
                'route_identifier_name' => 'especificacao_classe_id',
                'hydrator' => 'DoctrineModule\\Stdlib\\Hydrator\\DoctrineObject',
            ),
            'BancoPreco\\V1\\Rest\\EspecificacaoSerie\\EspecificacaoSerieEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'banco-preco.rest.especificacao-serie',
                'route_identifier_name' => 'especificacao_serie_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'BancoPreco\\V1\\Rest\\EspecificacaoSerie\\EspecificacaoSerieCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'banco-preco.rest.especificacao-serie',
                'route_identifier_name' => 'especificacao_serie_id',
                'is_collection' => true,
            ),
            'Application\\Entity\\AesbeEspSerieEntity' => array(
                'entity_identifier_name' => 'coEspSerie',
                'route_name' => 'banco-preco.rest.especificacao-serie',
                'route_identifier_name' => 'especificacao_serie_id',
                'hydrator' => 'DoctrineModule\\Stdlib\\Hydrator\\DoctrineObject',
            ),
            'BancoPreco\\V1\\Rest\\ArquivoProposta\\ArquivoPropostaEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'banco-preco.rest.arquivo-proposta',
                'route_identifier_name' => 'arquivo_proposta_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'BancoPreco\\V1\\Rest\\ArquivoProposta\\ArquivoPropostaCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'banco-preco.rest.arquivo-proposta',
                'route_identifier_name' => 'arquivo_proposta_id',
                'is_collection' => true,
            ),
            'BancoPreco\\V1\\Rest\\PropostaDados\\PropostaDadosEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'banco-preco.rest.proposta-dados',
                'route_identifier_name' => 'proposta_dados_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'BancoPreco\\V1\\Rest\\PropostaDados\\PropostaDadosCollection' => array(
                'entity_identifier_name' => 'coPropostaDados',
                'route_name' => 'banco-preco.rest.proposta-dados',
                'route_identifier_name' => 'proposta_dados_id',
                'is_collection' => true,
            ),
            'BancoPreco\\V1\\Rest\\Proposta\\PropostaEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'banco-preco.rest.proposta',
                'route_identifier_name' => 'proposta_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'BancoPreco\\V1\\Rest\\Proposta\\PropostaCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'banco-preco.rest.proposta',
                'route_identifier_name' => 'proposta_id',
                'is_collection' => true,
            ),
            'BancoPreco\\V1\\Rest\\Fornecedor\\FornecedorEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'banco-preco.rest.fornecedor',
                'route_identifier_name' => 'fornecedor_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'BancoPreco\\V1\\Rest\\ModeloArquivoProposta\\ModeloArquivoPropostaEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'banco-preco.rest.modelo-arquivo-proposta',
                'route_identifier_name' => 'modelo_arquivo_proposta_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'BancoPreco\\V1\\Rest\\ModeloArquivoProposta\\ModeloArquivoPropostaCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'banco-preco.rest.modelo-arquivo-proposta',
                'route_identifier_name' => 'modelo_arquivo_proposta_id',
                'is_collection' => true,
            ),
            'BancoPreco\\V1\\Rest\\Fornecedor\\FornecedorCollection' => array(
                'entity_identifier_name' => 'coFornecedor',
                'route_name' => 'banco-preco.rest.fornecedor',
                'route_identifier_name' => 'fornecedor_id',
                'is_collection' => true,
            ),
            'BancoPreco\\V1\\Rest\\Arquivo\\ArquivoEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'banco-preco.rest.arquivo',
                'route_identifier_name' => 'arquivo_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'BancoPreco\\V1\\Rest\\Arquivo\\ArquivoCollection' => array(
                'entity_identifier_name' => 'coArquivo',
                'route_name' => 'banco-preco.rest.arquivo',
                'route_identifier_name' => 'arquivo_id',
                'is_collection' => true,
            ),
            'Application\\Entity\\AesbeArquivoEntity' => array(
                'entity_identifier_name' => 'coArquivo',
                'route_name' => 'banco-preco.rest.arquivo',
                'route_identifier_name' => 'arquivo_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'Application\\Entity\\AesbeFornecedor' => array(
                'entity_identifier_name' => 'coFornecedor',
                'route_name' => 'banco-preco.rest.fornecedor',
                'route_identifier_name' => 'fornecedor_id',
                'hydrator' => 'DoctrineModule\\Stdlib\\Hydrator\\DoctrineObject',
            ),
            'BancoPreco\\V1\\Rest\\ModalidadeLicitacao\\ModalidadeLicitacaoEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'banco-preco.rest.modalidade-licitacao',
                'route_identifier_name' => 'modalidade_licitacao_id',
                'hydrator' => 'DoctrineModule\\Stdlib\\Hydrator\\DoctrineObject',
            ),
            'BancoPreco\\V1\\Rest\\ModalidadeLicitacao\\ModalidadeLicitacaoCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'banco-preco.rest.modalidade-licitacao',
                'route_identifier_name' => 'modalidade_licitacao_id',
                'is_collection' => true,
            ),
            'Application\\Entity\\AesbeModalidadeLicitacaoEntity' => array(
                'entity_identifier_name' => 'coModalidade',
                'route_name' => 'banco-preco.rest.modalidade-licitacao',
                'route_identifier_name' => 'modalidade_licitacao_id',
                'hydrator' => 'DoctrineModule\\Stdlib\\Hydrator\\DoctrineObject',
            ),
            'Application\\Entity\\AesbePropostaDadosEntity' => array(
                'entity_identifier_name' => 'coPropostaDados',
                'route_name' => 'banco-preco.rest.proposta-dados',
                'route_identifier_name' => 'proposta_dados_id',
                'hydrator' => 'DoctrineModule\\Stdlib\\Hydrator\\DoctrineObject',
            ),
            'BancoPreco\\V1\\Rest\\Relatorio\\RelatorioEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'banco-preco.rest.relatorio',
                'route_identifier_name' => 'relatorio_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'BancoPreco\\V1\\Rest\\Relatorio\\RelatorioCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'banco-preco.rest.relatorio',
                'route_identifier_name' => 'relatorio_id',
                'is_collection' => true,
            ),
        ),
    ),
    'zf-mvc-auth' => array(
        'authorization' => array(
            'BancoPreco\\V1\\Rest\\Material\\Controller' => array(
                'collection' => array(
                    'GET' => true,
                    'POST' => true,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
                'entity' => array(
                    'GET' => true,
                    'POST' => false,
                    'PUT' => true,
                    'PATCH' => true,
                    'DELETE' => true,
                ),
            ),
            'BancoPreco\\V1\\Rest\\ModeloArquivoProposta\\Controller' => array(
                'collection' => array(
                    'GET' => true,
                    'POST' => true,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
                'entity' => array(
                    'GET' => true,
                    'POST' => false,
                    'PUT' => true,
                    'PATCH' => true,
                    'DELETE' => true,
                ),
            ),
            'BancoPreco\\V1\\Rest\\ArquivoProposta\\Controller' => array(
                'collection' => array(
                    'GET' => true,
                    'POST' => true,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
                'entity' => array(
                    'GET' => true,
                    'POST' => false,
                    'PUT' => true,
                    'PATCH' => true,
                    'DELETE' => true,
                ),
            ),
            'BancoPreco\\V1\\Rest\\Arquivo\\Controller' => array(
                'collection' => array(
                    'GET' => true,
                    'POST' => true,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
                'entity' => array(
                    'GET' => true,
                    'POST' => false,
                    'PUT' => true,
                    'PATCH' => true,
                    'DELETE' => true,
                ),
            ),
            'BancoPreco\\V1\\Rest\\Descricao\\Controller' => array(
                'collection' => array(
                    'GET' => true,
                    'POST' => true,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
                'entity' => array(
                    'GET' => true,
                    'POST' => false,
                    'PUT' => true,
                    'PATCH' => true,
                    'DELETE' => true,
                ),
            ),
            'BancoPreco\\V1\\Rest\\EspecificacaoClasse\\Controller' => array(
                'collection' => array(
                    'GET' => true,
                    'POST' => true,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
                'entity' => array(
                    'GET' => true,
                    'POST' => false,
                    'PUT' => true,
                    'PATCH' => true,
                    'DELETE' => true,
                ),
            ),
            'BancoPreco\\V1\\Rest\\EspecificacaoSerie\\Controller' => array(
                'collection' => array(
                    'GET' => true,
                    'POST' => true,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
                'entity' => array(
                    'GET' => true,
                    'POST' => false,
                    'PUT' => true,
                    'PATCH' => true,
                    'DELETE' => true,
                ),
            ),
            'BancoPreco\\V1\\Rest\\Fornecedor\\Controller' => array(
                'collection' => array(
                    'GET' => true,
                    'POST' => true,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
                'entity' => array(
                    'GET' => true,
                    'POST' => false,
                    'PUT' => true,
                    'PATCH' => true,
                    'DELETE' => true,
                ),
            ),
            'BancoPreco\\V1\\Rest\\ModalidadeLicitacao\\Controller' => array(
                'collection' => array(
                    'GET' => true,
                    'POST' => true,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
                'entity' => array(
                    'GET' => true,
                    'POST' => false,
                    'PUT' => true,
                    'PATCH' => true,
                    'DELETE' => true,
                ),
            ),
            'BancoPreco\\V1\\Rest\\Proposta\\Controller' => array(
                'collection' => array(
                    'GET' => true,
                    'POST' => true,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
                'entity' => array(
                    'GET' => true,
                    'POST' => false,
                    'PUT' => true,
                    'PATCH' => true,
                    'DELETE' => true,
                ),
            ),
            'BancoPreco\\V1\\Rest\\PropostaDados\\Controller' => array(
                'collection' => array(
                    'GET' => true,
                    'POST' => true,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
                'entity' => array(
                    'GET' => true,
                    'POST' => false,
                    'PUT' => true,
                    'PATCH' => true,
                    'DELETE' => true,
                ),
            ),
            'BancoPreco\\V1\\Rest\\Relatorio\\Controller' => array(
                'collection' => array(
                    'GET' => true,
                    'POST' => true,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
                'entity' => array(
                    'GET' => true,
                    'POST' => false,
                    'PUT' => true,
                    'PATCH' => true,
                    'DELETE' => true,
                ),
            ),
        ),
    ),
    'zf-content-validation' => array(
        'BancoPreco\\V1\\Rest\\EspecificacaoSerie\\Controller' => array(
            'input_filter' => 'BancoPreco\\V1\\Rest\\EspecificacaoSerie\\Validator',
        ),
        'BancoPreco\\V1\\Rest\\ArquivoProposta\\Controller' => array(
            'input_filter' => 'BancoPreco\\V1\\Rest\\ArquivoProposta\\Validator',
        ),
        'BancoPreco\\V1\\Rest\\Descricao\\Controller' => array(
            'input_filter' => 'BancoPreco\\V1\\Rest\\Descricao\\Validator',
        ),
        'BancoPreco\\V1\\Rest\\PropostaDados\\Controller' => array(
            'input_filter' => 'BancoPreco\\V1\\Rest\\PropostaDados\\Validator',
        ),
    ),
    'input_filter_specs' => array(
        'BancoPreco\\V1\\Rest\\EspecificacaoSerie\\Validator' => array(),
        'BancoPreco\\V1\\Rest\\ArquivoProposta\\Validator' => array(
            0 => array(
                'required' => false,
                'validators' => array(),
                'filters' => array(),
                'name' => 'file',
                'type' => 'Zend\\InputFilter\\FileInput',
            ),
        ),
        'BancoPreco\\V1\\Rest\\Descricao\\Validator' => array(),
        'BancoPreco\\V1\\Rest\\PropostaDados\\Validator' => array(
            0 => array(
                'required' => true,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\NotEmpty',
                        'options' => array(
                            'message' => 'O campo Material é obrigatório',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\Digits',
                        'options' => array(),
                    ),
                ),
                'name' => 'coMaterial',
            ),
            1 => array(
                'required' => true,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\NotEmpty',
                        'options' => array(
                            'message' => 'O campo CNPJ é obrigatório.',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\Digits',
                        'options' => array(),
                    ),
                ),
                'name' => 'nuCnpj',
            ),
            2 => array(
                'required' => true,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\NotEmpty',
                        'options' => array(
                            'message' => 'O campo Nome Fornecedor é obrigatório.',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                        'options' => array(),
                    ),
                ),
                'name' => 'noFornecedor',
            ),
            3 => array(
                'required' => true,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\NotEmpty',
                        'options' => array(
                            'message' => 'O campo Preço é obrigatório.',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\Digits',
                        'options' => array(),
                    ),
                ),
                'name' => 'vlComImposto',
            ),
            4 => array(
                'required' => true,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\NotEmpty',
                        'options' => array(
                            'message' => 'O campo Quantidade é obrigatório',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\Digits',
                        'options' => array(),
                    ),
                ),
                'name' => 'qtProduto',
            ),
            5 => array(
                'required' => true,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\NotEmpty',
                        'options' => array(
                            'message' => 'O campo Data da Proposta é obrigatório.',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                        'options' => array(),
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'dtProposta',
            ),
            6 => array(
                'required' => true,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\NotEmpty',
                        'options' => array(
                            'message' => 'O campo Data da Contratação é obrigatório',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                        'options' => array(),
                    ),
                ),
                'name' => 'dtContratacao',
            ),
            7 => array(
                'required' => true,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\NotEmpty',
                        'options' => array(),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                        'options' => array(),
                    ),
                ),
                'name' => 'dtVigenciaContrato',
            ),
            8 => array(
                'required' => true,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\NotEmpty',
                        'options' => array(
                            'message' => 'O campo Data da Informação é obrigatório',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                        'options' => array(),
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'dtInformacao',
            ),
            9 => array(
                'required' => true,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\NotEmpty',
                        'options' => array(
                            'message' => 'O campo Número Contrato é obrigatório',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\Digits',
                        'options' => array(),
                    ),
                ),
                'name' => 'nuContrato',
            ),
            10 => array(
                'required' => true,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\NotEmpty',
                        'options' => array(
                            'message' => 'O campo Número Ata Registro de Preços é obrigatório.',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\Digits',
                        'options' => array(),
                    ),
                ),
                'name' => 'nuAta',
            ),
            11 => array(
                'required' => true,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\NotEmpty',
                        'options' => array(
                            'message' => 'O campo Modalidade Licitação é obrigatório',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\Digits',
                        'options' => array(),
                    ),
                ),
                'name' => 'coModalidade',
            ),
            12 => array(
                'required' => true,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\NotEmpty',
                        'options' => array(
                            'message' => 'O campo FOB/CIF é obrigatório',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                        'options' => array(),
                    ),
                ),
                'name' => 'stFobCif',
            ),
            13 => array(
                'required' => false,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\NotEmpty',
                        'options' => array(
                            'message' => 'O campo IPI é obrigatório',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\Digits',
                        'options' => array(),
                    ),
                ),
                'name' => 'vlIpi',
            ),
            14 => array(
                'required' => false,
                'validators' => array(),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\Digits',
                        'options' => array(),
                    ),
                ),
                'name' => 'vlIcms',
            ),
            15 => array(
                'required' => true,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\NotEmpty',
                        'options' => array(
                            'message' => 'O campo Valor ISS é obrigatório',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\Digits',
                        'options' => array(),
                    ),
                ),
                'name' => 'vlIss',
            ),
            16 => array(
                'required' => true,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\NotEmpty',
                        'options' => array(
                            'message' => 'O campo  Condições de Pagamento (dias) é obrigatório',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\Digits',
                        'options' => array(),
                    ),
                ),
                'name' => 'qtDiasPagamento',
            ),
            17 => array(
                'required' => true,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\NotEmpty',
                        'options' => array(
                            'message' => 'O campo Condições de Fornecimento é obrigatório',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                        'options' => array(),
                    ),
                ),
                'name' => 'stCondicaoFornecimento',
            ),
            18 => array(
                'required' => true,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\NotEmpty',
                        'options' => array(
                            'message' => 'O campo Tipo de Inspeção é obrigatório',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                        'options' => array(),
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'tpInspecao',
            ),
            19 => array(
                'required' => false,
                'validators' => array(),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                        'options' => array(),
                    ),
                ),
                'name' => 'dsObservacao',
            ),
            20 => array(
                'required' => false,
                'validators' => array(),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                        'options' => array(),
                    ),
                ),
                'name' => 'stProposta',
            ),
        ),
    ),
);

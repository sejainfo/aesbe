<?php
/**
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3-Clause
 * @copyright Copyright (c) 2014 Zend Technologies USA Inc. (http://www.zend.com)
 */

return array(
    'view_manager' => array(
        'display_exceptions' => true,
    ),
    'doctrine' => array(
        'connection' => array(
            'orm_default' => array(
                'driverClass' => 'Doctrine\DBAL\Driver\PDOMySql\Driver',
                'params' => array(
                    'host' => 'localhost', //local
                    'port' => '3306',
                    'user' => 'root',
                    'password' => '123456',
                    'dbname' => 'aesbe',
                    'charset' => 'utf8',
                    'driverOptions' => array(
                        1002=>'SET NAMES utf8'
                    )
                )
            )
        )
    )
);

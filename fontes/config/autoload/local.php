<?php
return array(
    'zf-mvc-auth' => array(
        'authentication' => array(
            'adapters' => array(
                'oauthadapter' => array(
                    'adapter' => 'ZF\\MvcAuth\\Authentication\\OAuth2Adapter',
                    'storage' => array(
                        'storage' => 'Application\\Acl\\OAuth2Adapter',
                        'dsn' => 'mysql:host=localhost;dbname=aesbe',
                        'route' => '/oauth',
                        'username' => 'root',
                        'password' => '123456',
                    ),
                ),
            ),
        ),
    ),
    'doctrine' => array(
        'connection' => array(
            'orm_default' => array(
                'driverClass' => 'Doctrine\\DBAL\\Driver\\PDOMySql\\Driver',
                'params' => array(
                    'host' => 'localhost',
                    'port' => '3306',
                    'user' => 'root',
                    'password' => '123456',
                    'dbname' => 'aesbe',
                    'charset' => 'utf8',
                    'driverOptions' => array(
                        1002 => 'SET NAMES utf8',
                    ),
                ),
            ),
        ),
    ),
);
